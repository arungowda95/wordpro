//
//  UnlockPowerPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 05/05/20.
//

#include "UnlockPowerPopUp.h"
UnlockPowerPopUp::UnlockPowerPopUp(std::function<void()> func)
{
    m_fnSelector = func;
        
    ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.5f);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.2);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
//    this->addChild(m_SprPopUpTop,2);
    
    std::string t_subtext = "LetterInfo";
    Label *t_tSubLabel = createLabelBasedOnLang(t_subtext,45);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,244,55));
    t_tSubLabel->enableOutline(Color4B(76,33,14,255),3);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-t_tSubLabel->getBoundingBox().size.height/2));
    this->addChild(t_tSubLabel, 2);

t_OkayButton = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_1(UnlockPowerPopUp::OnButtonPressed,this));
    t_OkayButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),10+m_SprPopUpBg->getBoundingBox().getMinY()+t_OkayButton->getBoundingBox().size.height*0.8));
    t_OkayButton->setTag(0);
    
    Label *OkLabel = createLabelBasedOnLang("Ok", 50);
    OkLabel->setColor(Color3B::WHITE);
    OkLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    OkLabel->enableOutline(Color4B(53,126,32,255),2);
    OkLabel->setWidth(t_OkayButton->getContentSize().width);
    OkLabel->setPosition(Vec2(t_OkayButton->getBoundingBox().size.width/2,t_OkayButton->getBoundingBox().size.height/2+15));
    t_OkayButton->addChild(OkLabel,1);

    Menu *m_Menu = Menu::create(t_OkayButton,NULL);
    m_Menu->setPosition(Vec2::ZERO);
    this->addChild(m_Menu,2);

}
MenuItemSprite *UnlockPowerPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
      
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;

}
void UnlockPowerPopUp::OnButtonPressed(Ref *p_Sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    int pButtonTag =  Button->getTag();
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"close";
          }break;
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}

Label *UnlockPowerPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize, Size(0, 150));
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize, Size(0, 150));
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

UnlockPowerPopUp::~UnlockPowerPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}

