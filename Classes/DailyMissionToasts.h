//
//  DailyMissionToasts.hpp
//  wordPro-mobile
//
//  Created by id on 05/05/20.
//

#ifndef DailyMissionToasts_h
#define DailyMissionToasts_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;
class DailyMissionToasts : public Node
{
        Size visibleSize;
        Vec2 Origin;

        Sprite                   *m_SprToast;
        std::string               m_MissionName;
        bool                      m_bReward;
        int                       m_iCointCount;
        std::string               m_SprIconName;
        int m_iMissionNum;
    public:
        // CONSTRUCTOR AND DESTRUCTOR
        DailyMissionToasts(const std::string &p_MissionName);
        ~DailyMissionToasts();
    
        // THIS WILL ANIMTE TOASTS
        void AnimateToasts();
    
        // THIS RETURN DESCRIPTION ACCORDING TO NAME
        std::string getDescription();
    
        std::string getIconsForMissions(std::string AchieveTag);
    
        Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};

#endif /* DailyMissionToasts_hpp */
