//
//  AchievementObserver.hpp
//  wordPro-mobile
//
//  Created by id on 20/04/20.
//

#ifndef AchievementObserver_hpp
#define AchievementObserver_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "GameStateManager.h"
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
//#include <memory>

using namespace rapidjson;
using namespace cocos2d;
using namespace std;

enum eAchObserverType
{
    Type_Int,
    Type_Float,
    Type_Bool,
};

class AchievementObserverCondition
{
        std::string m_keyTag;
        eAchObserverType m_eType;
        int m_iRequiredCount;
    public:
        int m_iCount;

        // CONSTRUCTOR AND DESTRUCTOR
        AchievementObserverCondition(const std::string &p_Tag, int p_fCount, int p_fRequiredCount);
        ~AchievementObserverCondition();
    
    
        // THIS UPDATE CONDITION
        void UpdateCondition(const std::string &p_Tag, int p_iCount);
    
        // THIS RESET
        void Reset();
    
        // CHECK CONDITION SATISFIED OR NOT
        bool ConditionSatisfied();
};

class AchievementObserver
{
        std::vector<AchievementObserverCondition*> m_ObserverConditionArray;
        rapidjson::Document m_JsonDocument;
    
    public:
    
        std::string m_KeyId;
        std::string m_AchievementName;
    
        bool m_bCompleted;
        bool m_bRewardClaimed;
    
        int m_iTargetCount;
        int m_iCurCount;
        int m_iReward;

        // CONSTRUCTOR AND DESTRUCTOR
        AchievementObserver(const std::string &p_JsonBuffer);
        ~AchievementObserver();
    
        // ON GAME OVER
        void OnGameOver();
            
        // THIS UPDATE ACHIEVEMENT
        void UpdateAchievement(const std::string &p_Type, int p_iCount);
    
        // CHECK ACHIEVEMENT COMPLETED OR NOT
        bool IsCompleted();
    
        // RETURN ACHIEVEMENT JSON DATA
        rapidjson::Value GetJsonData(rapidjson::Document::AllocatorType& allocator);
    
        // RETURN ACHIEVEMENT NAME CONDITION
        std::string getAchievementName();
    
        bool ConditionSatisfied();
    
        void updateRewardClaimed();
    
        bool getIsRewardClaimed();
};

#endif /* AchievementObserver_hpp */
