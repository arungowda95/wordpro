//
//  InterfaceManagerInstance.hpp
//  wordPro-mobile
//
//  Created by id on 31/03/20.
//

#ifndef InterfaceManagerInstance_h
#define InterfaceManagerInstance_h

#include <iostream>
#include "cocos2d.h"

#include "InterfaceManager.h"

using namespace cocos2d;

class InterfaceManagerInstance
{
    
    InterfaceManagerInstance();
    InterfaceManager *m_InterfaceManager;
public:
    static InterfaceManagerInstance *getInstance();
    InterfaceManager * getInterfaceManager();
};


#endif /* InterfaceManagerInstance_hpp */
