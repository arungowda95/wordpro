//
//  FacebookInfoPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 15/07/20.
//

#ifndef FacebookInfoPopUp_hpp
#define FacebookInfoPopUp_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "PopUp.h"

using namespace cocos2d;

class FacebookInfoPopUp:public PopUp
{
  
    Sprite* m_SprPopUpBg,*m_SprPopUpTop;

    public:

    FacebookInfoPopUp(std::function<void()> func);
    ~FacebookInfoPopUp();
    
    void OnButtonPressed(Ref *p_Sender);
 
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
};

#endif /* FacebookInfoPopUp_hpp */
