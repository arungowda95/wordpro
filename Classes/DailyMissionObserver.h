//
//  DailyMissionObserver.hpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#ifndef DailyMissionObserver_h
#define DailyMissionObserver_h

#include <stdio.h>
#include "cocos2d.h"
#include "GameStateManager.h"
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
//#include <memory>

using namespace rapidjson;
using namespace cocos2d;
using namespace std;



class DailyMissionObserver
{
    
        rapidjson::Document m_JsonDocument;
    public:
        std::string m_KeyId;
        std::string m_MissionName;
        bool m_bCompleted;
        int m_iTargetCount;
        int m_iCurCount;
        int m_iReward;
        bool m_bRewardClaimed;

        // CONSTRUCTOR AND DESTRUCTOR
        DailyMissionObserver(const std::string &p_JsonBuffer);
        ~DailyMissionObserver();
    
        // ON GAME OVER
        void OnGameOver();
            
        // THIS UPDATE MISSION
        void UpdateMission(const std::string &p_Type, int p_iCount);
    
        // CHECK MISSION COMPLETED OR NOT
        bool IsCompleted();
    
        // RETURN MISSON JSON DATA
        rapidjson::Value GetJsonData(rapidjson::Document::AllocatorType& allocator);
    
        // RETURN MISSION NAME CONDITION
        std::string getDailyMissionName();
    
        bool ConditionSatisfied();
    
        void updateRewardClaimed();
    
        bool getIsRewardClaimed();
};
#endif /* DailyEventObserver_hpp */
