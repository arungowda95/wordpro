//
//  FreeCoinsPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 23/03/20.
//

#ifndef FreeCoinsPopUp_hpp
#define FreeCoinsPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class FreeCoinsPopUp :public PopUp
{
    
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    MenuItemSprite *t_miWatchBtn,*t_miCloseButton;
    
    Size visibleSize;
    Vec2 origin;
    
    float Posy;
    bool m_bvideoAdSuccess;
public:
    FreeCoinsPopUp(std::function<void()> func);
    ~FreeCoinsPopUp();
    
    void OnButtonPressed(Ref *p_Sender) override;
    
    void watchVideoButtonPressed(Ref *p_Sender);
    
    void onVideoAdSuccess();
    
    void onVideoAdFailure();
    
    void videoAdsAvailble();
    
    void registerVideoEvent();

    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void coinAddAnimation();
    
    void removeSprite(Ref *_sender);
        
    void coinAnimationDoneClosePopUp();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};
#endif /* FreeCoinsPopUp_hpp */
