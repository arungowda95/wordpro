//
//  PopUpManager.cpp
//  wordPro-mobile
//
//  Created by id on 09/03/20.
//

#include "PopUpManager.h"
#include "GameConstants.h"
#include "MainMenuScene.h"
#include "HudLayer.h"
#include "LanguagePopUp.h"
#include "SettingPopUp.h"
#include "LevelCompletePopUp.h"
#include "BuyCoinsPopUp.h"
#include "RemoveAdsPopUp.h"
#include "FreeCoinsPopUp.h"
#include "LimitedSalePopUp.h"
#include "GameStateManager.h"
#include "DailyMissionPopUp.h"
#include "DailyRewardPopUp.h"
#include "LevelScene.h"
#include "ExitPopUp.h"
#include "AchievementPopUp.h"
#include "BonusWordPopUp.h"
#include "InterfaceManagerInstance.h"
#include "AchievementManager.h"
#include "WorldUnlockPopUp.h"
#include "GiftClaimPopUp.h"
#include "ReportWordPopUp.h"
#include "DailyMissionManager.h"
#include "UnlockPowerPopUp.h"
#include "RatePopUp.h"
#include "GameCompletePopUp.h"
#include "GamePromotionPopUp.h"
#include "LeaderBoardPopUp.h"
#include "FacebookInfoPopUp.h"
#include "CommonPopUp.h"

PopUpManager::PopUpManager(Layer *p_Layer, eScreenType p_eScreenType)
{

    m_SprBgAlpha = NULL;
    m_PtrPopUp   = NULL;
    m_eScreenType = p_eScreenType;
    m_fOpacity = 200;
    m_ePopUpType = POPUP_NONE;
    m_bPopUpExist = false;
    
    Rect ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    switch (m_eScreenType)
    {
        case SCREEN_MENU:
        {
            m_ptrMenuLayer  = (MenuLayer*)p_Layer;
            m_ptrHudLayer   = NULL;
            m_ptrLevelLayer = NULL;
        }
        break;
        case SCREEN_LEVEL:
        {
            m_ptrLevelLayer = (LevelLayer*)p_Layer;
            m_ptrMenuLayer  = NULL;
            m_ptrHudLayer   = NULL;
        }break;
        case SCREEN_GAME:
        {
            m_ptrHudLayer   = (HudLayer*)p_Layer;
            m_ptrMenuLayer  = NULL;
            m_ptrLevelLayer = NULL;
        }
        break;
        default:
            break;
    }
    
}
/// THIS ON KEY BACK BUTTON PRESSED
void PopUpManager::OnKeyButtonPressed()
{
        switch (m_eScreenType)
        {
             case SCREEN_MENU:
            {
               if(m_ePopUpType == POPUP_NONE)
                {
                    if(m_ptrMenuLayer)
                    {
                        m_ptrMenuLayer->setEnableButtons(false);
                        LoadPopUp(POPUP_GAME_EXIT);
                    }
                }
            }break;
            case SCREEN_LEVEL:
            {
               if(m_ePopUpType == POPUP_NONE)
                {
                    if(m_ptrLevelLayer!=NULL){
                        m_ptrLevelLayer->backBtnCallback(NULL);
                    }
                }
            }break;
            case SCREEN_GAME:
            {
                switch(GameStateManager::getInstance()->getGameState())
                {
                    case GAME_STATE_PLAY:
                    {
                        if(m_ptrHudLayer!=NULL){
                            m_ptrHudLayer->backBtnCallback(NULL);
                        }
                    }break;
                    case GAME_STATE_PAUSE:
                    {
                        
                    }break;
                    case GAME_STATE_OVER:
                    {
                        
                    }break;
                    default:
                        break;
                }
            }
            break;
            default:
            break;
        }

}
// THIS LOAD POPUP WITH TYPE
void PopUpManager::LoadPopUp(PopUpType p_ePopUpType)
{
    if(m_bPopUpExist) {
        return;
    }else{
        m_bPopUpExist = true;
    }
    CheckWithUpdateCoinEvent();
    m_ePopUpType = p_ePopUpType;
    switch (m_ePopUpType)
    {
        case POPUP_LANGUAGE:
        {
            AddAlphaBg();
            m_PtrPopUp = new LanguagePopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_SETTINGS:
        {
            AddAlphaBg();
            m_PtrPopUp = new SettingPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_LEVEL_COMPLETE:
        {
            AddAlphaBg();
            m_PtrPopUp = new LevelCompletePopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_BUY_COINS:
        {
            AddAlphaBg();
            m_PtrPopUp = new BuyCoinsPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_REMOVE_ADS:
        {
              AddAlphaBg();
              m_PtrPopUp = new RemoveAdsPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_FREE_COINS:
        {
            AddAlphaBg();
            m_PtrPopUp = new FreeCoinsPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_LIMITED_SALE:
        {
            AddAlphaBg();
            m_PtrPopUp = new LimitedSalePopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_DAILY_MISSION:
        {
            AddAlphaBg();
            m_PtrPopUp = new DailyMissionPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_DAILY_REWARD:
        {
            AddAlphaBg();
            m_PtrPopUp = new DailyRewardPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_GAME_EXIT:
        {
            AddAlphaBg();
            m_PtrPopUp = new ExitPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_ACHIEVEMENT:
        {
            AddAlphaBg();
            m_PtrPopUp = new AchievementPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_BONUS_WORD:
        {
            AddAlphaBg();
            m_PtrPopUp = new BonusWordPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_WORLD_UNLOCK:
        {
            AddAlphaBg();
            m_PtrPopUp = new WorldUnlockPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_GIFT_CLAIM:
        {
            AddAlphaBg();
            m_PtrPopUp = new GiftClaimPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_REPORT_WORD:
        {
            AddAlphaBg();
            m_PtrPopUp = new ReportWordPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_UNLOCK_POWER:
        {
            AddAlphaBg();
            m_PtrPopUp = new UnlockPowerPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_RATE_GAME:
        {
            AddAlphaBg();
            m_PtrPopUp = new RatePopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_GAME_COMPLETE:
        {
            AddAlphaBg();
            m_PtrPopUp = new GameCompletePopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_GAME_PROMOTION:
        {
            AddAlphaBg();
            m_PtrPopUp = new GamePromotionPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_GAME_LEADERBOARD:
        {
            AddAlphaBg();
            m_PtrPopUp = new LeaderBoardPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_FB_INFO:
        {
            AddAlphaBg();
            m_PtrPopUp = new FacebookInfoPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        case POPUP_COMMON_TUT:
        {
            AddAlphaBg();
            m_PtrPopUp = new CommonPopUp(std::bind(&PopUpManager::popUpCallback,this));
        }break;
        default:
            break;
    }
    m_PtrPopUp->setScale(m_fScale);
    m_PtrPopUp->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2));

    
    if(m_PtrPopUp != NULL)
    {
        m_PtrPopUp->autorelease();
        this->addChild(m_PtrPopUp, 3);
        if(m_SprBgAlpha != NULL)
        {
            FadeTo *t_FadeTo = FadeTo::create(0.2, m_fOpacity);
            m_SprBgAlpha->runAction(t_FadeTo);
            PopUpTransitionAnimation(POPUP_ENTER);
        }
    }
    
}
// POPUP TRANSITION
void PopUpManager::PopUpTransitionAnimation(PopUpTransitionType p_ePopUpTransitionType)
{
    if(m_PtrPopUp != NULL)
    {
        switch (p_ePopUpTransitionType) {
            case POPUP_ENTER:
            {
                GameController::getInstance()->playSFX(SFX_SCREEN_MOVE);

                float scale = m_PtrPopUp->getScale();
                m_PtrPopUp->runAction(Sequence::create(ScaleTo::create(0.0f, 0.1f),ScaleTo::create(0.2f, scale+0.1f),ScaleTo::create(0.1f, scale),NULL));

            }break;
            case POPUP_EXIT:
            {
                if(m_SprBgAlpha != NULL)
                {
                    DelayTime *t_DelayTime = DelayTime::create(0.3);
                    FadeOut *t_FadeOut = FadeOut::create(0.2f);
                    CallFunc *t_CallFunc = CallFunc::create(std::bind(&PopUpManager::RemoveAlphaBg,this));
                    Sequence *t_sequence = Sequence::create(t_DelayTime, t_FadeOut,t_CallFunc,NULL);
                    m_SprBgAlpha->runAction(t_sequence);
                }
                float scale = m_PtrPopUp->getScale();
                m_PtrPopUp->runAction(Sequence::create(ScaleTo::create(0.1f, scale+0.1f),ScaleTo::create(0.2f, 0.00f),CallFunc::create(CC_CALLBACK_0(PopUpManager::UnLoadPopUp,this)),NULL));

            }break;
            default:
                break;
        }
    }
}
/// THIS CHECK FOR UPDATE COIN EVENT CREATION
void PopUpManager::CheckWithUpdateCoinEvent()
{
    switch (m_eScreenType)
    {
        case SCREEN_MENU:
        {
            if(m_ptrMenuLayer != NULL)
                m_ptrMenuLayer->CreateUpdateCoinEvent();
        }break;
        case SCREEN_LEVEL:
        {
            if(m_ptrLevelLayer != NULL)
                m_ptrLevelLayer->CreateUpdateCoinEvent();
        }break;
        case SCREEN_GAME:
        {
            if(m_ptrHudLayer != NULL)
                m_ptrHudLayer->CreateUpdateCoinEvent();
        }break;
        default:
            break;
    }
}
void PopUpManager::UnLoadPopUp()
{
    
      if(m_PtrPopUp != NULL)
       {
           m_bPopUpExist = false;
           m_PtrPopUp->stopAllActions();
           this->removeChild(m_PtrPopUp,true);
           m_PtrPopUp = NULL;
           
           m_ePopUpType = POPUP_NONE;
       }
}
void PopUpManager::AddAlphaBg()
{
     float xscale = visibleSize.width/64.0f;
     float yscale = visibleSize.height/64.0f;
     m_SprBgAlpha = Sprite::create("pixel.png");
     m_SprBgAlpha->setScale(xscale,yscale);
     m_SprBgAlpha->setColor(Color3B(47, 52, 56)); //setColor(Color3B::BLACK);
     m_SprBgAlpha->setOpacity(0);
     m_SprBgAlpha->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2));
     this->addChild(m_SprBgAlpha, 2);
}
void PopUpManager::RemoveAlphaBg()
{
    if(m_SprBgAlpha != NULL)
      {
          this->removeChild(m_SprBgAlpha,true);
          m_SprBgAlpha = NULL;
      }
}
void PopUpManager::popUpCallback()
{
    if(m_PtrPopUp != NULL)
    {
            if(strcmp(m_PtrPopUp->status, "close")==0)
            {
                switch (m_eScreenType) {
                    case SCREEN_MENU:
                    {
                        if (m_ptrMenuLayer!=NULL)
                        {
                            m_ptrMenuLayer->UpdateMenuData();
                            m_ptrMenuLayer->setEnableButtons(true);
                        }
                        PopUpTransitionAnimation(POPUP_EXIT);
                    }break;
                    case SCREEN_LEVEL:
                    {
                        if (m_ptrLevelLayer!=NULL) {
                            m_ptrLevelLayer->UpdateMenuData();
                            m_ptrLevelLayer->setEnableButtons(true);
                        }
                        PopUpTransitionAnimation(POPUP_EXIT);
                    }break;
                    case SCREEN_GAME:
                    {
                        if (m_ptrHudLayer!=NULL)
                        {
                            m_ptrHudLayer->UpdateMenuData();
                            m_ptrHudLayer->setEnableButtons(true);
                        }
                        PopUpTransitionAnimation(POPUP_EXIT);
                        GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
                    }break;
                    default:
                        break;
                }
               }
               else if(strcmp(m_PtrPopUp->status, "shop_close")==0)
               {
                   PopUpTransitionAnimation(POPUP_EXIT);
                   
                   DelayTime *t_DelayeTime = DelayTime::create(0.2f);
                   CallFunc *t_CallFunc = CallFunc::create(std::bind(&PopUpManager::LoadOtherPopUp, this,"FreeCoins"));
                   this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
               }
               else if(strcmp(m_PtrPopUp->status, "Lang_confirm")==0)
               {
                   switch (m_eScreenType) {
                       case SCREEN_MENU:
                       {
                           PopUpTransitionAnimation(POPUP_EXIT);
                             if (m_ptrMenuLayer!=NULL) {
                                 m_ptrMenuLayer->UpdateMenuData();
                                 m_ptrMenuLayer->setEnableButtons(true);
                             }
                       }break;
                       case SCREEN_LEVEL:
                       {
                           PopUpTransitionAnimation(POPUP_EXIT);
                             if (m_ptrLevelLayer!=NULL) {
                                 m_ptrLevelLayer->setEnableButtons(true);
                             }
                       }break;
                       case SCREEN_GAME:
                       {
                           PopUpTransitionAnimation(POPUP_EXIT);
                             if (m_ptrHudLayer!=NULL) {
                                 GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
                             }
                       }break;
                       default:
                           break;
                   }

               }
               else if(strcmp(m_PtrPopUp->status,"Language")==0)
               {
                   printf("\n Language\n");
                   PopUpTransitionAnimation(POPUP_EXIT);
                   DelayTime *t_DelayeTime = DelayTime::create(0.2f);
                   CallFunc *t_CallFunc = CallFunc::create(std::bind(&PopUpManager::LoadOtherPopUp, this,"Language"));
                   this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
               }
               else if(strcmp(m_PtrPopUp->status,"HowtoPlay")==0)
               {
                   printf("\n HowtoPlay \n");
               }
               else if(strcmp(m_PtrPopUp->status,"Feedback")==0)
               {
                   printf("\n Feedback \n");
                   InterfaceManagerInstance::getInstance()->getInterfaceManager()->gameFeedBack();
               }
               else if(strcmp(m_PtrPopUp->status,"Rate")==0)
               {
                   printf("\n Rate \n");
                     printf("\n Yes \n");
                     switch (m_eScreenType)
                     {
                       case SCREEN_MENU:
                       {
                           PopUpTransitionAnimation(POPUP_EXIT);
                           DelayTime *t_DelayeTime = DelayTime::create(0.2f);
                           CallFunc *t_CallFunc = CallFunc::create(std::bind(&PopUpManager::LoadOtherPopUp, this,"Rate"));
                           this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
                       }break;
                       case SCREEN_LEVEL:
                       {
                           InterfaceManagerInstance::getInstance()->getInterfaceManager()->OpenInStore();
                           GameController::getInstance()->setBoolForKey("rate", true);
                             if (m_ptrLevelLayer!=NULL) {
                                 m_ptrLevelLayer->setEnableButtons(true);
                             }
                           PopUpTransitionAnimation(POPUP_EXIT);
                       }break;
                       default:
                           break;
                   }
               }
               else if(strcmp(m_PtrPopUp->status,"RemoveAds")==0)
               {
                             printf("\n RemoveAds \n");
                   PopUpTransitionAnimation(POPUP_EXIT);
                   DelayTime *t_DelayeTime = DelayTime::create(0.2f);
                   CallFunc *t_CallFunc = CallFunc::create(std::bind(&PopUpManager::LoadOtherPopUp, this,"RemoveAds"));
                   this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
               }
               else if(strcmp(m_PtrPopUp->status,"Share")==0)
               {
                   printf("\n Share \n");
                   InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShareGame();
               }
               else if(strcmp(m_PtrPopUp->status,"Yes")==0)
               {
                   printf("\n Yes \n");
                   switch (m_eScreenType) {
                     case SCREEN_MENU:
                     {
                         AchievementManager::getInstance()->OnApplicationQuit();
                         DailyMissionManager::getInstance()->OnApplicationQuit();
                         Director::getInstance()->end();
                     }break;
                     case SCREEN_GAME:
                     {
                         AchievementManager::getInstance()->OnApplicationQuit();
                         DailyMissionManager::getInstance()->OnApplicationQuit();
                         m_ptrHudLayer->UnloadPopUpAndExitGame(.5f, true);
                     }break;
                     default:
                         break;
                 }
               }
               else if(strcmp(m_PtrPopUp->status,"No")==0)
               {
                     switch (m_eScreenType) {
                       case SCREEN_MENU:
                       {
                           PopUpTransitionAnimation(POPUP_EXIT);
                           if (m_ptrMenuLayer!=NULL) {
                               m_ptrMenuLayer->setEnableButtons(true);
                           }
                       }break;
                         case SCREEN_LEVEL:
                         {
                             if (m_ptrLevelLayer!=NULL) {
                                 m_ptrLevelLayer->setEnableButtons(true);
                             }
                             PopUpTransitionAnimation(POPUP_EXIT);
                         }break;
                       case SCREEN_GAME:
                       {
                           PopUpTransitionAnimation(POPUP_EXIT);
                           DelayTime *t_DelayeTime = DelayTime::create(0.5f);
                           CallFunc *t_CallFunc = CallFunc::create(std::bind(&HudLayer::onNoFromExitPopUp, m_ptrHudLayer));
                           this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
                       }break;
                       default:
                           break;
                   }
               }
                else if(strcmp(m_PtrPopUp->status,"Next")==0)
                {
                   printf("\n Next \n");
                    PopUpTransitionAnimation(POPUP_EXIT);
                    if (m_ptrHudLayer!=NULL)
                    {
                        m_ptrHudLayer->loadNextLevel();
                    }
                }
               else if(strcmp(m_PtrPopUp->status,"Check_Out")==0)
               {
                  printf("\n Check_Out \n");
                   PopUpTransitionAnimation(POPUP_EXIT);
                   if (m_ptrLevelLayer!=NULL)
                   {
                       m_ptrLevelLayer->backBtnCallback(NULL);
                   }
               }
                else if(strcmp(m_PtrPopUp->status,"levelcomplete_close")==0)
                {
                   printf("\n levelcomplete_close \n");
                    PopUpTransitionAnimation(POPUP_EXIT);
                    if (m_ptrHudLayer!=NULL)
                    {
                        m_ptrHudLayer->onLevelComplete();
                    }
                }
                else if(strcmp(m_PtrPopUp->status,"GameComplete")==0)
                {
                   printf("\n GameComplete \n");
                    PopUpTransitionAnimation(POPUP_EXIT);
                    if (m_ptrLevelLayer!=NULL)
                    {
                        MainMenuScene *scene = new MainMenuScene();
                        scene->autorelease();
                        Director::getInstance()->replaceScene(TransitionSlideInL::create(0.4f,scene));
                        scene = NULL;
                    }
                }
                else if (strcmp(m_PtrPopUp->status,"dailyTask")==0)
                {
                    PopUpTransitionAnimation(POPUP_EXIT);
                    if (m_ptrMenuLayer!=NULL) {
                        DelayTime *t_DelayeTime = DelayTime::create(0.2f);
                        CallFunc *t_CallFunc = CallFunc::create(std::bind(&PopUpManager::LoadOtherPopUp, this,"dailyTask"));
                        this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
                    }
                }
                else if (strcmp(m_PtrPopUp->status,"openLeadearboard")==0)
                {
                    PopUpTransitionAnimation(POPUP_EXIT);
                    if (m_ptrMenuLayer!=NULL) {
                        DelayTime *t_DelayeTime = DelayTime::create(0.2f);
                        CallFunc *t_CallFunc = CallFunc::create(std::bind(&PopUpManager::LoadOtherPopUp, this,"Leaderboard"));
                        this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
                    }
                }
      }

}
/// THIS LOAD OTHER POP UP
void PopUpManager::LoadOtherPopUp(const std::string &p_String)
{
    switch (m_eScreenType)
    {
        case SCREEN_MENU:
        {
            if(m_ptrMenuLayer != NULL){
                m_ptrMenuLayer->LoadOtherPopUpWithDelay(0.4f, p_String);
            }
        }break;
        case SCREEN_LEVEL:
        {
            if(m_ptrLevelLayer != NULL)
            {
                m_ptrLevelLayer->LoadOtherPopUpWithDelay(0.4f, p_String);
            }
        }break;
        case SCREEN_GAME:
        {
            if(m_ptrHudLayer != NULL)
            {
                m_ptrHudLayer->setEnableButtons(false);
                m_ptrHudLayer->LoadOtherPopUpWithDelay(0.4f, p_String);
            }
        }break;
        default:
            break;
    }
}
// RETURN POPUP TYPE
PopUpType PopUpManager::getPopUpType()
{
    return m_ePopUpType;
}
// RETURN POP UP
PopUp * PopUpManager::getPopUp()
{
    return m_PtrPopUp;
}
PopUpManager::~PopUpManager()
{
    
    if(m_PtrPopUp!=NULL)
    {
        this->removeChild(m_PtrPopUp, true);
        m_PtrPopUp = NULL;
    }
    
}
