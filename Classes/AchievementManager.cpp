
//
//  AchievementManager.cpp
//  wordPro-mobile
//
//  Created by id on 20/04/20.
//

#include "AchievementManager.h"
#include "InterfaceManagerInstance.h"
#include "InterfaceManager.h"
#include "GameConstants.h"
#include "GameStateManager.h"
#include "GameController.h"
#include "AchievementToasts.h"
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
#include <memory>

using namespace rapidjson;

// Implement Achievement Manager
AchievementManager* AchievementManager::m_PtrAchievementManager = NULL;
// CONSTRUCTOR
AchievementManager::AchievementManager()
{
    m_iAchievementTag = 0;
    m_AchievementName = "";
    rapidjson::Document t_Document;
    std::string fullPath = FileUtils::getInstance()->getWritablePath().append("AchievementsData.json");
//    ssize_t bufferSize = 0;
//    char *p_Buffer = new char[sizeof(FileUtils::getInstance()->getFileData(fullPath.c_str(), "r", &bufferSize))];
//    p_Buffer = (char*)FileUtils::getInstance()->getFileData(fullPath.c_str(), "r", &bufferSize);
    if(FileUtils::getInstance()->getStringFromFile(fullPath).empty())
    {
        //CCLOG("INTERNAL FILE NOT FOUND");
        fullPath = FileUtils::getInstance()->fullPathForFilename("AchievementsData.json");
    }
    
    char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(fullPath).c_str()) + sizeof(int) + 1];
    strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(fullPath).c_str());
    
    t_Document.Parse<kParseStopWhenDoneFlag>(p_Buffer);
    
    if (t_Document.HasParseError())  // Print parse error
    {
        CCLOG("GetParseError %u\n", t_Document.GetParseError());
        
        delete [] p_Buffer;
        p_Buffer = NULL;
        
        return;
    }
    
    
    if(t_Document.HasMember("Achievements"))
    {
        if(t_Document["Achievements"].IsArray())
        {
            rapidjson::Document::AllocatorType& allocator = t_Document.GetAllocator();
            for(int i = 0; i<t_Document["Achievements"].Size(); i++)
            {
                rapidjson::Value objValue;
                objValue.SetObject();
                
                // ACHIEVEMENT NAME
                std::string t_String = t_Document["Achievements"][i]["Name"].GetString();
                objValue.AddMember("Name", rapidjson::Value().SetString(t_String.c_str(), allocator), allocator);
                
                // Key Tag
                std::string t_StrKeyTag = t_Document["Achievements"][i]["KeyTag"].GetString();
                objValue.AddMember("KeyTag", rapidjson::Value().SetString(t_StrKeyTag.c_str(),allocator), allocator);

                // CurrCount
                int t_iCurCount  = t_Document["Achievements"][i]["CurrCount"].GetInt();
                objValue.AddMember("CurrCount", rapidjson::Value().SetInt(t_iCurCount), allocator);

                // Target
                int t_iTarget  = t_Document["Achievements"][i]["Target"].GetInt();
                objValue.AddMember("Target", rapidjson::Value().SetInt(t_iTarget), allocator);

                // REWARD COUNT
                int t_iReward = t_Document["Achievements"][i]["Reward"].GetInt();
                objValue.AddMember("Reward", rapidjson::Value().SetInt(t_iReward), allocator);

                // COMPLETED
                bool t_bRewardClaimed = t_Document["Achievements"][i]["RewardClaimed"].GetBool();
                objValue.AddMember("RewardClaimed", rapidjson::Value().SetBool(t_bRewardClaimed), allocator);

                // COMPLETED
                bool t_bCompleted = t_Document["Achievements"][i]["Status"].GetBool();
                objValue.AddMember("Status", rapidjson::Value().SetBool(t_bCompleted), allocator);

                rapidjson::StringBuffer strbuf;
                rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
                objValue.Accept(writer);

                //CCLOG("LIST_%d %s", i, strbuf.GetString());
                AchievementObserver *t_AchievementObserver = new AchievementObserver(strbuf.GetString());
                m_ObserverArray.push_back(t_AchievementObserver);
                //delete t_AchievementObserver;
            }
        }
    }
    
    delete [] p_Buffer;
    p_Buffer = NULL;
}
// Return Achievement Manager
AchievementManager* AchievementManager::getInstance()
{
    if(!m_PtrAchievementManager)
    {
        m_PtrAchievementManager = new AchievementManager();
    }
    return m_PtrAchievementManager;
}
// CREATE ACHIEVEMENT TOASTS WITH ACHIEVEMENT NAME
void AchievementManager::createAchievementToast(const std::string &p_AchievementName)
{
    AchievementToasts *t_PtrAchievementToasts = new AchievementToasts(p_AchievementName);
    t_PtrAchievementToasts->autorelease();
    Director::getInstance()->getRunningScene()->addChild(t_PtrAchievementToasts, 4);
}
// THIS INITIALIZE ACHIEVEMENT LIST
void AchievementManager::LoadAchievementJson()
{
    // SIMPLY INTIALIZE
}
// THIS UPDATE ACHIEVEMENT
void AchievementManager::OnAchievementUpdate(const std::string &p_Type, int p_iCount)
{
    if(m_ObserverArray.size()>0)
    {
        for (int i = 0; i<m_ObserverArray.size(); i++)
        {
            AchievementObserver *t_AchievementObserver = (AchievementObserver *)m_ObserverArray.at(i);
            if(!t_AchievementObserver->IsCompleted())
                t_AchievementObserver->UpdateAchievement(p_Type, p_iCount);
        }
    }
}

// THIS SAVE ACHIEVEMENT JSON
void AchievementManager::SaveAchievements()
{
    rapidjson::Document t_doc;
    t_doc.SetObject();
    rapidjson::Document::AllocatorType& allocator = t_doc.GetAllocator();
    
    rapidjson::Value t_AchievementData(rapidjson::kArrayType);
    for(int i = 0 ; i < m_ObserverArray.size(); i++)
    {
        AchievementObserver *t_AchievementObserver = (AchievementObserver*)m_ObserverArray.at(i);
        rapidjson::Value t_AchievementObserverValue = t_AchievementObserver->GetJsonData(allocator);
        t_AchievementData.PushBack(t_AchievementObserverValue, allocator);
    }
    
    t_doc.AddMember("Achievements", t_AchievementData, allocator);
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    t_doc.Accept(writer);
    
    FILE *m_File = NULL;
    std::string path = FileUtils::getInstance()->getWritablePath().append("AchievementsData.json");
    //CCLOG("%s",path.c_str());
    m_File = fopen(path.c_str(), "w+");
    if (!m_File)
    {
        //CCLOG("File Not Found");
        return;
    }
    
    std::string buffer = strbuf.GetString();
    fwrite(buffer.c_str(), 1,strlen(buffer.c_str()), m_File);
    fclose(m_File);
}
// THIS CLEAR ALL CURRENT ACHIEVEMENTS
void AchievementManager::ClearAllCurrentAchievements()
{
    m_ObserverArray.clear();
}
// ON GAME STATE CHANGED
void AchievementManager::GameStateChanged()
{
    switch (GameStateManager::getInstance()->getGameState())
    {
        case GAME_STATE_OVER:
            for (int i = 0; i<m_ObserverArray.size(); i++)
            {
                AchievementObserver *t_AchievementObserver = (AchievementObserver*)m_ObserverArray.at(i);
                t_AchievementObserver->OnGameOver();
            }
            break;
        default:
            break;
    }
}
// ON APPLICATION QUIT
void AchievementManager::OnApplicationQuit()
{
    SaveAchievements();
}
// THIS CHECK AND UNLOCK ACHIEVEMNET
void AchievementManager::CheckAndUnlockAchievement(const std::string &p_Name)
{
    if (!UserDefault::getInstance()->getBoolForKey(p_Name.c_str()))
    {
        UserDefault::getInstance()->setBoolForKey(p_Name.c_str(), true);
        if(p_Name.compare(ACHIEVED_30_GOOD_COMBOS)==0)
        {
            m_iAchievementTag = 1;
            m_AchievementName = "Get 30 GOOD combos";
        }
        else if(p_Name.compare(ACHIEVED_30_GREAT_COMBOS)==0)
        {
            m_iAchievementTag = 2;
            m_AchievementName = "Get 30 GREAT combos";
        }
        else if(p_Name.compare(ACHIEVED_30_AMAZING_COMBOS)==0)
        {
            m_iAchievementTag = 3;
            m_AchievementName = "Get 30 Amazing combos";
        }
        else if(p_Name.compare(ACHIEVED_30_AWESOME_COMBOS)==0)
        {
            m_iAchievementTag = 4;
            m_AchievementName = "Get 30 Awesome combos";
        }
        else if(p_Name.compare(ACHIEVED_100_WORDS_SPELL)==0)
        {
            m_iAchievementTag = 5;
            m_AchievementName = "Spell 100 words";
        }
        else if(p_Name.compare(ACHIEVED_25_TIMES_HINT)==0)
        {
            m_iAchievementTag = 6;
            m_AchievementName = "Use Hint for 25 times";
        }
        else if(p_Name.compare(ACHIEVED_50_TIMES_HINT)==0)
        {
            m_iAchievementTag = 7;
            m_AchievementName = "Use Hint for 50 times";
        }
        else if(p_Name.compare(ACHIEVED_25_TIMES_LETTER_A)==0)
        {
            m_iAchievementTag = 8;
            m_AchievementName = "Use letter(A) count option for 25 times";
        }
        else if(p_Name.compare(ACHIEVED_50_TIMES_LETTER_A)==0)
        {
            m_iAchievementTag = 9;
            m_AchievementName = "Use letter(A) count option for 25 times";
        }
        else if(p_Name.compare(ACHIEVED_10_GIFT_COLLECT)==0)
        {
            m_iAchievementTag = 10;
            m_AchievementName = "Collect the Gift box items of level map for 10 times";
        }
        else if(p_Name.compare(ACHIEVED_25_GIFT_COLLECT)==0)
        {
            m_iAchievementTag = 11;
            m_AchievementName = "Collect the Gift box items of level map for 25 times";
        }
        else if(p_Name.compare(ACHIEVED_15_DAYS_BONUS_COLLECT)==0)
        {
            m_iAchievementTag = 12;
            m_AchievementName = "Collect daily Bonus for continuous 15 days";
        }
        else if(p_Name.compare(ACHIEVED_50_DAYS_BONUS_COLLECT)==0)
        {
            m_iAchievementTag = 13;
            m_AchievementName = "Collect daily Bonus for 50 times";
        }
        else if(p_Name.compare(ACHIEVED_50_LEVELS_WITHOUT_MISSPELL)==0)
        {
            m_iAchievementTag = 14;
            m_AchievementName = "Clear any 50 levels without misspelling";
        }
        else if(p_Name.compare(ACHIEVED_25_BONUS_WORDS_COLLECT)==0)
        {
            m_iAchievementTag = 15;
            m_AchievementName = "Collect 25 Bonus word";
        }
        else if(p_Name.compare(ACHIEVED_50_BONUS_WORDS_COLLECT)==0)
        {
            m_iAchievementTag = 16;
            m_AchievementName = "Collect 50 Bonus word";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_INDIA)==0)
        {
            m_iAchievementTag = 17;
            m_AchievementName = "Complete all levels of India";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_AMERICA)==0)
        {
            m_iAchievementTag = 18;
            m_AchievementName = "Complete all levels of America";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_AUSTRALIA)==0)
        {
            m_iAchievementTag = 19;
            m_AchievementName = "Complete all levels of Australia";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_ENGLAND)==0)
        {
            m_iAchievementTag = 20;
            m_AchievementName = "Complete all levels of England";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_RUSSIA)==0)
        {
            m_iAchievementTag = 21;
            m_AchievementName = "Complete all levels of Russia";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_INDONESIA)==0)
        {
            m_iAchievementTag = 22;
            m_AchievementName = "Complete all levels of Indonesia";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_JAPAN)==0)
        {
            m_iAchievementTag = 23;
            m_AchievementName = "Complete all levels of Japan";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_BRAZIL)==0)
        {
            m_iAchievementTag = 24;
            m_AchievementName = "Complete all levels of Brazil";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_FRANCE)==0)
        {
            m_iAchievementTag = 25;
            m_AchievementName = "Complete all levels of Brazil";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_CANADA)==0)
        {
            m_iAchievementTag = 26;
            m_AchievementName = "Complete all levels of France";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_CANADA)==0)
        {
            m_iAchievementTag = 27;
            m_AchievementName = "Complete all levels of Canada";
        }
        else if(p_Name.compare(ACHIEVED_ALL_LEVELS_OF_GERMANY)==0)
        {
            m_iAchievementTag = 28;
            m_AchievementName = "Complete all levels of Germany";
        }
        createAchievementToast(p_Name);
    }
    SaveAchievements();
}
// THIS CHECK ALL ACHIEVEMENTS COMPLETED OR NOT
bool AchievementManager::CheckAllAchievementsComplete()
{
    bool t_bAllComplete = true;
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        if(m_ObserverArray.at(i)->IsCompleted() == false)
        {
            t_bAllComplete = false;
        }
    }
    return t_bAllComplete;
}
// THIS RETURN ACHIEVEMENTS ARRAY
std::vector<AchievementObserver*> AchievementManager::GetAchievements()
{
    return m_ObserverArray;
}
// RETURN ACHIEVEMENT TAG
int AchievementManager::getAchievementTag()
{
    return m_iAchievementTag;
}
int AchievementManager::getCompletedAchievementsCount()
{
    int count = 0;
    
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        if(m_ObserverArray.at(i)->IsCompleted() == true)
        {
            count++;
        }
    }
    return count;
}
// RETURN ACHIEVEMENT NAME
std::string AchievementManager::getLastUnlockAchievementName()
{
    return m_AchievementName;
}
int AchievementManager::getCountOfAchievementsToClaim()
{
    int count = 0;
    
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        if(m_ObserverArray.at(i)->IsCompleted() == true&&!m_ObserverArray.at(i)->getIsRewardClaimed())
        {
            count++;
        }
    }
    return count;
}

/// CHECK FOR ALL DAILY MISSIONS CLAIMED OR NOT
bool AchievementManager::checkAllAchievementsClaimed()
{
    bool value = false;

    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        value = m_ObserverArray.at(i)->getIsRewardClaimed();
        if(value==false)
            break;
    }
    return value;
}

// THIS CHECK AND UNLOCK Claim
bool AchievementManager::checkIsAchievementClaimed(int index)
{
    bool value = false;
    if(m_ObserverArray.size()>0)
    {
        {
            AchievementObserver *t_AchievementObserver = (AchievementObserver *)m_ObserverArray.at(index);
                value = t_AchievementObserver->getIsRewardClaimed();
        }
    }
    return value;
}
// THIS CHECK AND UNLOCK
void AchievementManager::updateAchievementClaimed(int index)
{
    if(m_ObserverArray.size()>0)
    {
        {
            AchievementObserver *t_AchievementObserver = (AchievementObserver *)m_ObserverArray.at(index);
                t_AchievementObserver->updateRewardClaimed();
            SaveAchievements();
        }
    }
}

// DESTRUCTOR
AchievementManager::~AchievementManager()
{
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        delete m_ObserverArray[i];
        m_ObserverArray.erase(m_ObserverArray.begin()+i);
    }
    m_ObserverArray.clear();
}
