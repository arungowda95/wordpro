//
//  AnswerWord.hpp
//  wordPro-mobile
//
//  Created by id on 24/02/20.
//

#ifndef AnswerWord_hpp
#define AnswerWord_hpp

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class AnswerLetterTile;
class AnswerWord:public Ref
{
    
    GameLayer   *m_ptrGameLayer;
    Point        m_Position;
    int          m_iWordIndex;
    std::string  m_StrWord;
    Vector<AnswerLetterTile*>   *m_AnswerTileArray;
    
    
    
    Size t_fGridSize;
    
    bool m_bCoinReward;
public:
    bool m_bIsContain;
    
    /// CONSTRUCTOR AND DESTRUCTOR
    AnswerWord(GameLayer *t_ptrGamelayer,const std::string &p_Word, Point p_tPostion, int p_iWordIndex,float scale,bool reward);
    ~AnswerWord();
    
    ///  Creates Answer Letter tiles
    void createAnswerLetterTile(Point p_Position, const std::string &p_Letter,float scale,bool reward);
    
    std::size_t utf8_length(std::string const &s);
    
    /// THIS RETURN EMPTY ANSWER LETTER TILE OBJECT
    AnswerLetterTile* GetEmptyTile();
    
    AnswerLetterTile* GetTile();
    
    /// Fill The  Ans tile on correct ans
    void fillLettersOnCorrectAns();
    
    ///  THIS RETURN CONTAIN STATUS OF LETTER
    bool HasContain();
    
    /// Animates Answer Word Grid on repeted word found
    void AnimateGridsonRepeat();
    
    /// returns answer word to be found
    std::string getAnsWordString();
};

#endif /* AnswerWord_hpp */
