//
//  AchievementObserver.cpp
//  wordPro-mobile
//
//  Created by id on 20/04/20.
//

#include "AchievementObserver.h"
#include "AchievementManager.h"
#pragma mark ACHIEVEMENT OBSERVER CONDITION

//CONSTRUCTOR
AchievementObserverCondition::AchievementObserverCondition(const std::string &p_KeyTag, int p_iCount, int p_iRequiredCount)
{
    m_eType = Type_Int;
    m_keyTag = p_KeyTag;
    m_iCount = p_iCount;
    m_iRequiredCount = p_iRequiredCount;
}


// CHECK CONDITION SATISFIED OR NOT
bool AchievementObserverCondition::ConditionSatisfied()
{
    switch (m_eType) {
        case Type_Int:
            if(m_iCount >= m_iRequiredCount)
                return true;
            break;
        default:
            break;
    }
    return  false;
}
// THIS UPDATE CONDITION
void AchievementObserverCondition::UpdateCondition(const std::string &p_KeyTag, int p_iCount)
{
     if(m_keyTag.compare(p_KeyTag) == 0)
    {
        m_iCount += p_iCount;
    }
}
// THIS RESET
void AchievementObserverCondition::Reset()
{
    m_iCount = 0;
}
// DESTRUCTOR
AchievementObserverCondition::~AchievementObserverCondition()
{
    
}

#pragma mark ACHIEVEMENT OBSERVER
//+++++++++++++++++++++++++++++++++++ACHIEVEMENT OBSERVER++++++++++++++++++++++++++++++++++++++++//

//CONSTRUCTOR
AchievementObserver::AchievementObserver(const std::string &p_JsonBuffer)
{
    m_bCompleted = false;
    m_JsonDocument.Parse<kParseStopWhenDoneFlag>(p_JsonBuffer.c_str());
    
    
    if(m_JsonDocument.HasMember("Name"))
    {
        m_AchievementName = m_JsonDocument["Name"].GetString();
    }
    
    if(m_JsonDocument.HasMember("KeyTag"))
    {
        m_KeyId = m_JsonDocument["KeyTag"].GetString();
    }
    
    if(m_JsonDocument.HasMember("CurrCount"))
    {
        m_iCurCount = m_JsonDocument["CurrCount"].GetInt();
    }
    
    if(m_JsonDocument.HasMember("Target"))
    {
        m_iTargetCount = m_JsonDocument["Target"].GetInt();
    }

    if(m_JsonDocument.HasMember("Reward"))
    {
        m_iReward = m_JsonDocument["Reward"].GetInt();
    }
    
    if(m_JsonDocument.HasMember("RewardClaimed"))
    {
        m_bRewardClaimed = m_JsonDocument["RewardClaimed"].GetBool();
    }
    
    if(m_JsonDocument.HasMember("Status"))
    {
        m_bCompleted = m_JsonDocument["Status"].GetBool();
    }

    AchievementObserverCondition *tCondition = NULL;

    tCondition = new AchievementObserverCondition(m_KeyId,m_iCurCount,m_iTargetCount);
    
    m_ObserverConditionArray.push_back(tCondition);
    
}
// ON GAME OVER
void AchievementObserver::OnGameOver()
{
    
}
// CHECK CONDITION SATISFIED OR NOT
bool AchievementObserver::ConditionSatisfied()
{
    
    if(m_iCurCount >= m_iTargetCount)
    return true;
    else
    return  false;
}
// THIS UPDATE ACHIEVEMENT
void AchievementObserver::UpdateAchievement(const std::string &p_Type, int p_iCount)
{
      if(m_KeyId.compare(p_Type) == 0)
      {
          m_iCurCount += p_iCount;
      }
//    return;
//    for (int i = 0; i< m_ObserverConditionArray.size(); i++)
//    {
//        AchievementObserverCondition *t_AcceptanceCondtion = (AchievementObserverCondition*)m_ObserverConditionArray.at(i);
//        t_AcceptanceCondtion->UpdateCondition(p_Type, p_iCount);
//    }
}

// CHECK ACHIEVEMENT COMPLETED OR NOT
bool AchievementObserver::IsCompleted()
{
    if(!m_bCompleted)
    {
        bool approveConditions = true;
        
        
        if(ConditionSatisfied() == false)
        {
            approveConditions = false;
        }
        if(approveConditions)
        {
            m_bCompleted = true;
            AchievementManager::getInstance()->CheckAndUnlockAchievement(m_AchievementName);
        }

        
    }
    return m_bCompleted;
}
// RETURN ACHIEVEMENT JSON DATA
rapidjson::Value AchievementObserver::GetJsonData(rapidjson::Document::AllocatorType& allocator)
{
    rapidjson::Value objValue;
    objValue.SetObject();
    //rapidjson::Document::AllocatorType& allocator = objValue.GetAllocator();
    
    objValue.AddMember("Name", rapidjson::Value().SetString(m_AchievementName.c_str(), allocator), allocator);

//    rapidjson::Value t_CurValue(rapidjson::kObjectType);
//    for (int i = 0; i<m_ObserverConditionArray.size(); i++)
//    {
//        AchievementObserverCondition *t_PtrLanguageStats = (AchievementObserverCondition*)m_ObserverConditionArray.at(i);
//        t_CurValue.SetInt(t_PtrLanguageStats->m_iCount);
//    }
//    objValue.AddMember("CurrCount",t_CurValue,allocator);
    
    objValue.AddMember("CurrCount", rapidjson::Value().SetInt(m_iCurCount), allocator);

    objValue.AddMember("KeyTag", rapidjson::Value().SetString(m_KeyId.c_str(),allocator), allocator);

    objValue.AddMember("Target", rapidjson::Value().SetInt(m_iTargetCount), allocator);
        
    objValue.AddMember("Reward", rapidjson::Value().SetInt(m_iReward), allocator);
    
    objValue.AddMember("RewardClaimed", rapidjson::Value().SetBool(m_bRewardClaimed), allocator);

    objValue.AddMember("Status", rapidjson::Value().SetBool(m_bCompleted), allocator);

    return objValue;
}
// RETURN ACHIEVEMENT NAME CONDITION
std::string AchievementObserver::getAchievementName()
{
    return m_AchievementName;
}
void AchievementObserver::updateRewardClaimed()
{
    m_bRewardClaimed = true;
}
bool AchievementObserver::getIsRewardClaimed()
{
    return m_bRewardClaimed;
}

// DESTRUCTOR
AchievementObserver::~AchievementObserver()
{
    for (int i = 0; i<m_ObserverConditionArray.size(); i++)
    {
        delete m_ObserverConditionArray[i];
        m_ObserverConditionArray.erase(m_ObserverConditionArray.begin()+i);
    }
    m_ObserverConditionArray.clear();
}
