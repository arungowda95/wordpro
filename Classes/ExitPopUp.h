//
//  ExitPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 27/03/20.
//

#ifndef ExitPopUp_hpp
#define ExitPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class ExitPopUp : public PopUp
{
    
    Sprite* m_SprPopUpBg,*m_SprPopUpTop;
    
    Sprite *ExitPromoBg;
    
    MenuItemSprite *t_miYesButton,*t_miNoButton;
public:
    ExitPopUp(std::function<void()> func);
    ~ExitPopUp();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    void OnButtonPressed(Ref *p_Sender);
    
    void createExitPromotion(Point pos,Size promosize);

    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);


    void InstallBtnCallBack();
};

#endif /* ExitPopUp_hpp */
