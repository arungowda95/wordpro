//
//  RemoveAdsPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 23/03/20.
//

#ifndef RemoveAdsPopUp_hpp
#define RemoveAdsPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class RemoveAdsPopUp :public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;

    MenuItemSprite *t_miVideoBtn,*t_miShopBtn;
    bool t_VideoAdSuccess;
    
public:
    RemoveAdsPopUp(std::function<void()> func);
    ~RemoveAdsPopUp();
    
    void OnButtonPressed(Ref *p_Sender) override;
    
    void watchVideoBtnPressed(Ref *sender);
    
    void registerVideoEvent();
    
    void onVideoAdSuccess();
    
    void onVideoAdFailure();
    
    void videoAdsAvailble();
    
    void BuyButtonCallBack();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);

    void onRemoveAdsPurchaseSuccess();
    
    void onRemoveAdsPurchaseFailure();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};
#endif /* RemoveAdsPopUp_hpp */
