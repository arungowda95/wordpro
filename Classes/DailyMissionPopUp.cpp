//
//  DailyMissionPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 24/03/20.
//

#include "DailyMissionPopUp.h"

DailyMissionPopUp::DailyMissionPopUp(std::function<void()> func)
{
    
    m_fnSelector  = func;
    t_ClaimButton = NULL;
    
    //TIMER
    m_fTimer = 0.0f;
    m_fMissionTimer = 0.0f;
    m_fMissionTimeDelay = 24*60*60;
    
    m_MissionStr = "";

    
    ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.3);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    std::string t_Label = "DailyMission";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setDimensions(m_SprPopUpTop->getBoundingBox().size.width-70, 150);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tLabel, 2);

    if(UserDefault::getInstance()->getBoolForKey(NEW_TASK))
    {
        UserDefault::getInstance()->setBoolForKey(NEW_TASK, false);
    }
    
    if(DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete())
    {
        m_MissionStr = "MissionStart";
    }else{
        m_MissionStr = "MissionEnd";
    }
    
    std::string t_SubText = m_MissionStr;
    t_mMissionTimer = createLabelBasedOnLang(t_SubText, 30);
    t_mMissionTimer->setString(t_mMissionTimer->getString()+" : ");
    t_mMissionTimer->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_mMissionTimer->setColor(Color3B::WHITE);
    t_mMissionTimer->enableOutline(Color4B(0,0,0,255),3);
    t_mMissionTimer->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_mMissionTimer->setHeight(t_mMissionTimer->getBoundingBox().size.height+20);
    t_mMissionTimer->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-10-t_mMissionTimer->getBoundingBox().size.height/2));
    t_mMissionTimer->setVisible(false);
    this->addChild(t_mMissionTimer,2);
    
    if(DailyMissionManager::getInstance()->GetDailyMissions().size()>0)
    {
        Menu *t_ClaimMenu = Menu::create(NULL);
        t_ClaimMenu->setPosition(Vec2::ZERO);
        this->addChild(t_ClaimMenu,3);
        int misnCnt = 4;
        Point pos = Point(m_SprPopUpBg->getBoundingBox().getMidX(),t_mMissionTimer->getBoundingBox().getMinY()-20);
        for(int index=0; index<misnCnt; index++)
        {
            Sprite *missionBg = Sprite::create("coins_popup_Bg.png");
            missionBg->setPosition(Vec2(pos.x,pos.y-missionBg->getBoundingBox().size.height/2));
            this->addChild(missionBg,2);
            
            Sprite *missionIcon = Sprite::create("circle-image.png");
            missionIcon->setPosition(Vec2(missionIcon->getContentSize().width/2+20,missionBg->getContentSize().height/2+5));
            missionBg->addChild(missionIcon,2);

            std::string MissionTag = DailyMissionManager::getInstance()->GetDailyMissions().at(index)->m_KeyId;
            std::string icon = getIconsForMissions(MissionTag);
            Sprite *t_MIcon = Sprite::create(icon.c_str());
            t_MIcon->setScale(0.85f);
            t_MIcon->setPosition(Vec2(missionIcon->getContentSize().width/2,missionIcon->getContentSize().height/2));
            missionIcon->addChild(t_MIcon,2);

            std::string t_missionName = DailyMissionManager::getInstance()->GetDailyMissions().at(index)->m_MissionName;
        
            std::string langTag = getMissionNum(t_missionName);
            Label *t_tMissionLabel = createLabelBasedOnLang(langTag,25);
            t_tMissionLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
            t_tMissionLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
            t_tMissionLabel->setColor(Color3B::WHITE);
            t_tMissionLabel->enableOutline(Color4B(0,0,0,255),3);
            t_tMissionLabel->setWidth(280);
            t_tMissionLabel->setHeight(t_tMissionLabel->getBoundingBox().size.height+20);
        t_tMissionLabel->setPosition(Vec2(missionIcon->getBoundingBox().getMaxX()+10,missionBg->getBoundingBox().size.height/2));
            missionBg->addChild(t_tMissionLabel, 2);
        
         
            bool t_completed = DailyMissionManager::getInstance()->GetDailyMissions().at(index)->m_bCompleted;
            bool rewardclaimed = DailyMissionManager::getInstance()->GetDailyMissions().at(index)->getIsRewardClaimed();

           if(t_completed)
          {
             
             t_ClaimButton = getButtonMade("Buy_button.png","Buy_button.png",CC_CALLBACK_1(DailyMissionPopUp::claimButtonCallBack,this));
             t_ClaimButton->setScale(t_ClaimButton->getScale()-0.3f);
                t_ClaimButton->setPosition(Vec2(missionBg->getBoundingBox().getMaxX()-t_ClaimButton->getBoundingBox().size.width/2-10,missionBg->getBoundingBox().getMidY()+20));
             t_ClaimButton->setTag(index);

              Label *claimLbl = createLabelBasedOnLang("Claim",30);
              claimLbl->setColor(Color3B::WHITE);
              claimLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
              claimLbl->setDimensions(t_ClaimButton->getBoundingBox().size.width-10,150);
              claimLbl->enableOutline(Color4B(53,126,32,255),2);
              claimLbl->setPosition(Vec2(t_ClaimButton->getContentSize().width/2,t_ClaimButton->getContentSize().height/2+9.0f));
              claimLbl->setTag(1);
              t_ClaimButton->addChild(claimLbl,1);
             
             if(rewardclaimed)
             {
                 t_ClaimButton->setEnabled(false);
                 t_ClaimButton->setOpacity(150);
                 std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag("Claimed");
                 claimLbl->setString(langstr);
                 claimLbl->setSystemFontSize(25);
             }
             
             //REWARD BUTTON
              Sprite *coinsbar = Sprite::create("coins.png");
             coinsbar->setPosition(missionBg->getBoundingBox().size.width-35-t_ClaimButton->getBoundingBox().size.width/2,coinsbar->getBoundingBox().size.height);
              missionBg->addChild(coinsbar,2);
              
              std::string t_text = StringUtils::format("%d",50);
              Label *t_Coins = Label::createWithTTF(t_text, FONT_NAME,27, Size(0, 0));
              t_Coins->setAlignment(TextHAlignment::LEFT,TextVAlignment::CENTER);
              t_Coins->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
              t_Coins->setColor(Color3B::WHITE);
              t_Coins->enableOutline(Color4B::BLACK,2);
              t_Coins->setPosition(Vec2(coinsbar->getBoundingBox().getMaxX()+5,coinsbar->getPositionY()));
              missionBg->addChild(t_Coins, 2);

             t_ClaimMenu->addChild(t_ClaimButton);
         }
         else
         {
             Sprite *m_ProgressBarBg = Sprite::create("mission_bar.png");
               m_ProgressBarBg->setPosition(missionBg->getContentSize().width-20-m_ProgressBarBg->getContentSize().width/2,missionBg->getBoundingBox().size.height/2+10);
               missionBg->addChild(m_ProgressBarBg,2);
             
             int t_count = DailyMissionManager::getInstance()->GetDailyMissions().at(index)->m_iCurCount;
             int t_TargetCount = DailyMissionManager::getInstance()->GetDailyMissions().at(index)->m_iTargetCount;
             
             if (t_count>=t_TargetCount)
             {
                 t_count = t_TargetCount;
             }
             
             std::string t_text1 = StringUtils::format("%d/%d",t_count,t_TargetCount);
             Label *t_AchiveCount = Label::createWithTTF(t_text1, FONT_NAME,24, Size(0, 0));
             t_AchiveCount->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
             t_AchiveCount->setColor(Color3B::WHITE);
             t_AchiveCount->setPosition(Vec2(m_ProgressBarBg->getPositionX(),m_ProgressBarBg->getBoundingBox().getMaxY()+t_AchiveCount->getContentSize().height/2));
             missionBg->addChild(t_AchiveCount, 2);
             
             float percentage = (float(t_count)/float(t_TargetCount))*100;
             
             if (percentage>=100.0f)
             {
                 percentage = 100.0f;
             }
             
             ProgressTimer *m_ProgressBar = ProgressTimer::create(Sprite::create("mission_bar_Fill.png"));
             m_ProgressBar->setType(ProgressTimer::Type::BAR);
             m_ProgressBar->setBarChangeRate(Vec2(1,0));
             m_ProgressBar->setMidpoint(Vec2(0.0f, 0.0f));
             m_ProgressBar->setPosition(m_ProgressBarBg->getPosition());
             m_ProgressBar->setPercentage(percentage);
             missionBg->addChild(m_ProgressBar,3);
             
             //REWARD BUTTON
             Sprite *coinsbar = Sprite::create("coins.png");
              coinsbar->setPosition(Vec2(m_ProgressBarBg->getBoundingBox().getMidX()-coinsbar->getBoundingBox().size.width/2,m_ProgressBarBg->getBoundingBox().getMinY()-5-coinsbar->getContentSize().height/2));
             missionBg->addChild(coinsbar,2);
              
             std::string t_text = StringUtils::format("%d",50);
             Label *t_Coins = Label::createWithTTF(t_text, FONT_NAME,27, Size(0, 0));
             t_Coins->setAlignment(TextHAlignment::LEFT,TextVAlignment::CENTER);
             t_Coins->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
             t_Coins->setColor(Color3B::WHITE);
             t_Coins->enableOutline(Color4B::BLACK,2);
             t_Coins->setPosition(Vec2(coinsbar->getBoundingBox().getMaxX()+5,coinsbar->getPositionY()));
              missionBg->addChild(t_Coins, 2);
        }
        
        pos.y -= missionBg->getBoundingBox().size.height+5;
      }
    }
    
    // CLOSE BUTTON
      MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(DailyMissionPopUp::OnButtonPressed,this));
      t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
      t_miCloseButton->setTag(0);

      Menu *m_Menu = Menu::create(t_miCloseButton,NULL);
      m_Menu->setPosition(Vec2::ZERO);
      this->addChild(m_Menu,2);

    this->schedule(schedule_selector(DailyMissionPopUp::OnUpdateFrame));

}
MenuItemSprite *DailyMissionPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
      
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}
void DailyMissionPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

     MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
       
       int pButtonTag =  Button->getTag();

       switch (pButtonTag)
       {
           case 0:
           {
               status = (char*)"close";
           }break;
           case 1:
           {
               status = (char*)"Shop";
           }break;
            default:
         break;
       }
     
     if (m_fnSelector!=NULL) {
         m_fnSelector();
     }
}
void DailyMissionPopUp ::claimButtonCallBack(Ref *sender)
{
    MenuItemSprite *button  = (MenuItemSprite*)sender;
    int tag = button->getTag();
    Label *claimLbl  = (Label*)button->getChildByTag(1);
    
    DailyMissionManager::getInstance()->updateDailyMissionClaimed(tag);

    button->setOpacity(150);
    button->setEnabled(false);
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag("Claimed");
    claimLbl->setString(langstr);
    claimLbl->setSystemFontSize(27);

    int coins = DailyMissionManager::getInstance()->GetDailyMissions().at(tag)->m_iReward;
    me_iCoinCount += coins;
    UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
    UserDefault::getInstance()->flush();
    
    coinAddAnimation(button->getPosition());
}
void DailyMissionPopUp::coinAddAnimation(Point t_Position)
{
    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    Point _startPosOppo = Point(t_Position.x-50,t_Position.y-50);
    for (int i=0; i< 20; i++)
    {
        Sprite *_spr = Sprite::create("coins.png");
        _spr->setScale(_spr->getScale()+0.2);
        _spr->setPosition(_startPosOppo);
        _spr->setOpacity(0);
        ccBezierConfig bezier;
        bezier.controlPoint_1 = _startPosOppo;
        bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
        bezier.endPosition =_endPos;
        BezierTo *_bez = BezierTo::create(.5f, bezier);
        this->addChild(_spr,5);
        FadeTo *_fade = FadeTo::create(.04*i,255);
        FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(DailyMissionPopUp::removeSprite,this));
        FiniteTimeAction*_seq = Sequence::create(_fade,_bez,_call,NULL);
        _spr->runAction(_seq);
    }
}
void DailyMissionPopUp::removeSprite(Ref *_sender)
{
    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
    
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);

    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    
}
void DailyMissionPopUp::coinAnimationDoneClosePopUp()
{
    status = (char*)"close";
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
    
    GameController::getInstance()->showInterstial();
}
std::string DailyMissionPopUp::getIconsForMissions(std::string AchieveTag)
{

    std::string name = "circle-image";
    
    if(AchieveTag.compare("good")==0)
    {
        name = "Achievement_icons/good.png";
    }
   else if(AchieveTag.compare("great")==0) {
        name = "Achievement_icons/great.png";
    }
    else if(AchieveTag.compare("amazing")==0) {
         name = "Achievement_icons/amazing.png";
     }
    else if(AchieveTag.compare("awesome")==0) {
         name = "Achievement_icons/awesome.png";
     }
    else if(AchieveTag.compare("words")==0) {
         name = "Achievement_icons/words.png";
     }
    else if(AchieveTag.compare("hints")==0) {
         name = "Achievement_icons/hints.png";
     }
    else if(AchieveTag.compare("Letter_A")==0) {
         name = "Achievement_icons/Letter_A.png";
     }
    else if(AchieveTag.compare("giftbox")==0) {
         name = "Achievement_icons/giftbox.png";
     }
    else if(AchieveTag.compare("dailybonus")==0) {
         name = "Achievement_icons/dailybonus.png";
     }
    else if(AchieveTag.compare("misspell")==0) {
         name = "Achievement_icons/levels.png";
     }
    else if(AchieveTag.compare("levels")==0) {
         name = "Achievement_icons/levels.png";
     }
    else if(AchieveTag.compare("bonusword")==0) {
         name = "Achievement_icons/bonusword.png";
     }
    else {
        name = "Achievement_icons/clear_country.png";
    }
    
    return name;
}
/// UPDATE FRAME CHECK FOR TIME
void DailyMissionPopUp::OnUpdateFrame(float p_fDeltaTime)
{
    {
        m_fTimer += p_fDeltaTime;
        if(m_fTimer >= 1.0f)
        {
            m_fTimer = 0.0f;
            

            if(!UserDefault::getInstance()->getBoolForKey("DailyMissionAvailable"))
            {
                showdayTimer();
            }
        }
    }
}
void DailyMissionPopUp::showdayTimer()
{
  // GET BEGIN TIME
        std::string strTime = UserDefault::getInstance()->getStringForKey(MISSION_START_TIME);
        struct tm target;
        strptime(strTime.c_str(),"%c", &target);

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        m_MissionStart = timelocal(&target);
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        // THIS SET DAYLIGHT SET TIME STATUS UNKOWN
        target.tm_isdst = -1;
        //CCLOG("DST %d", target.tm_isdst);
        m_MissionStart = mktime(&target);
        //CCLOG("START TIME %s",ctime(&m_start));
    #endif
        // GET END TIME
        time(&m_MissionCurrent);
        
        //CCLOG("CURRENT TIME %s",ctime(&m_Current));
        
        double diff1 = difftime(m_MissionCurrent, m_MissionStart);
        
        //CCLOG("TIME DIFFERENCE %lf",diff1);
        
        if(diff1>m_fMissionTimeDelay)
        {
            // UPDATE BONUS AVAILABLE
            UserDefault::getInstance()->setBoolForKey("DailyMissionAvailable", true);
            int day = UserDefault::getInstance()->getIntegerForKey(MISSION_DAY);

            UserDefault::getInstance()->setIntegerForKey(MISSION_DAY, day+1);
            UserDefault::getInstance()->flush();
        }
        
        m_fMissionTimer = m_fMissionTimeDelay-(int)diff1;
        
        if(diff1<0)
            diff1*=-1;
        
        // CHECK IF TIME_DIFF IS LESS THAN ASSIGN_TIME_DELAY THAN SHOW TIMER
        if(m_fMissionTimer>=0 && diff1>0)
        {
            UpdateTimer();
        }
}
// THIS UPDATE BONUS TIMER
void DailyMissionPopUp::UpdateTimer()
{
    if(t_mMissionTimer != NULL)
    {
        int tot_S = (int)m_fMissionTimer%60;
        int tot_M = (int)m_fMissionTimer/60;
        int minute = tot_M%60;
        int tot_H = (int)tot_M/60;
        int hour = tot_H%60;
        std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(m_MissionStr.c_str());

        std::string  buffer = StringUtils::format("%s : %02d:%02d:%02d",langstr.c_str(),hour,minute,tot_S) ;
        t_mMissionTimer->setString(buffer);
        t_mMissionTimer->setVisible(true);
    }
}
Label *DailyMissionPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
/// THIS RETURN DESCRIPTION ACCORDING TO NAME
std::string DailyMissionPopUp::getMissionNum(std::string m_MissionName)
{
    std::string t_Description = "";
    
    if(m_MissionName.compare("Spell 25 words")==0)
    {
        t_Description = "Mission1";
    }
    else if(m_MissionName.compare("Clear 25 levels")==0)
    {
        t_Description = "Mission2";
    }
    else if(m_MissionName.compare("Get 25 GOOD combos")==0)
    {
        t_Description = "Mission3";
    }
    else if(m_MissionName.compare("Get 25 GREAT combos")==0)
    {
        t_Description = "Mission4";
    }
    else if(m_MissionName.compare("Spell 50 words")==0)
    {
        t_Description = "Mission5";
    }
    else if(m_MissionName.compare("Use Hint for 10 times")==0)
    {
        t_Description = "Mission6";
    }
    else if(m_MissionName.compare("Get 25 Amazing combos")==0)
    {
        t_Description = "Mission7";
    }
    else if(m_MissionName.compare("Get 25 Awesome combos")==0)
    {
        t_Description = "Mission8";
    }
    else if(m_MissionName.compare("Spell 25 words")==0)
    {
        t_Description = "Mission9";
    }
    else if(m_MissionName.compare("Use letter(A) count option for 10 times")==0)
    {
        t_Description = "Mission10";
    }
    else if(m_MissionName.compare("Collect the Gift box items of level map for 5 times")==0)
    {
        t_Description = "Mission11";
    }
    else if(m_MissionName.compare("Clear any 10 levels without misspelling")==0)
    {
        t_Description = "Mission12";
    }
    else if(m_MissionName.compare("Spell 50 words")==0)
    {
        t_Description = "Mission13";
    }
    else if(m_MissionName.compare("Collect 10 Bonus word")==0)
    {
        t_Description = "Mission14";
    }
    else if(m_MissionName.compare("Get 25 Good combos")==0)
    {
        t_Description = "Mission15";
    }
    else if(m_MissionName.compare("Use letter(A) count option for 10 times")==0)
    {
        t_Description = "Mission16";
    }
    else if(m_MissionName.compare("Spell 25 words")==0)
    {
        t_Description = "Mission17";
    }
    else if(m_MissionName.compare("Use Hint for 10 times")==0)
    {
        t_Description = "Mission18";
    }
    else if(m_MissionName.compare("Collect 10 Bonus word")==0)
    {
        t_Description = "Mission19";
    }
    else if(m_MissionName.compare("Clear any 10 levels without misspelling")==0)
    {
        t_Description = "Mission20";
    }
   
    return t_Description;
    
}
DailyMissionPopUp::~DailyMissionPopUp()
{
 
    this->removeAllChildrenWithCleanup(true);
    
}
