//
//  FacebookManager.hpp
//  wordPro-mobile
//
//  Created by id on 30/06/20.
//

#ifndef FacebookManager_hpp
#define FacebookManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "GameConstants.h"
#include "extensions/cocos-ext.h"

#ifdef SDKBOX_ENABLED
#include "PluginFacebook/PluginFacebook.h"
#endif

USING_NS_CC;

class FacebookManager
#ifdef SDKBOX_ENABLED
    : public sdkbox::FacebookListener{
#else
{
#endif
public:

    FacebookManager() {};
    ~FacebookManager() {};

    // To get static object of the game
    static FacebookManager* getInstance() {
        if (!fbManager) {
            fbManager = new FacebookManager();
            srand(time(0));
        }
        return fbManager;
    }

    std::string facebookName;
    std::string facebookID;
    
    void login();
    
    void callFacebookApi();
    bool isLoggedIn();
#ifdef SDKBOX_ENABLED


    void onLogin(bool isLogin, const std::string& msg);
    void onPermission(bool isLogin, const std::string& msg);
    void onAPI(const std::string& tag, const std::string& jsonData);
    
    void onSharedSuccess(const std::string& message);
    void onSharedFailed(const std::string& message);
    void onSharedCancel();
    
    void onFetchFriends( bool ok , const std::string & msg );
    void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo & friends );
    void onInviteFriendsWithInviteIdsResult ( bool result ,
                                              const std::string & msg );
    
    void onInviteFriendsResult ( bool result , const std::string & msg );
    void onGetUserInfo ( const sdkbox::FBGraphUser & userInfo );
#endif
    
private:
    // static game object through out the game
    static FacebookManager* fbManager;

};
#endif /* FacebookManager_hpp */
