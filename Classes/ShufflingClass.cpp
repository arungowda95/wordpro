//  THIS CLASS SET GAME STATE IN THE GAME (SIGELETON CLASS REMAIN THROUGH OUT THE GAME)

#include "ShufflingClass.h"

#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
#include <memory>

//SINGELTON STUFF
ShufflingClass* ShufflingClass::m_PtrShufflingClass = NULL;

// RETURN GAME HANDLER
ShufflingClass* ShufflingClass::getInstance()
{
    if(!m_PtrShufflingClass)
    {
        m_PtrShufflingClass = new ShufflingClass();
    }
    return m_PtrShufflingClass;
}

// CONSTRUCTOR
ShufflingClass::ShufflingClass()
{
    
}

#pragma mark
#pragma INTEEGER SHUFFLING

std::vector<int> & ShufflingClass::randomShuffle(const std::vector<int> &p_numsArray, std::vector<int> &elems)
{
    int t_iSize = (int)p_numsArray.size();
    int m_iShuffleIndex[t_iSize];
    
    for (int i = 0 ; i < t_iSize; i++)
        m_iShuffleIndex[i] = i;
    
    for(int i = 0 ; i < t_iSize;i++)
    {
        for(int j = 0 ; j < t_iSize;j++)
        {
            int t_iRand1 = arc4random()%t_iSize;
            int t_iRand2 = arc4random()%t_iSize;
            int temp = m_iShuffleIndex[t_iRand1];
            m_iShuffleIndex[t_iRand1] = m_iShuffleIndex[t_iRand2];
            m_iShuffleIndex[t_iRand2] = temp;
        }
    }
    for (int i = 0 ; i<t_iSize; i++)
    {
        elems.push_back(p_numsArray[m_iShuffleIndex[i]]);
    }
    return elems;
}
std::vector<int> ShufflingClass::randomShuffle(const std::vector<int> &p_numsArray)
{
    std::vector<int> elems;
    randomShuffle(p_numsArray, elems);
    return elems;
}



#pragma mark
#pragma INTEEGER ARRAY SHUFFLING
std::vector<std::vector<int>> & ShufflingClass::randomShuffle(const std::vector<std::vector<int>> &p_numsArray, std::vector<std::vector<int>> &elems)
{
    int t_iSize = (int)p_numsArray.size();
    int m_iShuffleIndex[t_iSize];
    
    for (int i = 0 ; i < t_iSize; i++)
        m_iShuffleIndex[i] = i;
    
    for(int i = 0 ; i < t_iSize;i++)
    {
        for(int j = 0 ; j < t_iSize;j++)
        {
            int t_iRand1 = arc4random()%t_iSize;
            int t_iRand2 = arc4random()%t_iSize;
            int temp = m_iShuffleIndex[t_iRand1];
            m_iShuffleIndex[t_iRand1] = m_iShuffleIndex[t_iRand2];
            m_iShuffleIndex[t_iRand2] = temp;
        }
    }
    for (int i = 0 ; i<t_iSize; i++)
    {
        elems.push_back(p_numsArray[m_iShuffleIndex[i]]);
    }
    return elems;
}

std::vector<std::vector<int>> ShufflingClass::randomShuffle(const std::vector<std::vector<int>> &p_numsArray)
{
    std::vector<std::vector<int>> elems;
    randomShuffle(p_numsArray, elems);
    return elems;
}



#pragma mark
#pragma GET THE RANDOM NON REPEATED ELEMENTS WITHIN THE RANGE
std::vector<int> ShufflingClass::shuffleForRange(int minRange,int maxRange,int _sizeVector)
{
    
    std::vector<int> tArray = std::vector<int>();
    int _index =0;
    bool _baccepted =false;
    int rand = random(minRange, maxRange);
    while (_index <_sizeVector)
    {
        rand =  random(minRange, maxRange);
        _baccepted =true;
        for (int i=0; i<_index; i++)
        {
            if(tArray[i]==rand)
            {
                _baccepted =false;
                break;
            }
        }
        if(_baccepted){
            tArray.push_back(rand);
            _index++;
            
        }
        
    }
    return tArray;
    
}


#pragma mark
#pragma POSTIONS  SHUFFLING
std::vector<cocos2d::Point> & ShufflingClass ::randomShuffle(const std::vector<cocos2d::Point> &p_numsArray, std::vector<cocos2d::Point> &elems)
{
    int t_iSize = (int)p_numsArray.size();
    int m_iShuffleIndex[t_iSize];
    
    for (int i = 0 ; i < t_iSize; i++)
        m_iShuffleIndex[i] = i;
    
    for(int i = 0 ; i < t_iSize;i++)
    {
        for(int j = 0 ; j < t_iSize;j++)
        {
            int t_iRand1 = arc4random()%t_iSize;
            int t_iRand2 = arc4random()%t_iSize;
            int temp = m_iShuffleIndex[t_iRand1];
            m_iShuffleIndex[t_iRand1] = m_iShuffleIndex[t_iRand2];
            m_iShuffleIndex[t_iRand2] = temp;
        }
    }
    for (int i = 0 ; i<t_iSize; i++)
    {
        elems.push_back(p_numsArray[m_iShuffleIndex[i]]);
    }
    return elems;
}

std::vector<cocos2d::Point> ShufflingClass::randomShuffle(const std::vector<cocos2d::Point> &p_numsArray)
{
    std::vector<cocos2d::Point> elems;
    randomShuffle(p_numsArray, elems);
    return elems;
}



#pragma mark
#pragma STRING SHUFFLING
std::vector<std::string> & ShufflingClass ::randomShuffle(const std::vector<std::string> &p_numsArray, std::vector<std::string> &elems)
{
    int t_iSize = (int)p_numsArray.size();
    int m_iShuffleIndex[t_iSize];
    
    for (int i = 0 ; i < t_iSize; i++)
        m_iShuffleIndex[i] = i;
    
    for(int i = 0 ; i < t_iSize;i++)
    {
        for(int j = 0 ; j < t_iSize;j++)
        {
            int t_iRand1 = arc4random()%t_iSize;
            int t_iRand2 = arc4random()%t_iSize;
            int temp = m_iShuffleIndex[t_iRand1];
            m_iShuffleIndex[t_iRand1] = m_iShuffleIndex[t_iRand2];
            m_iShuffleIndex[t_iRand2] = temp;
        }
    }
    for (int i = 0 ; i<t_iSize; i++)
    {
        elems.push_back(p_numsArray[m_iShuffleIndex[i]]);
    }
    return elems;
}

std::vector<std::string> ShufflingClass::randomShuffle(const std::vector<std::string> &p_numsArray)
{
    std::vector<std::string> elems;
    randomShuffle(p_numsArray, elems);
    return elems;
}
// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
std::vector<std::string> &ShufflingClass::split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
std::vector<std::string> ShufflingClass::split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
