#ifndef __GAME_CONTROLLER_H__
#define __GAME_CONTROLLER_H__

#include "cocos2d.h"
#include "GameConstants.h"
#include "InterfaceManagerInstance.h"
#include "StatsManager.h"

#ifdef USE_NEW_AUDIO_ENGINE
#include "audio/include/AudioEngine.h"
#else
#include "audio/include/SimpleAudioEngine.h"
using namespace CocosDenshion;
#endif

#ifdef SDKBOX_ENABLED
#include "PluginIAP/PluginIAP.h"
#include "PluginAdMob/PluginAdMob.h"
#endif

USING_NS_CC;

class GameController
#ifdef SDKBOX_ENABLED
	: public sdkbox::IAPListener,public sdkbox::AdMobListener{
#else
{
#endif
public:
	struct dayStruct {
		dayStruct() {}
		dayStruct(int _mday, int _month, int _year) {
			mday = _mday;
			month = _month;
			year = _year;
		}
		int mday, month, year;
	};
	GameController() {};
	~GameController() {};

	// To get static object of the game
	static GameController* getInstance() {
		if (!gameObject) {
			gameObject = new GameController();
			srand(time(0));
		}
        return gameObject;
    }


    void checkForRatePopUp();
    
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
    std::vector<std::string> split(const std::string &s, char delim);
    
    std::string getCoinsFormat(int chips);
    
	// call this function to set new scene
	void createScene(int stateIndex, int level = 0);

	// To get/set cur screen, dont call manually. It will call from CreateScene().
	inline int getCurGameScreen() { return curGameScreen; }
	inline void setCurGameScreen(int screen) { curGameScreen = screen; }

	// To get/set prev screen, dont call manually. It will call from CreateScene().
	inline int getPrevGameScreen() { return prevGameScreen; }
	inline void setPrevGameScreen(int screen) { prevGameScreen = screen; }

	// For Music / SFX
    bool isBGMEnabled();
	void setBGMEnabled(bool enableMusic);
    bool isSFXEnabled();  
	void setSFXEnabled(bool enableSFX);

	// For Vibrate
	bool isVibrateEnabled() { return vibrateEnabled; }
	void seVibrateEnabled(bool enableVibrate);
	void doVibrate();

	// To play the sfx sounds
	void playSFX(const char* index);

	// To play the bgm in loop
	void playBGM(const char* index, bool isLoop);

	// To stop all current BGM & SFX
	void stopSounds();

	// To pause / resume music on pause game
	void pauseSounds();
	void resumeSounds();

	inline int getWormSpeed() { return wormSpeed; }
	inline void setWormSpeed(int speed) { wormSpeed = speed; }

	// To get/set current game control
	inline int getCurGameControl() { return curGameControl; }
	inline void setCurGameControl(int type) { curGameControl = type; }

	
	inline bool isPlayWithComputer() { return isPlayWithComp; }
	inline void setPlayWithComputer(bool value) { isPlayWithComp = value; }

	// To get current Time in milli seconds
	static long currentTimeMillis();

	// To get random between lowerLimit & upperLimit
	static int getRandom(int lowerLimit, int upperLimit);
	static int getRandom(int lowerLimit, int upperLimit, int gap);

    void setTodayAsfirstDay();
    int getDayDifferenceCount();
    
	// To get last day info
	void initLastDay();
	bool isDayHasPassed();
	void setTodayAsLastDay();

	// To get/set User Default Values
	bool getBoolForKey(const char* key, bool defaultValue = false);
	void setBoolForKey(const char* key, bool value);

	int getIntegerForKey(const char* key, int defaultValue = 0);
	void setIntegerForKey(const char* key, int value);

	float getFloatForKey(const char* key, float defaultValue = 0.0f);
	void setFloatForKey(const char* key, float value);

	double getDoubleForKey(const char* key, double defaultValue = 0);
	void setDoubleForKey(const char* key, double value);

	std::string getStringForKey(const char* key, std::string defaultValue = "");
	void setStringForKey(const char* key, std::string value);

	unsigned char* getGameKey();
	void setGameKey();
	
    
    void doParticleEffect(Node *_node,cocos2d::Point p_Position,int noOfPartice);
    
    void updateUserDataToFireBase();
    
    void getLeaderboardData();
    
    void getUserLanguageData();
    
    void parseLanguageDataUserIds(std::string me_Strjson);
    
    bool checkIsFbIDAlreadyClaimedReward(std::string);
    
    void updateUserDataInGame(std::string userdata);
    
    std::vector<std::string> userIds;
	// To set boolean value true if game is online
	bool isGameOnline;

	// To set boolean value true if InterNet available
	bool isInternetOn;

    std::string currencycode;
	// for In-app
	std::vector<std::string> inAppTitle, inAppName, inAppPrice, inAppCoin,inAppPriceVal;

	int totalMatch, totalWin, totalLose, total2Match, total3Match, total4Match, xp_point, win_xp_point, lose_xp_point, currentBonusDay, matchFee, userLevel, totalCoin, roomAmt;
	int videoBonusTime, videoBonusChips, fbBonusChips;
	
	std::vector<int> bonusAmount, roomWinAmt, roomEntryAmt;
	std::vector<std::string> roomName;

	std::string getFacebookName();
	std::string getFacebookID();
	std::string getBase64EncodeForFile(std::string filePath);

	void setLanguageId(std::string value);
	std::string getLanguageId();

	void callFacebookApi();
    
    void connectedToAppleId(std::string str,std::string str2);
    void FailedToConnectAppleId();
    
    void saveUserRank(int);
    
    std::string  inAppAdsPrice = "";
    std::string  inAppAdsPriceVal = "";
    std::string  inAppLimitedSalePrice = "";

    std::string getMonthName(int num);
    
    
    bool m_bNoAdsForSession=false;
    
    // for moreGames
    bool isMoreGamesVisible;
    std::vector<std::string> moreGameAppUrl, moreGameImageUrl;
    
    // for Level complete moreGames
    bool isLevelCompGamesVisible;
    std::vector<std::string> t_LevelmoreGameAppUrl, t_levelmoreGameImageUrl;

    
    //for Exit Promotion
    
    std::string m_appIconUrl = "";
    std::string m_strAppPackage = "";
    
    std::string m_strAppTitle = "";
    std::string m_strDesc = "";
    std::string m_strAppUrl = "";
    
    //for POPUP Promotion
    std::string m_PopupIconUrl = "";
    std::string m_PopupAppPackage = "";
    
    std::string m_PopupAppTitle = "";
    std::string m_PopupAppDesc = "";
    std::string m_PopupAppUrl = "";


#ifdef SDKBOX_ENABLED
	
	// -------------- IAP-----------------------
	virtual void onInitialized(bool ok) override;
	virtual void onSuccess(sdkbox::Product const& p) override;
	virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
	virtual void onCanceled(sdkbox::Product const& p) override;
	virtual void onRestored(sdkbox::Product const& p) override;
	virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
	virtual void onProductRequestFailure(const std::string &msg) override;
    virtual void onRestoreComplete(bool ok, const std::string & msg) override;

	void purchaseThis(std::string productName);
	void restoreAllProduct();
    
    
    //---------------- Plugin Admob ---------------
    virtual void adViewDidReceiveAd(const std::string &name) override;

    virtual void adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg) override;
    
    virtual void adViewWillPresentScreen(const std::string &name) override;

    virtual void adViewDidDismissScreen(const std::string &name) override;

    virtual void adViewWillDismissScreen(const std::string &name) override;

    virtual void adViewWillLeaveApplication(const std::string &name) override;
    
    virtual void reward(const std::string &name, const std::string &currency, double amount) override;
    
    void cacheAds();
    
    void showBanner();
    
    void hideBanner();
    
    void showInterstial();
    
    void showVideoAd();
    
    bool isVideoAdsAvailable();
    
    bool adRewarded = false;
        
    bool m_bVideoAdsAvailable = false;
    
    
    void disableAdsForThisSession();
#endif

private:
	// static game object through out the game
	static GameController* gameObject;
	
	// will hold the current game screen value. see GameConstants.h for all defined screen values
	int curGameScreen;

	// will hold the previous game screen value. see GameConstants.h for all defined screen values
	int prevGameScreen;

	// if 'true', music/loop will play
	bool sfxEnabled;

	// if 'true', sfx sounds will play
	bool bgmEnabled;

	// if 'true', device will vibrate ingame
	bool vibrateEnabled;

	// will hold worm speed value : 2=low, 3=mid, 4=high
	int wormSpeed;

	// will hold current control type. default CONTROL_TOUCH_LEFT_RIGHT : see GameConstants.h for all defined control values
	int curGameControl;

	//game play offline mode
	bool isPlayWithComp;
    
	// Will hold information of the Day last time played
	dayStruct lastDay;
    
    dayStruct firstDay;

	// will hold game key (GAME_KEY + last 4 digit of Device Unique code)
	std::string gameKey;

};


#endif  // __GAME_CONTROLLER_H__
