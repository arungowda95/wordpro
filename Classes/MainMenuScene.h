//
//  MainMenuScene.hpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#ifndef MainMenuScene_hpp
#define MainMenuScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "network/HttpClient.h"
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
#include "TutorialManager.h"

using namespace cocos2d;
using namespace cocos2d::network;
using namespace rapidjson;

class MenuLayer;
class MainMenuScene:public Scene
{
    MenuLayer *m_ptrMenuLayer;
    
public:
    MainMenuScene();
    ~MainMenuScene();
    
};

#pragma mark MENULAYER

class LeaderBoardLayer;
class PopUpManager;
class MenuLayer:public Layer
{
    
    PopUpManager *m_ptrPopUpManager;
    MenuItemSprite *m_SettingsButton,*m_PlayButton,*m_AchievementButton,*m_ShopButton,
    *m_CoinsButton,*m_MoreGamesButton,*m_DailyEventBtn,*m_MegaDealButton;
    
    Label  *m_CoinCount,*m_tSaleTimerLbl;
    Sprite *m_SpSale;
    
    MenuItemSprite *signInApple;
    
    Size visibleSize;
    Vec2 Origin;
    
    Vector<MenuItemSprite*>  *moreGameArr;
    std::vector<HttpRequest*> httpRequest;
    
    int photoIndex;
    bool m_bIsEventCreated,m_bJsonError = false;
    
    time_t     m_SaleStart, m_SaleCurrent, m_start, m_Current;
    float      m_fSaleTimer,m_fSaleTimeDelay,m_fTimer;
    
    MenuItemSprite *m_rateButton,*m_shareButton;
    MenuItemSprite *m_FacebookButton,*m_LeaderBoardButton,*m_FBInfoButton;
    
    
    Layer *m_FbInfoLayer;
    TutorialManager *m_TutManager;
    bool m_bIsTutorial = false,m_bIsDailyReward = false;
public:
    
    MenuLayer();
    ~MenuLayer();/// DESTRUCTOR
    
    /// CUSTOM BUTTON MADE
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    /// CREATE MORE GAMES BUTTONS
    void createMoreGames();
    
    /// CREATE  CUSTOM EVENTS FOR PURCHASE AND VIDEO ADS SUCCESS
    void registerCustomEvents();
    
    ///  PLAY BUTTON CALLBACK
    /// @param sender  REFERENCE
    void playBtnCallback(Ref *sender);
    
    /// settings button callback
    void settingsBtnCallback(Ref *sender);
    
    /// achievement button callback
    void achievementBtnCallback(Ref *sender);
    
    /// shop coins button callback
    void shopBtnCallback(Ref *sender);
    
    /// add coins button callback
    void addCoinBtnCallback(Ref *sender);
    
    /// more games icons button callback
    void moreGamesBtnCallback(Ref *sender);
    
    /// daily event missions callaback
    void dailyEventBtnCallback(Ref *sender);
    
    /// limited sale button callback
    void megaDealBtnCallback(Ref *sender);
    
    ///  UPDATE COIN VALUE ON  COIN SUCCESS
    void UpdateCoins();
    
    /// check for more games json data
    void checkForMoreGames();
    
    /// on success of more games data download
    void onFetchCompleted(HttpClient *sender, HttpResponse *response);
    
    /// download more games icons
    void downloadMoreGamesIcon(int index);
    
    /// on success callback for more games icons download
    void onMoreGamesPhotoDownloaded(HttpClient *sender, HttpResponse *response);
    
    /// set enable and disable actions of buttons
    /// @param p_bEnable boolean value to enable or disable buttons
    void setEnableButtons(bool p_bEnable);
    
    /// indicate animation of coin label on coins adding
    void CoinLabelAnimation();
    
    /// THIS CREATE UPDATE COIN EVENT
    void CreateUpdateCoinEvent();
    
    /// THIS LOAD OTHER POP UP BY COMPARING WITH STRING
    void LoadOtherPopUp(Ref *p_Sender, const std::string &p_String);
    
    /// THIS WILL REMOVE CURRENT ONE AND CALL ANOTHER POPUP WAIT FOR ANIMATION OF CURRENT POPUP
    void LoadOtherPopUpWithDelay(float p_fDelayTime, const std::string &p_PopUpName);
    
    /// THIS SHOW TIMER
    void checkSaleTimer();
    void UpdateLimitedSaleTimer();
    
    /// THIS CHECK FOR DAILY REWARD
    void CheckDailyRewardAvailability();
    
    /// check for on launch popup's language daily reward  promotion
    void checkforDailyRewardPopUp();
    
    /// UPDATE FRAME CHECK FOR TIME
    void OnUpdateFrame(float p_fDeltaTime);
    
    ///****************************KEYPAD FUNCTIONS********************/
    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event *event);
    
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event *unused_event);
    
    /// this check for exit popup game promotion data and popup promotion on daily basis
    void checkForExitPromotion();
    
    /// success callback for exit popup promotion data
    void httpDataSuccessCallback(HttpClient* client, HttpResponse* response);
    
    /// parse exit popup app promotion data
    void parseExitData(std::string jsonStr);
    
    /// download exit popup game icon with url
    void downloadExitPromoGameIcon(int index,std::string url);
    
    /// success callback for exit popup on icon download
    void onExitPhotoDownloaded(HttpClient *sender, HttpResponse *response);
    
    /// check for limited sale availablity
    void checkforLimitedSaleAvailability();
    
    /// THIS UPDATE  DATA on POUP's change
    void UpdateMenuData();
    
    /// check for  DAILY MISSION button tags update on achivement's complete
    void checkForDailyMissionUpdates();
    
    /// check for  achievement button tags update on achivement's complete
    void checkForAchievementUpdates();
    
    ///  check for popup promotion data
    void checkForGamePromotionData();
    
    /// on success callback for promotion data http  request
    void httpPromotionSuccessCallback(HttpClient* client, HttpResponse* response);
    
    /// parse popup promotion data
    void parsePromotionData(std::string jsonStr);
    
    /// show app promotion popup
    void showPromotionPopUp();
    
    void checkforJson();
    
    /// get LevelComplete more games Json data
    /// @param url  Url of Json Data
    /// @param tag tag is that json or image
    void getLevelFinishMoreGames(std::string url,std::string tag);
    
    /// on Json Fetch completed
    void onJsonFetchCompletd(HttpClient* client, HttpResponse* response);
    
    /// Facebook Login CallBack
    void FacebookLoginCallback(Ref *sender);
    
    /// on facebook login success  and failure
    void FbLoginSuccess();
    void FbLoginFailure();
    
    /// Rate Button callback
    void rateButtonCallBack();
    
    void shareButtonCallBack();
    
    void leaderboardBtnCallback();
    
    /// Apple signin Button callback
    void signInWithApple(Ref *sender);
    
    /// Apple signin success and failure callback
    void AppleSignInSuccess();
    void AppleSignInFailure();
    
    virtual void onExit();
    
    ///  this checks App Update is availbale or not every 3 days
    void checkForGameUpdate();
    
    /// this is to check with notifiaction click  with notification data deeplink action
    void openNotificationAction();
    
    /// checks for leaderboard tutorial
    /// @param type type of tut for daily task and leaderboard
    void checkforLeaderBoardTutorial(int type);
    
    /// this checks for leaderboard and daily task tutorial condition
    void checkForChallengeAndboardTut();
    
    /// this is called when firebase pridiction called
    void userRewardsBasedPrediction();
};

#endif /* MainMenuScene_hpp */

