//
//  AnswerGrid.cpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#include "AnswerGrid.h"
#include "GameLayer.h"
#include "AnswerWord.h"
#include "AnswerLetterTile.h"
#include "HudLayer.h"
#include "GameConstants.h"
#include "OptionGrid.h"
#include "StatsManager.h"
#include "AchievementManager.h"
#include "DailyMissionManager.h"
#include "GameController.h"

AnswerGrid::AnswerGrid(GameLayer *p_ptrGameLayer)
{
    m_ptrGameLayer = p_ptrGameLayer;
    
    m_ptrAnswerWordArray = NULL;
    m_ptrAnswerWordArray = new Vector<AnswerWord*>;
    m_WordsArray = new std::vector<std::string>;
    m_tempBonusArray = new std::vector<std::string>;
    m_BonusWordsArray = new std::vector<std::string>;
    
    t_fGridSize = Size(111,115);
    t_fGridScale = m_fScale;
    m_iAttemptNum = 0;
    m_bAttempt  = true;
    
    m_bIsCoinReward = false;
    t_fGridSize = Size(t_fGridSize.width*t_fGridScale,t_fGridSize.height*t_fGridScale);
    
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    Origin = ResolutionSize.origin;
    
    float t_fPosY = m_ptrGameLayer->getAnswersBg()->getBoundingBox().getMidY();
    m_Position = Point(Origin.x+visibleSize.width/2, t_fPosY);
}
///  calcutes Postion by size of the words array size
/// @param wordsArray  size of Words array
/// @param bonusWordsArray bonus words array
void AnswerGrid::createAnswerGrid(std::vector<std::string> wordsArray,std::vector<std::string> bonusWordsArray)
{
    
    int t_RewardIndex;
    if(wordsArray.size()>3)
    {
        m_bIsCoinReward = true;
        t_RewardIndex = GameController::getInstance()->getRandom(0,int(wordsArray.size()-1));
    }
    
    std::string t_word = "";
    std::string t_TempWord = "";
    
    for(int index=0; index<bonusWordsArray.size(); index++)
    {
        t_word = bonusWordsArray.at(index);
        t_TempWord = t_word;
        
        t_TempWord.erase(remove(t_TempWord.begin(), t_TempWord.end(), ','), t_TempWord.end());
        m_BonusWordsArray->push_back(t_TempWord);
    }
    
    Point t_Position = m_Position;
    
    float t_iWordCount = (float)wordsArray.size();
    t_iWordCount = t_iWordCount/2;
    t_Position.y += t_iWordCount*(t_fGridSize.height);
    
    if (t_Position.y>=m_ptrGameLayer->getAnswersBg()->getBoundingBox().getMaxY())
    {
        float t_iCount = (float)wordsArray.size();
        
        t_fGridSize = Size(111,115);
        
        float t_fSize = (m_ptrGameLayer->getAnswersBg()->getBoundingBox().size.height)/t_iCount;
        
        float scale = t_fSize/t_fGridSize.height;
        
        t_fGridScale = scale;
        t_fGridSize = Size((t_fGridSize.width)*t_fGridScale,(t_fGridSize.height)*t_fGridScale);
        
        t_Position = m_Position;
        
        t_Position.y += t_iWordCount*(t_fGridSize.height);
        
    }
    
    
    t_Position.y -= (t_fGridSize.height)/2;
    
    for(int index=0; index<wordsArray.size(); index++)
    {
        t_word = wordsArray.at(index);
        t_TempWord = t_word;
        
        t_TempWord.erase(remove(t_TempWord.begin(), t_TempWord.end(), ','), t_TempWord.end());
        m_WordsArray->push_back(t_TempWord);
        
        if(m_bIsCoinReward&&t_RewardIndex==index)
        {
            createGrid(t_word,t_Position,index,true);
        }
        else
        {
            createGrid(t_word,t_Position,index,false);
        }
        t_Position.y -= (t_fGridSize.height);
    }
}
///  Creates Answer word Grid
/// @param p_Word word
/// @param t_Position  position of answer word
/// @param t_iWordIndex Answer word obj index
/// @param m_bCoinReward  boolean value of coin reward wheather this particular word reaward contains
void AnswerGrid::createGrid(const std::string &p_Word,Point t_Position,int t_iWordIndex,bool m_bCoinReward)
{
    AnswerWord *t_Answord = new AnswerWord(m_ptrGameLayer,p_Word,t_Position,t_iWordIndex,t_fGridScale,m_bCoinReward);
    m_ptrAnswerWordArray->pushBack(t_Answord);
    t_Answord->release();
    t_Answord = NULL;
    
}
/// on Hint Button Pressed this will check for hint operation by checking empty  word tile
bool AnswerGrid::doHint()
{
    bool t_HintSuccessful = false;
    AnswerLetterTile *t_AnswerLetterTile = NULL;
    
    //First Check for empty tiles and assign correct clue tile
    for (int i = 0; i<m_ptrAnswerWordArray->size(); i++)
    {
        AnswerWord *t_Word = (AnswerWord*)m_ptrAnswerWordArray->at(i);
        
        if(!t_Word->m_bIsContain)
        {
            t_AnswerLetterTile = t_Word->GetEmptyTile();
            t_Word->m_bIsContain = true;
            if(t_AnswerLetterTile != NULL)
            {
                checkforGridsFill();
                t_AnswerLetterTile->onHintLetterVisible(i*0.0f,true);
                m_ptrGameLayer->getOptionGrid()->updateOptionGridsAndDictionary(t_AnswerLetterTile->m_StringLetter);
                checkForLevelComplete();
                t_HintSuccessful = true;
                break;
            }
        }
    }
    return t_HintSuccessful;
}
/// checks for Answer words grids fill
void AnswerGrid::checkforGridsFill()
{
    bool contain = false;
    
    AnswerLetterTile *t_AnswerLetterTile = NULL;
    
    for (int i = 0; i<m_ptrAnswerWordArray->size(); i++)
    {
        AnswerWord *t_Word = (AnswerWord*)m_ptrAnswerWordArray->at(i);
        
        if(t_Word->m_bIsContain)
        {
            contain = true;
        }
        else{
            contain = false;
            break;
        }
    }
    
    
    if(contain==true)
    {
        for (int i = 0; i<m_ptrAnswerWordArray->size(); i++)
        {
            AnswerWord *t_Word = (AnswerWord*)m_ptrAnswerWordArray->at(i);
            
            t_AnswerLetterTile = t_Word->GetEmptyTile();
            
            if(t_AnswerLetterTile!=NULL) {
                if(!t_AnswerLetterTile->HasContain())
                    t_Word->m_bIsContain = false;
            }
            
        }
    }
}
/// checks wheather this answered word is last one or not
void AnswerGrid::checkIsLastWord()
{
    t_iEmptyGrids = 0;
    for (int i = 0; i<m_ptrAnswerWordArray->size(); i++)
    {
        AnswerWord *t_Word = (AnswerWord*)m_ptrAnswerWordArray->at(i);
        
        if(!t_Word->m_bIsContain)
            t_iEmptyGrids++;
    }
}
/// Recieves Answered Letters Array Swiped by the user
/// @param ansStrArr array of letters
void AnswerGrid::recieveLetterFromOption(std::vector<std::string> ansStrArr)
{
    
    std::string ansStr = m_ptrGameLayer->getOptionGrid()->getCombinedString(ansStrArr);// "തവള‍‍";//
    int ansSize = (int)ansStrArr.size();
    
    bool t_bFound =  std::find(m_WordsArray->begin(), m_WordsArray->end(), ansStr.c_str()) != m_WordsArray->end();
    
    if(t_bFound)
    {
        size_t index = std::distance(m_WordsArray->begin(),
                                     std::find(m_WordsArray->begin(), m_WordsArray->end(), ansStr.c_str()));
        if(index<m_WordsArray->size())
        {
            AnswerWord *t_Word = (AnswerWord*)m_ptrAnswerWordArray->at(index);
            
            if (t_Word!=NULL)
            {
                if (!t_Word->HasContain())
                {
                    GameController::getInstance()->playSFX(SFX_CORRECT_WORD);
                    t_Word->m_bIsContain = true;// for hint
                    
                    m_iAttemptNum++;
                    t_Word->fillLettersOnCorrectAns();
                    m_ptrGameLayer->getOptionGrid()->validationAnimation(t_bFound);
                    
                    if(m_iAttemptNum>=2)
                        m_ptrGameLayer->getHudLayer()->checkForAttemptAnim(m_iAttemptNum);
                    
                    
                    AchievementManager::getInstance()->OnAchievementUpdate("words",1);
                    AchievementManager::getInstance()->CheckAllAchievementsComplete();
                    
                    DailyMissionManager::getInstance()->OnDailyMissionUpdate("words",1);
                    DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();
                }
                else
                {
                    GameController::getInstance()->playSFX(SFX_CORRECT_WORD);
                    m_ptrGameLayer->getOptionGrid()->validationAnimation(false);
                    t_Word->AnimateGridsonRepeat();
                    m_ptrGameLayer->getHudLayer()->createToast("alreadyFound");
                }
                
                checkForLevelComplete();
            }
        }
    }
    else
    {
        printf("\nnot found STRING\n");
        
        bool t_bBonusFound =  std::find(m_BonusWordsArray->begin(), m_BonusWordsArray->end(), ansStr.c_str()) != m_BonusWordsArray->end();
        
        if (t_bBonusFound)
        {
            
            Point pos = Point(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2);
            if (m_ptrGameLayer->getOptionGrid()->getAnswerText()!=NULL) {
                pos = m_ptrGameLayer->getOptionGrid()->getAnswerText()->getPosition();
            }
            
            bool t_bRepeat =  std::find(m_tempBonusArray->begin(), m_tempBonusArray->end(), ansStr.c_str()) != m_tempBonusArray->end();
            
            m_ptrGameLayer->getOptionGrid()->validationAnimation(true);
            if (!t_bRepeat)
            {
                
                GameController::getInstance()->playSFX(SFX_HINT_LETTER_BONUS);
                
                m_tempBonusArray->push_back(ansStr);
                m_ptrGameLayer->getHudLayer()->createToast("bonusToast");
                
                Label *bonusLbl = Label::createWithTTF(ansStr.c_str(),FONT_NAME, 35);
                
                if(me_Language.compare(LANG_ENGLISH)!=0)
                    bonusLbl = Label::createWithSystemFont(ansStr.c_str(),FONT_NAME, 35,Size(100,100));
                
                bonusLbl->setPosition(Vec2(pos));
                bonusLbl->setColor(Color3B::WHITE);
                m_ptrGameLayer->addChild(bonusLbl,1);
                
                JumpTo *_jump  = JumpTo :: create(0.7f,m_ptrGameLayer->getHudLayer()->getbonusButton()->getPosition(), 150.0f, 1);
                ScaleTo *_scale  = ScaleTo::create(.5f,0.2f,0.2f);
                ScaleTo *_scale2 = ScaleTo::create(.5f,0.5f ,0.5f);
                EaseElasticOut *_elastic =   EaseElasticOut::create(Sequence::create(_scale,_scale2, NULL));
                Spawn *_spwn = Spawn :: create(_jump,_elastic, NULL);
                RemoveSelf *remove = RemoveSelf::create();
                Sequence *_seq = Sequence::create(_spwn,remove,NULL);
                bonusLbl->runAction(_seq);
                
                m_ptrGameLayer->getHudLayer()->UpdateBonusWord();
            }
            else
            {
                GameController::getInstance()->playSFX(SFX_HINT_LETTER_BONUS);
                m_ptrGameLayer->getHudLayer()->createToast("bonusToast_1");
            }
        }
        else
        {
            m_ptrGameLayer->getOptionGrid()->validationAnimation(false);
            if(ansSize==1)
            {
                m_ptrGameLayer->getHudLayer()->createToast("swipeToast");
            }
            else
            {
                m_ptrGameLayer->getHudLayer()->onWrongAnswer(ansStr);
                GameController::getInstance()->playSFX(SFX_WRONG_WORD);
                m_ptrGameLayer->getHudLayer()->createToast("wrongToast");
                m_iAttemptNum = 0;
                m_bAttempt = false;
            }
        }
    }
}
/// checks for all the answer word's grids filled or not to complete level
void AnswerGrid::checkForLevelComplete()
{
    bool t_bFill = didAllGridsFill();
    
    if (t_bFill)
    {
        if(m_bAttempt)
        {
            AchievementManager::getInstance()->OnAchievementUpdate("misspell",1);
            AchievementManager::getInstance()->CheckAllAchievementsComplete();
            
            DailyMissionManager::getInstance()->OnDailyMissionUpdate("misspell",1);
            DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();
            
        }
        
        DelayTime *delay = DelayTime::create(1.0f);
        CallFunc *callfunc = CallFunc::create([&,this]() {
            m_ptrGameLayer->getHudLayer()->onLevelCompleted();
        });
        m_ptrGameLayer->runAction(Sequence::create(delay,callfunc, NULL));
        
    }
}
/// RETURN ANSWER GRID IS EMPTY OR NOT
bool AnswerGrid::didAllGridsFill()
{
    bool t_bHasContain = false;
    for (int i = 0; i<m_ptrAnswerWordArray->size(); i++)
    {
        AnswerWord *t_Word = (AnswerWord*)m_ptrAnswerWordArray->at(i);
        if(t_Word->HasContain())
        {
            t_bHasContain = true;
        }
        else
        {
            t_bHasContain = false;
            break;
        }
    }
    return t_bHasContain;
}
/// returns Answerword's objects array
Vector<AnswerWord*> *AnswerGrid::getAnswerWordArray()
{
    return m_ptrAnswerWordArray;
    
}
AnswerGrid::~AnswerGrid()
{
    
    if(m_ptrAnswerWordArray != NULL)
    {
        m_ptrAnswerWordArray->clear();
        delete m_ptrAnswerWordArray;
        m_ptrAnswerWordArray = NULL;
    }
    
    if(m_WordsArray != NULL)
    {
        m_WordsArray->clear();
        delete m_WordsArray;
        m_WordsArray = NULL;
    }
    
    if(m_tempBonusArray != NULL)
    {
        m_tempBonusArray->clear();
        delete m_tempBonusArray;
        m_tempBonusArray = NULL;
    }
    
    
    if(m_BonusWordsArray != NULL)
    {
        m_BonusWordsArray->clear();
        delete m_BonusWordsArray;
        m_BonusWordsArray = NULL;
    }
    
}
