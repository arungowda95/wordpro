//
//  PopUpManager.hpp
//  wordPro-mobile
//
//  Created by id on 09/03/20.
//

#ifndef PopUpManager_h
#define PopUpManager_h

#include <stdio.h>
#include "cocos2d.h"
#include "PopUp.h"

using namespace cocos2d;

enum eScreenType
{
    SCREEN_NONE,
    SCREEN_MENU,
    SCREEN_LEVEL,
    SCREEN_GAME,
};

class HudLayer;
class MenuLayer;
class LevelLayer;
class PopUpManager :public Node
{
 
    HudLayer     *m_ptrHudLayer;
    MenuLayer    *m_ptrMenuLayer;
    LevelLayer   *m_ptrLevelLayer;
    PopUp        *m_PtrPopUp;
    Sprite       *m_SprBgAlpha;
    eScreenType   m_eScreenType;
    PopUpType     m_ePopUpType;
    float         m_fOpacity;
    
    Size          visibleSize;
    Vec2          origin;
    
    bool          m_bPopUpExist;
    
public:
    
    // CONSTRUCTOR AND DESTRUCTOR
    PopUpManager(Layer *p_Layer, eScreenType p_eScreenType);
    ~PopUpManager();
    
    // THIS ADD AND REMOVE ALPHA BACKGROUND FROM POPUP
    void AddAlphaBg();
    void RemoveAlphaBg();
    
    /// THIS LOAD POPUP WITH TYPE
    void LoadPopUp(PopUpType p_ePopUpType);
    
    /// POPUP TRANSITION
    void PopUpTransitionAnimation(PopUpTransitionType p_ePopUpTransitionType);
    
    /// RETURN POPUP TYPE
    PopUpType getPopUpType();
    
    /// RETURN POP UP
    PopUp * getPopUp();
    
    void popUpCallback();
    
    void UnLoadPopUp();
    
    /// THIS LOAD OTHER POP UP
    void LoadOtherPopUp(const std::string &p_String);
    
    /// THIS CHECK FOR UPDATE COIN EVENT CREATION
    void CheckWithUpdateCoinEvent();
    
    /// THIS ON KEY BACK BUTTON PRESSED
    void OnKeyButtonPressed();
};

#endif /* PopUpManager_hpp */
