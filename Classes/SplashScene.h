//
//  SplashScene.hpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#ifndef SplashScene_h
#define SplashScene_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class SplashLayer;
class SplashScene:public Scene
{
    
    SplashLayer *m_ptrSplashLayer;
public:
    SplashScene();
    ~SplashScene();
};


class SplashLayer:public Layer
{
    
    Sprite *m_SprSplashBg;
public:
    /// CONSTRUCTOR AND DESTRUCTOR
    SplashLayer();
    ~SplashLayer();
    
    /// THIS LOAD MENU SCENE
    void loadMenuScene();
    
    /// THIS LOAD ALL GAME ASSTES
    void loadGameAssets();
    
    /// checks Wheather is connected to facebook or if apple it will check for apple id connected
    bool IsUserConnected();
    
};

#endif /* SplashScene_hpp */
