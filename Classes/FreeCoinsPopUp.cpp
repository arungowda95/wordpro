//
//  FreeCoinsPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 23/03/20.
//

#include "FreeCoinsPopUp.h"


FreeCoinsPopUp::FreeCoinsPopUp(std::function<void()> func)
{
    
    m_fnSelector = func;
    t_miWatchBtn = NULL;
    t_miCloseButton = NULL;
    m_bvideoAdSuccess = false;
    
    Rect Res = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(Res.size.width,Res.size.height);
    origin = Res.origin;
           
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);

    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY());
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);

    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleY()+0.15);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);

    std::string t_Label = "FreeCoins";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-64);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+50);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMidY()));
    this->addChild(t_tLabel, 2);

    m_PopUpRect = m_SprPopUpBg->getBoundingBox();
    
    if(me_Language.compare(LANG_TAMIL)==0||me_Language.compare(LANG_MALAYALAM)==0)
    {
        t_tLabel->setHeight(0);
        t_tLabel->setSystemFontSize(40);
        t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+20);
    }
    
    std::string t_SubText = "VideoMsg";
    Label *t_tSubLabel = createLabelBasedOnLang(t_SubText,50);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-10-t_tSubLabel->getBoundingBox().size.height/2));
    t_tSubLabel->setColor(Color3B::WHITE);
    this->addChild(t_tSubLabel,2);
    
    if(me_Language.compare(LANG_BANGLA)==0||me_Language.compare(LANG_TAMIL)==0)
    {
        t_tSubLabel->setHeight(0);
        t_tSubLabel->setSystemFontSize(40);
        t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+10);
        t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-t_tSubLabel->getBoundingBox().size.height/2));
    }
    
    Sprite *sprBg = Sprite::create("large_box.png");
    sprBg->setScaleY(sprBg->getScaleY()+0.4);
    sprBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tSubLabel->getBoundingBox().getMinY()-10-sprBg->getBoundingBox().size.height/2));
    this->addChild(sprBg,2);
    
    
    Sprite *sprCoins = Sprite::create("popup-coins-images.png");
    sprCoins->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMidY()+30));
    this->addChild(sprCoins,2);
    
    Posy = sprBg->getBoundingBox().getMidY()+30;
    
       Sprite *sprVideoCoin = Sprite::create("video-image.png");
       sprVideoCoin->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX()+sprVideoCoin->getBoundingBox().size.width/2,sprBg->getBoundingBox().getMidY()-sprVideoCoin->getBoundingBox().size.width/2));
       this->addChild(sprVideoCoin,2);
    
    
    std::string t_Coin = "X 20";
    Label *t_tCoinText = Label::createWithTTF(t_Coin,FONT_NAME,60,Size(0,0));
    t_tCoinText->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCoinText->setPosition(Vec2(sprCoins->getBoundingBox().getMidX()-t_tCoinText->getBoundingBox().size.width/2,sprCoins->getBoundingBox().getMinY()+t_tCoinText->getBoundingBox().size.height/2));
    t_tCoinText->setColor(Color3B::WHITE);
    t_tCoinText->enableOutline(Color4B(206,85,44,255),2);
    t_tCoinText->setHeight(t_tCoinText->getBoundingBox().size.height);
    this->addChild(t_tCoinText, 2);
    
    
    // WATCH BUTTTON
    t_miWatchBtn = getButtonMade("large_button_1.png","large_button_1.png",CC_CALLBACK_1(FreeCoinsPopUp::watchVideoButtonPressed,this));
    t_miWatchBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMinY()-10-t_miWatchBtn->getBoundingBox().size.height/2));
    t_miWatchBtn->setTag(0);

     
    if(!GameController::getInstance()->isVideoAdsAvailable())
    {
        t_miWatchBtn->setEnabled(false);
        t_miWatchBtn->setOpacity(150);
    }
    
    
       std::string t_watch = "Watch";
       Label *t_tWatchText = createLabelBasedOnLang(t_watch, 55);
       t_tWatchText->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
     t_tWatchText->setPosition(Vec2(t_miWatchBtn->getBoundingBox().size.width/2,t_miWatchBtn->getBoundingBox().size.height/2+12.0f));
       t_tWatchText->setColor(Color3B::WHITE);
       t_tWatchText->enableOutline(Color4B(25,121,15,255),2);
       t_tWatchText->setHeight(t_tWatchText->getBoundingBox().size.height+10);
       t_miWatchBtn->addChild(t_tWatchText, 2);

       // CLOSE BUTTON
       t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(FreeCoinsPopUp::OnButtonPressed,this));
       t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
       t_miCloseButton->setTag(0);

       Menu *m_Menu = Menu::create(t_miCloseButton,t_miWatchBtn,NULL);
       m_Menu->setPosition(Vec2::ZERO);
       this->addChild(m_Menu,2);

       registerVideoEvent();
}
void FreeCoinsPopUp::registerVideoEvent()
{
    auto t_VideoAdAvailble = EventListenerCustom::create(EVENT_VIDEO_ADS_AVAILABLE,CC_CALLBACK_0(FreeCoinsPopUp::videoAdsAvailble,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdAvailble, this);
    
    auto t_VideoAdSuccess = EventListenerCustom::create(EVENT_VIDEO_ADS_SUCCEED, CC_CALLBACK_0(FreeCoinsPopUp::onVideoAdSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdSuccess, this);
    
    auto t_VideoAdFailure = EventListenerCustom::create(EVENT_VIDEO_ADS_FAILED, CC_CALLBACK_0(FreeCoinsPopUp::onVideoAdFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdFailure, this);

}
void FreeCoinsPopUp::OnButtonPressed(Ref *p_Sender)
{
   MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
   int pButtonTag =  Button->getTag();
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
     switch (pButtonTag)
     {
         case 0:
         {
             status = (char*)"close";
         }break;
          default:
       break;
     }
   
   if (m_fnSelector!=NULL) {
       m_fnSelector();
   }

}
void FreeCoinsPopUp::watchVideoButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameController::getInstance()->showVideoAd();
}
void FreeCoinsPopUp::onVideoAdSuccess()
{
    int coincount = 20;
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);
    UserDefault::getInstance()->setIntegerForKey(COINS,UserDefault::getInstance()->getIntegerForKey(COINS)+coincount);
    
    me_iCoinCount = UserDefault::getInstance()->getIntegerForKey(COINS);
    
    log("onVideoAdSuccess->%d",me_iCoinCount);
    
    UserDefault::getInstance()->flush();
    
    m_bvideoAdSuccess = true;
    if(!GameController::getInstance()->isVideoAdsAvailable())
    {
        t_miWatchBtn->setEnabled(false);
        t_miWatchBtn->setOpacity(150);
    }
    FiniteTimeAction *_call2 = CallFuncN::create(CC_CALLBACK_0(FreeCoinsPopUp::coinAddAnimation, this));
    DelayTime *_delay = DelayTime::create(1.8f);
    FiniteTimeAction *_call3 = CallFuncN::create(CC_CALLBACK_0(FreeCoinsPopUp::coinAnimationDoneClosePopUp, this));
    FiniteTimeAction *_seq  = Sequence::create(_call2,_delay,_call3, NULL);
    this->runAction(_seq);

}
void FreeCoinsPopUp::onVideoAdFailure()
{
    log("onVideoAdFailure");
//    registerVideoEvent();
    if(!GameController::getInstance()->isVideoAdsAvailable())
    {
        t_miWatchBtn->setEnabled(false);
        t_miWatchBtn->setOpacity(150);
    }

}
void FreeCoinsPopUp::videoAdsAvailble()
{
    log("videoAdsAvailble");
    if (t_miWatchBtn!=NULL&&!m_bvideoAdSuccess)
    {
        t_miWatchBtn->setEnabled(true);
        t_miWatchBtn->setOpacity(255);
    }
}
MenuItemSprite *FreeCoinsPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
      Sprite *_normalSpr = Sprite::create(normalSpr);
      
      Sprite *_SelectedSpr = Sprite::create(selSpr);
      _SelectedSpr->setOpacity(150);
      
      MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
      return Button;
}
void FreeCoinsPopUp::coinAddAnimation()
{
 
    this->runAction(CallFunc::create([&,this]()
    {
        Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
        Point _startPosOppo = Point(m_SprPopUpBg->getBoundingBox().getMidX(),Posy);
            for (int i=0; i< 20; i++)
            {
                Sprite *_spr = Sprite::create("coins.png");
                _spr->setScale(_spr->getScale()+0.2);
                _spr->setPosition(_startPosOppo);
                _spr->setOpacity(0);
                ccBezierConfig bezier;
                bezier.controlPoint_1 = _startPosOppo;
                bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
                bezier.endPosition =_endPos;
                BezierTo *_bez = BezierTo::create(.5f, bezier);
                this->addChild(_spr,5);
                FadeTo *_fade = FadeTo::create(.04*i,255);
                FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(FreeCoinsPopUp::removeSprite,this));
                FiniteTimeAction*_seq = Sequence::create(_fade,_bez,_call,NULL);
                _spr->runAction(_seq);
            }
            if(t_miCloseButton)
            {
                t_miCloseButton->setEnabled(false);
            }
     }));

}
void FreeCoinsPopUp::removeSprite(Ref *_sender)
{
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);

    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
    
    
    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    
}
void FreeCoinsPopUp::coinAnimationDoneClosePopUp()
{
    status = (char*)"close";
    if(m_fnSelector!=NULL) {
        m_fnSelector();
    }
    
    if(t_miCloseButton){
        t_miCloseButton->setEnabled(true);
    }
    GameController::getInstance()->showInterstial();
}
Label *FreeCoinsPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

FreeCoinsPopUp::~FreeCoinsPopUp()
{
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_AVAILABLE);
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_SUCCEED);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_FAILED);

    this->removeAllChildrenWithCleanup(true);
}
