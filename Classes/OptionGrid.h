//
//  OptionGrid.hpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#ifndef OptionGrid_h
#define OptionGrid_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class OptionLetterTile;
class OptionGrid:public Ref
{
    GameLayer               *m_ptrGameLayer;
    std::vector<Point>       m_PositionArray;
    Size visibleSize;
    Vec2 Origin;
    Vector<OptionLetterTile*> *m_OptionLetterTileArray;
    Vector<OptionLetterTile*> *m_OptionArrayForBack;
    
    DrawNode *m_fDrawNode;
    DrawNode *m_fTempNode;
    int mb_touched;
    Point m_pStartPos,m_pEndPos,m_prevPos;
    Color4F m_color;
    Vector<DrawNode *> *nodeArray;
    
    bool m_bTouchBegan,m_bTouchEnd;
    
    CC_SYNTHESIZE(Sprite*, optBg, OptionsBg);
    CC_SYNTHESIZE(Label*, m_AnswerText, AnswerText);
    
    CC_SYNTHESIZE(Sprite*, m_AnswerSpr, AnswerSpr);
    
    std::map<std::string,int> m_DictLetterCount;
    
    std::string m_strAnswer;
    
    bool m_bRevealed;
    
    Vector<Sprite*> *m_sprArr;
    
    float m_fGridScale;
    
    int order;
    
    std::string m_strLevelWord;
    
    std::vector<std::string> m_strLetterArray;
    std::vector<Sprite*> lineSprArr;
    
    Label *wordslbl;
    
public:
    Sprite *sprHand;
    Sprite *sprLine;
    
    /// CONSTRUCTOR AND DESTRUCTOR
    OptionGrid(GameLayer *p_ptrGameLayer);
    ~OptionGrid();
    
    /// creates the position for letters based on number of letters
    /// @param noOfLeters  count of number of letters
    void createPositionsForLetters(int noOfLeters);
    
    /// Creates Option letters
    /// @param p_Letters Letters array
    void createOptionLetterTiles(const std::vector<std::string> &p_Letters,std::map<std::string ,int> dictLetterCount);
    
    void onTouchBeganOptionGrid(Point p_Location);
    
    void onTouchMoveOptionGrid(Point p_Location);
    
    void OnTouchEnded(Point p_Location);
    
    /// LINE CREATION TO DRAG
    /// @param startPos Start position
    /// @param endPos end position
    void drawLineOnDrag(Point startPos,Point endPos);
    
    /// Draws line on letter reach from one letter grid to another
    /// @param start start tile position
    /// @param end  end tile postion
    void drawingLineonLetterReach(Point start,Point end);
    
    /// erase line on reverse swipe
    void eraseLine();
    
    /// on touch end of grids
    void onTouchEndOnGrids();
    
    /// this creates answer  Text label to display when user swipes letters
    void createAnswerTextLabel();
    
    /// here we will recieve clicked String from option letter tile
    /// @param str  string from tile
    void getClickedString(std::string str);
    
    /// clears last string when user swipes back touch on option tiles
    void clearLastStringonTouchBack();
    
    /// ON SHUFFLING OPTION LETTERS
    void shuffleOptionLetters();
    
    /// checks for validation when user releases touch from option grids
    void checkForValidation();
    
    /// display's number of strings to be used in level
    void showCountofLetters();
    
    ///  this is used to show animation on correct and wrong word
    /// @param isValid  boolean value of  correct or wrong
    void validationAnimation(bool isValid);
    
    /// returns map of  letters count
    std::map<std::string,int> getDictionaryLetter();
    
    ///  updates dictionary of letter count after correct answer found to update letter count label
    /// @param foundStr  founded string
    void updateOptionGridsAndDictionary(std::string foundStr);
    
    /// removes Touch effect sprite on touch end
    void removeEffectSpriteOnEnd();
    
    /// returns option letter tile arrray
    Vector<OptionLetterTile*> *getOptionTileArray();
    
    /// sets the z order of tiles for tutorial
    void setLocalZorderForTutorial();
    
    /// checks for letter touch tutorial
    void checkforTutorialRemove();
    
    /// adding Hand animation for tutorial from start point and end point letters
    /// @param origin  strt letter position
    /// @param destination destination  letter position
    void addHandAnimation(Point origin,Point destination);
    
    /// remove hand animation after user drags letters
    void removeHandAnim();
    
    /// set's hand position on swiping animation
    /// @param pos  position of hand
    void setHandPosition(Point pos);
    
    ///  gets the combiled string of swiped letters
    /// @param str  array of swiped strings array
    std::string getCombinedString(std::vector<std::string> str);
    
    /// adds the tutorial for 3 letters
    void addThreelettersTutorial();
    
    /// fades out dotted line sprite
    void fadeoutLines();
};


#endif /* OptionGrid_hpp */
