//
//  LevelCompletePopUp.cpp
//  wordPro-mobile
//
//  Created by id on 16/03/20.
//

#include "LevelCompletePopUp.h"
LevelCompletePopUp::LevelCompletePopUp(std::function<void()> func)
{
    m_fnSelector = func;
    
    ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;

    m_bPlayedAgain = UserDefault::getInstance()->getBoolForKey("PlayedAgain");
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    int levelcount = UserDefault::getInstance()->getIntegerForKey("level");

    if(levelcount>10)
    {
        if(levelcount%2==0)
        {
            me_bShowAds = true;
        }
    }//9986688632
    m_GiftNode  =  NULL;
    m_gamesNode = NULL;
    m_tCoins = 20;
    m_tCoinCount = NULL;
    t_NextButton = NULL;
    t_miCloseButton = NULL;
    t_DoubleCoinButton = NULL;
    m_bVideoSuccess = false;
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.1f);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,(origin.y+visibleSize.height/2)+30)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    
    m_GiftNode = Node::create();
    m_GiftNode->setPosition(Vec2::ZERO);
    this->addChild(m_GiftNode,3);
    
    std::string t_Label = "Complete";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-70);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+10);
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(t_tLabel, 2);
    
    float scalex = (m_SprPopUpBg->getBoundingBox().size.width-100)/70;
    m_SprLocation = Sprite::create("pixel.png");
    m_SprLocation->setScale(scalex,m_SprLocation->getScaleY()+0.05);
    m_SprLocation->setColor(Color3B(95,154,27));
    m_SprLocation->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getPositionY()-15-m_SprLocation->getBoundingBox().size.height));
    this->addChild(m_SprLocation,1);
    
    std::string Currloc = StatsManager::getInstance()->getCurrentState();
    std::string CurrCountry = StatsManager::getInstance()->getCurrentCountry();

    if(CurrCountry!="India")
    {
        Currloc = CurrCountry;
    }
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        std::transform(Currloc.begin(), Currloc.end(), Currloc.begin(), ::toupper);
        m_LocationLabel = Label::createWithTTF(Currloc,FONT_NAME,45,Size(0,100));
    }
    else
    {
        if(CurrCountry!="India")
        {
           Currloc = LanguageTranslator::getInstance()->getTranslatorStringWithTag(Currloc);
        }else{
           Currloc = UserDefault::getInstance()->getStringForKey("CURR_STATE_TRANSLATE","India");
        }
        m_LocationLabel = Label::createWithSystemFont(Currloc,SYS_FONT_NAME,45,Size(m_SprLocation->getBoundingBox().size.width-20,150));
    }
    
    m_LocationLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    m_LocationLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprLocation->getPositionY()));
    m_LocationLabel->setColor(Color3B::WHITE);
    m_LocationLabel->enableOutline(Color4B(24,78,16,255),4);
    this->addChild(m_LocationLabel, 2);

    
    Sprite *sprBg1 = Sprite::create("small_box.png");
    sprBg1->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprLocation->getBoundingBox().getMinY()-25-sprBg1->getBoundingBox().size.height/2));
    this->addChild(sprBg1,2);
    
    sprBg2 = Sprite::create("large_box.png");
    sprBg2->setScaleY(sprBg2->getScaleY()-0.05);
    sprBg2->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg1->getBoundingBox().getMinY()-5-sprBg2->getBoundingBox().size.height/2));
    this->addChild(sprBg2,2);
    
    std::string t_Coin = "+ 25";
     t_tCoinText = Label::createWithTTF(t_Coin,FONT_NAME,50,Size(0,0));
    t_tCoinText->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCoinText->setPosition(Vec2(sprBg2->getBoundingBox().getMidX(),sprBg2->getBoundingBox().getMinY()+20+t_tCoinText->getBoundingBox().size.height/2));
    t_tCoinText->setColor(Color3B::WHITE);
    t_tCoinText->enableOutline(Color4B(206,85,44,255),2);
    t_tCoinText->setHeight(t_tCoinText->getBoundingBox().size.height);
    t_tCoinText->setVisible(false);
    this->addChild(t_tCoinText, 4);

    sprCoin = Sprite::create("big-coins.png");
    sprCoin->setVisible(false);
    sprCoin->setScale(0.7f);
    sprCoin->setPosition(Vec2(t_tCoinText->getBoundingBox().getMaxX()+10+sprCoin->getBoundingBox().size.width/2,t_tCoinText->getPositionY()));
    this->addChild(sprCoin,3);
    
    createLocationBar(sprBg1);
    
  
    
    t_NextButton = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_0(LevelCompletePopUp::NextButtonCallBack,this));
    t_NextButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX()+10+t_NextButton->getBoundingBox().size.width/2,10+m_SprPopUpBg->getBoundingBox().getMinY()+t_NextButton->getBoundingBox().size.height));
    t_NextButton->setTag(1);
    
    Label *NxtLabel = createLabelBasedOnLang("Next", 40);
    NxtLabel->setColor(Color3B::WHITE);
    NxtLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    NxtLabel->enableOutline(Color4B(53,126,32,255),2);
    NxtLabel->setWidth(t_NextButton->getContentSize().width-20);
    NxtLabel->setHeight(NxtLabel->getContentSize().height+10);
    NxtLabel->setPosition(Vec2(t_NextButton->getBoundingBox().size.width/2,t_NextButton->getBoundingBox().size.height/2+15));
    t_NextButton->addChild(NxtLabel,1);
    
    
    t_DoubleCoinButton = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_0(LevelCompletePopUp::doubleCoinBtnClick,this));
    t_DoubleCoinButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX()-10-t_DoubleCoinButton->getBoundingBox().size.width/2,10+m_SprPopUpBg->getBoundingBox().getMinY()+t_DoubleCoinButton->getBoundingBox().size.height));
    t_DoubleCoinButton->setTag(1);
    
    Label *dbCoinLabel = createLabelBasedOnLang("DoubleCoin",30);
    dbCoinLabel->setColor(Color3B::WHITE);
    dbCoinLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    dbCoinLabel->enableOutline(Color4B(53,126,32,255),2);
    dbCoinLabel->setWidth(t_DoubleCoinButton->getContentSize().width-50);
    dbCoinLabel->setHeight(dbCoinLabel->getBoundingBox().size.height+10);
    dbCoinLabel->setPosition(Vec2(t_DoubleCoinButton->getBoundingBox().size.width/2,t_DoubleCoinButton->getBoundingBox().size.height/2+10));
    t_DoubleCoinButton->addChild(dbCoinLabel,1);
    
    if(!GameController::getInstance()->isVideoAdsAvailable())
     {
         t_DoubleCoinButton->setEnabled(false);
     }
    
    if(m_bPlayedAgain)
    {
        t_DoubleCoinButton->setVisible(false);
        t_NextButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),10+m_SprPopUpBg->getBoundingBox().getMinY()+t_NextButton->getBoundingBox().size.height));

    }
    
    // CLOSE BUTTON
    t_miCloseButton = getButtonMade("close_button.png","close_button.png",CC_CALLBACK_0(LevelCompletePopUp::NextButtonCallBack,this));
    t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
    t_miCloseButton->setTag(0);

    m_menu = Menu::create(t_miCloseButton,t_NextButton,t_DoubleCoinButton,NULL);
    m_menu->setPosition(Vec2::ZERO);
    this->addChild(m_menu,2);
    
    createCoinsCollection(Point(sprBg2->getBoundingBox().getMinX()+20,sprBg2->getBoundingBox().getMaxY()),sprBg2);

      if(!m_bPlayedAgain)
      {
          createProgressBar(Point(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg2->getBoundingBox().getMinY()));
      }
    
    registerVideoEvent();
    
    createMoregamesonLevelComplete();
    
}
void LevelCompletePopUp::registerVideoEvent()
{
    auto t_VideoAdAvailble = EventListenerCustom::create(EVENT_VIDEO_ADS_AVAILABLE,CC_CALLBACK_0(LevelCompletePopUp::videoAdsAvailble,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdAvailble, this);
    
    auto t_VideoAdSuccess = EventListenerCustom::create(EVENT_VIDEO_ADS_SUCCEED, CC_CALLBACK_0(LevelCompletePopUp::onVideoAdSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdSuccess, this);
    
    auto t_VideoAdFailure = EventListenerCustom::create(EVENT_VIDEO_ADS_FAILED, CC_CALLBACK_0(LevelCompletePopUp::onVideoAdFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdFailure, this);

}
void LevelCompletePopUp::createLocationBar(Sprite*sprBar1)
{
    Point Pos = Point(sprBar1->getBoundingBox().getMinX(),sprBar1->getBoundingBox().getMidY());
    
    
    
    std::string prevloc = StatsManager::getInstance()->getPrevLocation();
    std::string Currloc = StatsManager::getInstance()->getCurLocation();
    std::string nextLoc = StatsManager::getInstance()->getNxtLocation();
    
    m_PrevLocationSpr = Sprite::create("location-image.png");    m_PrevLocationSpr->setPosition(Vec2(Pos.x+30+m_PrevLocationSpr->getBoundingBox().size.width/2,Pos.y+m_PrevLocationSpr->getBoundingBox().size.height*0.3));
    this->addChild(m_PrevLocationSpr,2);
    
    std::string loc1 = prevloc;
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        std::transform(loc1.begin(),loc1.end(),loc1.begin(),::toupper);
        m_PrevLocationLbl = Label::createWithTTF(loc1,FONT_NAME,17,Size(0,100));
    }else{
        m_PrevLocationLbl = Label::createWithSystemFont(loc1,SYS_FONT_NAME,17);
        m_PrevLocationLbl->setWidth(m_PrevLocationLbl->getBoundingBox().size.width+10);
        m_PrevLocationLbl->setHeight(100);
    }
    m_PrevLocationLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    m_PrevLocationLbl->setColor(Color3B(250,77,30));
    m_PrevLocationLbl->enableOutline(Color4B(77,34,15,255),2);//77 34 ,15
    m_PrevLocationLbl->setPosition(Vec2(m_PrevLocationSpr->getContentSize().width/2,-5));
    m_PrevLocationSpr->addChild(m_PrevLocationLbl,2);

    printf("width-->%f\n",m_PrevLocationLbl->getBoundingBox().size.width);
    
    if(m_PrevLocationLbl->getBoundingBox().size.width>130)
    {
        m_PrevLocationLbl->setWidth(130);
    }
    
    Sprite *arrow1 = Sprite::create("arrow-image.png");
    arrow1->setPosition(Vec2(m_PrevLocationSpr->getBoundingBox().getMaxX()+arrow1->getBoundingBox().size.width/2,Pos.y+arrow1->getBoundingBox().size.height*0.3));
    this->addChild(arrow1,2);
    
    
    if(prevloc.empty())
    {
        m_PrevLocationSpr->setVisible(false);
        m_PrevLocationLbl->setVisible(false);
        arrow1->setVisible(false);
    }
    
    
    
    m_CurLocationSpr = Sprite::create("location-image.png");    m_CurLocationSpr->setPosition(Vec2(sprBar1->getBoundingBox().getMidX(),Pos.y+m_CurLocationSpr->getBoundingBox().size.height*0.3));
      this->addChild(m_CurLocationSpr,3);
      
      std::string loc2 = Currloc;
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        std::transform(loc2.begin(), loc2.end(), loc2.begin(), ::toupper);
        m_CurLocationLbl = Label::createWithTTF(loc2,FONT_NAME,25,Size(0,100));
    }else{
        m_CurLocationLbl = Label::createWithSystemFont(loc2,SYS_FONT_NAME,25);
        m_CurLocationLbl->setWidth(m_CurLocationLbl->getBoundingBox().size.width+10);
        m_CurLocationLbl->setHeight(100);
    }
      m_CurLocationLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
      m_CurLocationLbl->setColor(Color3B::WHITE);
      m_CurLocationLbl->enableOutline(Color4B(77,34,15,255),2);//77 34 ,15
      m_CurLocationLbl->setPosition(Vec2(m_CurLocationSpr->getContentSize().width/2,-10));
      m_CurLocationSpr->addChild(m_CurLocationLbl,2);

      printf("width-->%f\n",m_CurLocationLbl->getBoundingBox().size.width);
      
      if(m_CurLocationLbl->getBoundingBox().size.width>220)
      {
          m_CurLocationLbl->setWidth(250);
      }
    
    Sprite *arrow2 = Sprite::create("arrow-image.png");
    arrow2->setPosition(Vec2(m_CurLocationSpr->getBoundingBox().getMaxX()+arrow2->getBoundingBox().size.width*0.75,Pos.y+arrow2->getBoundingBox().size.height*0.3));
    this->addChild(arrow2,2);

    
      Pos = Point(sprBar1->getBoundingBox().getMaxX(),sprBar1->getBoundingBox().getMidY());

    m_NxtLocationSpr = Sprite::create("location-image.png");    m_NxtLocationSpr->setPosition(Vec2(Pos.x-30-m_NxtLocationSpr->getBoundingBox().size.width/2,Pos.y+m_NxtLocationSpr->getBoundingBox().size.height*0.3));
      this->addChild(m_NxtLocationSpr,2);
      
      std::string loc3 = nextLoc;
        if(me_Language.compare(LANG_ENGLISH)==0)
        {
            std::transform(loc3.begin(), loc3.end(), loc3.begin(), ::toupper);
            m_NxtLocationLbl = Label::createWithTTF(loc3,FONT_NAME,17,Size(0,100));
        }
        else
        {
            m_NxtLocationLbl = Label::createWithSystemFont(loc3,SYS_FONT_NAME,17);
            m_NxtLocationLbl->setWidth(m_NxtLocationLbl->getBoundingBox().size.width+10);
            m_NxtLocationLbl->setHeight(100);
        }
      m_NxtLocationLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
      m_NxtLocationLbl->setColor(Color3B(250,77,30));
      m_NxtLocationLbl->enableOutline(Color4B(77,34,15,255),2);//77 34 ,15
      m_NxtLocationLbl->setPosition(Vec2(m_NxtLocationSpr->getContentSize().width/2,-5));
      m_NxtLocationSpr->addChild(m_NxtLocationLbl,2);

      printf("width-->%f\n",m_NxtLocationLbl->getBoundingBox().size.width);
      
      if(m_NxtLocationLbl->getBoundingBox().size.width>130)
      {
          m_NxtLocationLbl->setWidth(130);
      }
    
    if(nextLoc.empty())
    {
        m_NxtLocationSpr->setVisible(false);
        m_NxtLocationLbl->setVisible(false);
        arrow2->setVisible(false);
    }

    if (m_bPlayedAgain)
    {
        m_PrevLocationSpr->setVisible(false);
        m_PrevLocationLbl->setVisible(false);
        arrow1->setVisible(false);
        
        m_NxtLocationSpr->setVisible(false);
        m_NxtLocationLbl->setVisible(false);
        arrow2->setVisible(false);

    }

}
void LevelCompletePopUp::createCoinsCollection(Point Pos,Sprite *SprBg2)
{
    
    Label *coinCollectLbl = createLabelBasedOnLang("CoinsCollected",45);
    coinCollectLbl->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
    coinCollectLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    coinCollectLbl->setColor(Color3B::WHITE);
    coinCollectLbl->enableOutline(Color4B(75,34,16,255),2);
    coinCollectLbl->setWidth(SprBg2->getBoundingBox().size.width-30-120);
    coinCollectLbl->setHeight(coinCollectLbl->getBoundingBox().size.height+10);
    coinCollectLbl->setPosition(Vec2(Pos.x,Pos.y-20-coinCollectLbl->getBoundingBox().size.height/2));
    m_GiftNode->addChild(coinCollectLbl,2);

    float xpos = SprBg2->getBoundingBox().getMaxX()-30;
    
    coinSpr = Sprite::create("big-coins.png");
    coinSpr->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    coinSpr->setPosition(Vec2(xpos,coinCollectLbl->getPositionY()-5));
    m_GiftNode->addChild(coinSpr,2);
    
    float offx = 30.0f;
    if (me_Language.compare(LANG_GUJARATI)==0)
    {
        offx = 80.0f;
    }
    coinCollectLbl->setAlignment(TextHAlignment::RIGHT,TextVAlignment::CENTER);
    coinCollectLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    coinCollectLbl->setPositionX(coinSpr->getBoundingBox().getMinX()-offx);
    
    Sprite *coincountBg = Sprite::create("circle.png");
    coincountBg->setScale(coincountBg->getScale()-0.25);
    coincountBg->setColor(Color3B::RED);
    coincountBg->setPosition(Vec2(coinSpr->getContentSize().width-25,coinSpr->getContentSize().height-10));
    coinSpr->addChild(coincountBg,1);
    
    m_tCoins = 5;
    m_tCoinCount = Label::createWithTTF(StringUtils::format("%d",m_tCoins), FONT_NAME, 30);
    m_tCoinCount->setPosition(Vec2(coincountBg->getContentSize().width/2,coincountBg->getContentSize().height/2));
    m_tCoinCount->setColor(Color3B::WHITE);
    coincountBg->addChild(m_tCoinCount,1);
    
    
    Sprite *coinShadow = Sprite::create("shadow.png");
    coinShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    coinShadow->setPosition(Vec2(coinSpr->getContentSize().width/2,15));
    coinSpr->addChild(coinShadow,-1);
    
    
    if (m_bPlayedAgain) {
        Label *AgainLabel = createLabelBasedOnLang("CompletedAgain",45);
        AgainLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        AgainLabel->setColor(Color3B::WHITE);
        AgainLabel->setWidth(sprBg2->getBoundingBox().size.width-64);
        AgainLabel->setHeight(AgainLabel->getBoundingBox().size.height+5);
        AgainLabel->enableOutline(Color4B(53,126,32,255),2);
        AgainLabel->setPosition(Vec2(sprBg2->getBoundingBox().getMidX(),coinCollectLbl->getBoundingBox().getMinY()-AgainLabel->getBoundingBox().size.height/2-40));
        this->addChild(AgainLabel,3);
    }
    
    
    if(!m_bPlayedAgain)
    {
        giftRewardLbl = createLabelBasedOnLang("Gift",40);
        giftRewardLbl->setAlignment(TextHAlignment::LEFT,TextVAlignment::CENTER);
        giftRewardLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        giftRewardLbl->setColor(Color3B::WHITE);
        giftRewardLbl->setWidth(giftRewardLbl->getBoundingBox().size.width+10);
        giftRewardLbl->setHeight(giftRewardLbl->getBoundingBox().size.height+5);
        giftRewardLbl->enableOutline(Color4B(75,34,16,255),2);
        giftRewardLbl->setPosition(Vec2(Pos.x,sprBg2->getBoundingBox().getMinY()+50+giftRewardLbl->getBoundingBox().size.height/2));
        m_GiftNode->addChild(giftRewardLbl,2);
        
        if(me_Language.compare(LANG_MALAYALAM)==0||me_Language.compare(LANG_BANGLA)==0)
        {
            giftRewardLbl->setSystemFontSize(25);
            giftRewardLbl->setWidth(0);
            giftRewardLbl->setWidth(giftRewardLbl->getBoundingBox().size.width+10);
        }
    }
}

void LevelCompletePopUp::createProgressBar(Point pos)
{
    
    float xpos = sprBg2->getBoundingBox().getMaxX()-25;
    
    giftIcon = Sprite::create("gift-image.png");
    giftIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    giftIcon->setPosition(Vec2(xpos,giftRewardLbl->getPositionY()+5));
    this->addChild(giftIcon,3);
    
    Sprite *giftShadow = Sprite::create("shadow.png");
    giftShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    giftShadow->setPosition(Vec2(giftIcon->getContentSize().width/2,10));
    giftIcon->addChild(giftShadow,-1);

    Sprite *giftcountBg = Sprite::create("gift_countBg.png");
    giftcountBg->setColor(Color3B::RED);
    giftcountBg->setPosition(Vec2(giftIcon->getContentSize().width-25,giftIcon->getContentSize().height-10));
    giftIcon->addChild(giftcountBg,1);
    
    m_tGiftCount = Label::createWithTTF("1/5", FONT_NAME,25);
    m_tGiftCount->setPosition(Vec2(giftcountBg->getContentSize().width/2,giftcountBg->getContentSize().height/2));
    m_tGiftCount->setColor(Color3B::WHITE);
    giftcountBg->addChild(m_tGiftCount,1);

    
     std::string country = StatsManager::getInstance()->getCurrentCountry();
    
     int currCount = StatsManager::getInstance()->getPlayedLevelCountForCountries(country);
    
     int remCount = (currCount%5);
    
     float percentage = remCount*20;

     m_tGiftCount->setString(StringUtils::format("%d/5",remCount));

     if(remCount==0)
     {
        remCount = 5;
        percentage = 100.0f;
        m_tGiftCount->setString(StringUtils::format("%d/5",remCount));
     }
    
     m_LevelProgressBarBg = Sprite::create("Progress_barBg.png");
     m_LevelProgressBarBg->setPosition(giftRewardLbl->getBoundingBox().getMaxX()+m_LevelProgressBarBg->getBoundingBox().size.width/2+5,giftRewardLbl->getPositionY());
     m_GiftNode->addChild(m_LevelProgressBarBg,2);

     m_LevelProgressBar = ProgressTimer::create(Sprite::create("Progress_bar_filler.png"));
     m_LevelProgressBar->setType(ProgressTimer::Type::BAR);
     m_LevelProgressBar->setBarChangeRate(Vec2(1,0));
     m_LevelProgressBar->setMidpoint(Vec2(0.0f, 0.0f));
     m_LevelProgressBar->setPercentage(percentage-20.0f);
     m_LevelProgressBar->setPosition(m_LevelProgressBarBg->getPosition());
     m_GiftNode->addChild(m_LevelProgressBar,2);
    
    ProgressFromTo *t_ProgressfromTo = ProgressFromTo::create(1.5f, m_LevelProgressBar->getPercentage(),percentage);
    EaseSineInOut *t_EaseSineInOut = EaseSineInOut::create(t_ProgressfromTo);
    DelayTime *delay = DelayTime::create(0.5f);

    CallFunc *callfunc =NULL;
    FiniteTimeAction *_call = NULL;
    if(percentage==100)
    {
        t_miCloseButton->setEnabled(false);
        t_NextButton->setEnabled(false);
        
        _call = CallFuncN::create(CC_CALLBACK_0(LevelCompletePopUp::enableClaimButton,this));

        callfunc =CallFunc::create([&,this]()
            {
                m_GiftNode->setVisible(false);
                giftIcon->removeAllChildrenWithCleanup(true);
            });
    }
    m_LevelProgressBar->runAction(Sequence::create(t_EaseSineInOut,callfunc,delay,_call, NULL));

}
void LevelCompletePopUp::enableClaimButton()
{
    Point pos = giftIcon->getPosition();
    
    std::string CurrCountry = StatsManager::getInstance()->getCurrentCountry();

    int playedcount = StatsManager::getInstance()->getPlayedLevelCountForCountries(CurrCountry);
    int count = 0;//playedcount%5;

      int giftunclk = 0;
      if(playedcount>0)
      {
          count = playedcount%5;
          
          if(count==0)
          {
              giftunclk = playedcount/5;
              int t_iGiftTag = giftunclk;
              StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(CurrCountry, t_iGiftTag-1);

              StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(CurrCountry, giftunclk);
              
              AchievementManager::getInstance()->OnAchievementUpdate("giftbox", 1);
              AchievementManager::getInstance()->CheckAllAchievementsComplete();
              
              DailyMissionManager::getInstance()->OnDailyMissionUpdate("giftbox", 1);
              DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();

          }
      }
    Label *t_unlockLbl = createLabelBasedOnLang("NewGift",35);
    t_unlockLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    t_unlockLbl->setColor(Color3B(255,244,55));
    t_unlockLbl->setWidth(m_SprPopUpBg->getBoundingBox().size.width-100);
    t_unlockLbl->setHeight(t_unlockLbl->getBoundingBox().size.height+20);

    t_unlockLbl->enableOutline(Color4B(196,56,63,255),3);
    t_unlockLbl->setPosition(Vec2(sprBg2->getBoundingBox().getMidX(),sprBg2->getBoundingBox().getMaxY()-10-t_unlockLbl->getBoundingBox().size.height/2));
    this->addChild(t_unlockLbl,2);

    m_tCoins +=25;
    
    JumpBy *jump = JumpBy::create(0.7f,Vec2(m_LevelProgressBarBg->getBoundingBox().getMidX()-200,m_LevelProgressBarBg->getBoundingBox().getMidY()),100,1);
    CallFuncN *callfunc = CallFuncN::create(CC_CALLBACK_0(LevelCompletePopUp::showGiftBox,this));

    giftIcon->runAction(Sequence::create(jump,callfunc,NULL));
}
void LevelCompletePopUp::showGiftBox()
{
    GameController::getInstance()->playSFX(SFX_GIFT_UNLOCK);
    giftIcon->setTexture("gift-box2.png");
    giftIcon->setScale(1.0);
    giftIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    giftIcon->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg2->getBoundingBox().getMinY()+giftIcon->getContentSize().height/2+10));
    
    
    t_tCoinText->setVisible(true);
    t_tCoinText->setString(StringUtils::format("+ %d",m_tCoins));
    t_tCoinText->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),giftIcon->getBoundingBox().getMidY()+t_tCoinText->getContentSize().height/2+30));
    
    sprCoin->setVisible(true);
sprCoin->setPosition(Vec2(t_tCoinText->getBoundingBox().getMaxX()+10+sprCoin->getContentSize().width/2,t_tCoinText->getPositionY()));
    
    t_miCloseButton->setEnabled(true);
    t_NextButton->setEnabled(true);

}
void LevelCompletePopUp::claimButtonCallBack(Ref *_sender)
{
    
    
}
/// CUSTOM BUTTON MADE
MenuItemSprite * LevelCompletePopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    Sprite *_disabledSpr = Sprite::create(normalSpr);
    _disabledSpr->setOpacity(150);

    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,_disabledSpr,func);
    return Button;
}
void LevelCompletePopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    MenuItemSprite *button = (MenuItemSprite*)p_Sender;
    
    int buttonTag = button->getTag();
    
    switch (buttonTag) {
        case 0:
        {
            status = (char*)"levelcomplete_close";
        }break;
        case 1:
        {
            status = (char*)"Next";
        }break;
        default:
            break;
    }
    
    if (m_fnSelector!=NULL)
    {
        m_fnSelector();
    }
}
void LevelCompletePopUp::NextButtonCallBack()
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
//    if(m_bPlayedAgain)
//    {
//        status = (char*)"levelcomplete_close";
//           if (m_fnSelector!=NULL) {
//               m_fnSelector();
//           }
//    }
//    else
    {
        me_iCoinCount += m_tCoins;
        UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
        UserDefault::getInstance()->flush();
        
        if(t_NextButton!=NULL)
        {
            t_NextButton->setEnabled(false);
            t_NextButton->setOpacity(150);
        }
        
        if(t_DoubleCoinButton)
        {
            m_bVideoSuccess = true;
            t_DoubleCoinButton->setEnabled(false);
        }

        Point _startPosOppo = Point(coinSpr->getBoundingBox().getMidX()-20,coinSpr->getPositionY());
        
        if(t_tCoinText->isVisible())
        {
            _startPosOppo =  Point(sprCoin->getBoundingBox().getMidX()-20,sprCoin->getPositionY());
        }

        
        CallFuncN *_call2 = CallFuncN::create(std::bind(&LevelCompletePopUp::coinAddAnimation, this,_startPosOppo));
        DelayTime *_delay = DelayTime::create(1.7f);
        FiniteTimeAction *_call3 = CallFuncN::create(CC_CALLBACK_0(LevelCompletePopUp::coinAnimationDoneClosePopUp,this));
        FiniteTimeAction *_seq  = Sequence::create(_call2,_delay,_call3, NULL);
        this->runAction(_seq);
    }
    
}
void LevelCompletePopUp::doubleCoinBtnClick()
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameController::getInstance()->showVideoAd();
}
void LevelCompletePopUp::onVideoAdSuccess()
{
    log("onVideoAdSuccess");
    
    int coincount = m_tCoins;
    
    m_bVideoSuccess = true;
    
    if(m_tCoinCount) {
        m_tCoinCount->setString(StringUtils::format("%d",m_tCoins*2));
    }
    
    if(t_tCoinText &&t_tCoinText->isVisible())
    {
        t_tCoinText->setString(StringUtils::format("+ %d",m_tCoins*2));
    }
    UserDefault::getInstance()->setIntegerForKey(COINS,UserDefault::getInstance()->getIntegerForKey(COINS)+coincount);
    
    me_iCoinCount = UserDefault::getInstance()->getIntegerForKey(COINS);
    UserDefault::getInstance()->flush();
    if(t_DoubleCoinButton!=NULL)
    {
        t_DoubleCoinButton->setEnabled(false);
    }

}
void LevelCompletePopUp::onVideoAdFailure()
{
    if(!GameController::getInstance()->isVideoAdsAvailable())
    {
        t_DoubleCoinButton->setEnabled(false);
    }
}
void LevelCompletePopUp::videoAdsAvailble()
{
    if(t_DoubleCoinButton!=NULL&&!m_bVideoSuccess)
    {
        t_DoubleCoinButton->setEnabled(true);
    }

}
void LevelCompletePopUp::coinAddAnimation(Point t_StartPos)
{
    this->runAction(CallFunc::create([&,this]()
    {
        Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
        Point _startPosOppo = t_StartPos;// Point(coinSpr->getBoundingBox().getMidX()-20,coinSpr->getPositionY());
        for (int i=0; i< 20; i++)
        {
            Sprite *_spr = Sprite::create("coins.png");
            _spr->setScale(_spr->getScale()+0.2);
            _spr->setPosition(_startPosOppo);
            _spr->setOpacity(0);
            ccBezierConfig bezier;
            bezier.controlPoint_1 = _startPosOppo;
            bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
            bezier.endPosition =_endPos;
            BezierTo *_bez = BezierTo::create(.5f, bezier);
            this->addChild(_spr,5);
            FadeTo *_fade = FadeTo::create(.04*i,255);
            FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(LevelCompletePopUp::removeSprite,this));
            FiniteTimeAction*_seq = Sequence::create(_fade,_bez,_call,NULL);
            _spr->runAction(_seq);
        }
            if(t_miCloseButton)
            {
                t_miCloseButton->setEnabled(false);
            }
    }));

}
void LevelCompletePopUp::removeSprite(Ref *_sender)
{
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);
    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
        
    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    
}
void LevelCompletePopUp::coinAnimationDoneClosePopUp()
{
    if(t_miCloseButton)
    {
        t_miCloseButton->setEnabled(true);
    }

    status = (char*)"levelcomplete_close";
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}
Label *LevelCompletePopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
void LevelCompletePopUp::createMoregamesonLevelComplete()
{
//    GameController::getInstance()->hideBanner();
    float size = 142*0.9f;
    
    m_gamesNode = Node::create();
    m_gamesNode->setPosition(Vec2::ZERO);
    this->addChild(m_gamesNode,2);
    
    moreitems = new Vector<MenuItemSprite*>();
    int noOfGames = (int)GameController::getInstance()->t_levelmoreGameImageUrl.size();

    float midPoint = m_SprPopUpBg->getBoundingBox().getMidX() - (noOfGames*size)/2.0f;

    Point m_Position = Point(midPoint,m_SprPopUpBg->getBoundingBox().getMinY()-10);

    for(int i=0;i<noOfGames; i++)
    {
        std::string str = FileUtils::getInstance()->getWritablePath();
        str += StringUtils::format("gamesIcon%d.png", i+1);
        
        MenuItemSprite *iconBtn = getButtonMade("more-games-button.png", "more-games-button.png", CC_CALLBACK_1(LevelCompletePopUp::moreGamesBtnCallback, this));
        iconBtn->setScale(0.9f);
        iconBtn->setPosition(Vec2(m_Position.x+iconBtn->getBoundingBox().size.width/2,m_Position.y-iconBtn->getBoundingBox().size.height/2));
        iconBtn->setTag(i);
      
        Sprite *giftcountBg = Sprite::create("gift_countBg.png");
        giftcountBg->setColor(Color3B::RED);
        giftcountBg->setPosition(Vec2(iconBtn->getContentSize().width/2,iconBtn->getContentSize().height*0.25));
        iconBtn->addChild(giftcountBg,6);
        
        Label *m_tAdLabel = Label::createWithTTF("Ad", FONT_NAME,25);
        m_tAdLabel->setPosition(Vec2(giftcountBg->getContentSize().width/2,giftcountBg->getContentSize().height/2));
        m_tAdLabel->setColor(Color3B::WHITE);
        giftcountBg->addChild(m_tAdLabel,1);
        
        Sprite *sprGameIcon;
        if (FileUtils::getInstance()->isFileExist(str))
        {
            sprGameIcon = Sprite::create(str);
        }
        else
        {
            sprGameIcon = Sprite::create("pixel.png");
            sprGameIcon->setColor(Color3B(134,235,20));
        }
        
        DrawNode*m_Draw = DrawNode::create();
        m_Draw->drawSolidCircle(Vec2(0.0f, 0.0f),52.0f, 0.0f, 32.0f, Color4F::GREEN);
        
        ClippingNode *clipper = ClippingNode::create();
        clipper->setStencil(m_Draw);
        clipper->setPosition(Vec2(iconBtn->getContentSize().width/2,iconBtn->getContentSize().height/2+5.5f));
         // add the clipping node to the main node
        iconBtn->addChild(clipper,5,1);

        sprGameIcon->setOpacity(220);
        clipper->setName("GameIcon");
        clipper->addChild(sprGameIcon,1);

        moreitems->pushBack(iconBtn);
        Menu *menu = Menu::create(iconBtn, nullptr);
        menu->setPosition(Vec2::ZERO);
        m_gamesNode->addChild(menu);
        
        m_Position.x += size;
    }

    
    CallFuncN *_call = CallFuncN::create(CC_CALLBACK_0(LevelCompletePopUp::animateButtons,this));
    this->runAction(RepeatForever::create(Sequence::create(DelayTime::create(1.0f),_call,NULL)));
}
void LevelCompletePopUp::animateButtons()
{
    for (int i=0; i<moreitems->size(); i++)
    {
        float scale = moreitems->at(i)->getScale();
        DelayTime *delay = DelayTime::create(0.1f*i);
        ScaleTo *Scale1 = ScaleTo::create(0.1f,scale+0.15f, scale+0.15f);
        ScaleTo *Scale2 = ScaleTo::create(0.1f,scale,scale);
        Sequence *spawn = Sequence::create(Scale1,Scale2, NULL);
        moreitems->at(i)->runAction(Sequence::create(delay,spawn, NULL));
    }
}
void LevelCompletePopUp::moreGamesBtnCallback(Ref *sender)
{
        GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
        MenuItemSprite *tag = (MenuItemSprite*)sender;
        Application::getInstance()->openURL(GameController::getInstance()->t_LevelmoreGameAppUrl.at(tag->getTag()).c_str());
}
LevelCompletePopUp::~LevelCompletePopUp()
{
    this->removeAllChildrenWithCleanup(true);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_AVAILABLE);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_SUCCEED);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_FAILED);
}
