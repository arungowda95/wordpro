//
//  CommonPopUp.cpp
//  wordPro-mobile
//
//  Created by Arun on 10/02/21.
//

#include "CommonPopUp.h"
CommonPopUp::CommonPopUp(std::function<void()> func)
{
    m_fnSelector = func;
    
    ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.4);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    
    std::string textStr = UserDefault::getInstance()->getStringForKey(POPUP_TEXT);
    
    std::string t_SubText = textStr;
    Label *t_tSubLabel = createLabelBasedOnLang(t_SubText,55);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,255,255));
    t_tSubLabel->enableOutline(Color4B(196,56,63,255),2);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()-30-t_tSubLabel->getContentSize().height/2));
    this->addChild(t_tSubLabel, 2);
    
    // OK BUTTON
    int tag = UserDefault::getInstance()->getIntegerForKey("BtnTag",0);
    
    t_miOkBtn = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_1(CommonPopUp::OnButtonPressed,this));
    t_miOkBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()+t_miOkBtn->getBoundingBox().size.height));
    t_miOkBtn->setTag(tag);
    
    Label *NxtLabel = createLabelBasedOnLang("CheckOut",35);
    NxtLabel->setColor(Color3B::WHITE);
    NxtLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    NxtLabel->setDimensions(t_miOkBtn->getContentSize().width-10,150);
    NxtLabel->enableOutline(Color4B(53,126,32,255),2);
    NxtLabel->setPosition(Vec2(t_miOkBtn->getBoundingBox().size.width/2,t_miOkBtn->getBoundingBox().size.height/2+15));
    t_miOkBtn->addChild(NxtLabel,1);
    
    Menu *m_Menu = Menu::create(t_miOkBtn,NULL);
    m_Menu->setPosition(Vec2::ZERO);
    this->addChild(m_Menu,2);
    
    
    
}
void CommonPopUp::OnButtonPressed(Ref *p_Sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    
    int pButtonTag =  Button->getTag();
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    
    switch (pButtonTag)
    {
        case 0:
        {
            status = (char*)"close";
        }break;
        case 1:
        {
            status = (char*)"dailyTask";
        }break;
        case 2:
        {
            status = (char*)"openLeadearboard";
        }break;
            
        default:
            break;
    }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
    
}

MenuItemSprite *CommonPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
    
    
}


Label *CommonPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr);
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
        t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
        t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

CommonPopUp::~CommonPopUp()
{
    
}
