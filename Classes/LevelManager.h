//
//  LevelManager.h
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#ifndef LevelManager_h
#define LevelManager_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class OptionGrid;
class AnswerGrid;
class LevelManager : public Ref
{
    GameLayer     *m_ptrGameLayer;
    OptionGrid    *m_ptrOptionGrid;
    AnswerGrid    *m_ptrAnswerGrid;
    
    int           m_iIndex;
    
    std::map<std::string ,int> dictLetterCount;
    
    std::string m_strRepString;
    
    std::vector<std::string> m_LettersArray;
    std::vector<std::string> m_WordsArray;
    std::vector<std::string> m_BonusWordsArray;
    
public:
    
    // CONSTRUCTOR AND DESTRUCTOR
    LevelManager(GameLayer *p_ptrGameLayer);
    ~LevelManager();
    
    /// loads current level data
    void loadLevel();
    
    /// PARSE JSON FILE
    void parseJson();
    
    ///To count num of string
    void findCountOfString(std::vector<std::string> t_LettersArray,std::vector<std::string> t_WordsArray);
    
    /// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
    
    /// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
    std::vector<std::string> split(const std::string &s, char delim);
    
    /// Sorts words Based on length of word
    /// @param t_WordsArray Array of words
    void sortWordsLowtoHigh(std::vector<std::string> t_WordsArray);
    
    void checkJson(std::string t_fullPath);
};
#endif /* LevelManager_h */
