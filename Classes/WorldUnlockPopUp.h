//
//  WorldUnlockPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 22/04/20.
//

#ifndef WorldUnlockPopUp_hpp
#define WorldUnlockPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class WorldUnlockPopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    MenuItemSprite *t_NextButton;
    
public:
     WorldUnlockPopUp(std::function<void()> func);
    ~WorldUnlockPopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);

};

#endif /* WorldUnlockPopUp_hpp */
