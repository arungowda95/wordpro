//
//  RemoveAdsPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 23/03/20.
//

#include "RemoveAdsPopUp.h"

RemoveAdsPopUp::RemoveAdsPopUp(std::function<void()> func)
{
    m_fnSelector = func;
 
    Rect Res = Director::getInstance()->getSafeAreaRect();
    Size visibleSize = Size(Res.size.width,Res.size.height);
    Vec2 origin = Res.origin;
            
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    t_VideoAdSuccess = false;
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.17);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleY()+0.15);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    
    printf("posX-->%f_posY-->%f\n",m_SprPopUpBg->getPositionX(),m_SprPopUpBg->getPositionY());
    
    std::string t_Label = "RemoveAds";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-64);
    t_tLabel->setHeight(150);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMidY()));
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tLabel, 2);
    
    log("size-->%f__%f",t_tLabel->getBoundingBox().size.width,m_SprPopUpTop->getBoundingBox().size.width-64);
    
    if((t_tLabel->getBoundingBox().size.width+5)>=(m_SprPopUpTop->getBoundingBox().size.width-64))
    {
        t_tLabel->setSystemFontSize(50);
    }
    
    if(me_Language.compare(LANG_TAMIL)==0)
    {
        t_tLabel->setSystemFontSize(42);
    }
    
    m_PopUpRect = m_SprPopUpBg->getBoundingBox();
    
    
    Sprite *bgSpr = Sprite::create("large_box.png");
    bgSpr->setScaleY(bgSpr->getScaleY()+0.65);
    bgSpr->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()-10));
    this->addChild(bgSpr,2);
    
    
    Sprite *removeAdsIcon = Sprite::create("ads-icon.png");
    removeAdsIcon->setPosition(Vec2(bgSpr->getBoundingBox().getMidX(),bgSpr->getBoundingBox().getMaxY()));
    this->addChild(removeAdsIcon,2);
    
     
    std::string t_txt = "ForSession";
    Label *t_tSession = createLabelBasedOnLang(t_txt, 45);
    t_tSession->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSession->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSession->setHeight(t_tSession->getBoundingBox().size.height+20);
    t_tSession->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),removeAdsIcon->getBoundingBox().getMinY()-30));
    t_tSession->setColor(Color3B(240,121,73));
    t_tSession->setHeight(t_tSession->getBoundingBox().size.height);
    this->addChild(t_tSession, 2);
    
    // WATCH VIDEO BUTTTON
    t_miVideoBtn =getButtonMade("large_button_1.png","large_button_1.png",CC_CALLBACK_1(RemoveAdsPopUp::watchVideoBtnPressed,this));
    t_miVideoBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tSession->getBoundingBox().getMinY()-t_miVideoBtn->getBoundingBox().size.height/2));
    t_miVideoBtn->setTag(0);
    
    if (!GameController::getInstance()->isVideoAdsAvailable())
    {
        t_miVideoBtn->setEnabled(false);
    }
    
    Sprite *watchIcon = Sprite::create("haw-to-play.png");
    watchIcon->setPosition(Vec2(20+watchIcon->getBoundingBox().size.width,t_miVideoBtn->getBoundingBox().size.height/2+10.0f));
    t_miVideoBtn->addChild(watchIcon,2);

    
    std::string t_watch = "WatchVideo";
    Label *t_tWatchvideo = createLabelBasedOnLang(t_watch,45);
    t_tWatchvideo->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tWatchvideo->setColor(Color3B::WHITE);
    t_tWatchvideo->enableOutline(Color4B(25,121,15,255),2);
    t_tWatchvideo->setWidth(t_miVideoBtn->getContentSize().width-watchIcon->getContentSize().width);
    t_tWatchvideo->setHeight(t_tWatchvideo->getBoundingBox().size.height+10);

    t_tWatchvideo->setPosition(Vec2(t_miVideoBtn->getContentSize().width/2,t_miVideoBtn->getBoundingBox().size.height/2+12.0f));
    t_miVideoBtn->addChild(t_tWatchvideo, 2);
    
    if(me_Language.compare(LANG_TAMIL)==0)
    {
        t_tWatchvideo->setSystemFontSize(40);
    }

    std::string t_remove = "RemoveForever";
    Label *t_tRemoveforEver = createLabelBasedOnLang(t_remove, 45);
    t_tRemoveforEver->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tRemoveforEver->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tRemoveforEver->setHeight(t_tRemoveforEver->getBoundingBox().size.height+10);
    t_tRemoveforEver->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_miVideoBtn->getBoundingBox().getMinY()-t_tRemoveforEver->getBoundingBox().size.height/2));
    t_tRemoveforEver->setColor(Color3B(240,121,73));
    this->addChild(t_tRemoveforEver, 2);
    
    
    if(me_Language.compare(LANG_TAMIL)==0||me_Language.compare(LANG_MALAYALAM)==0)
    {
        t_tRemoveforEver->setSystemFontSize(40);
    }
    
    if(me_Language.compare(LANG_BANGLA)!=0) {
        t_tRemoveforEver->setPositionY(t_tRemoveforEver->getPositionY()-30);
    }
    
    // SHOP BUTTTON
    t_miShopBtn = getButtonMade("large_button_1.png","large_button_1.png",CC_CALLBACK_0(RemoveAdsPopUp::BuyButtonCallBack,this));
    t_miShopBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),bgSpr->getBoundingBox().getMinY()+15+t_miShopBtn->getBoundingBox().size.height*0.5));
    t_miShopBtn->setTag(0);

    Sprite *shopIcon = Sprite::create("shop-icon.png");
     shopIcon->setPosition(Vec2(20+shopIcon->getBoundingBox().size.width,t_miShopBtn->getBoundingBox().size.height/2+10.0f));
     t_miShopBtn->addChild(shopIcon,2);

     
     std::string t_shop = "Buy";
    Label *t_tshopText = createLabelBasedOnLang(t_shop, 45);
    t_tshopText->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tshopText->setWidth(t_miShopBtn->getContentSize().width-shopIcon->getContentSize().width);
    t_tshopText->setHeight(t_tshopText->getBoundingBox().size.height+10);
     t_tshopText->setPosition(Vec2(t_miShopBtn->getBoundingBox().size.width/2,t_miShopBtn->getBoundingBox().size.height/2+12.0f));
     t_tshopText->setColor(Color3B::WHITE);
     t_tshopText->enableOutline(Color4B(25,121,15,255),2);
     t_miShopBtn->addChild(t_tshopText, 2);
  
    
    // CLOSE BUTTON
       MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(RemoveAdsPopUp::OnButtonPressed,this));
       t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
       t_miCloseButton->setTag(0);

       Menu *m_Menu = Menu::create(t_miCloseButton,t_miShopBtn,t_miVideoBtn,NULL);
       m_Menu->setPosition(Vec2::ZERO);
       this->addChild(m_Menu,2);

       registerVideoEvent();
}
void RemoveAdsPopUp::registerVideoEvent()
{
    auto t_VideoAdAvailble = EventListenerCustom::create(EVENT_VIDEO_ADS_AVAILABLE,CC_CALLBACK_0(RemoveAdsPopUp::videoAdsAvailble,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdAvailble, this);
    
    auto t_VideoAdSuccess = EventListenerCustom::create(EVENT_VIDEO_ADS_SUCCEED, CC_CALLBACK_0(RemoveAdsPopUp::onVideoAdSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdSuccess, this);
    
    auto t_VideoAdFailure = EventListenerCustom::create(EVENT_VIDEO_ADS_FAILED, CC_CALLBACK_0(RemoveAdsPopUp::onVideoAdFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdFailure, this);
    
    
    auto t_PurchaseSuccess = EventListenerCustom::create(EVENT_REMOVE_ADS_SUCCESS, CC_CALLBACK_0(RemoveAdsPopUp::onVideoAdSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_PurchaseSuccess, this);
    
    auto t_PurchaseFailure = EventListenerCustom::create(EVENT_PURCHASE_FAILURE, CC_CALLBACK_0(RemoveAdsPopUp::onVideoAdFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_PurchaseFailure, this);

}
void RemoveAdsPopUp::OnButtonPressed(Ref *p_Sender)
{
 MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
   
   int pButtonTag =  Button->getTag();

    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

   switch (pButtonTag)
   {
       case 0:
       {
           status = (char*)"close";
       }break;
       case 1:
       {
           status = (char*)"Shop";
       }break;
        default:
     break;
   }
 
    if (m_fnSelector!=NULL) {
     m_fnSelector();
    }
}
MenuItemSprite *RemoveAdsPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    Sprite *_DisabledSpr = Sprite::create(selSpr);
    _DisabledSpr->setOpacity(150);
      
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr  ,_DisabledSpr,func);
    return Button;
}

void RemoveAdsPopUp::watchVideoBtnPressed(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameController::getInstance()->showVideoAd();
}
void RemoveAdsPopUp::onVideoAdSuccess()
{
    GameController::getInstance()->setBoolForKey(NO_ADS_FOR_SESSION, true);
    
    GameController::getInstance()->disableAdsForThisSession();
    
    t_VideoAdSuccess = true;
    
    if(t_miVideoBtn!=NULL) {
          t_miVideoBtn->setEnabled(false);
    }
    if(t_miShopBtn!=NULL)
    {
        t_miShopBtn->setEnabled(false);
    }
    DelayTime *delay = DelayTime::create(0.5f);
    auto cb = CallFuncN::create( [&] (Node* sender)
    {
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    });
    Sequence *seq = Sequence::create(delay,cb,NULL);
    this->runAction(seq);

}
void RemoveAdsPopUp::onVideoAdFailure()
{
    if(!GameController::getInstance()->isVideoAdsAvailable())
    {
        if (t_miVideoBtn!=NULL) {
              t_miVideoBtn->setEnabled(true);
          }
    }
}
void RemoveAdsPopUp::videoAdsAvailble()
{
      if(!GameController::getInstance()->isVideoAdsAvailable()&&!t_VideoAdSuccess)
      {
          if (t_miVideoBtn!=NULL) {
                t_miVideoBtn->setEnabled(true);
            }
      }
}
void RemoveAdsPopUp::BuyButtonCallBack()
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameController::getInstance()->purchaseThis(REMOVE_ADS);
}
void RemoveAdsPopUp::onRemoveAdsPurchaseSuccess()
{
    GameController::getInstance()->hideBanner();
    
    DelayTime *delay = DelayTime::create(0.5f);
    auto cb = CallFuncN::create( [&] (Node* sender)
    {
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    });
    Sequence *seq = Sequence::create(delay,cb,NULL);
    this->runAction(seq);

}
void RemoveAdsPopUp::onRemoveAdsPurchaseFailure()
{
 
}
Label *RemoveAdsPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr);
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

RemoveAdsPopUp::~RemoveAdsPopUp()
{
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_AVAILABLE);
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_SUCCEED);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_FAILED);
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_REMOVE_ADS_SUCCESS);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_PURCHASE_FAILURE);

    this->removeAllChildrenWithCleanup(true);
}
