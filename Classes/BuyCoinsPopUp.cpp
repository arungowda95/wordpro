//
//  BuyCoinsPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 16/03/20.
//

#include "BuyCoinsPopUp.h"

BuyCoinsPopUp::BuyCoinsPopUp(std::function<void()> func)
{
    
    m_fnSelector = func;
    m_RemoveAdNode = NULL;
    m_ptrScrolView = NULL;
    t_iCoinAdded   = 0;
    m_bRestore     = false;
    Rect ResolutionSize = Director::getInstance()->getSafeAreaRect();
    Size visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    Point origin = ResolutionSize.origin;

    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()+0.15);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    
    std::string t_Label = "Shop";
    Label *t_tLabel = createLabelBasedOnLang(t_Label, 60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tLabel->setWidth(m_SprPopUpTop->getContentSize().width-70);
    t_tLabel->setHeight(150);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(t_tLabel, 2);

    m_RemoveAdNode = Node::create();
    m_RemoveAdNode->setPosition(Vec2::ZERO);
    this->addChild(m_RemoveAdNode,2);

    
    float posx = m_SprPopUpBg->getBoundingBox().getMidX();
    m_RemoveAdBg = Sprite::create("small_box.png");
    m_RemoveAdBg->setScaleY(m_RemoveAdBg->getScaleY()+0.6);
    m_RemoveAdBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-30-m_RemoveAdBg->getBoundingBox().size.height/2));
    m_RemoveAdNode->addChild(m_RemoveAdBg,2);
    
    m_RemoveAdLbl = createLabelBasedOnLang("RemoveAds",40);
    m_RemoveAdLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    m_RemoveAdLbl->setColor(Color3B::WHITE);
    m_RemoveAdLbl->enableOutline(Color4B::BLACK,2);
    m_RemoveAdLbl->setWidth(m_RemoveAdBg->getBoundingBox().size.width-50);
    m_RemoveAdLbl->setHeight(m_RemoveAdLbl->getBoundingBox().size.height+20);
    m_RemoveAdLbl->setPosition(Vec2(posx,m_RemoveAdBg->getBoundingBox().getMaxY()-m_RemoveAdLbl->getBoundingBox().size.height/2));
    m_RemoveAdNode->addChild(m_RemoveAdLbl,2);
    
    if(me_Language.compare(LANG_ENGLISH)!=0) {
        m_RemoveAdLbl->setPosition(Vec2(posx,m_RemoveAdBg->getBoundingBox().getMaxY()-5-m_RemoveAdLbl->getBoundingBox().size.height/2));
    }
    
    
    m_coinsBg = Sprite::create("large_box.png");
    m_coinsBg->setScaleY(m_RemoveAdBg->getScaleY()+0.4);
    m_coinsBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_RemoveAdBg->getBoundingBox().getMinY()-20-m_coinsBg->getBoundingBox().size.height/2));
    this->addChild(m_coinsBg,2);

    
     m_BuyMenu = Menu::create(NULL);
     m_BuyMenu->setPosition(Vec2::ZERO);
     this->addChild(m_BuyMenu,3);
    
    createRemoveAdsButton(m_RemoveAdBg,m_RemoveAdLbl->getBoundingBox().getMinY());

    
    if(GameController::getInstance()->getBoolForKey("isAdsRemoved") == true||GameController::getInstance()->m_bNoAdsForSession)
    {
        m_RemoveAdNode->setVisible(false);
        
        m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.15);
        m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
        t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));

        m_coinsBg->setScaleY(m_RemoveAdBg->getScaleY()+0.5);
        m_coinsBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()));
    }
    
 
    m_ptrScrolView = cocos2d::ui::ScrollView::create();
    m_ptrScrolView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_ptrScrolView->setTouchEnabled(true);
    m_ptrScrolView->setBounceEnabled(true);
    Size contentsize = Size(m_coinsBg->getBoundingBox().size.width,m_coinsBg->getBoundingBox().size.height-50);
    m_ptrScrolView->setContentSize(Size(contentsize));
    m_ptrScrolView->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_coinsBg->getBoundingBox().getMidY()));
    m_ptrScrolView->setScrollBarEnabled(false);
    m_ptrScrolView->setSwallowTouches(false);
    m_ptrScrolView->setAnchorPoint(Vec2(0.5f,0.5f));
    
    this->addChild(m_ptrScrolView,3);
    
    totalNum = 5;

    if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS && GameController::getInstance()->getBoolForKey("isAdsRemoved") == false)
    {
        totalNum = totalNum+1;
        m_bRestore = true;
    }

    Sprite *path = Sprite::create("coins_popup_Bg.png");

    itemSize = Size(path->getBoundingBox().size.width,path->getBoundingBox().size.height+5);
    
    Size Containersize = Size(contentsize.width,itemSize.height*(totalNum));
    m_ptrScrolView->setInnerContainerSize(Size(Containersize.width,Containersize.height));

    Point t_position = Vec2(m_ptrScrolView->getContentSize().width/2,Containersize.height-5);

    addBuyButton(t_position,false);
    
    // CLOSE BUTTON
    MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(BuyCoinsPopUp::OnButtonPressed,this));
    t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
    t_miCloseButton->setTag(0);

    Menu *m_Menu = Menu::create(t_miCloseButton,NULL);
    m_Menu->setPosition(Vec2::ZERO);
    this->addChild(m_Menu,2);

    
    auto t_PurchaseSuccess = EventListenerCustom::create(EVENT_PURCHASE_SUCCESS,CC_CALLBACK_0(BuyCoinsPopUp::onCoinPurchaseSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_PurchaseSuccess, this);
    
    auto t_PurchaseFailure = EventListenerCustom::create(EVENT_PURCHASE_FAILURE, CC_CALLBACK_0(BuyCoinsPopUp::onCoinPurchaseFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_PurchaseFailure, this);
    
    auto t_RestoreSuccess = EventListenerCustom::create(EVENT_RESTORE_SUCCESS, CC_CALLBACK_0(BuyCoinsPopUp::onRestoreSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_RestoreSuccess, this);


}
/// CUSTOM BUTTON MADE
MenuItemSprite * BuyCoinsPopUp :: getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}
void BuyCoinsPopUp::addBuyButton(Point Pos,bool bestOffer)
{
        
    bestOffer = true;
    int percent = 20;
    
//    if(CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID)
    {
        if(GameController::getInstance()->inAppTitle.size()>0)
        {
            coinList = GameController::getInstance()->inAppTitle;
            
            priceList = GameController::getInstance()->inAppPrice;
            
            priceValue = GameController::getInstance()->inAppPriceVal;
            
            inAppNames = GameController::getInstance()->inAppName;
        }
    }
    
    Point t_position = Pos;
    for (int index =0 ;index<=int(totalNum-1); index++)
    {
        
        
       Layout *layout = Layout::create();
       layout->setColor(Color3B::WHITE);
       layout->setContentSize(itemSize);
       layout->setAnchorPoint(Vec2(0.5f,0.5f));
       layout->setPosition(Vec2(t_position.x,t_position.y-layout->getBoundingBox().size.height/2));
//       layout->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::SOLID);
       m_ptrScrolView->addChild(layout,1);

        if(index==totalNum-1&&m_bRestore)
        {
            
            Sprite *AdBg = Sprite::create("Ads_popup_Bg.png");
            AdBg->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
            layout->addChild(AdBg,2);
            
            Label *m_RestoreLbl = createLabelBasedOnLang("Restore Purchase",40);
            m_RestoreLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
//            m_RestoreLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
            m_RestoreLbl->setColor(Color3B::WHITE);
            m_RestoreLbl->enableOutline(Color4B::BLACK,2);
            m_RestoreLbl->setWidth(layout->getContentSize().width/2);
            m_RestoreLbl->setHeight(layout->getBoundingBox().size.height);
            m_RestoreLbl->setPosition(Vec2(layout->getContentSize().width*0.5f-m_RestoreLbl->getContentSize().width/2,AdBg->getBoundingBox().getMidY()-13.0f));
            layout->addChild(m_RestoreLbl,2);
            
            MenuItemSprite *t_BuyButton = getButtonMade("Buy_button.png","Buy_button.png",CC_CALLBACK_0(BuyCoinsPopUp::onRestorePressed,this));
            t_BuyButton->setPosition(Vec2(AdBg->getBoundingBox().getMaxX()-10-t_BuyButton->getBoundingBox().size.width/2,AdBg->getContentSize().height/2));
            t_BuyButton->setTag(index);
            t_BuyButton->setName("Restore");
//            t_BuyButton->setScale(0.9f);

             Label *buy_label =  createLabelBasedOnLang("Restore",32);
            buy_label->setColor(Color3B::WHITE);
            buy_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
            buy_label->enableOutline(Color4B(53,126,32,255),2);
            buy_label->setWidth(t_BuyButton->getBoundingBox().size.width-20);
            buy_label->setHeight(buy_label->getBoundingBox().size.height+20);
            buy_label->setPosition(Vec2(t_BuyButton->getContentSize().width/2,t_BuyButton->getContentSize().height/2+7.0f));
            t_BuyButton->addChild(buy_label,1);
            
            Menu *m_BuyMenu = Menu::create(t_BuyButton,NULL);
            m_BuyMenu->setPosition(Vec2::ZERO);
            layout->addChild(m_BuyMenu,3);

            return;
        }
        
        int item = ItemIndex.at(index);

        Sprite *AdBg = Sprite::create("coins_popup_Bg.png");
        AdBg->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
        layout->addChild(AdBg,2);

        Sprite *CoinAd = Sprite::create("shop-coins.png");
        CoinAd->setPosition(Vec2(AdBg->getBoundingBox().getMinX()+CoinAd->getBoundingBox().size.width/2,AdBg->getBoundingBox().getMidY()+10));
        layout->addChild(CoinAd,2);
        
        std::string Product_name = StringUtils::format("%s",coinList.at(item).c_str());
        Label *coins = Label::createWithSystemFont(Product_name, FONT_NAME,35,Size(0,0));
        coins->setColor(Color3B::WHITE);
        coins->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        coins->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        coins->enableBold();
        coins->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX()+30,AdBg->getBoundingBox().getMidY()+coins->getContentSize().height/2+10));
        layout->addChild(coins,2);
        
        std::string price = StringUtils::format("%s",priceList.at(item).c_str());
         Label *price_label = Label::createWithSystemFont(price, FONT_NAME,35,Size(0,0));
        price_label->setColor(Color3B::WHITE);
        price_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        price_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        price_label->enableBold();
        price_label->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX()+30,AdBg->getBoundingBox().getMidY()-price_label->getContentSize().height/2));
        layout->addChild(price_label,2);


        if(index==1||index==2)
        {
            Sprite *offerSpr = Sprite::create("Discount-rate-image.png");
            offerSpr->setPosition(Vec2(AdBg->getBoundingBox().getMinX()+offerSpr->getBoundingBox().size.width/2-10,AdBg->getBoundingBox().getMidY()+10));
            layout->addChild(offerSpr,2);

            std::string str = StringUtils::format("%d",percent);
            str += "% off";
             Label *off_label = Label::createWithTTF(str.c_str(),FONT_NAME,27,Size(0,100));
            off_label->setColor(Color3B::WHITE);
            off_label->enableOutline(Color4B(129,0,14,255),3);
            off_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
            off_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
            off_label->setRotation(-35);
            off_label->setPosition(Vec2(offerSpr->getBoundingBox().size.width/2-20.0f,offerSpr->getBoundingBox().size.height/2+15.0f));
            offerSpr->addChild(off_label,2);
            
            int actPrice =  getActPrice(float(percent),atoi(priceValue.at(item).c_str()));

            std::string symb = InterfaceManagerInstance::getInstance()->getInterfaceManager()->getCurrencySymbol(GameController::getInstance()->currencycode);
            std::string price = StringUtils::format("%s%d.00",symb.c_str(),actPrice);

            Label *ActPrice = Label::createWithSystemFont(price, FONT_NAME,35,Size(0,100));
            ActPrice->setColor(Color3B::WHITE);
            ActPrice->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
            ActPrice->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
            ActPrice->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX()+30,AdBg->getBoundingBox().getMidY()+10));
            ActPrice->enableBold();
            layout->addChild(ActPrice,2);
        
            float scaleX = (ActPrice->getBoundingBox().size.width+10)/70;
            Sprite *coinstrike = Sprite::create("pixel.png");
            coinstrike->setScale(scaleX,0.046875);
            coinstrike->setPosition(Vec2(ActPrice->getContentSize().width / 2, ActPrice->getContentSize().height/2));
            ActPrice->addChild(coinstrike, 1);
            coins->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX()+30,AdBg->getBoundingBox().getMaxY()-coins->getContentSize().height/2));
            
            price_label->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX()+30,AdBg->getBoundingBox().getMinY()+price_label->getContentSize().height/2+15));

            percent +=10;
        }
 
        std::string name = inAppNames.at(item);
        MenuItemSprite *t_BuyButton = getButtonMade("Buy_button.png","Buy_button.png",CC_CALLBACK_1(BuyCoinsPopUp::OnBuyButtonPressed,this));
        t_BuyButton->setPosition(Vec2(AdBg->getBoundingBox().getMaxX()-10-t_BuyButton->getBoundingBox().size.width/2,AdBg->getBoundingBox().getMidY()));
        t_BuyButton->setTag(index);
        t_BuyButton->setName(name);
        t_BuyButton->setScale(0.9f);

         Label *buy_label =  createLabelBasedOnLang("Buy",35);
        buy_label->setColor(Color3B::WHITE);
        buy_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        buy_label->enableOutline(Color4B(53,126,32,255),2);
        buy_label->setWidth(t_BuyButton->getBoundingBox().size.width-20);
        buy_label->setHeight(buy_label->getBoundingBox().size.height+20);
        buy_label->setPosition(Vec2(t_BuyButton->getContentSize().width/2,t_BuyButton->getContentSize().height/2+7.0f));
        t_BuyButton->addChild(buy_label,1);
        
        Menu *m_BuyMenu = Menu::create(t_BuyButton,NULL);
        m_BuyMenu->setPosition(Vec2::ZERO);
        layout->addChild(m_BuyMenu,3);
        
        t_position.y -= layout->getBoundingBox().size.height;
    }
}
void BuyCoinsPopUp::createRemoveAdsButton(Sprite *Ref,float yPos)
{
        Point pos = Point(Ref->getBoundingBox().getMidX(),Ref->getBoundingBox().getMidY());
    
        Sprite *AdBg = Sprite::create("Ads_popup_Bg.png");
        AdBg->setPosition(Vec2(pos.x,pos.y-10));
        m_RemoveAdNode->addChild(AdBg,2);
    
        Sprite *offerSpr = Sprite::create("Discount-rate-image.png");
    offerSpr->setPosition(Vec2(AdBg->getBoundingBox().getMinX()+offerSpr->getBoundingBox().size.width/2-10,AdBg->getBoundingBox().getMidY()+10));
        m_RemoveAdNode->addChild(offerSpr,3);

            
        Label *off_label = Label::createWithTTF("10% off",FONT_NAME,27,Size(0,100));
        off_label->setColor(Color3B::WHITE);
        off_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        off_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        off_label->enableOutline(Color4B(129,0,14,255),3);
        off_label->setRotation(-35);
       off_label->setPosition(Vec2(offerSpr->getBoundingBox().size.width/2-20.0f,offerSpr->getBoundingBox().size.height/2+15.0f));
        offerSpr->addChild(off_label,2);
    

        int price = 90;
        std::string Adprice = "₹90.00";
        if(!GameController::getInstance()->inAppAdsPriceVal.empty())
        {
            price = atoi(GameController::getInstance()->inAppAdsPriceVal.c_str());
            Adprice = GameController::getInstance()->inAppAdsPrice.c_str();
        }

        int actPrice =  getActPrice(10.0f,price);

        Sprite *CoinAd = Sprite::create("ads-icon.png");
    CoinAd->setPosition(Vec2(AdBg->getBoundingBox().getMinX()+20+CoinAd->getBoundingBox().size.width/2,AdBg->getBoundingBox().getMidY()+10));
        m_RemoveAdNode->addChild(CoinAd,2);
    
        Label *price_label = Label::createWithSystemFont(Adprice,FONT_NAME,35,Size(0,100));
        price_label->setColor(Color3B::WHITE);
        price_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        price_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        price_label->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX()+30,AdBg->getBoundingBox().getMidY()-20));
        price_label->enableBold();
        m_RemoveAdNode->addChild(price_label,2);
        
    std::string symb = InterfaceManagerInstance::getInstance()->getInterfaceManager()->getCurrencySymbol(GameController::getInstance()->currencycode);
        std::string Actprice = StringUtils::format("%s%d.00",symb.c_str(),actPrice);


        Label *ActPriceLbl = Label::createWithSystemFont(Actprice, FONT_NAME,35,Size(0,100));
        ActPriceLbl->setColor(Color3B::WHITE);
        ActPriceLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        ActPriceLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        ActPriceLbl->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX()+30,price_label->getBoundingBox().getMaxY()));
        ActPriceLbl->enableBold();
        m_RemoveAdNode->addChild(ActPriceLbl,2);

    
        float scaleX = (ActPriceLbl->getBoundingBox().size.width+10)/70;
        Sprite *coinstrike = Sprite::create("pixel.png");
        coinstrike->setScale(scaleX,0.046875);
        coinstrike->setPosition(Vec2(ActPriceLbl->getContentSize().width / 2, ActPriceLbl->getContentSize().height/2));
        ActPriceLbl->addChild(coinstrike, 1);
    
    
        t_RemoveAdButton = getButtonMade("Buy_button.png","Buy_button.png",CC_CALLBACK_1(BuyCoinsPopUp::OnButtonPressed,this));
       t_RemoveAdButton->setPosition(Vec2(AdBg->getBoundingBox().getMaxX()-10-t_RemoveAdButton->getBoundingBox().size.width/2,AdBg->getBoundingBox().getMidY()));
        t_RemoveAdButton->setTag(1);

        Label *buy_label = createLabelBasedOnLang("Buy",35);
        buy_label->setColor(Color3B::WHITE);
        buy_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        buy_label->enableOutline(Color4B(53,126,32,255),2);
        buy_label->setWidth(t_RemoveAdButton->getBoundingBox().size.width-20);
        buy_label->setHeight(buy_label->getBoundingBox().size.height+20);
       buy_label->setPosition(Vec2(t_RemoveAdButton->getBoundingBox().size.width/2,t_RemoveAdButton->getBoundingBox().size.height/2+7.0f));
        t_RemoveAdButton->addChild(buy_label,1);
       
        Menu *m_AdMenu = Menu::create(t_RemoveAdButton,NULL);
        m_AdMenu->setPosition(Vec2::ZERO);
        m_RemoveAdNode->addChild(m_AdMenu,3);
}
Label *BuyCoinsPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
void BuyCoinsPopUp::OnButtonPressed(Ref *p_Sender)
{
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    
    int pButtonTag =  Button->getTag();

    switch (pButtonTag)
    {
        case 0:
        {
            status = (char*)"shop_close";
        }break;
        case 1:
        {
            status = (char*)"RemoveAds";
        }break;
        default:
        break;
    }
    
    if (m_fnSelector)
    {
        m_fnSelector();
    }
    
}
// BUTTON PRESSED FUNCTION
void BuyCoinsPopUp::OnBuyButtonPressed(Ref *p_Sender)
{
    if(!InterfaceManagerInstance::getInstance()->getInterfaceManager()->isNetworkAvailable())
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage("check your internet connection");
    
    m_BuyMenu->setEnabled(true);
    if(me_bSound)
        SimpleAudioEngine::getInstance()->playEffect(SFX_BUTTON_CLICKED);
    
    std::string _strVal;
    std::string t_String("");
    MenuItem *t_miMenuItem = (MenuItem*)p_Sender;
    switch (t_miMenuItem->getTag())
    {
        case 0:
            t_iCoinAdded = 500;
            break;
        case 1:
            t_iCoinAdded = 1000;
            break;
        case 2:
            t_iCoinAdded = 2000;
            break;
        case 3:
            t_iCoinAdded = 5000;
            break;
        case 4:
            t_iCoinAdded = 10000;
            break;
        default:
            break;
    }
    
    log("coins-->%d",t_iCoinAdded);
    
    GameController::getInstance()->purchaseThis(t_miMenuItem->getName());
    
    ShowIndicator();
}
void BuyCoinsPopUp::ShowIndicator()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    activityIndicator = CCActivityIndicator::create();
    this->addChild(activityIndicator,200);
    activityIndicator->startAnimating();
#endif
}

void BuyCoinsPopUp::HideIndicator()
{
#if !(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    activityIndicator->stopAnimating();
#endif

}

int BuyCoinsPopUp::getActPrice(float per ,int price)
{
    float discount = (float)((per/100.0f)*(float)price);
    
    int offPrice = price+discount;
    
    
    std::string str = to_string(offPrice);
    if (CC_TARGET_PLATFORM==CC_PLATFORM_IOS) {
           str.erase(0,5);
    }else{
        str.erase(0,3);
    }

    return offPrice;

}
void BuyCoinsPopUp::onCoinPurchaseSuccess()
{
 HideIndicator();
    
 me_iCoinCount += t_iCoinAdded;
 UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
 Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    
    DelayTime *delay = DelayTime::create(0.5f);
    auto cb = CallFuncN::create( [&] (Node* sender)
    {
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    });
    Sequence *seq = Sequence::create(delay,cb,NULL);
    this->runAction(seq);

}
void BuyCoinsPopUp::onCoinPurchaseFailure()
{
    HideIndicator();
}
void BuyCoinsPopUp::onRestorePressed()
{
    GameController::getInstance()->restoreAllProduct();
}
void BuyCoinsPopUp::onRestoreSuccess()
{
    GameController::getInstance()->hideBanner();
    
    DelayTime *delay = DelayTime::create(0.5f);
    auto cb = CallFuncN::create( [&] (Node* sender)
    {
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    });
    Sequence *seq = Sequence::create(delay,cb,NULL);
    this->runAction(seq);

}

BuyCoinsPopUp::~BuyCoinsPopUp()
{
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_PURCHASE_SUCCESS);
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_PURCHASE_FAILURE);

    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_RESTORE_SUCCESS);

    if(m_ptrScrolView)
    {
        m_ptrScrolView->removeAllChildren();
        m_ptrScrolView = NULL;
    }
    this->removeAllChildrenWithCleanup(true);
}
