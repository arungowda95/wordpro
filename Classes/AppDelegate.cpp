/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "AppDelegate.h"
#include "SplashScene.h"
#include "GameConstants.h"
#include "GameController.h"
#include "StatsManager.h"
#include "FacebookManager.h"
#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#include "PluginIAP/PluginIAP.h"
#include "PluginFacebook/PluginFacebook.h"
#endif
 #define USE_AUDIO_ENGINE 0
 #define USE_SIMPLE_AUDIO_ENGINE 1

#if USE_AUDIO_ENGINE && USE_SIMPLE_AUDIO_ENGINE
#error "Don't use AudioEngine and SimpleAudioEngine at the same time. Please just select one in your game!"
#endif

#if USE_AUDIO_ENGINE
#include "audio/include/AudioEngine.h"
using namespace cocos2d::experimental;
#elif USE_SIMPLE_AUDIO_ENGINE
#include "audio/include/SimpleAudioEngine.h"
using namespace CocosDenshion;
#endif

USING_NS_CC;



#ifdef IS_LandScape
static cocos2d::Size designResolutionSize = cocos2d::Size(1136, 768);
#else
static cocos2d::Size designResolutionSize = cocos2d::Size(768, 1136);
#endif

float  m_fScale;
std::string me_Language,me_strScreenName,me_strLeaderBoardJson,me_strUserIdsJson,me_strUserDataJson;
int me_iCoinCount;
bool me_bSound,me_bMusic,me_bNotification,me_bDailyRewardAvailable,me_bShowAds;

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
#if USE_AUDIO_ENGINE
    AudioEngine::end();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::end();
#endif
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs()
{
    // set OpenGL context attributes: red,green,blue,alpha,depth,stencil,multisamplesCount
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8, 0};

    GLView::setGLContextAttrs(glContextAttrs);
}

// if you want to use the package manager to install more packages,  
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    #ifdef SDKBOX_ENABLED
    sdkbox::IAP::setListener(GameController::getInstance());
    sdkbox::IAP::init();
    #endif
    #ifdef SDKBOX_ENABLED
    sdkbox::PluginAdMob::setListener(GameController::getInstance());
    sdkbox::PluginAdMob::init();
    GameController::getInstance()->cacheAds();
    #endif
    #ifdef SDKBOX_ENABLED
    sdkbox::PluginFacebook::setListener(FacebookManager::getInstance());
    sdkbox::PluginFacebook::init();
    #endif
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("wordPro", cocos2d::Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("wordPro");
#endif
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0f / 60);


    
        // Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height,
                                    ResolutionPolicy::NO_BORDER);
    
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    log("resolution-->%f %f",ResolutionSize.size.height,Director::getInstance()->getVisibleSize().height);

    m_fScale = std::min(ResolutionSize.size.width / 720,
            ResolutionSize.size.height /1280);
    
    register_all_packages();

    StatsManager::getInstance()->LoadLevelMapData();
    
    if(!UserDefault::getInstance()->getBoolForKey("isFirstLaunch"))
    {
        UserDefault::getInstance()->setBoolForKey("isFirstLaunch",true);
        UserDefault::getInstance()->setStringForKey(LANGUAGE,LANG_ENGLISH);
        UserDefault::getInstance()->setIntegerForKey(COINS,150);
        UserDefault::getInstance()->setBoolForKey(DAILY_REWARD_AVAILABLE, true);
        UserDefault::getInstance()->setBoolForKey("DailyMissionAvailable", true);

        UserDefault::getInstance()->setBoolForKey("Sound", true);
        UserDefault::getInstance()->setBoolForKey("BgMusic", true);
        UserDefault::getInstance()->setBoolForKey("Notification", true);
        
        UserDefault::getInstance()->setIntegerForKey("level",1);
        
        UserDefault::getInstance()->setIntegerForKey(MISSION_DAY,1);
        
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_START,true);
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT,false);
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE,false);
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_REVEAL,true);
        
        GameController::getInstance()->setBoolForKey("showrate",false);
        GameController::getInstance()->setBoolForKey("rate",false);
        
        GameController::getInstance()->setBoolForKey("isSFXEnable",true);
        GameController::getInstance()->setBoolForKey("isBGMEnable",true);

    }
    
    me_bShowAds = false;

    // create a scene. it's an autorelease object
    auto scene = new SplashScene();
    scene->autorelease();
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    log("app in background");
    
#if USE_AUDIO_ENGINE
    AudioEngine::pauseAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    SimpleAudioEngine::getInstance()->pauseAllEffects();
#endif
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    Director::getInstance()->startAnimation();
    
    log("app in Foreground");

#if USE_AUDIO_ENGINE
    AudioEngine::resumeAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    SimpleAudioEngine::getInstance()->resumeAllEffects();
#endif
}
