//
//  AnswerLetterTile.cpp
//  wordPro-mobile
//
//  Created by id on 25/02/20.
//

#include "AnswerLetterTile.h"
#include "GameLayer.h"
#include "GameConstants.h"
#include "GameController.h"
#include "HudLayer.h"

AnswerLetterTile::AnswerLetterTile(GameLayer *t_ptrGamelayer,Point p_Position, const std::string &t_LetterStr,float scale,bool reward)
{
    m_ptrGameLayer = t_ptrGamelayer;
    m_Position = p_Position;
    m_StringLetter = t_LetterStr;
    m_bHasContent = false;
    m_bIsHint     = false;
    m_bReward     = reward;
    m_CoinSpr     = NULL;
    
    m_Sprite = Sprite::create("text-box.png");
    m_Sprite->setPosition(Vec2(m_Position));
    m_Sprite->setScale(scale);
    m_ptrGameLayer->addChild(m_Sprite,2);
    
    
    if(m_bReward)
    {
        m_CoinSpr = Sprite::create("big-coins.png");
        m_CoinSpr->setPosition(m_Sprite->getContentSize().width/2+5,m_Sprite->getContentSize().height/2+5.5);
        m_Sprite->addChild(m_CoinSpr,1);
        
        RotateBy *rotate = RotateBy::create(0.7f,Vec3(0,45,0));
        RepeatForever *repeat = RepeatForever::create(rotate);
        m_CoinSpr->runAction(repeat);
    }
    
    
    
    Size size = Size(0,200);
    m_tLetter = Label::createWithTTF("",FONT_NAME,55,size);
    
    if(me_Language.compare(LANG_ENGLISH)!=0){
        m_tLetter = Label::createWithSystemFont("",FONT_NAME,60,size);
    }
    
    m_tLetter->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    m_tLetter->setPosition(m_Sprite->getContentSize().width/2+5,m_Sprite->getContentSize().height/2+5.5);
    //    m_tLetter->setColor(Color3B(105,81,52));
    m_tLetter->setColor(Color3B::WHITE);
    //    m_tLetter->setScale(0);
    m_tLetter->setVisible(false);
    m_Sprite->addChild(m_tLetter,2);
    
    if(m_tLetter->getContentSize().width>(m_Sprite->getBoundingBox().size.width-10))
    {
        float scale = (m_Sprite->getBoundingBox().size.width-10)/(m_tLetter->getContentSize().width-5);
        
        m_tLetter->setSystemFontSize(60*scale);
    }
    
    
}
/// RETURN CONTAIN STATUS
bool AnswerLetterTile::HasContain()
{
    return m_bHasContent;
}
/// this Sets letter visible on Right Word
void AnswerLetterTile::setLetterVisible(float p_fDelayTime,bool m_isHint)
{
    {
        if(m_CoinSpr)
        {
            m_CoinSpr->setVisible(false);
        }
        m_bHasContent = true;
        
        float scale = m_Sprite->getScale();
        
        m_tLetter->setString(m_StringLetter);
        
        DelayTime *delay = DelayTime::create(p_fDelayTime);
        ScaleTo *Scale1 = ScaleTo::create(0.1f,scale+0.15f, scale+0.15f);
        ScaleTo *Scale2 = ScaleTo::create(0.1f,scale,scale);
        Sequence *spawn = Sequence::create(Scale1,Scale2, NULL);
        //        EaseElasticIn *t_Ease2 = EaseElasticIn::create(spawn);
        
        CallFunc *callfunc = CallFunc::create([&,this]() {
            m_tLetter->setVisible(true);
            m_Sprite->setColor(Color3B(232,33,41));
            GameController::getInstance()->doParticleEffect(m_ptrGameLayer, m_Sprite->getPosition(),10);
            
        });
        
        m_Sprite->runAction(Sequence::create(delay,spawn,callfunc, NULL));
        
        if(m_bReward)
        {
            Point Pos = Vec2(m_Sprite->getPositionX(),m_Sprite->getPositionY()+35);
            DelayTime *delay  = DelayTime::create(0.4f);
            CallFuncN *_call2 = CallFuncN::create(std::bind(&AnswerLetterTile::CoinAnimation,this,Pos));
            m_ptrGameLayer->runAction(Sequence::create(delay,_call2,NULL));
        }
    }
}
/// THIS SET LETTER VISIBLE on HINT
void AnswerLetterTile::onHintLetterVisible(float p_fDelayTime,bool m_isHint)
{
    {
        if(m_CoinSpr)
        {
            m_CoinSpr->setVisible(false);
        }
        m_bHasContent = true;
        
        m_tLetter->setString(m_StringLetter);
        
        float scale = m_Sprite->getScale();
        
        DelayTime *delay = DelayTime::create(p_fDelayTime);
        ScaleTo *Scale1 = ScaleTo::create(0.1f,scale+0.15f, scale+0.15f);
        ScaleTo *Scale2 = ScaleTo::create(0.1f,scale,scale);
        Sequence *spawn = Sequence::create(Scale1,Scale2, NULL);
        //        EaseElasticIn *t_Ease2 = EaseElasticIn::create(Scale1);
        
        CallFunc *callfunc = CallFunc::create([&,this]() {
            
            m_tLetter->setVisible(true);
            m_Sprite->setColor(Color3B(232,33,41));
        });
        
        m_Sprite->runAction(Sequence::create(delay,spawn,callfunc, NULL));
        
        
        if(m_bReward)
        {
            Point Pos = Vec2(m_Sprite->getPositionX(),m_Sprite->getPositionY()+35);
            DelayTime *delay  = DelayTime::create(0.4f);
            CallFuncN *_call2 = CallFuncN::create(std::bind(&AnswerLetterTile::CoinAnimation,this,Pos));
            m_ptrGameLayer->runAction(Sequence::create(delay,_call2,NULL));
        }
    }
}
/// Coin Animation on finding Coin Reward Word in HUD Layer by passing ans letter tile position
void AnswerLetterTile::CoinAnimation(Point pos)
{
    m_ptrGameLayer->getHudLayer()->coinAddAnimation(Vec2(pos.x,pos.y));
}
/// animates Grids on wrong ans moves left and right tiles
void AnswerLetterTile::animateGrids()
{
    MoveTo *move1 = MoveTo::create(0.1f, Vec2(m_Sprite->getPositionX()-15,m_Sprite->getPositionY()));
    MoveTo *move2 = MoveTo::create(0.1f, Vec2(m_Sprite->getPositionX()+15,m_Sprite->getPositionY()));
    MoveTo *move3 = MoveTo::create(0.1f, Vec2(m_Sprite->getPositionX(),m_Sprite->getPositionY()));
    
    m_Sprite->runAction(Sequence::create(move1,move2,move3, NULL));
}
/// RETURN'S LETTER LABEL
Label *AnswerLetterTile::getLetterLabel()
{
    return m_tLetter;
}
/// RETURN'S  LETTER SPRITE
Sprite *AnswerLetterTile::getAnsSprite()
{
    return m_Sprite;
}

AnswerLetterTile::~AnswerLetterTile()
{
    if(m_tLetter != NULL)
    {
        m_Sprite->removeChild(m_tLetter, true);
        m_tLetter = NULL;
    }
    
    if (m_Sprite != NULL)
    {
        m_ptrGameLayer->removeChild(m_Sprite, true);
        m_Sprite = NULL;
    }
}
