//
//  GameCompletePopUp.h
//  wordPro-mobile
//
//  Created by id on 19/05/20.
//

#ifndef GameCompletePopUp_h
#define GameCompletePopUp_h

#include <stdio.h>
#include "PopUp.h"

class GameCompletePopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    MenuItemSprite *t_NextButton;
    
public:
     GameCompletePopUp(std::function<void()> func);
    ~GameCompletePopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
};

#endif /* GameCompletePopUp_h */
