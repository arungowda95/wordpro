//
//  PopUp.cpp
//  wordPro-mobile
//
//  Created by id on 09/03/20.
//

#include "PopUp.h"
// CONSTRUCTOR
PopUp::PopUp()
{
    
}
// BUTTON PRESSED FUNCTIONS RETURN CALL BACK TO RESPECTIVE LAYER FROM IT CALLED
void PopUp::OnButtonPressed(Ref *p_Sender)
{
    
}
// THIS ENABLE NATIVE ADS ENABLE OR DISABLE
void PopUp::setNativeAdsVisible(bool p_bVisible)
{

}

// RETURN RECT OF POPUP BG IMAGE
Rect PopUp::getPopUpContentRect()
{
    return m_PopUpRect;
}
// DESTRUCTOR
PopUp::~PopUp()
{
    Director::getInstance()->getTextureCache()->removeAllTextures();
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    CCLOG("DEALLOC POPUP");
}

