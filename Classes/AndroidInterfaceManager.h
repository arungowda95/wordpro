//
//  AndroidInterfaceManager.hpp
//  wordPro-mobile
//
//  Created by id on 31/03/20.
//

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include <iostream>
#include "cocos2d.h"
#include "InterfaceManager.h"
using namespace cocos2d;

class AndroidInterfaceManager : public InterfaceManager
{
    JNIEnv *jniEnv;
    jobject activity;
    jclass clazz;
public:
    AndroidInterfaceManager();
    
    ~AndroidInterfaceManager();

    void OpenUrl(std::string url);
    
    void OpenInStore();

    void ShareGame();
    
    void gameFeedBack();
    
    void CallVoidMethod(char *method);
    
    void ShowToastMessage(const std::string &p_Message);
    
    bool appInstalledOrNot(const std::string &package);
    
    void PostFirebaseAnalyticsEvent(const std::string &eventName,const std::string &param1,const std::string &paramVal_1,const std::string &param2,const std::string &paramVal_2);
    
    void PostFirebaseAnalyticsScreen(const std::string &p_ScreenName);
    
    bool isNetworkAvailable();
    
    std::string getCurrencySymbol(const std::string &p_CurrCode);

    void openFbInviteDialog();
    
    void loginWithFaceBook();
    
    bool isFacebookLoggedIn();

    void pushDataToUserdata(const std::string &fbId,const std::string &username,const std::string &profileUrl,const std::string &language,int level,int coins);

    void pushFirebaseUserLanguageData(const std::string &fbId,const std::string &language,int currentLevel,int coins);

    void getLeaderBoardJson(const std::string &language);
    
    void getUserLanguageDataJson(const std::string &fbId);

    void getLanguageDataUserIds();
    
    void checkForGameUpdate();
    
    void initialize();

    void setUserFbId(const std::string &fbId);

    void getUserRank(const std::string &language);
};

#endif
