//
//  TutorialScene.hpp
//  wordPro-mobile
//
//  Created by id on 28/04/20.
//

#ifndef TutorialScene_hpp
#define TutorialScene_hpp

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class TutorialManager:public Node
{
    Node *tempNode;
   
    Size visibleSize;
    Sprite *_touchLayer;
    
    EventListenerTouchOneByOne *listener;
    Vec2 Origin;

    public:
    TutorialManager();
    ~TutorialManager();
    
    CREATE_FUNC(TutorialManager)
    
    void showHintTutorial(Point ArrowPos);
    
    void showShuffleTutorial(Point ArrowPos);
    
    void showRevealButtonTutorial(Point ArrowPos);
    
    void removeTuorial();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);

    /*****************TOUCH DELEGATES FUNCTIONS****************/
    virtual bool onTouchBegan(Touch *p_Touch, Event *p_Event);
    virtual void onTouchMoved(Touch *p_Touch, Event *p_Event);
    virtual void onTouchEnded(Touch *p_Touch, Event *p_Event);
    
    void createTempTutNode();
    
    void createTuorialTouch();
    
    void showTutorialForDailyTaskButton(Point ArrowPos);
    
    void showTutorialForLeaderboard(Point ArrowPos);

};
#endif /* TutorialScene_hpp */
