//
//  GameScene.hpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#ifndef GameScene_hpp
#define GameScene_hpp

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class HudLayer;
class GameScene:public Scene
{
    
    GameLayer *m_ptrGameLayer;
    HudLayer  *m_ptrHudLayer;
public:
    GameScene();
    ~GameScene();
};

#endif /* GameScene_hpp */
