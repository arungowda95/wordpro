//
//  ReportWordPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#include "ReportWordPopUp.h"
ReportWordPopUp::ReportWordPopUp(std::function<void()> func)
{
    m_fnSelector = func;
        
    m_StrName = "Enter Words Here";
    ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.1f);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.2);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);

    m_EditBoxNode = Node::create();
    this->addChild(m_EditBoxNode,1);

    std::string t_Label = "ReportWord";
    Label *t_tLabel =createLabelBasedOnLang(t_Label, 55);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-70);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+20);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tLabel, 2);
    
    if (me_Language.compare(LANG_KANNADA)||me_Language.compare(LANG_BANGLA)) {
        t_tLabel->setSystemFontSize(40);
    }
    
    std::string t_subtext = "ReportMsg";
    Label *t_tSubLabel = createLabelBasedOnLang(t_subtext,35);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,244,55));
    t_tSubLabel->enableOutline(Color4B(76,33,14,255),3);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-20-t_tSubLabel->getBoundingBox().size.height/2));
    this->addChild(t_tSubLabel, 2);
    
    
    float scalex = (m_SprPopUpBg->getBoundingBox().size.width-64)/70;
    Sprite *t_sprEditBoxBg = Sprite::create("pixel.png");
    t_sprEditBoxBg->setScale(scalex,280/70);
//    t_sprEditBoxBg->setColor(Color3B(99,41,24));
    t_sprEditBoxBg->setColor(Color3B::BLACK);
    t_sprEditBoxBg->setOpacity(150);
    t_sprEditBoxBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tSubLabel->getBoundingBox().getMinY()-20-t_sprEditBoxBg->getBoundingBox().size.height/2));
    this->addChild(t_sprEditBoxBg,1);

    
    std::string pNormalSprite = "pixel.png";
    ui::Scale9Sprite *t_SprBox = ui::Scale9Sprite::create(pNormalSprite);
    
    t_EditBox = ui::EditBox::create(Size(t_sprEditBoxBg->getBoundingBox().size.width, t_sprEditBoxBg->getBoundingBox().size.height),t_SprBox);
    t_EditBox->setColor(Color3B(99,41,24));
    t_EditBox->setOpacity(150);
    t_EditBox->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tSubLabel->getBoundingBox().getMinY()-20-t_sprEditBoxBg->getBoundingBox().size.height/2));//;Vec2(384, 512) - m_ZeroPosition);
    t_EditBox->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    t_EditBox->setPositionType(cocos2d::ui::Widget::PositionType::ABSOLUTE);
    t_EditBox->setFontName("arial");
    t_EditBox->setPlaceHolder(m_StrName.c_str());
    t_EditBox->setPlaceholderFontSize(25);
    t_EditBox->setFontSize(30);
    t_EditBox->setFontColor(Color3B::WHITE);
    t_EditBox->setPlaceholderFontColor(Color3B::WHITE);
    t_EditBox->setInputFlag(ui::EditBox::InputFlag::INITIAL_CAPS_WORD);
    t_EditBox->setInputMode(ui::EditBox::InputMode::SINGLE_LINE);
    t_EditBox->setReturnType(ui::EditBox::KeyboardReturnType::DONE);
    t_EditBox->setTag(1);
    t_EditBox->setDelegate(this);
    m_EditBoxNode->addChild(t_EditBox, 2);

    
    MenuItemSprite *t_SendButton = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_0(ReportWordPopUp::sendButtonCallBack,this));
    t_SendButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),10+m_SprPopUpBg->getBoundingBox().getMinY()+t_SendButton->getBoundingBox().size.height));
    t_SendButton->setTag(1);
    
    Label *NxtLabel = createLabelBasedOnLang("Send",50);
    NxtLabel->setColor(Color3B::WHITE);
    NxtLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    NxtLabel->enableOutline(Color4B(53,126,32,255),2);
    NxtLabel->setWidth(t_SendButton->getBoundingBox().size.width-10);
    NxtLabel->setHeight(150);
    NxtLabel->setPosition(Vec2(t_SendButton->getBoundingBox().size.width/2,t_SendButton->getBoundingBox().size.height/2+15));
    t_SendButton->addChild(NxtLabel,1);

    if (me_Language.compare(LANG_MALAYALAM)==0) {
        NxtLabel->setSystemFontSize(40);
    }
    
    // CLOSE BUTTON
    MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(ReportWordPopUp::OnButtonPressed,this));
    t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
    t_miCloseButton->setTag(0);

    Menu *m_Menu = Menu::create(t_miCloseButton,t_SendButton,NULL);
    m_Menu->setPosition(Vec2::ZERO);
    this->addChild(m_Menu,2);
        
}
void ReportWordPopUp::sendButtonCallBack()
{
    printf("\n Entered Text--%s\n",m_EnteredStr.c_str());
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

    
    if(m_EnteredStr.empty())
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage("Please Enter your words");
        return;
    }
    
    if(InterfaceManagerInstance::getInstance()->getInterfaceManager()->isNetworkAvailable())
    {
        std::string level = StatsManager::getInstance()->getCurrentLevelLetters();
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsEvent("reportword","level",level.c_str(),"userReport",m_EnteredStr.c_str());
        
        CallFuncN *_call2 = CallFuncN::create(std::bind(&ReportWordPopUp::closePopUpOnReportSuccess, this,true));
        DelayTime *_delay = DelayTime::create(0.8f);
        FiniteTimeAction *_seq  = Sequence::create(_delay,_call2, NULL);
        this->runAction(_seq);
    }
    else
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage(INTERNET_ERROR);
    }

}
void ReportWordPopUp::closePopUpOnReportSuccess(bool success)
{

    if(success)
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage("Thanks for Your Feedback");
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    }

}
MenuItemSprite *ReportWordPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
      
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}
Label *ReportWordPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
void ReportWordPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    int pButtonTag =  Button->getTag();

      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"close";
          }break;
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}
// EDIT BOX EDITING BEGIN
void ReportWordPopUp::editBoxEditingDidBegin(ui::EditBox* editBox)
{
    CCLOG("1. editBoxEditingDidBegin");
}
// EDIT BOX TEXT CHANGED
void ReportWordPopUp::editBoxTextChanged(ui::EditBox* editBox, const std::string& text)
{
    if(!text.empty())
        m_EnteredStr = text;
        
}
// EDIT BOX RETURN
void ReportWordPopUp::editBoxReturn(ui::EditBox* editBox)
{
    CCLOG("1. editBoxReturn");
    if (t_EditBox)
           t_EditBox->setPlaceHolder(m_StrName.c_str());
}
// EDIT BOX EDITING DONE
void ReportWordPopUp::editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox)
{
    CCLOG("1. editBoxEditingDidEndWithAction");
}
ReportWordPopUp::~ReportWordPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
