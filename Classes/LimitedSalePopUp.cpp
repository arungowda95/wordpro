//
//  LimitedSalePopUp.cpp
//  wordPro-mobile
//
//  Created by id on 23/03/20.
//

#include "LimitedSalePopUp.h"

LimitedSalePopUp::LimitedSalePopUp(std::function<void()> func)
{
    
    m_fnSelector = func;
    t_mSaleTimer = NULL;
    
    Rect ResolutionSize = Director::getInstance()->getSafeAreaRect();
    Size visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    Point origin = ResolutionSize.origin;

    //LIMITED SALE
    m_fTimer = 0.0f;
    m_fSaleTimer = 0.0f;
    m_fSaleTimeDelay = 12*60*60;

        
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.2);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    std::string t_Label = "limitedSale";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-70);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+20);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tLabel, 2);
    
    if(me_Language.compare(LANG_MALAYALAM)==0||me_Language.compare(LANG_TAMIL)==0)
    {
        t_tLabel->setSystemFontSize(45);
    }
    
       std::string t_SubText = "11:58:25";
       t_mSaleTimer = Label::createWithTTF(t_SubText, FONT_NAME,45,Size(m_SprPopUpBg->getBoundingBox().size.width-64, 0));
       t_mSaleTimer->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
       t_mSaleTimer->setColor(Color3B::WHITE);
       t_mSaleTimer->setHeight(t_mSaleTimer->getBoundingBox().size.height);
    t_mSaleTimer->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-10-t_mSaleTimer->getBoundingBox().size.height/2));
        t_mSaleTimer->setVisible(false);
       this->addChild(t_mSaleTimer,2);
       
    
    if(UserDefault::getInstance()->getBoolForKey(LIMITED_SALE_AVAILABLE))
    {
         int tot_S = (int)m_fSaleTimer%60;
         int tot_M = (int)m_fSaleTimer/60;
         int minute = tot_M%60;
         int tot_H = (int)tot_M/60;
         int hour = tot_H%60;
         std::string  buffer = StringUtils::format("%02d:%02d:%02d",hour,minute,tot_S) ;
         t_mSaleTimer->setString(buffer);
         //t_mSaleTimer->setVisible(true);
    }
    
       Sprite *sprBg = Sprite::create("large_box.png");
       sprBg->setScaleY(sprBg->getScaleY()+0.6);
       sprBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_mSaleTimer->getBoundingBox().getMinY()-10-sprBg->getBoundingBox().size.height/2));
       this->addChild(sprBg,2);
    
          Sprite *grabItSpr = Sprite::create("GRAB_IT.png");
          grabItSpr->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMaxY()-10-grabItSpr->getBoundingBox().size.height/2));
          this->addChild(grabItSpr,2);
    
    
    Sprite *sprCoins = Sprite::create("popup-coins-images.png");
    sprCoins->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),grabItSpr->getBoundingBox().getMidY()-sprCoins->getBoundingBox().size.height/2));
    this->addChild(sprCoins,2);
    
    
    std::string t_Coins = "Coins";
    Label *t_tCoinLabel = createLabelBasedOnLang(t_Coins, 50);
    t_tCoinLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCoinLabel->setColor(Color3B::WHITE);
    t_tCoinLabel->enableOutline(Color4B(168,84,23,255),3);
    t_tCoinLabel->setWidth(t_tCoinLabel->getBoundingBox().size.width+20);
    t_tCoinLabel->setHeight(t_tCoinLabel->getBoundingBox().size.height+20);
    t_tCoinLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprCoins->getBoundingBox().getMinY()+30+t_tCoinLabel->getBoundingBox().size.height));
     this->addChild(t_tCoinLabel, 2);
    
    std::string t_CoinsCount = "5000";
    Label *t_tCoinCount = Label::createWithTTF(t_CoinsCount, FONT_NAME,60,Size(0, 0));
    t_tCoinCount->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCoinCount->setColor(Color3B::WHITE);
    t_tCoinCount->enableOutline(Color4B(168,84,23,255),3);
    t_tCoinCount->setHeight(t_tCoinCount->getBoundingBox().size.height);
    t_tCoinCount->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMinY()+10+t_tCoinCount->getBoundingBox().size.height/2));

     this->addChild(t_tCoinCount, 2);

        
 
    // WATCH BUTTTON
      t_miBuyBtn = getButtonMade("large_button_1.png","large_button_1.png",CC_CALLBACK_1(LimitedSalePopUp::buyBtnCallBack,this));
      t_miBuyBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMinY()-10-t_miBuyBtn->getBoundingBox().size.height/2));
      t_miBuyBtn->setTag(0);
       
       std::string t_BuyText = "Buynow";
       Label *t_tBuyLabel = createLabelBasedOnLang(t_BuyText,50.0f);
       t_tBuyLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
       t_tBuyLabel->setColor(Color3B::WHITE);
       t_tBuyLabel->enableOutline(Color4B(25,121,15,255),2);
       t_tBuyLabel->setWidth(t_miBuyBtn->getContentSize().width);
       t_tBuyLabel->setHeight(t_tBuyLabel->getBoundingBox().size.height);
    t_tBuyLabel->setPosition(Vec2(t_miBuyBtn->getBoundingBox().size.width/2,t_miBuyBtn->getBoundingBox().size.height/2+12.0f));
       t_miBuyBtn->addChild(t_tBuyLabel, 2);
    
       std::string t_Price = "₹400.00";
       if(!GameController::getInstance()->inAppLimitedSalePrice.empty())
       {
           t_Price = GameController::getInstance()->inAppLimitedSalePrice.c_str();
       }
       Label *t_tPriceLabel = Label::createWithSystemFont(t_Price, FONT_NAME,45,Size(0, 0));
       t_tPriceLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
       t_tPriceLabel->setColor(Color3B::WHITE);
       t_tPriceLabel->setHeight(t_tPriceLabel->getBoundingBox().size.height);
       t_tPriceLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_miBuyBtn->getBoundingBox().getMinY()-t_tPriceLabel->getBoundingBox().size.height*0.37));
        this->addChild(t_tPriceLabel, 2);
    
    // CLOSE BUTTON
       MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(LimitedSalePopUp::OnButtonPressed,this));
       t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
       t_miCloseButton->setTag(0);

       Menu *m_Menu = Menu::create(t_miCloseButton,t_miBuyBtn,NULL);
       m_Menu->setPosition(Vec2::ZERO);
       this->addChild(m_Menu,2);
    
    	this->schedule(schedule_selector(LimitedSalePopUp::OnUpdateFrame));
    
    auto t_PurchaseSuccess = EventListenerCustom::create(EVENT_PURCHASE_SUCCESS,CC_CALLBACK_0(LimitedSalePopUp::onCoinPurchaseSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_PurchaseSuccess, this);
    
    auto t_PurchaseFailure = EventListenerCustom::create(EVENT_PURCHASE_FAILURE, CC_CALLBACK_0(LimitedSalePopUp::onCoinPurchaseFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_PurchaseFailure, this);

    
}
void LimitedSalePopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
      
      int pButtonTag =  Button->getTag();

      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"close";
          }break;
          case 1:
          {
              status = (char*)"Shop";
          }break;
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}
Label *LimitedSalePopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

void LimitedSalePopUp::buyBtnCallBack(Ref *p_Sender)
{
   GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
   GameController::getInstance()->purchaseThis("coins_deal");
    
   ShowIndicator();
}
void LimitedSalePopUp::onCoinPurchaseSuccess()
{
    HideIndicator();
    me_iCoinCount += 5000;
    UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    
    DelayTime *delay = DelayTime::create(0.3f);
    auto cb = CallFuncN::create( [&] (Node* sender)
    {
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    });
    Sequence *seq = Sequence::create(delay,cb,NULL);
    this->runAction(seq);

}
void LimitedSalePopUp::onCoinPurchaseFailure()
{
    HideIndicator();
}
void LimitedSalePopUp::ShowIndicator()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    activityIndicator = CCActivityIndicator::create();
    this->addChild(activityIndicator,200);
    activityIndicator->startAnimating();
#endif
}

void LimitedSalePopUp::HideIndicator()
{
#if !(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    activityIndicator->stopAnimating();
#endif

}

MenuItemSprite *LimitedSalePopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
   Sprite *_normalSpr = Sprite::create(normalSpr);
   
   Sprite *_SelectedSpr = Sprite::create(selSpr);
   _SelectedSpr->setOpacity(150);
   
   MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
   return Button;
}
/// UPDATE FRAME CHECK FOR TIME
void LimitedSalePopUp::OnUpdateFrame(float p_fDeltaTime)
{
    {
        m_fTimer += p_fDeltaTime;
        if(m_fTimer >= 1.0f)
        {
            m_fTimer = 0.0f;
            
            if(UserDefault::getInstance()->getBoolForKey(LIMITED_SALE_AVAILABLE))
            {
                showlimitedSaleTimer();
            }
        }
    }
}
void LimitedSalePopUp::showlimitedSaleTimer()
{
  // GET BEGIN TIME
        std::string strTime = UserDefault::getInstance()->getStringForKey(SALE_START_TIME);
        struct tm target;
        strptime(strTime.c_str(),"%c", &target);

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        m_SaleStart = timelocal(&target);
    #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        // THIS SET DAYLIGHT SET TIME STATUS UNKOWN
        target.tm_isdst = -1;
        //CCLOG("DST %d", target.tm_isdst);
        m_SaleStart = mktime(&target);
        //CCLOG("START TIME %s",ctime(&m_start));
    #endif
        // GET END TIME
        time(&m_SaleCurrent);
        
        //CCLOG("CURRENT TIME %s",ctime(&m_Current));
        
        double diff1 = difftime(m_SaleCurrent, m_SaleStart);
        
        //CCLOG("TIME DIFFERENCE %lf",diff1);
        
        if(diff1>m_fSaleTimeDelay)
        {
            // UPDATE BONUS AVAILABLE
            UserDefault::getInstance()->setBoolForKey(LIMITED_SALE_AVAILABLE, false);
            UserDefault::getInstance()->flush();
        }
        
        m_fSaleTimer = m_fSaleTimeDelay-(int)diff1;
        
        if(diff1<0)
            diff1*=-1;
        
        // CHECK IF TIME_DIFF IS LESS THAN ASSIGN_TIME_DELAY THAN SHOW TIMER
        if(m_fSaleTimer>=0 && diff1>0)
        {
            UpdateLimitedSaleTimer();
        }
}
// THIS UPDATE BONUS TIMER
void LimitedSalePopUp::UpdateLimitedSaleTimer()
{
    if(t_mSaleTimer != NULL)
    {
        int tot_S = (int)m_fSaleTimer%60;
        int tot_M = (int)m_fSaleTimer/60;
        int minute = tot_M%60;
        int tot_H = (int)tot_M/60;
        int hour = tot_H%60;
        std::string  buffer = StringUtils::format(" %02d:%02d:%02d",hour,minute,tot_S) ;
        t_mSaleTimer->setString(buffer);
        t_mSaleTimer->setVisible(true);
    }
}
LimitedSalePopUp::~LimitedSalePopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
