//
//  BuyCoinsPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 16/03/20.
//

#ifndef BuyCoinsPopUp_h
#define BuyCoinsPopUp_h

#include <stdio.h>
#include "cocos2d.h"
#include "PopUp.h"
#include "ui/UIListView.h"
#include "ui/CocosGUI.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "CCActivityIndicator.h"
#endif

using namespace cocos2d::ui;

using namespace cocos2d;

class BuyCoinsPopUp:public PopUp
{
    Sprite* m_SprPopUpBg,* m_SprPopUpTop;

    Menu *m_BuyMenu;
    Node *m_RemoveAdNode;
    ScrollView *m_ptrScrolView;
    
    Label *m_RemoveAdLbl;
    Sprite *m_RemoveAdBg,*m_coinsBg;
    
    MenuItemSprite *t_RemoveAdButton;
    
    Size itemSize;
    
    int t_iCoinAdded;
    std::vector<std::string> coinList = {"500 Coins","1000 Coins","2000 Coins","5000 Coins","10000 Coins"};

    std::vector<std::string> inAppNames = {"coins_500","coins_1000","coins_2000","coins_5000","coins_10000"};
    
    std::vector<std::string> priceList = {"₹129.00","₹199.00","₹299.00","₹499.00","₹750.00"};
    
    std::vector<std::string> priceValue = {"129.00","199.00","299.00","499.00","750.00"};
    
    std::vector<int> ItemIndex = {3,0,2,4,1};
    

    std::string currCode = "₹";
    
    #if !(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        CCActivityIndicator * activityIndicator;
    #endif
    
    float totalNum;

    bool m_bRestore;
public:
    BuyCoinsPopUp(std::function<void()> func);
    ~BuyCoinsPopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);
    
    void createRemoveAdsButton(Sprite *Ref,float Ypos);
    
    void addBuyButton(Point Pos,bool bestOffer);
    
    void OnBuyButtonPressed(Ref *p_Sender);
    
    int getActPrice(float per ,int price);
    
    void onCoinPurchaseSuccess();
    
    void onCoinPurchaseFailure();

    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    void ShowIndicator();
    
    void HideIndicator();
    
    void onRestorePressed();
    
    void onRestoreSuccess();
};

#endif /* BuyCoinsPopUp_hpp */
