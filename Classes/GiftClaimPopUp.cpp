//
//  GiftClaimPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#include "GiftClaimPopUp.h"

GiftClaimPopUp::GiftClaimPopUp(std::function<void()> func)
{
    
    m_fnSelector  = func;
    t_ClaimButton = NULL;
    
    Rect Res = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(Res.size.width,Res.size.height);
    origin = Res.origin;
           
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);

    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.2);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);

   
    m_PopUpRect = m_SprPopUpBg->getBoundingBox();
    
  
    std::string name = "GiftMsg";
    Label *t_tSubLabel = createLabelBasedOnLang(name, 45);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,255,255));
    t_tSubLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()-30-t_tSubLabel->getBoundingBox().size.height/2));
    this->addChild(t_tSubLabel, 2);

    
    Sprite *sprBg = Sprite::create("large_box.png");
    sprBg->setScaleY(sprBg->getScaleY()+0.3);
    sprBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()+55));
    sprBg->setVisible(false);
    this->addChild(sprBg,2);
    
    
    Sprite *sprCoins = Sprite::create("popup-coins-images.png");
    sprCoins->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMidY()));
    this->addChild(sprCoins,2);
    
    Posy = sprCoins->getPositionY()-50;
    
    m_tCoinsCount = UserDefault::getInstance()->getIntegerForKey(PREDICTION_COINS,25);
    std::string t_Coin =  StringUtils::format("X %d",m_tCoinsCount);
    Label *t_tCoinText = Label::createWithTTF(t_Coin,FONT_NAME,60,Size(0,0));
    t_tCoinText->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCoinText->setPosition(Vec2(sprCoins->getBoundingBox().getMidX(),sprCoins->getBoundingBox().getMinY()+20+t_tCoinText->getBoundingBox().size.height/2));
    t_tCoinText->setColor(Color3B::WHITE);
    t_tCoinText->enableOutline(Color4B(206,85,44,255),2);
    t_tCoinText->setHeight(t_tCoinText->getBoundingBox().size.height);
    this->addChild(t_tCoinText, 2);
    
    // CLAIM BUTTON
    t_ClaimButton = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_0(GiftClaimPopUp::claimButtonPressed,this));
    t_ClaimButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()+10+t_ClaimButton->getBoundingBox().size.height));
    t_ClaimButton->setTag(1);
    
    Label *NxtLabel = createLabelBasedOnLang("Claim",35);
    NxtLabel->setColor(Color3B::WHITE);
    NxtLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    NxtLabel->setDimensions(t_ClaimButton->getContentSize().width-10,150);
    NxtLabel->enableOutline(Color4B(53,126,32,255),2);
    NxtLabel->setPosition(Vec2(t_ClaimButton->getBoundingBox().size.width/2,t_ClaimButton->getBoundingBox().size.height/2+15));
    t_ClaimButton->addChild(NxtLabel,1);

    
       Menu *m_Menu = Menu::create(t_ClaimButton,NULL);
       m_Menu->setPosition(Vec2::ZERO);
       this->addChild(m_Menu,2);
}
Label *GiftClaimPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize, Size(0, 0));
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize, Size(0, 0));
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

void GiftClaimPopUp::claimButtonPressed()
{
    me_iCoinCount += m_tCoinsCount;
    UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
    UserDefault::getInstance()->flush();
    
    if(t_ClaimButton){
        t_ClaimButton->setEnabled(false);
        t_ClaimButton->setOpacity(150);
    }
    
    FiniteTimeAction *_call2 = CallFuncN::create(CC_CALLBACK_0(GiftClaimPopUp::coinAddAnimation, this));
    DelayTime *_delay = DelayTime::create(1.8f);
    FiniteTimeAction *_call3 = CallFuncN::create(CC_CALLBACK_0(GiftClaimPopUp::coinAnimationDoneClosePopUp,this));
    FiniteTimeAction *_seq  = Sequence::create(_call2,_delay,_call3, NULL);
    this->runAction(_seq);

}
void GiftClaimPopUp::OnButtonPressed(Ref *p_Sender)
{
   MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
   int pButtonTag =  Button->getTag();

     switch (pButtonTag)
     {
         case 0:
         {
             status = (char*)"close";
         }break;
          default:
       break;
     }
   
   if (m_fnSelector!=NULL) {
       m_fnSelector();
   }

}

MenuItemSprite *GiftClaimPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
      Sprite *_normalSpr = Sprite::create(normalSpr);
      
      Sprite *_SelectedSpr = Sprite::create(selSpr);
      _SelectedSpr->setOpacity(150);
      
      MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
      return Button;
}
void GiftClaimPopUp::coinAddAnimation()
{
        Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
        Point _startPosOppo = Point(m_SprPopUpBg->getBoundingBox().getMidX(),Posy);
            for (int i=0; i< 20; i++)
            {
                Sprite *_spr = Sprite::create("coins.png");
                _spr->setScale(_spr->getScale()+0.2);
                _spr->setPosition(_startPosOppo);
                _spr->setOpacity(0);
                ccBezierConfig bezier;
                bezier.controlPoint_1 = _startPosOppo;
                bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
                bezier.endPosition =_endPos;
                BezierTo *_bez = BezierTo::create(.5f, bezier);
                this->addChild(_spr,5);
                FadeTo *_fade = FadeTo::create(.04*i,255);
                FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(GiftClaimPopUp::removeSprite,this));
                FiniteTimeAction*_seq = Sequence::create(_fade,_bez,_call,NULL);
                _spr->runAction(_seq);
            }

}
void GiftClaimPopUp::removeSprite(Ref *_sender)
{
    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
    
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);

    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    
}
void GiftClaimPopUp::coinAnimationDoneClosePopUp()
{
    status = (char*)"close";
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
    
    GameController::getInstance()->showInterstial();
}
GiftClaimPopUp::~GiftClaimPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
