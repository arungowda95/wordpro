//
//  FacebookInfoPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 15/07/20.
//

#include "FacebookInfoPopUp.h"
FacebookInfoPopUp::FacebookInfoPopUp(std::function<void()> func)
{
      m_fnSelector = func;

      Rect Res = Director::getInstance()->getSafeAreaRect();
      visibleSize = Size(Res.size.width,Res.size.height);
      origin = Res.origin;
              
      m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
      m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
      m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.25);
      m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
      this->addChild(m_SprPopUpBg,1);
      
//      m_SprPopUpTop = Sprite::create("PopUp_Top.png");
//      m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleY()+0.15);
//      m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
//      this->addChild(m_SprPopUpTop,2);
//
       
//       std::string t_Label = "Rate Us";
//       Label *t_tLabel = createLabelBasedOnLang(t_Label, 60);
//       t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
//       t_tLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
//       t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+10);
//       t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMidY()));
//       t_tLabel->setColor(Color3B(255,244,55));
//       t_tLabel->enableOutline(Color4B(196,56,63,255),3);
//       this->addChild(t_tLabel, 2);
       
       Sprite *sprBg = Sprite::create("large_box.png");
       sprBg->setScaleY(sprBg->getScaleY()+0.3);
       sprBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()-20-sprBg->getBoundingBox().size.height/2));
       this->addChild(sprBg,2);
    
      Sprite *grabItSpr = Sprite::create("connect_Get.png");
        grabItSpr->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
      grabItSpr->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMinY()+20));
      this->addChild(grabItSpr,2);
       
       std::string t_Label1 = "FbInfo";
       Label *t_tSubRateLabel = createLabelBasedOnLang(t_Label1,30);
       t_tSubRateLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
       t_tSubRateLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
       t_tSubRateLabel->setHeight(t_tSubRateLabel->getBoundingBox().size.height+10);
       t_tSubRateLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprBg->getBoundingBox().getMinY()-20-t_tSubRateLabel->getBoundingBox().size.height/2));
       
       t_tSubRateLabel->setColor(Color3B(255,255,255));
       this->addChild(t_tSubRateLabel, 2);
    
    // CLOSE BUTTON
    MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(FacebookInfoPopUp::OnButtonPressed,this));
    t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
    t_miCloseButton->setTag(0);

    Menu *m_Menu = Menu::create(t_miCloseButton,NULL);
    m_Menu->setPosition(Vec2::ZERO);
    this->addChild(m_Menu,2);

}
   
void FacebookInfoPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    
    int pButtonTag =  Button->getTag();

    switch (pButtonTag)
    {
        case 0:
        {
            status = (char*)"close";
        }break;
        default:
            status = (char*)"close";
            break;
    }
    
    if(m_fnSelector != NULL)
    {
        m_fnSelector();
    }

}

MenuItemSprite *FacebookInfoPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;

    
}
Label *FacebookInfoPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
    

}
FacebookInfoPopUp::~FacebookInfoPopUp()
{
    
}
