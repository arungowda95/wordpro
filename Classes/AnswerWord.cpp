//
//  AnswerWord.cpp
//  wordPro-mobile
//
//  Created by id on 24/02/20.
//

#include "AnswerWord.h"
#include "GameLayer.h"
#include "AnswerLetterTile.h"
#include <iostream>
#include <codecvt>
#include <regex>
#include "GameConstants.h"
#include "OptionGrid.h"
#include "GameController.h"

#define GRID_SIZE 120

AnswerWord::AnswerWord(GameLayer *t_ptrGamelayer,const std::string &p_Word, Point t_Postion, int p_iWordIndex,float scale,bool reward)
{
    
    m_ptrGameLayer = t_ptrGamelayer;
    m_Position     = t_Postion;
    m_iWordIndex   = p_iWordIndex;
    m_bIsContain   = false;
    m_StrWord      = p_Word;
    m_AnswerTileArray = new Vector<AnswerLetterTile*>;
    m_bCoinReward  = reward;
    t_fGridSize = Size(111*scale,115*scale);
    
    std::vector<std::string> t_TempArray =  GameController::getInstance()->split(p_Word,',');
    float t_iWordCount = (float)t_TempArray.size();
    
    std::string letter = "";
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    Size visibleSize = ResolutionSize.size;
    Vec2 Origin = ResolutionSize.origin;
    
    
    float Totgridwidth = t_fGridSize.width*t_iWordCount;
    float width = 0;//59+3
    if (Totgridwidth>(visibleSize.width-40))
    {
        
        float emptyspace = Totgridwidth - (visibleSize.width-40);
        
        float x = emptyspace/t_iWordCount;
        
        width = t_fGridSize.width-x;
        scale = (width)/t_fGridSize.width;
        
        t_fGridSize.width = width;
    }
    
    
    float midPoint = (Origin.x+visibleSize.width/2) - (t_iWordCount*t_fGridSize.width)/2.0f;
    m_Position.x = midPoint;
    
    std::string t_TempWord = p_Word;
    t_TempWord.erase(remove(t_TempWord.begin(), t_TempWord.end(), ','), t_TempWord.end());
    
    for(int i =0; i<t_TempArray.size(); i++)
    {
        m_Position.x += t_fGridSize.width/2;
        letter = t_TempArray.at(i);
        createAnswerLetterTile(m_Position,letter,scale,m_bCoinReward);
        m_Position.x += t_fGridSize.width/2;
    }
    
    
}

///  Creates Answer Letter tiles
void AnswerWord::createAnswerLetterTile(Point p_Position, const std::string &p_Letter,float scale,bool reward)
{
    AnswerLetterTile *t_PtrAnswerTile = new AnswerLetterTile(m_ptrGameLayer, p_Position, p_Letter,scale,reward);
    m_AnswerTileArray->pushBack(t_PtrAnswerTile);
    t_PtrAnswerTile->release();
    t_PtrAnswerTile = NULL;
    
}
// THIS RETURN EMPTY ANSWER LETTER TILE OBJECT
AnswerLetterTile* AnswerWord::GetEmptyTile()
{
    AnswerLetterTile *returnTile = NULL;
    for (int i = 0; i<m_AnswerTileArray->size(); i++)
    {
        AnswerLetterTile *t_AnswerLetterTile = (AnswerLetterTile*)m_AnswerTileArray->at(i);
        if(!t_AnswerLetterTile->HasContain())
        {
            returnTile = t_AnswerLetterTile;
            break;
        }
    }
    return returnTile;
}
///  THIS RETURN CONTAIN STATUS OF LETTER
bool AnswerWord::HasContain()
{
    bool t_bHasContain = false;
    for (int i = 0; i<m_AnswerTileArray->size(); i++)
    {
        AnswerLetterTile *t_Letter = (AnswerLetterTile*)m_AnswerTileArray->at(i);
        if(t_Letter->HasContain())
        {
            t_bHasContain = true;
        }else
        {
            t_bHasContain = false;
        }
    }
    return t_bHasContain;
    
}
/// Fill The  Ans tile on correct ans
void AnswerWord::fillLettersOnCorrectAns()
{
    AnswerLetterTile *t_AnswerLetterTile = NULL;
    for (int i = 0; i<m_AnswerTileArray->size(); i++)
    {
        t_AnswerLetterTile = (AnswerLetterTile*)m_AnswerTileArray->at(i);
        if(!t_AnswerLetterTile->HasContain())
        {
            t_AnswerLetterTile->setLetterVisible(i*0.1f,false);
            m_ptrGameLayer->getOptionGrid()->updateOptionGridsAndDictionary(t_AnswerLetterTile->m_StringLetter);
        }
    }
}
/// Animates Answer Word Grid on repeted word found
void AnswerWord::AnimateGridsonRepeat()
{
    AnswerLetterTile *t_AnswerLetterTile = NULL;
    for (int i = 0; i<m_AnswerTileArray->size(); i++)
    {
        t_AnswerLetterTile = (AnswerLetterTile*)m_AnswerTileArray->at(i);
        
        t_AnswerLetterTile->animateGrids();
    }
}
/// returns answer word to be found
std::string AnswerWord::getAnsWordString()
{
    return m_StrWord;
}
AnswerWord::~AnswerWord()
{
    if(m_AnswerTileArray != NULL)
    {
        m_AnswerTileArray->clear();
        delete m_AnswerTileArray;
        m_AnswerTileArray = NULL;
    }
}
