//
//  GameLayer.cpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#include "GameLayer.h"
#include "HudLayer.h"
#include "OptionGrid.h"
#include "AnswerGrid.h"
#include "LevelManager.h"
#include "GameStateManager.h"
#include "GameConstants.h"
#include "StatsManager.h"
#include "AnswerWord.h"
#include "OptionLetterTile.h"
#include "GameController.h"
#include "LanguageTranslator.h"

GameLayer::GameLayer()
{
    m_ptrOptionGrid   = NULL;
    m_ptrAnswerGrid   = NULL;
    m_ptrLevelManager = NULL;
    gameBg            = NULL;
    m_tutorNode       = NULL;
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    Origin = ResolutionSize.origin;
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    
    GameController::getInstance()->playSFX(SFX_GAMEPLAY_ENTER);
    gameBg = Sprite::create("ingame-bg.jpg");
    //    gameBg->setScale((visibleSize.width)/gameBg->getContentSize().width,visibleSize.height/gameBg->getContentSize().height);
    gameBg->setScale(m_fScale);
    gameBg->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
    this->addChild(gameBg);
    
    AnswersBg = Sprite::create("AnswerLetterBg.png");
    AnswersBg->setScale((visibleSize.width-64)/AnswersBg->getContentSize().width,m_fScale);
    AnswersBg->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height-110-AnswersBg->getBoundingBox().size.height/2));
    AnswersBg->setColor(Color3B(85,221,224));
    AnswersBg->setOpacity(0);
    this->addChild(AnswersBg);
    
    std::string StrCountry = StatsManager::getInstance()->getCurrentCountry();
    
    std::string placeStr = StringUtils::format("In_Regions/%s.png",StatsManager::getInstance()->getCurrentState().c_str());
    
    std::string emptyPlace = "In_Regions/Delhi.png";
    if(StrCountry!="India")
    {
        placeStr = StringUtils::format("%s/%s.png",StrCountry.c_str(),StatsManager::getInstance()->getCurrentState().c_str());
        emptyPlace = "Countries/"+StrCountry+".png";
    }
    
    placeBg = Sprite::create(placeStr);
    if(placeBg==NULL)
        placeBg = Sprite::create(emptyPlace);
    
    placeBg->setScale(placeBg->getScale()+0.1);
    placeBg->setPosition(Vec2(Origin.x+visibleSize.width/2,AnswersBg->getBoundingBox().getMidY()));
    this->addChild(placeBg);
    
    placeStr = StatsManager::getInstance()->getCurLocation();
    
    if(StrCountry=="India")
    {
        placeStr = StatsManager::getInstance()->getCurLocation();
    }
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        std::transform(placeStr.begin(), placeStr.end(), placeStr.begin(), ::toupper);
        placeLbl = Label::createWithTTF(placeStr,FONT_NAME,50,Size(0,100));
    }
    else
    {
        placeLbl = Label::createWithSystemFont(placeStr,FONT_NAME,50);
        placeLbl->setWidth(placeLbl->getBoundingBox().size.width+10);
        placeLbl->setHeight(placeLbl->getBoundingBox().size.height+10);
    }
    
    placeLbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    placeLbl->enableOutline(Color4B(165,103,78,255),2);
    placeLbl->setColor(Color3B(108,52,28));//161,101,77
    placeLbl->setPosition(Vec2(Origin.x+visibleSize.width/2,AnswersBg->getBoundingBox().getMaxY()-15));
    this->addChild(placeLbl);
    
    if(placeLbl->getContentSize().width>(visibleSize.width))
    {
        float scale = (visibleSize.width)/(placeLbl->getContentSize().width);
        
        placeLbl->setSystemFontSize(50*scale);
        
        CCLOG("\nBIGGER_SIZE\n");
    }
    
    
    m_ptrOptionGrid   = new OptionGrid(this);
    m_ptrAnswerGrid   = new AnswerGrid(this);
    m_ptrLevelManager = new LevelManager(this);
    
    if(UserDefault::getInstance()->getBoolForKey(TUTORIAL_START))
    {
        UserDefault::getInstance()->setIntegerForKey("firstTutNum",0);
        checkforFirstLevelTutorial();
    }
}
/************************************TOUCH DELEGATE FUNCTIONS*****************************/
// TOUCH BEGAN
bool GameLayer::onTouchBegan(Touch *p_Touch, Event *p_Event)
{
    bool b_enable = true;
    Vec2 t_Location = p_Touch->getLocationInView();
    t_Location = Director::getInstance()->convertToGL(t_Location);
    t_Location = this->convertToNodeSpace(t_Location);
    
    m_pStartPos = t_Location;
    
    if(GameStateManager::getInstance()->getGameState() ==GAME_STATE_PLAY)
        m_ptrOptionGrid->onTouchBeganOptionGrid(t_Location);
    
    return b_enable;
}
// TOUCH MOVED
void GameLayer::onTouchMoved(cocos2d::Touch *p_Touch, cocos2d::Event *p_Event)
{
    Vec2 t_Location = p_Touch->getLocationInView();
    t_Location = Director::getInstance()->convertToGL(t_Location);
    t_Location = this->convertToNodeSpace(t_Location);
    
    {
        if(m_ptrOptionGrid)
        {
            if( GameStateManager::getInstance()->getGameState() ==GAME_STATE_PLAY)
            {
                m_ptrOptionGrid->onTouchMoveOptionGrid(t_Location);
            }
        }
    }
    
}
// TOUCH ENDED
void GameLayer::onTouchEnded(cocos2d::Touch *p_Touch, cocos2d::Event *p_Event)
{
    Vec2 t_Location = p_Touch->getLocationInView();
    t_Location = Director::getInstance()->convertToGL(t_Location);
    t_Location = this->convertToNodeSpace(t_Location);
    
    if(m_ptrOptionGrid)
    {
        if(GameStateManager::getInstance()->getGameState()==GAME_STATE_PLAY)
            m_ptrOptionGrid->OnTouchEnded(t_Location);
    }
}
/// shows tutorial for First level of game
void GameLayer::checkforFirstLevelTutorial()
{
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    
    m_tutorNode = Node::create();
    m_tutorNode->setPosition(Vec2::ZERO);
    this->addChild(m_tutorNode,3);
    
    float xscale = visibleSize.width/64.0f;
    float yscale = visibleSize.height/64.0f;
    m_SprBgAlpha = Sprite::create("pixel.png");
    m_SprBgAlpha->setScale(xscale,yscale);
    m_SprBgAlpha->setColor(Color3B::BLACK); //setColor(Color3B::BLACK);
    m_SprBgAlpha->setOpacity(200);
    m_SprBgAlpha->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
    m_tutorNode->addChild(m_SprBgAlpha,1);
    
    m_SprTutorBox = Sprite::create("Tutorials-box.png");
    m_SprTutorBox->setScale(m_fScale);
    m_SprTutorBox->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2+120));
    m_tutorNode->addChild(m_SprTutorBox,1);
    
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag("Tutorfirst");
    wordslbl = Label::createWithSystemFont(langstr,FONT_NAME, 45);
    wordslbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    wordslbl->setColor(Color3B::WHITE);
    wordslbl->setWidth(m_SprTutorBox->getBoundingBox().size.width-20);
    wordslbl->setHeight(150);
    wordslbl->setPosition(Vec2(m_SprTutorBox->getContentSize().width/2,m_SprTutorBox->getContentSize().height/2+10));
    m_SprTutorBox->addChild(wordslbl,1);
    
    
    if(me_Language.compare(LANG_TAMIL)==0)
    {
        wordslbl->setSystemFontSize(30);
    }
    
    getOptionGrid()->setLocalZorderForTutorial();
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
    
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsEvent("Tutorial_Start","", "", "","");
    
}
/// removes First level tutorial Node
void GameLayer::removeTutorial()
{
    if(m_tutorNode!=NULL)
    {
        m_tutorNode->removeAllChildren();
        this->removeChild(m_tutorNode,true);
        m_tutorNode = NULL;
    }
    
    if(getHudLayer())
        getHudLayer()->setEnableButtons(true);
}
// THIS SET HUDLAYER
void GameLayer::setHudLayer(Layer *p_PtrHudlayer)
{
    m_ptrHudLayer = (HudLayer*)p_PtrHudlayer;
}
/// RETURN HUDLAYER OBJECT
HudLayer* GameLayer::getHudLayer()
{
    return m_ptrHudLayer;
}
/// RETURN OptionGrid OBJECT
OptionGrid* GameLayer::getOptionGrid()
{
    return m_ptrOptionGrid;
}
/// RETURN ANSWERGRID OBJECT
AnswerGrid* GameLayer::getAnswerGrid()
{
    return m_ptrAnswerGrid;
}
/// Returns Level Manager Object
LevelManager* GameLayer::getLevelManager()
{
    return m_ptrLevelManager;
}
GameLayer::~GameLayer()
{
    if(m_ptrOptionGrid != NULL)
    {
        m_ptrOptionGrid->release();
        m_ptrOptionGrid = NULL;
    }
    
    if(m_ptrAnswerGrid!= NULL)
    {
        m_ptrAnswerGrid->release();
        m_ptrAnswerGrid = NULL;
    }
    
    if(m_ptrLevelManager!= NULL)
    {
        m_ptrLevelManager->release();
        m_ptrLevelManager = NULL;
    }
    
    this->removeAllChildrenWithCleanup(true);
    
}
