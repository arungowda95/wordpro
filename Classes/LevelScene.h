//
//  LevelScene.hpp
//  wordPro-mobile
//
//  Created by id on 27/02/20.
//

#ifndef LevelScene_hpp
#define LevelScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/UIListView.h"
#include "ui/CocosGUI.h"

#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"

using namespace cocos2d;
using namespace rapidjson;
using namespace cocos2d::ui;

class LevelLayer;
class LevelScene:public Scene
{
    LevelLayer *m_ptrLevelLayer;
    
public:
    LevelScene();
    ~LevelScene();
};
#pragma mark LEVEL LAYER

class PopUpManager;
class LevelLayer:public Layer
{
    ScrollView      *m_ptrScrolView;
    PopUpManager    *m_ptrPopUpManager;
    MenuItemSprite  *m_BackButton,*m_SettingsButton,*m_AchievementButton,*m_ShopButton,*m_CoinsButton;
    Size visibleSize;
    Vec2 origin;
    Sprite *bgSprite;
    Label *m_CoinCount;
    
    Sprite *m_SpSale;
    
    Vector<ui::Button*> *levelButtonsArray;
    
    Vector<MenuItemImage*> *giftButtonArray;
    
    
    bool m_bIsEventCreated;
    
    int m_iStatesTotCount;
    
    std::vector<std::string> m_StatesArray;
    std::vector<std::string> m_StatesRegArray;
    
    std::vector<std::string> m_StatesTransArray;
    std::vector<std::string> m_StatesRegTransArray;
    
    std::string m_StrCountry;
    int t_iGiftTag = 0;
    
    
    Sprite *m_SprCloud;
    std::map<std::string,std::string> m_RegionsTransData;
public:
    LevelLayer();
    ~LevelLayer();
    
    ///  Create Level Map Scroll View
    void createLevelMapScroll();
    
    /// level button callback
    void levelBtnCallBack(Ref *sender);
    
    /// custom menu item button
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);

    
    /****************************KEYPAD FUNCTIONS********************/
    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* unused_event);
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* unused_event);
    
    /// UI Buttons callBack
    void backBtnCallback(Ref *sender);
    void settingsBtnCallback(Ref *sender);
    void achievementBtnCallback(Ref *sender);
    void shopBtnCallback(Ref *sender);
    void addCoinBtnCallback(Ref *sender);
    
    
    void setEnableButtons(bool p_bEnable);
    
    /// THIS WILL REMOVE CURRENT ONE AND CALL ANOTHER POPUP WAIT FOR ANIMATION OF CURRENT POPUP
    /// @param p_fDelayTime delayTime
    /// @param p_PopUpName name of popup
    void LoadOtherPopUpWithDelay(float p_fDelayTime, const std::string &p_PopUpName);
    
    /// THIS LOAD OTHER POP UP BY COMPARING WITH STRING
    /// @param p_Sender ref of popup
    /// @param p_String name of popUp
    void LoadOtherPopUp(Ref *p_Sender, const std::string &p_String);
    
    /// Adding region level Buttons each one 5 levels
    /// @param sprBg Sprite to get pos of level buttons
    /// @param tag button tag
    void addLevelButtons(Sprite *sprBg,int tag);
    
    /// coin count label animation when its updated
    void CoinLabelAnimation();
    
    /// this is called to set the current updated coins count to label
    void UpdateCoins();
    
    /// THIS CREATE UPDATE COIN EVENT
    void CreateUpdateCoinEvent();
    
    ///  this will returns starting state according to language
    /// @return std::string  name of the state
    std::string getLevelStartPlaceOnLang();
    
    /// parse the level map  Json data 
    void ParseLevelMapData();
    
    /// Adding region Bg and name
    /// @param layot Layout where the  current state image need to be added
    /// @param t_iTag  tag of current region
    void addLevelRegions(Layout *layot,int t_iTag);
    
    /// Adding State region Gift Box
    /// @param layout Layout where the  current state gift box need to be added
    /// @param t_iTag tag of gift box
    void addGiftBoxes(Layout *layout,int t_iTag);
    
    /// region gift box click callback
    void GiftButtonCallback(Ref* sender);
    
    /// checks for gift is unlocked after completeing region
    /// @param playedcount levels played count of particular region
    void checkForGiftUnlock(int playedcount);
    
    /// creates level map for other countries other than india
    void createLevelMapForDiffCountries();
    
    ///  adding level buttons for other countries other than india
    /// @param sprBg laout  for postion for buttons
    /// @param tag tag of buttons
    /// @param scale scale value of level buttons
    void addLevelButtonsFordiffCuntries(Layout *sprBg, int tag, float scale);
    
    ///  adding level Place image for other countries other than india
    /// @param sprBg laout  for postion for buttons
    /// @param tag tag of buttons
    /// @param scale scale value of level buttons
    void addLevelPlacesForDiffCountries(Layout *sprBg, int tag,float scale);
    
    ///  this checks the current country levels completed or not to unlock new country
    void checkforCurrentCountryCompletion();
    
    /// after new country unlocked displays popup new country is unlocked
    void LoadNextWorldUnlockedPopUp();
    
    /// checks for gifts in level map claimed or not to change the claimed gift image
    void checkForGiftsClaimed();
        
    /// Adding  gift boxes for every 5 levels in other than india country
    /// @param layout Layout where the  current  gift box need to be added
    /// @param t_iTag tag of gift box
    void addGiftBoxesForDiffCountries(Layout *layout,int t_iTag);
    
    /// Checks Any new achievemnet completed or not if its completed changes the achievement button tag
    void checkForAchievementUpdates();
    
    /// THIS UPDATE  BUTTON TAGS AND DATA
    void UpdateMenuData();
    
    /// displays rate popup
    void showRatePopUp();
    
    /// checks for rate popup display condition
    void checkForRatePopUp();
    
    /// parse the regional names translation data  to display on level map
    void parseJsonForlevelTranslation();
    
    /// displays Ad on every level complete
    void showAdOfLevelComplete();
};
#endif /* LevelScene_hpp */
