//
//  StatsManager.hpp
//  wordPro-mobile
//
//  Created by id on 01/04/20.
//

#ifndef StatsManager_h
#define StatsManager_h

#include <stdio.h>
#include "cocos2d.h"
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
#include <memory>
#include "GameConstants.h"

using namespace cocos2d;

class StatsManager
{
    
    // CONSTRUCTOR
       StatsManager();
       /**
        *  The singleton pointer of StatsManager.
        */
       static StatsManager* m_PtrStatsManager;
public:
    /**
       *  Gets the instance of StatsManager.
       */

    std::string m_strLevelWord  = "";
    
    std::string m_strCurCountry  = "";
    
    std::string m_strPrevLocation = "";
    std::string m_strCurLocation  = "";
    std::string m_strNxtLocation  = "";
    
    std::string m_strCurState  = "";

    std::vector<std::string> m_CountriesArray;
    std::vector<std::string> m_PlayedCountries;
    
    std::map<std::string ,int> m_CountrieslevelCntMap;

    std::string LangStrArr[10] = {LANG_ENGLISH,LANG_HINDI,LANG_KANNADA,LANG_TELUGU,LANG_TAMIL,LANG_MALAYALAM,LANG_MARATHI,LANG_BANGLA,LANG_ODIA,LANG_GUJARATI};

    
      static StatsManager* getInstance();
    
    // DESTRUCTOR
       virtual ~StatsManager();
    
    void setLocations(std::string prevLoc,std::string curLoc,std::string nextLoc);
    
    std::string getPrevLocation();
    
    std::string getCurLocation();
    
    std::string getNxtLocation();
    
    void setCurrentState(std::string Loc);
    
    std::string getCurrentState();
    
    void setCurrentCountry(std::string Loc);
    
    std::string getCurrentCountry();
    
    
    void setNextUnlockCountry(std::string Loc);
    std::string getNextUnlockCountry();

    
    
    void updateLangPlayedlevelCount(int count,bool isUpdate);
    
    int getLangPlayedlevelCount();
    
    void InitializeLevelCountForLanguages();
    void InitialUnlockedCountryForLanguages();
    
    int getLangUnlockedCountryCount();
    
    void updateLangBonusWordsCount(int count);
    
    int getLangBonusWordsCount();
    
    
    int getTotalLevelCountForCountries(std::string countryname);
    
    void updatePlayedLevelCountForCountries(std::string countryname, int count);
    
    int getPlayedLevelCountForCountries(std::string countryname);
        
    void LoadLevelMapData();

    void initializeTotLevelCountForCountries();
    
    void initialLevelCountForCountriesforLanguages();

    
    
    void updateIsCountryPlayed(std::string countryname);
    
    bool checkIsCountryPlayed(std::string countryname);
    
    
    
    
    
    
    
    int getUnlockedGiftsCountInLangForCountries(std::string countryname);

    void updateUnlockedGiftsCountInLangForCountries(std::string countryname, int count);

    void updateIsClaimedGiftInLangForCountries(std::string countryname, int count);

    bool getClaimedGiftInLangForCountries(std::string countryname,int tag);

    
    
    void setCurrentLevelLetters(std::string strword);
    
    std::string getCurrentLevelLetters();
    
    void setplayedCountriesArray(std::vector<std::string> playedArr);
    std::vector<std::string> getplayedCountriesArray();
    
    int m_index = 0;
    void setcurrentCountryindex(int index);
    int  getCurrentCountryIndex();

    
    void InitializeLaunchData();
    
    bool checkAllCountriesPlayed();
    
    void updateInGameLevelDataFromUserData(int level);
};

#endif /* StatsManager_hpp */
