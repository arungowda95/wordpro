//
//  GameScene.cpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#include "GameScene.h"
#include "GameLayer.h"
#include "HudLayer.h"

GameScene::GameScene()
{
   LayerColor *t_BgColor = LayerColor::create(Color4B(0,0,0,255));
   this->addChild(t_BgColor);
           
   m_ptrGameLayer = new GameLayer();
   m_ptrGameLayer->autorelease();
   this->addChild(m_ptrGameLayer);
   
   m_ptrHudLayer = new HudLayer();
   m_ptrHudLayer->autorelease();
   this->addChild(m_ptrHudLayer);
           
    m_ptrGameLayer->setHudLayer(m_ptrHudLayer);
    
    m_ptrHudLayer->setGameLayer(m_ptrGameLayer);
}
GameScene::~GameScene()
{
    this->removeAllChildrenWithCleanup(true);
}
