//
//  LanguageTranslator.cpp
//  wordPro-mobile
//
//  Created by id on 27/03/20.
//

#include "LanguageTranslator.h"
#include "GameConstants.h"
#include <memory>

//SINGELTON STUFF
LanguageTranslator* LanguageTranslator::m_PtrLanguageTranslator = NULL;

// RETURN QUESTION MANAGER
LanguageTranslator* LanguageTranslator::getInstance()
{
    if(!m_PtrLanguageTranslator)
    {
        m_PtrLanguageTranslator = new LanguageTranslator();
    }
    return m_PtrLanguageTranslator;
}
// CONSTRUCTOR
LanguageTranslator::LanguageTranslator()
{

}
// THIS LOAD TRANSLATION
void LanguageTranslator::LoadTranslationFile()
{
    // Read From JSON File
    std::string t_fullPath = FileUtils::getInstance()->fullPathForFilename("LocalData/En_LocalizedData.json");
    printf("\nLanguageData  ==%s\n",t_fullPath.c_str());
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_fullPath="LocalData/En_LocalizedData.json";
    }
    else if(me_Language.compare(LANG_KANNADA)==0)
    {
        t_fullPath="LocalData/Kannada_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_HINDI)==0)
    {
        t_fullPath="LocalData/Hindi_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_MALAYALAM)==0)
    {
        t_fullPath="LocalData/Malayalam_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_MARATHI)==0)
    {
        t_fullPath="LocalData/Marathi_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_ODIA)==0)
    {
        t_fullPath="LocalData/Odia_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_TAMIL)==0)
    {
        t_fullPath="LocalData/Tamil_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_TELUGU)==0)
    {
        t_fullPath="LocalData/Telugu_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_GUJARATI)==0)
    {
        t_fullPath="LocalData/Gujarati_LocalizedData.json";
    }
    else if (me_Language.compare(LANG_BANGLA)==0)
    {
        t_fullPath="LocalData/Bengali_LocalizedData.json";
    }
    char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str()) + sizeof(int) + 1];
    strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str());
    m_JsonDocument.Parse<kParseStopWhenDoneFlag>(p_Buffer);
    
    if (m_JsonDocument.HasParseError())  // Print parse error
    {
        CCLOG("GetParseError %u\n", m_JsonDocument.GetParseError());
        delete []p_Buffer;
        p_Buffer = NULL;
        return;
    }
    
    delete []p_Buffer;
    p_Buffer = NULL;
    
    
    std::string t_Path = FileUtils::getInstance()->fullPathForFilename("LocalData/En_LocalizedData.json");
    char *p_EngBuffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_Path).c_str()) + sizeof(int) + 1];
    strcpy(p_EngBuffer, FileUtils::getInstance()->getStringFromFile(t_Path).c_str());
    m_EngDocument.Parse<kParseStopWhenDoneFlag>(p_EngBuffer);
    
    if (m_EngDocument.HasParseError())  // Print parse error
    {
        CCLOG("GetParseError %u\n", m_EngDocument.GetParseError());
        delete []p_EngBuffer;
        p_EngBuffer = NULL;
        return;
    }
    
    delete []p_EngBuffer;
    p_EngBuffer = NULL;

    
    
}
// THIS RETURN TRANSLATED STRING
std::string LanguageTranslator::getTranslatorStringWithTag(const std::string &p_StringTag)
{
    if (me_Language.length()==0)
    {
        me_Language = LANG_ENGLISH;
    }
    std::string str = p_StringTag;
    
    if(m_JsonDocument.HasMember(p_StringTag.c_str()))
    {
        str = m_JsonDocument[p_StringTag.c_str()].GetString();
    }
    else if(m_EngDocument.HasMember(p_StringTag.c_str()))
    {
        str = m_EngDocument[p_StringTag.c_str()].GetString();
    }
    return str;
}
