//
//  PopUp.hpp
//  wordPro-mobile
//
//  Created by id on 09/03/20.
//

#ifndef PopUp_h
#define PopUp_h

#include <stdio.h>
#include "cocos2d.h"
#include "GameConstants.h"
#include "GameController.h"
#include "StatsManager.h"
#include "InterfaceManagerInstance.h"
#include "AchievementManager.h"
#include "DailyMissionManager.h"
#include "LanguageTranslator.h"

enum PopUpType
{
    POPUP_NONE,
    POPUP_BUY_COINS,
    POPUP_LANGUAGE,
    POPUP_SETTINGS,
    POPUP_LEVEL_COMPLETE,
    POPUP_REMOVE_ADS,
    POPUP_FREE_COINS,
    POPUP_LIMITED_SALE,
    POPUP_DAILY_MISSION,
    POPUP_DAILY_REWARD,
    POPUP_GAME_EXIT,
    POPUP_ACHIEVEMENT,
    POPUP_BONUS_WORD,
    POPUP_WORLD_UNLOCK,
    POPUP_GIFT_CLAIM,
    POPUP_REPORT_WORD,
    POPUP_UNLOCK_POWER,
    POPUP_RATE_GAME,
    POPUP_GAME_COMPLETE,
    POPUP_GAME_PROMOTION,
    POPUP_GAME_LEADERBOARD,
    POPUP_FB_INFO,
    POPUP_COMMON_TUT,
};

enum PopUpTransitionType
{
    POPUP_ENTER,
    POPUP_EXIT
};

using namespace cocos2d;

class PopUp : public Node
{
    
    
public:
    Rect  m_PopUpRect;
    Point m_ZeroPosition;
    
    Rect ResolutionSize ;
    Size visibleSize;
    Point origin ;
      
    
    char *status;
    bool m_bActionCompleted;
    std::function<void()>  m_fnSelector;
    // CONSTRUCTOR AND DESTRUCTOR
    PopUp();
    ~PopUp();
    
     // BUTTON PRESSED FUNCTIONS RETURN CALL BACK TO RESPECTIVE LAYER FROM IT CALLED
     virtual void OnButtonPressed(Ref *p_Sender);

     // THIS ENABLE NATIVE ADS ENABLE OR DISABLE
     virtual void setNativeAdsVisible(bool p_bVisible);

    // RETURN RECT OF POPUP BG IMAGE
    virtual Rect getPopUpContentRect();

};
#endif /* PopUp_hpp */
