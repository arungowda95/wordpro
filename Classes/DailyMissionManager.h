//
//  DailyMissionManager.hpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#ifndef DailyMissionManager_hpp
#define DailyMissionManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "DailyMissionObserver.h"

enum MissionDay
{
    MISSION_NONE,
    DAY_1_MISSION,
    DAY_2_MISSION,
    DAY_3_MISSION,
    DAY_4_MISSION,
    DAY_5_MISSION,
};

using namespace cocos2d;
class DailyMissionManager
{
        // CONSTRUCTOR
        DailyMissionManager();
        /**
         *  The singleton pointer of AchievementManager.
         */
        static DailyMissionManager* m_PtrDailyMissionManager;    
        
        std::vector<DailyMissionObserver*> m_ObserverArray;
        rapidjson::Document m_JsonDocument;
    
        std::string m_MissionName;
        int m_iMissionTag;
        MissionDay m_eMissionDay;
        std::string m_StrDay;
    
    public:
        /**
         *  Gets the instance of DailyMissionManager.
         */
        static DailyMissionManager* getInstance();
    
        // DESTRUCTOR
        virtual ~DailyMissionManager();
    
        // THIS INITIALIZE MISSIONS LIST
        void LoadDailyMissionsJson();
    
        /// CREATE MISSIONS TOASTS WITH ACHIEVEMENT NAME
        void createMissionToast(const std::string &p_AchievementName);
        
        /// THIS UPDATE
        void OnDailyMissionUpdate(const std::string &p_Type, int p_iCount);
    
        /// THIS SAVE MISSIONS JSON
        void SaveMissions();
        
        // THIS CLEAR ALL CURRENT MISSIONS
        void ClearAllCurrentMissions();
        
        // ON GAME STATE CHANGED
        void GameStateChanged();
        
        // ON APPLICATION QUIT
        void OnApplicationQuit();
    
        // THIS CHECK AND UNLOCK ACHIEVEMNET
        void CheckAndUnlockDailyMission(const std::string &p_Name);
    
        // RETURN ACHIEVEMENT TAG
        int getMissionTag();
    
        // RETURN ACHIEVEMENT NAME
        std::string getLastUnlockDailyMissionName();
    
        // THIS CHECK ALL  COMPLETED OR NOT
        bool CheckAllDailyMissionsComplete();
    
        // THIS RETURN  ARRAY
        std::vector<DailyMissionObserver*> GetDailyMissions();
    
    
        int getCompletedDailyMissionsCount();
    
        bool checkIsDailyMissionClaimed(int index);
    
        void updateDailyMissionClaimed(int index);
    
    
        void CheckDailyMissionAvailability();
    
        void checkForDailyMissionDay();
    
        std::string getDayString();
    
        void clearPreviousDayMissionsData();
    
        /// CHECK FOR ALL DAILY MISSIONS CLAIMED OR NOT
        bool checkAllDailyMissionsClaimed();
    
        int getCountOfDailyMissionsToClaim();
};

#endif /* DailyMissionManager_hpp */
