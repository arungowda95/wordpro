//
//  ExitPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 27/03/20.
//

#include "ExitPopUp.h"
ExitPopUp::ExitPopUp(std::function<void()> func)
{
        m_fnSelector = func;
        
        Rect ResolutionSize = Director::getInstance()->getSafeAreaRect();
        Size visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
        Point origin = ResolutionSize.origin;
        
        m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
        ExitPromoBg = NULL;
    
        m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
        m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
        this->addChild(m_SprPopUpBg,1);
        
    
        if(!GameController::getInstance()->getBoolForKey("ExitPromotionAvailable"))
        {
           m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.3);
        }
    
        m_SprPopUpTop = Sprite::create("PopUp_Top.png");
        m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
        this->addChild(m_SprPopUpTop,2);
        
        std::string t_Label = "Exit";
        std::string t_SubText = "ExitMsg1";

        if(me_strScreenName=="GameScreen")
        {
            t_Label   = "QuitMsg1";
            t_SubText = "QuitMsg2";
            m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScale()+0.1f);
        }

        Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
        t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        t_tLabel->setColor(Color3B(255,244,55));
        t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-30.0f);
        t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+30);
        t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
        t_tLabel->enableOutline(Color4B(196,56,63,255),3);
        this->addChild(t_tLabel, 2);
    
    if(me_Language.compare(LANG_TELUGU)==0) {
        t_tLabel->setSystemFontSize(40);
    }
    
    if(me_Language.compare(LANG_BANGLA)==0||me_Language.compare(LANG_MARATHI)==0)
    {
        t_tLabel->setSystemFontSize(45);
    }
    
    if (me_Language.compare(LANG_MALAYALAM)==0&&me_strScreenName=="GameScreen") {
        t_tLabel->setSystemFontSize(50);
    }
    if(me_strScreenName=="GameScreen"&&(me_Language.compare(LANG_KANNADA)==0||me_Language.compare(LANG_TAMIL)==0))
    {
        t_tLabel->setSystemFontSize(40);
    }
    
    
        Label *t_tSubLabel = createLabelBasedOnLang(t_SubText,45);
        t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
        t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
        t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-20-t_tSubLabel->getBoundingBox().size.height/2));
        t_tSubLabel->setColor(Color3B(255,255,255));
        this->addChild(t_tSubLabel, 2);
    
        if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        {
         t_tSubLabel->enableOutline(Color4B(196,56,63,255),3);
        }

    
        // YES BUTTON
       float posy = (m_SprPopUpBg->getBoundingBox().getMinY());
       t_miYesButton = getButtonMade("empty_smallButton.png", "empty_smallButton.png", CC_CALLBACK_1(ExitPopUp::OnButtonPressed,this));
         t_miYesButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX()-5-t_miYesButton->getBoundingBox().size.width/2,posy+30+t_miYesButton->getBoundingBox().size.height));
         t_miYesButton->setTag(0);

       Label *t_tYesLabel = createLabelBasedOnLang("Yes",45);
       t_tYesLabel->setColor(Color3B::WHITE);
       t_tYesLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
       t_tYesLabel->enableOutline(Color4B(53,126,32,255),2);
       t_tYesLabel->setWidth(t_miYesButton->getBoundingBox().size.width);
       t_tYesLabel->setHeight(t_tYesLabel->getBoundingBox().size.height+20);
       t_tYesLabel->setPosition(Vec2(t_miYesButton->getBoundingBox().size.width/2,t_miYesButton->getBoundingBox().size.height/2+15));
       t_miYesButton->addChild(t_tYesLabel,1);
      
    
       Size contentsize = Size(m_SprPopUpBg->getBoundingBox().size.width,t_tSubLabel->getBoundingBox().getMinY()-t_miYesButton->getBoundingBox().getMaxY()-40);

    float offy = 0.0f;
    if(me_Language.compare(LANG_BANGLA)==0)
    {
        contentsize.height -= 40.0f;
        offy = 20.0f;
    }
    
       if(GameController::getInstance()->getBoolForKey("ExitPromotionAvailable"))
       {
         createExitPromotion(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tSubLabel->getBoundingBox().getMinY()-offy),contentsize);
        }
        else{
           t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()+50));
        }
    // EXIT PROMOTION CREATION
    
    
      // NO BUTTON
      t_miNoButton = getButtonMade("empty_smallButton.png", "empty_smallButton.png", CC_CALLBACK_1(ExitPopUp::OnButtonPressed,this));
    t_miNoButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX()+5+t_miNoButton->getBoundingBox().size.width/2,posy+30+t_miNoButton->getBoundingBox().size.height));
         t_miNoButton->setTag(1);
      
      Label *t_NoLabel = createLabelBasedOnLang("No",45);
      t_NoLabel->setColor(Color3B::WHITE);
      t_NoLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
      t_NoLabel->enableOutline(Color4B(53,126,32,255),2);
      t_NoLabel->setWidth(t_miNoButton->getBoundingBox().size.width);
      t_NoLabel->setHeight(t_NoLabel->getBoundingBox().size.height+20);
      t_NoLabel->setPosition(Vec2(t_miNoButton->getBoundingBox().size.width/2,t_miNoButton->getBoundingBox().size.height/2+15));
      t_miNoButton->addChild(t_NoLabel,1);
    
    
    
      Menu *exitMenu = Menu::create(t_miYesButton,t_miNoButton,NULL);
      exitMenu->setPosition(Vec2::ZERO);
      this->addChild(exitMenu,2);
    
}
MenuItemSprite *ExitPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
    
}
void ExitPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
     MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
     
     int pButtonTag =  Button->getTag();

     switch (pButtonTag)
     {
         case 0:
         {
             status = (char*)"Yes";
         }break;
         case 1:
         {
             status = (char*)"No";
         }break;
         default:
             break;
     }
     
     if(m_fnSelector != NULL)
     {
         m_fnSelector();
     }
}
Label *ExitPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
void ExitPopUp::createExitPromotion(Point pos,Size promosize)
{
    ExitPromoBg = Sprite::create("large_box.png");
    ExitPromoBg->setScale(ExitPromoBg->getScaleX(),promosize.height/ExitPromoBg->getBoundingBox().size.height);
    ExitPromoBg->setPosition(Vec2(pos.x,pos.y-ExitPromoBg->getBoundingBox().size.height/2));
    this->addChild(ExitPromoBg,2);
    
    std::string str = FileUtils::getInstance()->getWritablePath().append("ExitPromoIcon.png");
    
    if(FileUtils::getInstance()->getStringFromFile(str).empty())
    {
        str = "pixel.png";
    }
    

    MenuItemSprite *ExitIcon = getButtonMade(str,str,CC_CALLBACK_0(ExitPopUp::InstallBtnCallBack,this));
    ExitIcon->setScale(1.6);
    ExitIcon->setPosition(Vec2(pos.x,ExitPromoBg->getBoundingBox().getMaxY()-20-ExitIcon->getBoundingBox().size.height/2));
    
    
    Label *t_GameTitle = Label::createWithTTF(GameController::getInstance()->m_strAppTitle,FONT_NAME, 35);
    t_GameTitle->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    t_GameTitle->setColor(Color3B::WHITE);
    t_GameTitle->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    t_GameTitle->setPosition(Vec2(pos.x,ExitIcon->getBoundingBox().getMinY()-10-t_GameTitle->getBoundingBox().size.height/2));
    t_GameTitle->setColor(Color3B(255,244,55));
    t_GameTitle->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_GameTitle,2);
    
    Label *t_GameDesc = Label::createWithTTF(GameController::getInstance()->m_strDesc,FONT_NAME,27);
    t_GameDesc->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    t_GameDesc->setColor(Color3B::WHITE);
    t_GameDesc->enableOutline(Color4B(196,56,63,255),3);
    t_GameDesc->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    t_GameDesc->setPosition(Vec2(pos.x,t_GameTitle->getBoundingBox().getMinY()-15-t_GameDesc->getBoundingBox().size.height/2));
    t_GameDesc->setWidth(ExitPromoBg->getContentSize().width-50);
    t_GameDesc->setHeight(t_GameDesc->getBoundingBox().size.height);
    this->addChild(t_GameDesc,2);
    
    
    MenuItemSprite *t_miInstallBtn = getButtonMade("medium_button.png","medium_button.png",CC_CALLBACK_0(ExitPopUp::InstallBtnCallBack,this));
    t_miInstallBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),ExitPromoBg->getBoundingBox().getMinY()+10+t_miInstallBtn->getBoundingBox().size.height/2));
    t_miInstallBtn->setScale(0.9f);
    t_miInstallBtn->setTag(0);

    Label *installLabel = createLabelBasedOnLang("Install",40);
    installLabel->setWidth(t_miInstallBtn->getContentSize().width-10);
    installLabel->setHeight(150);
    installLabel->setColor(Color3B::WHITE);
    installLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    installLabel->enableOutline(Color4B(53,126,32,255),2);
    installLabel->setPosition(Vec2(t_miInstallBtn->getContentSize().width/2,t_miInstallBtn->getContentSize().height/2+10));
    t_miInstallBtn->addChild(installLabel,1);

    
    Menu *menu = Menu::create(ExitIcon,t_miInstallBtn,NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu,2);

}
void ExitPopUp::InstallBtnCallBack()
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    Application::getInstance()->openURL(GameController::getInstance()->m_strAppUrl);
}
ExitPopUp::~ExitPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
