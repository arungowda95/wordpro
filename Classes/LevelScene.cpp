//
//  LevelScene.cpp
//  wordPro-mobile
//
//  Created by id on 27/02/20.
//

#include "LevelScene.h"
#include "GameScene.h"
#include "MainMenuScene.h"
#include "GameConstants.h"
#include "GameController.h"
#include "PopUpManager.h"
#include "CW_LevelScene.h"
#include "ShufflingClass.h"
#include "StatsManager.h"
#include "AchievementManager.h"
#include "DailyMissionManager.h"

LevelScene::LevelScene()
{
    
    me_strScreenName = "LevelScreen";
    
    LayerColor *colorLayer = LayerColor::create(Color4B(0,0,0,255));
    this->addChild(colorLayer);
    
    m_ptrLevelLayer = new LevelLayer();
    m_ptrLevelLayer->autorelease();
    this->addChild(m_ptrLevelLayer);
    
}
LevelScene::~LevelScene()
{
    this->removeAllChildrenWithCleanup(true);
}

#pragma mark LEVEL LAYER

LevelLayer::LevelLayer()
{
    m_ptrScrolView = NULL;
    m_BackButton = NULL;
    m_SettingsButton = NULL;
    m_AchievementButton = NULL;
    m_ShopButton        = NULL;
    m_CoinsButton = NULL;
    bgSprite = NULL;
    m_ptrPopUpManager = NULL;
    m_bIsEventCreated = false;
    m_SpSale = NULL;
    
    
    if(GameController::getInstance()->isBGMEnabled())
    {
        if(SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
        {
            SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        }else{
            GameController::getInstance()->playBGM(BGM_GAME, true);
        }
        
    }
    
    m_iStatesTotCount = 0;
    
    m_StrCountry = "";
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    origin = ResolutionSize.origin;
    
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsScreen("LevelScreen");
    
    auto KeyListener = EventListenerKeyboard::create();
    KeyListener->setEnabled(true);
    KeyListener->onKeyPressed = CC_CALLBACK_2(LevelLayer::onKeyPressed, this);
    KeyListener->onKeyReleased = CC_CALLBACK_2(LevelLayer::onKeyReleased, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(KeyListener, this);
    
    bgSprite = Sprite::create("ingame-bg.jpg");
    bgSprite->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2));
    bgSprite->setScale(m_fScale);
    this->addChild(bgSprite);
    
    // Settings Button
    m_SettingsButton = getButtonMade("setting-button.png","setting-button.png",CC_CALLBACK_1(LevelLayer::settingsBtnCallback, this));
    m_SettingsButton->setScale(m_fScale);
    m_SettingsButton->setPosition(Vec2(origin.x+20+m_SettingsButton->getBoundingBox().size.width/2,origin.y+visibleSize.height-20-m_SettingsButton->getBoundingBox().size.height/2));
    
    // Achievement Button
    m_AchievementButton = getButtonMade("achievements-button.png","achievements-button.png",CC_CALLBACK_1(LevelLayer::achievementBtnCallback, this));
    m_AchievementButton->setScale(m_fScale);
    
    float xpos = m_SettingsButton->getBoundingBox().getMaxX();
    m_AchievementButton->setPosition(Vec2(xpos+20+m_AchievementButton->getBoundingBox().size.width/2,origin.y+visibleSize.height-20-m_AchievementButton->getBoundingBox().size.height/2));
    
    Sprite *t_SprClaimIcon = Sprite::create("Claim.png");
    t_SprClaimIcon->setScale(0.9f);
    t_SprClaimIcon->setPosition(Vec2(m_AchievementButton->getContentSize().width/2,t_SprClaimIcon->getBoundingBox().size.height/2));
    t_SprClaimIcon->setName("tag");
    t_SprClaimIcon->setVisible(false);
    m_AchievementButton->addChild(t_SprClaimIcon,2);
    
    
    //Shop Button
    m_ShopButton = getButtonMade("shop-button.png","shop-button.png",CC_CALLBACK_1(LevelLayer::shopBtnCallback, this));
    m_ShopButton->setScale(m_fScale);
    
    xpos = m_AchievementButton->getBoundingBox().getMaxX();
    m_ShopButton->setPosition(Vec2(xpos+20+m_ShopButton->getBoundingBox().size.width/2,origin.y+visibleSize.height-20-m_ShopButton->getBoundingBox().size.height/2));
    
    m_SpSale = Sprite::create("sale_image.png");
    m_SpSale->setPosition(Vec2(m_ShopButton->getContentSize().width/2,m_SpSale->getContentSize().height/2));
    m_ShopButton->addChild(m_SpSale,1);
    
    
    
    //Coin Button
    m_CoinsButton = getButtonMade("add-dollar-button.png","add-dollar-button.png",CC_CALLBACK_1(LevelLayer::addCoinBtnCallback,this));
    m_CoinsButton->setScale(m_fScale);
    m_CoinsButton->setPosition(Vec2(origin.x+visibleSize.width-20-m_CoinsButton->getBoundingBox().size.width/2,m_ShopButton->getPositionY()));
    
    
    std::string coinStr = GameController::getInstance()->getCoinsFormat(me_iCoinCount);
    m_CoinCount = Label::createWithTTF(coinStr,FONT_NAME,20);
    m_CoinCount->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    m_CoinCount->enableOutline(Color4B(30, 30, 11, 255),3);
    m_CoinCount->setColor(Color3B::WHITE);
    if (me_iCoinCount<=50) {
        m_CoinCount->setColor(Color3B::RED);
    }
    m_CoinCount->setPosition(Vec2(m_CoinsButton->getContentSize().width/2-10,m_CoinsButton->getContentSize().height/2));
    m_CoinsButton->addChild(m_CoinCount,2);
    
    //Back Button
    m_BackButton = getButtonMade("more-games-button.png","more-games-button.png",CC_CALLBACK_1(LevelLayer::backBtnCallback,this));
    m_BackButton->setScale(m_fScale);
    m_BackButton->setPosition(Vec2(origin.x+visibleSize.width*0+20+m_BackButton->getBoundingBox().size.width/2,origin.y+visibleSize.height-20-m_BackButton->getBoundingBox().size.height/2));
    
    Sprite *backIcon = Sprite::create("back.png");
    backIcon->setPosition(Vec2(m_BackButton->getContentSize().width/2,m_BackButton->getContentSize().height/2+15));
    m_BackButton->addChild(backIcon,1);
    
    
    Menu *topMenu = Menu::create(m_AchievementButton,m_ShopButton,m_CoinsButton,m_BackButton,NULL);
    topMenu->setPosition(Vec2::ZERO);
    this->addChild(topMenu,2);
    
    m_StrCountry = StatsManager::getInstance()->getCurrentCountry();
    
    // Level map data
    ParseLevelMapData();
    
    GameController::getInstance()->hideBanner();
    
    levelButtonsArray  = new Vector<ui::Button*>();
    giftButtonArray    = new Vector<MenuItemImage*>();
    
    m_ptrScrolView = cocos2d::ui::ScrollView::create();
    m_ptrScrolView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_ptrScrolView->setTouchEnabled(true);
    Size contentsize = Size(visibleSize.width,visibleSize.height);
    m_ptrScrolView->setContentSize(Size(contentsize));
    m_ptrScrolView->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2));
    m_ptrScrolView->setScrollBarEnabled(false);
    m_ptrScrolView->setSwallowTouches(false);
    //       m_ptrScrolView->setBounceEnabled(true);
    m_ptrScrolView->scrollToBottom(0, true);
    m_ptrScrolView->setAnchorPoint(Vec2(0.5f,0.5f));
    //Scroll View map
    this->addChild(m_ptrScrolView);
    
    if(m_StrCountry=="India")
    {
        createLevelMapScroll();
    }else{
        createLevelMapForDiffCountries();
    }
    
    m_ptrPopUpManager = new PopUpManager(this,SCREEN_LEVEL);
    m_ptrPopUpManager->autorelease();
    this->addChild(m_ptrPopUpManager,3);
    
    checkForAchievementUpdates();
    
    checkForRatePopUp();
    
    if(me_bShowAds)
    {
        me_bShowAds = false;
        CallFunc *t_CallShowAdFun = CallFunc::create(std::bind(&LevelLayer::showAdOfLevelComplete,this));
        this->runAction(Sequence::create(DelayTime::create(0.7f),t_CallShowAdFun,NULL));
    }
}
/// displays Ad on every level complete
void LevelLayer::showAdOfLevelComplete()
{
    GameController::getInstance()->showInterstial();
}
/// creates level map for other countries other than india
void LevelLayer::createLevelMapForDiffCountries()
{
    
    float totalPages = 3;
    float diff = 0.65;
    
    float hieght = visibleSize.height*(totalPages-diff);
    
    Size Containersize = Size(visibleSize.width,hieght);
    m_ptrScrolView->setInnerContainerSize(Size(Containersize.width,Containersize.height));
    
    Point t_position = Vec2(m_ptrScrolView->getContentSize().width/2,Containersize.height);
    
    Layout *layout = Layout::create();
    layout->setContentSize(Size(visibleSize.width,Containersize.height));
    layout->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::SOLID);
    layout->setAnchorPoint(Vec2(0.5f,0.5f));
    layout->setPosition(Vec2(t_position.x,t_position.y-layout->getBoundingBox().size.height/2));
    layout->setName("layout");
    m_ptrScrolView->addChild(layout,1);
    
    t_position = Vec2(layout->getContentSize().width/2,0);
    
    float scaley = m_fScale;
    for(int i=0; i<totalPages; i++)
    {
        
        Sprite *t_sprBgImageView = Sprite::create("ingame-bg.jpg");        t_sprBgImageView->setScale(visibleSize.width/t_sprBgImageView->getContentSize().width,visibleSize.height/t_sprBgImageView->getContentSize().height);
        t_sprBgImageView->setPosition(Vec2(t_position.x,t_position.y+t_sprBgImageView->getBoundingBox().size.height/2));
        layout->addChild(t_sprBgImageView);
        
        Sprite *t_sprBgImagemap = Sprite::create("Level-road.png");
        t_sprBgImagemap->setScale(visibleSize.width/t_sprBgImagemap->getContentSize().width,visibleSize.height/t_sprBgImagemap->getContentSize().height);
        t_sprBgImagemap->setPosition(Vec2(t_position.x,t_position.y+t_sprBgImagemap->getBoundingBox().size.height/2));
        t_sprBgImagemap->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        t_sprBgImagemap->setOpacity(150);
        t_sprBgImagemap->setName("roadmap");
        layout->addChild(t_sprBgImagemap,1);
        
        t_position.y += t_sprBgImageView->getBoundingBox().size.height;
        
        scaley = t_sprBgImagemap->getScaleY();
    }
    
    
    addLevelButtonsFordiffCuntries(layout,0,scaley);
    addLevelPlacesForDiffCountries(layout,0,0.5);
    addGiftBoxesForDiffCountries(layout,1);
    
    
    float percentage = m_ptrScrolView->getScrolledPercentVertical();
    
    
    //    return;
    printf("\nSize of Buttons-->%zd\n",levelButtonsArray->size());
    
    printf("\npercentage-->%f\n",percentage);
    int m_iLevelnum = StatsManager::getInstance()->getPlayedLevelCountForCountries(m_StrCountry);
    int m_iTotLevel = StatsManager::getInstance()->getTotalLevelCountForCountries(m_StrCountry);
    
    m_iLevelnum = m_iLevelnum;
    m_ptrScrolView->scrollToPercentVertical((100-(m_iLevelnum*10)), 0.0f, true);
    
    std::string prevloc="";
    std::string curloc="";
    std::string nextloc="";
    std::string CurState="";
    Sprite *sprite = NULL;
    
    if(m_iLevelnum>=levelButtonsArray->size()-2)
    {
        m_ptrScrolView->scrollToPercentVertical(0, 0.0f, true);
    }
    
    if(m_iLevelnum==m_iTotLevel)
    {
        m_ptrScrolView->scrollToPercentVertical(0, 0.0f, true);
        
        for(int Btnnum = 0; Btnnum<m_iLevelnum;Btnnum++)
        {
            Button *LevelBtn  = levelButtonsArray->at(Btnnum);
            {
                LevelBtn->setEnabled(true);
                LevelBtn->loadTextureNormal("working-level.png");
                LevelBtn->loadTexturePressed("working-level.png");
                
                Label *m_LevelNum = Label::createWithTTF(StringUtils::format("%d",LevelBtn->getTag()),FONT_NAME,50);
                m_LevelNum->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
                m_LevelNum->setColor(Color3B(139,47,20));
                m_LevelNum->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height*0.7));
                LevelBtn->addChild(m_LevelNum,2);
            }
        }
        
        checkforCurrentCountryCompletion();
        
        checkForGiftUnlock(m_iLevelnum);
        checkForGiftsClaimed();
        return;
    }
    
    
    for(int Btnnum = 0; Btnnum<=m_iLevelnum;Btnnum++)
    {
        Button *LevelBtn  = levelButtonsArray->at(Btnnum);
        
        
        if (m_iLevelnum==Btnnum)
        {
            LevelBtn->setEnabled(true);
            
            LevelBtn->loadTextureNormal("level-on.png");
            LevelBtn->loadTexturePressed("level-on.png");
            
            sprite = Sprite::create("level_tip_02.png");
            sprite->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height/2+5));
            LevelBtn->addChild(sprite,-1);
            
            Sprite *spriteloc = Sprite::create("location-image.png");
            spriteloc->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height));
            LevelBtn->addChild(spriteloc,1);
            
            JumpTo *jump = JumpTo::create(0.3f,spriteloc->getPosition(),5,1);
            JumpTo *jump1 = JumpTo::create(0.3f,Vec2(spriteloc->getPositionX(),spriteloc->getPositionY()+30.0f),5,3);
            Sequence *seq =Sequence::create(jump1,jump,NULL);
            RepeatForever *repeat = RepeatForever::create(seq);
            spriteloc->runAction(repeat);
            
            
            Label *m_LevelName = (Label*)LevelBtn->getChildByName("location");
            
            m_LevelName->setPositionY(m_LevelName->getPositionY()+25);
        }
        else
        {
            LevelBtn->setEnabled(true);
            LevelBtn->loadTextureNormal("working-level.png");
            LevelBtn->loadTexturePressed("working-level.png");
            
            Label *m_LevelNum = Label::createWithTTF(StringUtils::format("%d",LevelBtn->getTag()),FONT_NAME,50);
            m_LevelNum->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
            m_LevelNum->setColor(Color3B(139,47,20));
            m_LevelNum->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height*0.7));
            LevelBtn->addChild(m_LevelNum,2);
            
        }
        
    }
    
    
    //    if(sprite!=NULL)
    //    {
    //      Animation *t_Animation = Animation::create();
    //      t_Animation->setDelayPerUnit(0.2f);
    //      char* t_Buffer = new char[30];
    //      for (int i = 1; i<=6; i++ )
    //      {
    //          sprintf(t_Buffer, "level_tip_0%d.png", i );
    //          t_Animation->addSpriteFrameWithFile(t_Buffer);
    //      }
    //      Animate *t_Animate = Animate::create(t_Animation);
    //      sprite->runAction(RepeatForever::create(t_Animate));
    //    }
    
    FadeTo *fade = FadeTo::create(0.2f,255);
    ScaleTo *scale0 = ScaleTo::create(0.2f, 0.8f);
    ScaleTo *scale1 = ScaleTo::create(0.2f, 1.0f);
    ScaleTo *scale2 = ScaleTo::create(0.2f, 1.2f);
    ScaleTo *scale3 = ScaleTo::create(0.2f, 1.3f);
    
    Sequence *seq =Sequence::create(scale0,scale1,scale2,scale3,scale2,scale1,scale0,NULL);
    sprite->runAction(RepeatForever::create(seq));
    
    checkForGiftUnlock(m_iLevelnum);
    checkForGiftsClaimed();
}
///  adding level Place image for other countries other than india
/// @param sprBg layout  for postion for buttons
/// @param tag tag of buttons
/// @param scale scale value of level buttons
void LevelLayer::addLevelPlacesForDiffCountries(Layout *sprLayout, int tag,float scale)
{
    //0.5
    std::vector<Point> PosArr;
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.25,sprLayout->getContentSize().height*0.08));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.75,sprLayout->getContentSize().height*0));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.8,sprLayout->getContentSize().height*0.19));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.2,sprLayout->getContentSize().height*0.25));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.7,sprLayout->getContentSize().height*0.36));
    
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.25,sprLayout->getContentSize().height*0.52));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.8,sprLayout->getContentSize().height*0.63));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.23,sprLayout->getContentSize().height*0.72));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.75,sprLayout->getContentSize().height*0.81));
    PosArr.push_back(Vec2(sprLayout->getContentSize().width*0.25,sprLayout->getContentSize().height*0.93));
    
    
    std::string country = m_StrCountry;
    for(int index=0;index<m_iStatesTotCount;index++)
    {
        std::string t_Strplace = m_StatesArray.at(index);
        t_Strplace = country+"/"+t_Strplace+".png";
        Sprite *t_ImagePlace = Sprite::create(t_Strplace);
        if (t_ImagePlace==NULL)
        {
            std::string t_Strplace = StringUtils::format("%s/%s.png",country.c_str(),country.c_str());
            t_ImagePlace = Sprite::create(t_Strplace);
            if (t_ImagePlace==NULL) {
                t_ImagePlace = Sprite::create("Countries/India.png");
            }
        }
        t_ImagePlace->setScale(0.6);
        t_ImagePlace->setPosition(Vec2(PosArr.at(index)));
        t_ImagePlace->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
        sprLayout->addChild(t_ImagePlace);
    }
    
    
}
///  adding level buttons for other countries other than india
/// @param sprBg laout  for postion for buttons
/// @param tag tag of buttons
/// @param scale scale value of level buttons
void LevelLayer::addLevelButtonsFordiffCuntries(Layout *sprBg, int tag,float scale)
{
    std::vector<Point> PosArr;
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.25,sprBg->getContentSize().height*0.04));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.75,sprBg->getContentSize().height*0.09));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.6,sprBg->getContentSize().height*0.19));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.36,sprBg->getContentSize().height*0.28));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.42,sprBg->getContentSize().height*0.35));
    
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.25,sprBg->getContentSize().height*0.48));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.71,sprBg->getContentSize().height*0.6));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.34,sprBg->getContentSize().height*0.69));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.61,sprBg->getContentSize().height*0.78));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.2,sprBg->getContentSize().height*0.89));
    
    for(int index=0;index<m_iStatesTotCount;index++)
    {
        Button *LevelBtn = Button::create("level-off.png");
        LevelBtn->setScale(scale-0.1);
        LevelBtn->setTag(index+1);
        LevelBtn->setPosition(Vec2(PosArr.at(index)));
        LevelBtn->setEnabled(false);
        LevelBtn->addClickEventListener(CC_CALLBACK_1(LevelLayer::levelBtnCallBack, this));
        LevelBtn->setPressedActionEnabled(true);
        sprBg->addChild(LevelBtn,3);
        
        
        Label *m_LevelName = Label::createWithTTF(m_StatesArray.at(index),FONT_NAME,45);
        if(me_Language.compare(LANG_ENGLISH)!=0) {
            m_LevelName = Label::createWithSystemFont(m_StatesTransArray.at(index),SYS_FONT_NAME,45);
            m_LevelName->setWidth(m_LevelName->getBoundingBox().size.width+10);
            m_LevelName->setHeight(m_LevelName->getBoundingBox().size.height+10);
        }
        
        m_LevelName->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        m_LevelName->enableOutline(Color4B(230,126,39,255),3);
        m_LevelName->setColor(Color3B::WHITE);
        m_LevelName->setName("location");
        m_LevelName->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height+10+m_LevelName->getContentSize().height/2));
        LevelBtn->addChild(m_LevelName,2);
        
        
        levelButtonsArray->pushBack(LevelBtn);
    }
    
}
///  Create Level Map Scroll View
void LevelLayer::createLevelMapScroll()
{
    
    
    float totalPages = m_iStatesTotCount;
    
    
    float hieght = visibleSize.height*(totalPages);
    
    Size Containersize = Size(visibleSize.width,hieght);
    m_ptrScrolView->setInnerContainerSize(Size(Containersize.width,Containersize.height));
    
    Point t_position = Vec2(m_ptrScrolView->getContentSize().width/2,Containersize.height);
    
    
    Layout *Scrolllayout = Layout::create();
    Scrolllayout->setContentSize(Size(visibleSize.width,Containersize.height));
    //        Scrolllayout->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::SOLID);
    Scrolllayout->setAnchorPoint(Vec2(0.5f,0.5f));
    Scrolllayout->setPosition(Vec2(t_position.x,t_position.y-Scrolllayout->getBoundingBox().size.height/2));
    Scrolllayout->setName("layout");
    Scrolllayout->setColor(Color3B::WHITE);
    m_ptrScrolView->addChild(Scrolllayout);
    
    t_position = Vec2(Scrolllayout->getContentSize().width/2,0);
    
    int ButtonTag = 0;
    for(int i=0; i<totalPages; i++)
    {
        
        Layout *layout = Layout::create();
        layout->setContentSize(Size(visibleSize.width,visibleSize.height));
        layout->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::SOLID);
        layout->setPosition(Vec2(t_position.x,t_position.y+layout->getContentSize().height/2));
        layout->setName("layout");
        layout->setAnchorPoint(Vec2(0.5f,0.5f));
        layout->setColor(Color3B::WHITE);
        Scrolllayout->addChild(layout,1);
        
        
        Sprite *t_sprBgImageView = Sprite::create("ingame-bg.jpg");              t_sprBgImageView->setScale(visibleSize.width/t_sprBgImageView->getContentSize().width,visibleSize.height/t_sprBgImageView->getContentSize().height);
        t_sprBgImageView->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
        layout->addChild(t_sprBgImageView);
        
        
        Sprite *t_sprBgImagemap = Sprite::create("Level-road.png");
        t_sprBgImagemap->setScale(visibleSize.width/t_sprBgImagemap->getContentSize().width,visibleSize.height/t_sprBgImagemap->getContentSize().height);
        t_sprBgImagemap->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
        t_sprBgImagemap->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        t_sprBgImagemap->setOpacity(200);
        layout->addChild(t_sprBgImagemap,1);
        
        
        addLevelRegions(layout,i);//Adding region Bg and name
        addGiftBoxes(layout,i);//Adding region Gift
        addLevelButtons(t_sprBgImagemap,ButtonTag);// Adding regionlevel Buttons each one 5 levels
        
        
        ButtonTag += 5;
        t_position.y += layout->getContentSize().height;
    }
    
    int m_iLevelnum = StatsManager::getInstance()->getPlayedLevelCountForCountries(m_StrCountry);
    int m_iTotLevel = StatsManager::getInstance()->getTotalLevelCountForCountries(m_StrCountry);
    
    float Scrollval = (float)(float(m_iLevelnum)/(m_iTotLevel))*100.0f;
    
    Scrollval = round(100-Scrollval);
    m_ptrScrolView->scrollToPercentVertical(Scrollval, 0.0f, true);
    
    if(m_iLevelnum==m_iTotLevel)
    {
        m_ptrScrolView->scrollToPercentVertical(0, 0.0f, true);
        
        for(int Btnnum = 0; Btnnum<m_iLevelnum;Btnnum++)
        {
            Button *LevelBtn  = levelButtonsArray->at(Btnnum);
            {
                LevelBtn->setEnabled(true);
                LevelBtn->loadTextureNormal("working-level.png");
                LevelBtn->loadTexturePressed("working-level.png");
                
                Label *m_LevelNum = Label::createWithTTF(StringUtils::format("%d",LevelBtn->getTag()),FONT_NAME,50);
                m_LevelNum->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
                m_LevelNum->setColor(Color3B(139,47,20));
                m_LevelNum->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height*0.7));
                LevelBtn->addChild(m_LevelNum,2);
            }
        }
        
        checkforCurrentCountryCompletion();
        
        checkForGiftUnlock(m_iLevelnum);
        checkForGiftsClaimed();
        return;
    }
    
    std::string prevloc="";
    std::string curloc="";
    std::string nextloc="";
    std::string CurState="";
    Sprite *sprite = NULL;
    
    
    for(int Btnnum = 0; Btnnum<=m_iLevelnum;Btnnum++)
    {
        Button *LevelBtn  = levelButtonsArray->at(Btnnum);
        
        if (m_iLevelnum==Btnnum)
        {
            LevelBtn->setEnabled(true);
            
            LevelBtn->loadTextureNormal("level-on.png");
            LevelBtn->loadTexturePressed("level-on.png");
            
            sprite = Sprite::create("level_tip_02.png");
            sprite->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height/2+5));
            LevelBtn->addChild(sprite,-1);
            
            Sprite *spriteloc = Sprite::create("location-image.png");
            spriteloc->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height));
            LevelBtn->addChild(spriteloc,1);
            
            JumpTo *jump = JumpTo::create(0.3f,spriteloc->getPosition(),5,1);
            JumpTo *jump1 = JumpTo::create(0.3f,Vec2(spriteloc->getPositionX(),spriteloc->getPositionY()+30.0f),5,3);
            Sequence *seq =Sequence::create(jump1,jump,NULL);
            RepeatForever *repeat = RepeatForever::create(seq);
            spriteloc->runAction(repeat);
            
            Label *m_LevelName = (Label*)LevelBtn->getChildByName("location");
            
            m_LevelName->setPositionY(m_LevelName->getPositionY()+25);
        }
        else
        {
            LevelBtn->setEnabled(true);
            LevelBtn->loadTextureNormal("working-level.png");
            LevelBtn->loadTexturePressed("working-level.png");
            
            Label *m_LevelNum = Label::createWithTTF(StringUtils::format("%d",LevelBtn->getTag()),FONT_NAME,50);
            m_LevelNum->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
            m_LevelNum->setColor(Color3B(139,47,20));
            m_LevelNum->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height*0.7));
            LevelBtn->addChild(m_LevelNum,2);
            
        }
        
    }
    
    ScaleTo *scale0 = ScaleTo::create(0.2f, 0.8f);
    ScaleTo *scale1 = ScaleTo::create(0.2f, 1.0f);
    ScaleTo *scale2 = ScaleTo::create(0.2f, 1.2f);
    ScaleTo *scale3 = ScaleTo::create(0.2f, 1.3f);
    
    Sequence *seq =Sequence::create(scale0,scale1,scale2,scale3,scale2,scale1,scale0,NULL);
    sprite->runAction(RepeatForever::create(seq));
    
    checkForGiftUnlock(m_iLevelnum);
    checkForGiftsClaimed();
    
}
/// Adding State region Gift Box
/// @param layout Layout where the  current state gift box need to be added
/// @param t_iTag tag of gift box
void LevelLayer::addGiftBoxes(Layout *layout,int t_iTag)
{
    
    MenuItemImage *giftBtn = MenuItemImage::create("gift-box1.png","gift-box1.png",CC_CALLBACK_1(LevelLayer::GiftButtonCallback, this));
    giftBtn->setScale(m_fScale-0.2f);
    giftBtn->setPosition(Vec2(layout->getContentSize().width/2+giftBtn->getBoundingBox().size.width*0.6,layout->getContentSize().height*0.87));
    giftBtn->setTag(t_iTag);
    //    giftBtn->setColor(Color3B(150,150,150));
    
    Sprite *spr =  Sprite::create("gift-box1.png");
    spr->setColor(Color3B(150,150,150));
    giftBtn->setNormalImage(spr);
    giftBtn->setEnabled(false);
    
    Sprite *sprQuesmark =  Sprite::create("question-mark.png");
    sprQuesmark->setScale(2);
    sprQuesmark->setOpacity(170);
    sprQuesmark->setPosition(Vec2(giftBtn->getContentSize().width/2,giftBtn->getContentSize().height/2-35));
    sprQuesmark->setName("mark");
    giftBtn->addChild(sprQuesmark,1);
    
    
    Sprite* giftGlow = Sprite::create("glow.png");
    giftGlow->setScale(0.6);
    giftGlow->setPosition(Vec2(giftBtn->getBoundingBox().getMidX(), giftBtn->getBoundingBox().getMidY()-25));
    layout->addChild(giftGlow,1);
    giftGlow->runAction(RepeatForever::create(RotateBy::create(1.0f, 80.0f)));
    
    giftButtonArray->pushBack(giftBtn);
    
    Menu *menu = Menu::create(giftBtn,NULL);
    menu->setPosition(Vec2::ZERO);
    layout->addChild(menu,1);
}
/// Adding  gift boxes for every 5 levels in other than india country
/// @param layout Layout where the  current  gift box need to be added
/// @param t_iTag tag of gift box
void LevelLayer::addGiftBoxesForDiffCountries(Layout *layout,int t_iTag)
{
    
    Menu *menu = Menu::create(NULL);
    menu->setPosition(Vec2::ZERO);
    layout->addChild(menu,2);
    
    std::vector<Point> PosArr;
    PosArr.push_back(Vec2(layout->getContentSize().width*0.17,layout->getContentSize().height*0.42));
    PosArr.push_back(Vec2(layout->getContentSize().width*0.62,layout->getContentSize().height*0.93));
    
    for (int index = 0; index<2; index++)
    {
        MenuItemImage *giftBtn = MenuItemImage::create("gift-box1.png","gift-box1.png",CC_CALLBACK_1(LevelLayer::GiftButtonCallback, this));
        giftBtn->setScale(m_fScale-0.2f);
        giftBtn->setPosition(PosArr.at(index));
        giftBtn->setTag(index);
        //    giftBtn->setColor(Color3B(150,150,150));
        
        Sprite *spr =  Sprite::create("gift-box1.png");
        spr->setColor(Color3B(150,150,150));
        giftBtn->setNormalImage(spr);
        giftBtn->setEnabled(false);
        
        Sprite *sprQuesmark =  Sprite::create("question-mark.png");
        sprQuesmark->setScale(2);
        sprQuesmark->setOpacity(170);
        sprQuesmark->setPosition(Vec2(giftBtn->getContentSize().width/2,giftBtn->getContentSize().height/2-35));
        sprQuesmark->setName("mark");
        giftBtn->addChild(sprQuesmark,1);
        
        Sprite* giftGlow = Sprite::create("glow.png");
        giftGlow->setScale(0.6);
        giftGlow->setPosition(Vec2(giftBtn->getBoundingBox().getMidX(), giftBtn->getBoundingBox().getMidY()-25));
        layout->addChild(giftGlow,1);
        giftGlow->runAction(RepeatForever::create(RotateBy::create(1.0f, 80.0f)));
        
        menu->addChild(giftBtn);
        giftButtonArray->pushBack(giftBtn);
    }
}
void LevelLayer::addLevelRegions(Layout *layout,int t_iTag)
{
    
    Sprite *t_ImageView = Sprite::create("coins_popup_Bg.png");
    t_ImageView->setScaleY(m_fScale);
    t_ImageView->setScaleX((visibleSize.width-32)/t_ImageView->getContentSize().width);
    t_ImageView->setScaleY(t_ImageView->getScaleY()+0.6);
    t_ImageView->setPosition(Vec2(layout->getContentSize().width/2,t_ImageView->getBoundingBox().size.height/2));
    layout->addChild(t_ImageView,2);
    
    std::string str = m_StatesArray.at(t_iTag);
    
    std::string path = "In_Regions/";
    std::string t_Strplace = path.append(str).append(".png");
    
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
    
    Label *m_tPlaceName = Label::createWithTTF(str,FONT_NAME,40);
    if(me_Language.compare(LANG_ENGLISH)!=0) {
        m_tPlaceName = Label::createWithSystemFont(m_StatesTransArray.at(t_iTag),SYS_FONT_NAME,40);
        m_tPlaceName->setWidth(m_tPlaceName->getBoundingBox().size.width+10);
        m_tPlaceName->setHeight(m_tPlaceName->getBoundingBox().size.height+10);
    }
    m_tPlaceName->setColor(Color3B::WHITE);
    m_tPlaceName->enableOutline(Color4B(0,0,0,255),2);
    m_tPlaceName->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    m_tPlaceName->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    m_tPlaceName->setPosition(Vec2(layout->getContentSize().width/2,t_ImageView->getBoundingBox().getMinY()+10+m_tPlaceName->getContentSize().height/2));
    layout->addChild(m_tPlaceName,3);
    
    
    Sprite *t_ImagePlace = Sprite::create(t_Strplace);
    if (t_ImagePlace==NULL) {
        t_ImagePlace = Sprite::create("Countries/India.png");
    }
    float hieght = t_ImageView->getBoundingBox().size.height-40;
    float scale = hieght/t_ImagePlace->getBoundingBox().size.height;
    t_ImagePlace->setScale(scale);
    t_ImagePlace->setPosition(Vec2(t_ImageView->getBoundingBox().getMidX(),t_ImageView->getBoundingBox().getMidY()+20));
    layout->addChild(t_ImagePlace,2);
    
    
    // Adding to Layout above the bg
    Sprite *t_RegionImage = Sprite::create(t_Strplace);
    if (t_RegionImage==NULL) {
        t_RegionImage = Sprite::create("Countries/India.png");
    }
    hieght = t_ImageView->getBoundingBox().size.height;
    scale = hieght/t_RegionImage->getBoundingBox().size.height;
    t_RegionImage->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height*0.58));
    t_RegionImage->setRotation(random(-5,0));
    layout->addChild(t_RegionImage);
}
/// Adding region level Buttons each one 5 levels
/// @param sprBg Sprite to get pos of level buttons
/// @param tag button tag
void LevelLayer::addLevelButtons(Sprite *sprBg, int tag)
{
    std::vector<Point> PosArr;
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.8,sprBg->getContentSize().height*0.25));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.6,sprBg->getContentSize().height*0.45));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.35,sprBg->getContentSize().height*0.6));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.8,sprBg->getContentSize().height*0.7));
    PosArr.push_back(Vec2(sprBg->getContentSize().width*0.3,sprBg->getContentSize().height*0.81));
    for(int index=0;index<5;index++)
    {
        Button *LevelBtn = Button::create("level-off.png");
        LevelBtn->setPosition(Vec2(PosArr.at(index)));
        LevelBtn->setScale(m_fScale);
        LevelBtn->setTag(tag+1);
        LevelBtn->setEnabled(false);
        LevelBtn->addClickEventListener(CC_CALLBACK_1(LevelLayer::levelBtnCallBack, this));
        LevelBtn->setPressedActionEnabled(true);
        sprBg->addChild(LevelBtn);
        
        
        Label *m_LevelName = Label::createWithTTF(m_StatesRegArray.at(tag),FONT_NAME,45);
        if(me_Language.compare(LANG_ENGLISH)!=0) {
            m_LevelName = Label::createWithSystemFont(m_StatesRegTransArray.at(tag),SYS_FONT_NAME,45);
            m_LevelName->setWidth(m_LevelName->getBoundingBox().size.width+10);
            m_LevelName->setHeight(m_LevelName->getBoundingBox().size.height+10);
        }
        m_LevelName->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        m_LevelName->enableOutline(Color4B(230,126,39,255),3);
        m_LevelName->setColor(Color3B::WHITE);
        m_LevelName->setName("location");
        m_LevelName->setPosition(Vec2(LevelBtn->getContentSize().width/2,LevelBtn->getContentSize().height+10+m_LevelName->getContentSize().height/2));
        LevelBtn->addChild(m_LevelName,2);
        
        tag++;
        
        levelButtonsArray->pushBack(LevelBtn);
    }
    
}
void LevelLayer::levelBtnCallBack(Ref *sender)
{
    if(!m_ptrScrolView->isTouchEnabled()){
        return;
    }
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    
    Button *levelBtn = (Button*)sender;
    
    int curLevel = levelBtn->getTag();
    int m_iLevelnum = curLevel-1;
    
    
    
    UserDefault::getInstance()->setIntegerForKey(CURR_LEVEL,curLevel-1);
    
    std::string prevloc="";
    std::string curloc="";
    std::string nextloc="";
    std::string CurState="";
    
    //    std::string str = StringUtils::format(CountriesPlayedLevelCount,"India",me_Language.c_str());
    //    UserDefault::getInstance()->setIntegerForKey(str.c_str(),89);
    
    if(m_StrCountry=="India")
    {
        if (m_iLevelnum==0)
        {
            curloc  = m_StatesRegTransArray.at(m_iLevelnum);
            nextloc = m_StatesRegTransArray.at(m_iLevelnum+1);
        }
        else if (m_iLevelnum>0)
        {
            prevloc = m_StatesRegTransArray.at(m_iLevelnum-1);
            curloc  = m_StatesRegTransArray.at(m_iLevelnum);
            if(m_iLevelnum<m_StatesRegTransArray.size()-1)
                nextloc = m_StatesRegTransArray.at(m_iLevelnum+1);
        }
        
        
        int index = m_iLevelnum/5;
        
        CurState = m_StatesArray.at(index);
        
        StatsManager::getInstance()->setCurrentState(CurState);
        CurState = m_StatesTransArray.at(index);
        
        UserDefault::getInstance()->setStringForKey("CURR_STATE_TRANSLATE", CurState.c_str());
        
        StatsManager::getInstance()->setLocations(prevloc,curloc,nextloc);
    }
    else
    {
        std::string curLocation = "";
        if(m_iLevelnum==0)
        {
            curloc  = m_StatesArray.at(m_iLevelnum);
            curLocation = m_StatesTransArray.at(m_iLevelnum);
            nextloc = m_StatesTransArray.at(m_iLevelnum+1);
        }
        else if (m_iLevelnum>0)
        {
            prevloc = m_StatesTransArray.at(m_iLevelnum-1);
            curloc  = m_StatesArray.at(m_iLevelnum);
            curLocation = m_StatesTransArray.at(m_iLevelnum);
            
            if(m_iLevelnum<m_StatesTransArray.size()-1)
                nextloc = m_StatesTransArray.at(m_iLevelnum+1);
        }
        StatsManager::getInstance()->setCurrentState(curloc);
        
        StatsManager::getInstance()->setLocations(prevloc,curLocation,nextloc);
    }
    GameScene *scene = new GameScene();
    scene->autorelease();
    Director::getInstance()->replaceScene(TransitionMoveInR::create(0.5f, scene));
    scene = NULL;
}
/// checks for gift is unlocked after completeing region
/// @param playedcount levels played count of particular region
void LevelLayer::checkForGiftUnlock(int playedcount)
{
    int count = 0;//playedcount%5;
    
    int giftunclk = 0;
    if(playedcount>0)
    {
        count = playedcount%5;
        
        if(count==0)
        {
            giftunclk = playedcount/5;
            
            for (int index = 0;index<giftunclk; index++)
            {
                MenuItemImage *giftItem = giftButtonArray->at(index);
                
                if(!StatsManager::getInstance()->getClaimedGiftInLangForCountries(m_StrCountry, giftItem->getTag()))
                {
                    giftItem->setNormalImage(Sprite::create("gift-box1.png"));
                    giftItem->setSelectedImage(Sprite::create("gift-box1.png"));
                    giftItem->setEnabled(true);
                    
                    giftItem->removeChildByName("mark",true);
                    
                    ScaleTo *scaleto = ScaleTo::create(0.3f, giftItem->getScale()+0.15f);
                    ScaleTo *scaleto1 = ScaleTo::create(0.3f, giftItem->getScale());
                    RepeatForever *rep = RepeatForever::create(Sequence::create(scaleto,scaleto1,NULL));
                    rep->setTag(1);
                    giftItem->runAction(rep);
                    
                    
                    StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(m_StrCountry, giftunclk);
                }
            }
        }
    }
}
/// checks for gifts in level map claimed or not to change the claimed gift image
void LevelLayer::checkForGiftsClaimed()
{
    int giftunclk = StatsManager::getInstance()->getUnlockedGiftsCountInLangForCountries(m_StrCountry);
    
    for (int index = 0;index<giftunclk; index++)
    {
        MenuItemImage *giftItem = giftButtonArray->at(index);
        
        if(StatsManager::getInstance()->getClaimedGiftInLangForCountries(m_StrCountry, giftItem->getTag()))
        {
            Sprite *spr =  Sprite::create("gift-box2.png");
            spr->setColor(Color3B(150,150,150));
            giftItem->setDisabledImage(spr);
            giftItem->setEnabled(false);
            
            giftItem->removeChildByName("mark",true);
        }
    }
    
    
}
//CUSTOM BUTTON MADE
MenuItemSprite * LevelLayer :: getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}

/****************************KEYPAD FUNCTIONS********************/
void LevelLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event *event)
{
    
    
}
void LevelLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *unused_event)
{
    if(keyCode==EventKeyboard::KeyCode::KEY_BACK)
    {
        m_ptrPopUpManager->OnKeyButtonPressed();
        GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    }
}
void LevelLayer::backBtnCallback(Ref *sender)
{
    GameController::getInstance()->updateUserDataToFireBase();
    GameController::getInstance()->getLeaderboardData();
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameController::getInstance()->playSFX(SFX_SCREEN_MOVE);
    AchievementManager::getInstance()->SaveAchievements();
    DailyMissionManager::getInstance()->SaveMissions();
    
    Director::getInstance()->purgeCachedData();
    CW_LevelScene *scene = new CW_LevelScene();
    scene->autorelease();
    Director::getInstance()->replaceScene(TransitionSlideInL::create(0.4f,scene));
    scene = NULL;
    
    
}
/// THIS WILL REMOVE CURRENT ONE AND CALL ANOTHER POPUP WAIT FOR ANIMATION OF CURRENT POPUP
/// @param p_fDelayTime delayTime
/// @param p_PopUpName name of popup
void LevelLayer::LoadOtherPopUpWithDelay(float p_fDelayTime, const std::string &p_PopUpName)
{
    DelayTime *t_DelayeTime = DelayTime::create(p_fDelayTime);
    CallFuncN *t_CallFunc = CallFuncN::create(std::bind(&LevelLayer::LoadOtherPopUp, this, this, p_PopUpName));
    this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
}

/// THIS LOAD OTHER POP UP BY COMPARING WITH STRING
/// @param p_Sender ref of popup
/// @param p_String name of popUp
void LevelLayer::LoadOtherPopUp(Ref *p_Sender, const std::string &p_String)
{
    if(p_String.compare("DailyReward")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_DAILY_REWARD);
    }
    else if(p_String.compare("Language")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_LANGUAGE);
    }
    else if(p_String.compare("RemoveAds")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_REMOVE_ADS);
    }
    else if(p_String.compare("FreeCoins")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_FREE_COINS);
    }
    
}
void LevelLayer::settingsBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_SETTINGS);
    setEnableButtons(false);
}
void LevelLayer::achievementBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_ACHIEVEMENT);
    setEnableButtons(false);
    
}
void LevelLayer::shopBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_BUY_COINS);
    setEnableButtons(false);
}
void LevelLayer::addCoinBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_BUY_COINS);
    setEnableButtons(false);
}
void LevelLayer::setEnableButtons(bool p_bEnable)
{
    m_AchievementButton->setEnabled(p_bEnable);
    m_ShopButton->setEnabled(p_bEnable);
    m_CoinsButton->setEnabled(p_bEnable);
    m_BackButton->setEnabled(p_bEnable);
    
    if(m_ptrScrolView)
        m_ptrScrolView->setTouchEnabled(p_bEnable);
}
/// this is called to set the current updated coins count to label
void LevelLayer::UpdateCoins()
{
    std::string coinStr = GameController::getInstance()->getCoinsFormat(me_iCoinCount);
    
    
    if(m_CoinCount)
    {
        m_CoinCount->setString(coinStr);
        m_CoinCount->setColor(Color3B::WHITE);
        if (me_iCoinCount<=50) {
            m_CoinCount->setColor(Color3B::RED);
        }
        
        CoinLabelAnimation();
    }
}
/// coin count label animation when its updated
void LevelLayer::CoinLabelAnimation()
{
    if(m_CoinCount)
    {
        ScaleTo *scale1 = ScaleTo::create(0.4f,1.2f,1.2f);
        ScaleTo *scale2 = ScaleTo::create(0.4f,1.0f,1.0f);
        m_CoinCount->runAction(Sequence::create(scale1,scale2, NULL));
    }
}
/// THIS CREATE UPDATE COIN EVENT
void LevelLayer::CreateUpdateCoinEvent()
{
    if(!m_bIsEventCreated)
    {
        m_bIsEventCreated = true;
        auto t_UpdateCoins = EventListenerCustom::create(EVENT_UPDATE_COINS, CC_CALLBACK_0(LevelLayer::UpdateCoins, this));
        Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_UpdateCoins, this);
    }
}
/// parse the level map  Json data
void LevelLayer::ParseLevelMapData()
{
    
    std::string country = m_StrCountry;
    std::string t_fullPath = FileUtils::getInstance()->fullPathForFilename("Regions_Data.json");
    
    char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str()) + sizeof(int) + 1];
    strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str());
    
    rapidjson::Document t_Regiondoc;
    t_Regiondoc.Parse<kParseStopWhenDoneFlag>(p_Buffer);
    
    if(t_Regiondoc.HasParseError()) // Print parse error
    {
        CCLOG("GetParseError %u\n",t_Regiondoc.GetParseError());
        delete [] p_Buffer;
        p_Buffer = NULL;
        
        return;
    }
    std::vector<std::string> t_StatesArray;
    if(!t_Regiondoc.IsNull())
    {
        if(t_Regiondoc.HasMember("Cntry_States"))
        {
            if(t_Regiondoc["Cntry_States"].HasMember(country.c_str()))
            {
                rapidjson::SizeType size = t_Regiondoc["Cntry_States"][country.c_str()].Size();
                
                m_iStatesTotCount = (int)size;
                
                for(int index = 0; index<size; index++)
                {
                    std::string StateName = t_Regiondoc["Cntry_States"][country.c_str()][index].GetString();
                    t_StatesArray.push_back(StateName);
                }
            }
        }
        std::string startState =  getLevelStartPlaceOnLang();
        
        
        if(country!="India")
        {
            m_StatesArray = t_StatesArray;
        }
        else
        {
            if(me_Language.compare(LANG_TELUGU)==0)
            {
                m_StatesArray.push_back("Andhra Pradesh");
                m_StatesArray.push_back("Telangana");
                
                startState = "Telangana";
                auto itr =  std::find(t_StatesArray.begin(),t_StatesArray.end(), "Andhra Pradesh");
                //
                t_StatesArray.erase(itr);
            }
            else if(me_Language.compare(LANG_ENGLISH)!=0)
            {
                m_StatesArray.push_back(startState);
            }
            
            for (int i=0;i<t_StatesArray.size(); i++)
            {
                if (startState.compare(t_StatesArray.at(i))!=0) {
                    m_StatesArray.push_back(t_StatesArray.at(i));
                }
            }
        }
        
        
        std::string reg = country+"_Regions";
        if(t_Regiondoc.HasMember(reg.c_str()))
        {
            
            size_t count = m_StatesArray.size();
            
            for(int i = 0; i<count; i++)
            {
                std::string strState = m_StatesArray.at(i);
                rapidjson::SizeType size = t_Regiondoc[reg.c_str()][strState.c_str()].Size();
                
                for(int j = 0; j<size; j++)
                {
                    std::string StateName = t_Regiondoc[reg.c_str()][strState.c_str()][j].GetString();
                    m_StatesRegArray.push_back(StateName);
                }
            }
        }
    }
    
    printf("\n StatesCount = %d\n",m_iStatesTotCount);
    printf("\n StatesArrCount = %lu\n",m_StatesArray.size());
    printf("\n RegArrCount = %lu\n",m_StatesRegArray.size());
    
    parseJsonForlevelTranslation();
}
/// parse the regional names translation data  to display on level map
void LevelLayer::parseJsonForlevelTranslation()
{
    
    if (me_Language.compare(LANG_ENGLISH)==0)
    {
        m_StatesTransArray = m_StatesArray;
        m_StatesRegTransArray = m_StatesRegArray;
        return;
    }
    
    std::string country = m_StrCountry;
    std::string t_fullPath = FileUtils::getInstance()->fullPathForFilename("Regions_Data.json");
    
    std::string startState = "";
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_fullPath="Regions_Data.json";
    }
    else if(me_Language.compare(LANG_KANNADA)==0)
    {
        t_fullPath="RegionData/Kan_Reg_Data.json";
        startState="ಕರ್ನಾಟಕ";
    }
    else if (me_Language.compare(LANG_HINDI)==0)
    {
        t_fullPath="RegionData/Hindi_Reg_Data.json";
        startState="दिल्ली";
    }
    else if (me_Language.compare(LANG_MALAYALAM)==0)
    {
        t_fullPath="RegionData/Mal_Reg_Data.json";
        startState="കേരളം";
    }
    else if (me_Language.compare(LANG_MARATHI)==0)
    {
        t_fullPath="RegionData/Marathi_Reg_Data.json";
        startState="महाराष्ट्र";
    }
    else if (me_Language.compare(LANG_ODIA)==0)
    {
        t_fullPath="RegionData/Odia_Reg_Data.json";
        startState="ଓଡିଶା";
    }
    else if (me_Language.compare(LANG_TAMIL)==0)
    {
        t_fullPath="RegionData/Tamil_Reg_Data.json";
        startState="தமிழ்நாடு";
    }
    else if (me_Language.compare(LANG_TELUGU)==0)
    {
        t_fullPath="RegionData/Telugu_Reg_Data.json";
    }
    else if (me_Language.compare(LANG_GUJARATI)==0)
    {
        t_fullPath="RegionData/Gj_Reg_Data.json";
        startState="ગુજરાત";
    }
    else if (me_Language.compare(LANG_BANGLA)==0)
    {
        t_fullPath="RegionData/Bangla_Reg_Data.json";
        startState="পশ্চিমবঙ্গ";
    }
    
    
    char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str()) + sizeof(int) + 1];
    strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str());
    
    rapidjson::Document t_Regiondoc;
    t_Regiondoc.Parse<kParseStopWhenDoneFlag>(p_Buffer);
    
    if(t_Regiondoc.HasParseError()) // Print parse error
    {
        CCLOG("GetParseError %u\n",t_Regiondoc.GetParseError());
        delete [] p_Buffer;
        p_Buffer = NULL;
        
        return;
    }
    std::vector<std::string> t_StatesArray;
    
    country  = LanguageTranslator::getInstance()->getTranslatorStringWithTag(country);
    if(!t_Regiondoc.IsNull())
    {
        if(t_Regiondoc.HasMember("Cntry_States"))
        {
            if(t_Regiondoc["Cntry_States"].HasMember(country.c_str()))
            {
                rapidjson::SizeType size = t_Regiondoc["Cntry_States"][country.c_str()].Size();
                
                for(int index = 0; index<size; index++)
                {
                    std::string StateName = t_Regiondoc["Cntry_States"][country.c_str()][index].GetString();
                    t_StatesArray.push_back(StateName);
                }
            }
        }
        
        
        country = m_StrCountry;
        
        if(country!="India")
        {
            m_StatesTransArray = t_StatesArray;
        }
        else
        {
            if(me_Language.compare(LANG_TELUGU)==0)
            {
                m_StatesTransArray.push_back("ఆంధ్ర ప్రదేశ్");
                m_StatesTransArray.push_back("తెలంగాణా");
                
                startState = "తెలంగాణా";
                auto itr =  std::find(t_StatesArray.begin(),t_StatesArray.end(), "ఆంధ్ర ప్రదేశ్");
                t_StatesArray.erase(itr);
            }
            else if(me_Language.compare(LANG_ENGLISH)!=0)
            {
                m_StatesTransArray.push_back(startState);
            }
            
            for (int i=0;i<t_StatesArray.size(); i++)
            {
                if (startState.compare(t_StatesArray.at(i))!=0) {
                    m_StatesTransArray.push_back(t_StatesArray.at(i));
                }
            }
        }
        
        std::string reg = country+"_Regions";
        if(t_Regiondoc.HasMember(reg.c_str()))
        {
            size_t count = m_StatesTransArray.size();
            for(int i = 0; i<count; i++)
            {
                std::string strState = m_StatesTransArray.at(i);
                rapidjson::SizeType size = t_Regiondoc[reg.c_str()][strState.c_str()].Size();
                
                for(int j = 0; j<size; j++)
                {
                    std::string StateName = t_Regiondoc[reg.c_str()][strState.c_str()][j].GetString();
                    m_StatesRegTransArray.push_back(StateName);
                }
            }
        }
    }
    
    if(m_StatesTransArray.size()<m_StatesArray.size())
    {
        m_StatesTransArray = m_StatesArray;
    }
    
    if(m_StatesRegTransArray.size()<m_StatesRegArray.size())
    {
        m_StatesRegTransArray = m_StatesRegArray;
    }
    
    printf("\n RegArrCount = %lu\n",m_StatesTransArray.size());
    printf("\n RegArrCount = %lu\n",m_StatesRegTransArray.size());
}
///  this checks the current country levels completed or not to unlock new country
void LevelLayer::checkforCurrentCountryCompletion()
{
    
    int TargetCount = StatsManager::getInstance()->getTotalLevelCountForCountries(m_StrCountry);
    int currCount = StatsManager::getInstance()->getPlayedLevelCountForCountries(m_StrCountry);
    
    
    if(currCount>=TargetCount)
    {
        if(!StatsManager::getInstance()->checkIsCountryPlayed(m_StrCountry))
        {
            std::string str = StringUtils::format(CountriesUnlockedCount,me_Language.c_str());
            int count = UserDefault::getInstance()->getIntegerForKey(str.c_str());
            UserDefault::getInstance()->setIntegerForKey(str.c_str(),count+1);
            
            
            this->runAction(Sequence::create(DelayTime::create(0.8f),CallFunc::create(CC_CALLBACK_0( LevelLayer::LoadNextWorldUnlockedPopUp,this)), NULL));
            
            StatsManager::getInstance()->updateIsCountryPlayed(m_StrCountry);
        }
        else
        {
            m_ptrScrolView->scrollToPercentVertical(100,0.0f, true);
        }
    }
    
}
/// LoadNextWorldUnlockedPopUp
void LevelLayer::LoadNextWorldUnlockedPopUp()
{
    
    std::vector<std::string> Arr = StatsManager::getInstance()->m_CountriesArray;
    int unloclcount = StatsManager::getInstance()->getLangUnlockedCountryCount();
    
    
    if(StatsManager::getInstance()->checkAllCountriesPlayed())
    {
        m_ptrPopUpManager->LoadPopUp(POPUP_WORLD_UNLOCK);
        setEnableButtons(false);
        return;
    }
    if(unloclcount<=Arr.size())
    {
        GameController::getInstance()->playSFX(SFX_COUNTRY_UNLOCK);
        m_ptrPopUpManager->LoadPopUp(POPUP_WORLD_UNLOCK);
        setEnableButtons(false);
    }
}
///  this will returns starting state according to language
/// @return std::string  name of the state
std::string LevelLayer::getLevelStartPlaceOnLang()
{
    std::string t_StrPlace = "";
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        
    }
    else if(me_Language.compare(LANG_KANNADA)==0)
    {
        t_StrPlace = "Karnataka";
    }
    else if(me_Language.compare(LANG_HINDI)==0)
    {
        t_StrPlace = "Delhi";
    }
    else if(me_Language.compare(LANG_TELUGU)==0)
    {
        t_StrPlace = "";
    }
    else if(me_Language.compare(LANG_MALAYALAM)==0)
    {
        t_StrPlace = "Kerala";
    }
    else if(me_Language.compare(LANG_MARATHI)==0)
    {
        t_StrPlace = "Maharastra";
    }
    else if(me_Language.compare(LANG_ODIA)==0)
    {
        t_StrPlace = "Orissa";
    }
    else if(me_Language.compare(LANG_TAMIL)==0)
    {
        t_StrPlace = "Tamil Nadu";
    }
    else if(me_Language.compare(LANG_BANGLA)==0)
    {
        t_StrPlace = "WestBengal";
    }
    else if(me_Language.compare(LANG_GUJARATI)==0)
    {
        t_StrPlace = "Gujarat";
    }
    return t_StrPlace;
}
/// region gift box click callback
void LevelLayer::GiftButtonCallback(Ref* sender)
{
    
    MenuItemImage *GiftBtn = (MenuItemImage*)sender;
    
    GiftBtn->stopActionByTag(1);
    
    Sprite *spr =  Sprite::create("gift-box2.png");
    spr->setColor(Color3B(150,150,150));
    GiftBtn->setDisabledImage(spr);
    GiftBtn->setEnabled(false);
    
    t_iGiftTag = GiftBtn->getTag();
    
    StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(m_StrCountry, t_iGiftTag);
    
    AchievementManager::getInstance()->OnAchievementUpdate("giftbox", 1);
    AchievementManager::getInstance()->CheckAllAchievementsComplete();
    
    DailyMissionManager::getInstance()->OnDailyMissionUpdate("giftbox", 1);
    DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();
    
    
    m_ptrPopUpManager->LoadPopUp(POPUP_GIFT_CLAIM);
    setEnableButtons(false);
}
/// THIS UPDATE  BUTTON TAGS AND DATA
void LevelLayer::UpdateMenuData()
{
    switch (m_ptrPopUpManager->getPopUpType())
    {
        case POPUP_DAILY_MISSION:
        {
        }break;
        case POPUP_DAILY_REWARD:
        {
            
        }break;
        case POPUP_ACHIEVEMENT:
        {
            checkForAchievementUpdates();
        }break;
        case POPUP_LANGUAGE:
        {
            
        }break;
        default:
            break;
    }
}
/// Checks Any new achievemnet completed or not if its completed changes the achievement button tag
void LevelLayer::checkForAchievementUpdates()
{
    if(m_AchievementButton)
    {
        Sprite* SprTask = (Sprite*)m_AchievementButton->getChildByName("tag");
        
        if(AchievementManager::getInstance()->checkAllAchievementsClaimed())
        {
            SprTask->setTexture("Task_Completed.png");
            SprTask->setVisible(true);
            return;
        }
        
        if(AchievementManager::getInstance()->getCountOfAchievementsToClaim()>=1)
        {
            SprTask->setTexture("Claim.png");
            SprTask->setVisible(true);
        }
        else
        {
            SprTask->setVisible(false);
        }
    }
}
/// checks for rate popup display condition
void LevelLayer::checkForRatePopUp()
{
    
    if(GameController::getInstance()->getIntegerForKey("rateCounter") % 2 == 0 &&GameController::getInstance()->getIntegerForKey(LEVEL_RATE)==1&& GameController::getInstance()->getBoolForKey("rate") == false && GameController::getInstance()->getBoolForKey("showrate") == false)
    {
        GameController::getInstance()->setBoolForKey("showrate",true);
        
        DelayTime *delay = DelayTime::create(1.5f);
        CallFuncN *callfunc = CallFuncN::create(std::bind(&LevelLayer::showRatePopUp,this));
        this->runAction(Sequence::create(delay,callfunc,NULL));
    }
}
/// displays rate popup
void LevelLayer::showRatePopUp()
{
    {
        m_ptrPopUpManager->LoadPopUp(POPUP_RATE_GAME);
        setEnableButtons(false);
    }
}
LevelLayer::~LevelLayer()
{
    if(m_ptrScrolView)
    {
        m_ptrScrolView->removeAllChildren();
        this->removeChild(m_ptrScrolView,true);
        m_ptrScrolView = NULL;
    }
    if(m_bIsEventCreated)
    {
        Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_UPDATE_COINS);
    }
    
    if(m_ptrPopUpManager)
    {
        this->removeChild(m_ptrPopUpManager,true);
        m_ptrPopUpManager = NULL;
    }
    
    if(levelButtonsArray != NULL)
    {
        levelButtonsArray->clear();
        delete levelButtonsArray;
        levelButtonsArray = NULL;
    }
    
    if(giftButtonArray != NULL)
    {
        giftButtonArray->clear();
        delete giftButtonArray;
        giftButtonArray = NULL;
    }
    
    if (m_StatesArray.size()>0) {
        m_StatesArray.clear();
    }
    
    if (m_StatesRegArray.size()>0) {
        m_StatesRegArray.clear();
    }
    
    this->removeAllChildrenWithCleanup(true);
}
