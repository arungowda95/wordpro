//
//  LanguagePopUp.cpp
//  wordPro-mobile
//
//  Created by id on 11/03/20.
//

#include "LanguagePopUp.h"

LanguagePopUp::LanguagePopUp(std::function<void()> func)
{
    
    m_fnSelector = func;
     t_miLanguageMenu = NULL;

    t_strLanguage = "";
    Rect Res = Director::getInstance()->getSafeAreaRect();
    Size visibleSize = Size(Res.size.width,Res.size.height);
    Vec2 origin = Res.origin;
            
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    float width = (m_fScale*90)/2;
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()+0.15);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    
    std::string t_Label = "Language";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tLabel->setWidth(m_SprPopUpTop->getContentSize().width-64);
    t_tLabel->setHeight(150);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMidY()));
    this->addChild(t_tLabel, 2);
    
    m_PopUpRect = m_SprPopUpBg->getBoundingBox();
    
    
    
    printf("LANGUAGE_POS-->%f\n",t_tLabel->getPositionY());
    
    std::string t_SubTxt = "LangMsg";
    Label *t_tSubLabel =  createLabelBasedOnLang(t_SubTxt,32);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::TOP);
    t_tSubLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-20));
    t_tSubLabel->setColor(Color3B::WHITE);
    this->addChild(t_tSubLabel, 2);

    if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    {
     t_tSubLabel->enableOutline(Color4B::BLACK,3);
    }
    
    t_miLanguageMenu = Menu::create();
    t_miLanguageMenu->setPosition(Vec2::ZERO);
    this->addChild(t_miLanguageMenu,1);

    
    // UP BUTTON
    t_miUpButton =getButtonMade("up-arrow_Btn.png","up-arrow_Btn.png",CC_CALLBACK_1(LanguagePopUp::arrowbtnpressed,this));
    t_miUpButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tSubLabel->getBoundingBox().getMinY()-t_miUpButton->getBoundingBox().size.height/2));
    t_miUpButton->setName("TopArrow");

    printf("t_miUpButton-->%f\n",t_miUpButton->getPositionY());
    
    // Confirm BUTTON
    MenuItemSprite *t_miConfrmBtn =getButtonMade("medium_button.png","medium_button.png",CC_CALLBACK_1(LanguagePopUp::OnButtonPressed,this));
    t_miConfrmBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()+t_miConfrmBtn->getBoundingBox().size.height+40));
    t_miConfrmBtn->setTag(1);

    
    // DOWN BUTTON
        t_miDownButton =getButtonMade("down-arrow_Btn.png","down-arrow_Btn.png",CC_CALLBACK_1(LanguagePopUp::arrowbtnpressed,this));
        t_miDownButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_miConfrmBtn->getBoundingBox().getMaxY()+t_miDownButton->getBoundingBox().size.height/2+10));
        t_miDownButton->setName("DownArrow");
    
    
        m_ptrScrolView = cocos2d::ui::ScrollView::create();
        m_ptrScrolView->setDirection(ui::ScrollView::Direction::VERTICAL);
        m_ptrScrolView->setTouchEnabled(true);
        m_ptrScrolView->setBounceEnabled(false);
        Size contentsize = Size(m_SprPopUpBg->getBoundingBox().size.width,t_miUpButton->getBoundingBox().getMinY()-t_miDownButton->getBoundingBox().getMaxY());
        m_ptrScrolView->setContentSize(Size(contentsize));
        m_ptrScrolView->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_miUpButton->getBoundingBox().getMinY()-m_ptrScrolView->getBoundingBox().size.height/2));
        m_ptrScrolView->addEventListener(CC_CALLBACK_2(LanguagePopUp::onScrolling,this));
        m_ptrScrolView->setScrollBarEnabled(false);
        m_ptrScrolView->setSwallowTouches(false);
        m_ptrScrolView->setAnchorPoint(Vec2(0.5f,0.5f));
    
    
    
    float totalNum = 10;

    
       float pathHeight = 124;
       
       Size Containersize = Size(contentsize.width,pathHeight*(totalNum));
       m_ptrScrolView->setInnerContainerSize(Size(Containersize.width,Containersize.height));

       Point t_position = Vec2(m_ptrScrolView->getContentSize().width/2,Containersize.height);
    
    std::string curLang = UserDefault::getInstance()->getStringForKey(LANGUAGE);
    t_strLanguage = curLang;
    for(int index =1; index<=10; index++)
    {
        std::string str = LangStrArr.at(index-1);
        Button *m_menuBtn =  ui::Button::create("Lang_Btn_Norm.png","Lang_Btn_Norm.png");
        m_menuBtn->setPosition(Vec2(t_position.x,t_position.y-m_menuBtn->getBoundingBox().size.height/2));
        m_menuBtn->addClickEventListener(CC_CALLBACK_1(LanguagePopUp::languageBtnCallback, this));
        m_menuBtn->setTitleFontSize(40);
        
        m_menuBtn->setTitleText(getLanguageString(index));
        
        m_menuBtn->setTitleColor(Color3B::WHITE);
        m_menuBtn->setTag(index);
        m_menuBtn->setName("");
        m_ptrScrolView->addChild(m_menuBtn,1);
        
        t_position.y -= m_menuBtn->getBoundingBox().size.height;
        
        if(curLang.compare(str)==0)
        {
            m_menuBtn->loadTextureNormal("Lang_Btn_Sel.png");
            m_menuBtn->loadTexturePressed("Lang_Btn_Sel.png");
        }
    }

    this->addChild(m_ptrScrolView,2);
    
    if(!curLang.empty())
    {
        size_t index = std::distance(LangStrArr.begin(),
                                     std::find(LangStrArr.begin(), LangStrArr.end(), curLang.c_str()));

        float val = (float)(float(index)/(10))*100;

        if(index>1)
        {
            m_ptrScrolView->scrollToPercentVertical(val,0.0f,true);
            
            if(index>=7)
            {
                m_ptrScrolView->scrollToPercentVertical(100.0f,0.0f,true);

            }
        }
    }

    float percentage = m_ptrScrolView->getScrolledPercentVertical();
      if(percentage<=10.0f)
      {
          t_miUpButton->setEnabled(false);
          t_miDownButton->setEnabled(true);
      }
     else if(percentage>=10.0f)
      {
          t_miUpButton->setEnabled(true);
          t_miDownButton->setEnabled(true);
          
          if (percentage>=85)
          {
              t_miUpButton->setEnabled(true);
              t_miDownButton->setEnabled(false);
          }
     }
    
    std::string t_Txt = "Confirm";
    Label *ConfirmLabel = createLabelBasedOnLang(t_Txt,40);
    ConfirmLabel->setColor(Color3B::WHITE);
    ConfirmLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    ConfirmLabel->enableOutline(Color4B(53,126,32,255),2);
    ConfirmLabel->setWidth(t_miConfrmBtn->getContentSize().width-30);
    ConfirmLabel->setHeight(150);
    ConfirmLabel->setPosition(Vec2(t_miConfrmBtn->getBoundingBox().size.width/2,t_miConfrmBtn->getBoundingBox().size.height/2+10));
    t_miConfrmBtn->addChild(ConfirmLabel,1);
    
    
    if(me_Language.compare(LANG_TAMIL)==0)
    {
        ConfirmLabel->setSystemFontSize(35);
    }
    
    // CLOSE BUTTON
     MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(LanguagePopUp::OnButtonPressed,this));
     t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
     t_miCloseButton->setTag(0);

     Menu *m_Menu = Menu::create(t_miCloseButton,t_miConfrmBtn,t_miUpButton,t_miDownButton,NULL);
     m_Menu->setPosition(Vec2::ZERO);
     this->addChild(m_Menu,2);

    if(!UserDefault::getInstance()->getBoolForKey("FIRST_LANG"))
    {
        t_miCloseButton->setVisible(false);
    }
}
Label *LanguagePopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        if (langstr=="Language") {
           std::transform(langstr.begin(), langstr.end(), langstr.begin(), ::toupper);
        }

        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
void LanguagePopUp::addLanguageButton(Point Pos,const std::string &name)
{
      MenuItemSprite *t_milanguageBtn = getButtonMade("","",CC_CALLBACK_1(LanguagePopUp::OnButtonPressed,this));
      t_milanguageBtn->setPosition(Pos);
      t_milanguageBtn->setName(name);

    
      Label *t_LangText = Label::createWithSystemFont(name,SYS_FONT_NAME, 35);
      t_LangText->setPosition(Vec2(t_LangText->getContentSize().width/2,t_LangText->getContentSize().height/2));
      t_LangText->setColor(Color3B::BLACK);
      t_milanguageBtn->addChild(t_LangText,1);
    
    t_miLanguageMenu->addChild(t_milanguageBtn,1);
}
/// CUSTOM BUTTON MADE
MenuItemSprite *LanguagePopUp :: getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    Sprite *_DisableSpr = Sprite::create(normalSpr);
    _DisableSpr->setOpacity(150);

    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr,_SelectedSpr,_DisableSpr,func);
    return Button;
}
void LanguagePopUp::onScrolling(Ref* sender, ui::ScrollView::EventType type)
{
    if (type == ui::ScrollView::EventType::SCROLLING_ENDED)
    {
        printf("Scroll_percent-->%f",m_ptrScrolView->getScrolledPercentVertical());
        
        float percent = m_ptrScrolView->getScrolledPercentVertical();
        
        if(percent<=5)
        {
            t_miUpButton->setEnabled(false);
            t_miDownButton->setEnabled(true);
        }
        else if(percent>=5)
        {
             t_miUpButton->setEnabled(true);
             t_miDownButton->setEnabled(true);

             if (percent>=90)
            {
                t_miDownButton->setEnabled(false);
                t_miUpButton->setEnabled(true);
            }

        }
    }
}

///  BUTTON PRESSED FUNCTIONS RETURN CALL BACK TO RESPECTIVE LAYER FROM IT CALLED
void LanguagePopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
      
      int pButtonTag =  Button->getTag();

      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"close";
              printf("\n Language-->%s\n",me_Language.c_str());
          }break;
          case 1:
          {
              status = (char*)"Lang_confirm";
              if(t_strLanguage=="")
              {
                  {
                      me_Language = UserDefault::getInstance()->getStringForKey(LANGUAGE);
                  }
              }
              else
              {
                  if(!UserDefault::getInstance()->getBoolForKey("FIRST_LANG"))
                  {
                      UserDefault::getInstance()->setBoolForKey("FIRST_LANG",true);
                  }
                  UserDefault::getInstance()->setStringForKey(LANGUAGE,t_strLanguage);
                  me_Language = UserDefault::getInstance()->getStringForKey(LANGUAGE);
              }
              printf("\n Language-->%s\n",me_Language.c_str());
          }
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}
void LanguagePopUp::languageBtnCallback(Ref *p_Sender)
{
    Button *Langbtn = (Button*)p_Sender;
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    int tag = Langbtn->getTag();
    
    int count = (int)m_ptrScrolView->getChildrenCount();
    
    for(int i=1; i<=count; i++)
    {
        Button *Langbtn1 = (Button*)m_ptrScrolView->getChildByTag(i);
        
        if (i==tag)
        {
            Langbtn1->setName("");
            Langbtn1->loadTextureNormal("Lang_Btn_Sel.png");
            Langbtn1->loadTexturePressed("Lang_Btn_Sel.png");
        }else{
            Langbtn1->setName("selected");
            Langbtn1->loadTextureNormal("Lang_Btn_Norm.png");
            Langbtn1->loadTexturePressed("Lang_Btn_Norm.png");
        }
    }
    
    
    switch (tag)
     {
         case 1:
             t_strLanguage = LANG_ENGLISH;
             break;
         case 2:
             t_strLanguage = LANG_HINDI;
             break;
         case 3:
             t_strLanguage = LANG_KANNADA;
             break;
         case 4:
             t_strLanguage = LANG_TELUGU;
             break;
         case 5:
             t_strLanguage = LANG_TAMIL;
             break;
         case 6:
             t_strLanguage = LANG_MALAYALAM;
             break;
         case 7:
             t_strLanguage = LANG_MARATHI;
             break;
         case 8:
             t_strLanguage = LANG_BANGLA;
             break;
         case 9:
             t_strLanguage = LANG_ODIA;
             break;
        case 10:
             t_strLanguage = LANG_GUJARATI;
            break;
         default:
             break;
     }
    log("\n Language-->%s\n",t_strLanguage.c_str());

    
}
void LanguagePopUp::arrowbtnpressed(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    MenuItemSprite *arrowBtn = (MenuItemSprite*) sender;
    std::string btnName = arrowBtn->getName();
    
    if(btnName.compare("TopArrow")==0)
    {
        m_ptrScrolView->scrollToTop(0.8f, true);
        arrowBtn->setEnabled(false);
        
        t_miDownButton->setEnabled(true);
    }
    else if (btnName.compare("DownArrow")==0)
    {
      m_ptrScrolView->scrollToBottom(0.8f, true);
      arrowBtn->setEnabled(false);

      t_miUpButton->setEnabled(true);
    }
}
std::string LanguagePopUp::getLanguageString(int tag)
{
    //    std::string LangStrArr[10] = {LANG_ENGLISH,"हिंदी","ಕನ್ನಡ","తెలుగు","தமிழ்","മലയാളം","मराठी","বাংলা","ଓଡ଼ିଆ","ગુજરાતી"};//

    std::string lang = LANG_ENGLISH;
    switch (tag)
     {
         case 1:
             lang = "ENGLISH";
             break;
         case 2:
             lang = "हिंदी";
             break;
         case 3:
             lang = "ಕನ್ನಡ";
             break;
         case 4:
             lang = "తెలుగు";
             break;
         case 5:
             lang = "தமிழ்";
             break;
         case 6:
             lang = "മലയാളം";
             break;
         case 7:
             lang = "मराठी";
             break;
         case 8:
             lang = "বাংলা";
             break;
         case 9:
             lang = "ଓଡ଼ିଆ";
             break;
        case 10:
             lang = "ગુજરાતી";
            break;
         default:
             break;
     }
    return lang;
}
LanguagePopUp::~LanguagePopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
