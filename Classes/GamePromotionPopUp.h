//
//  GamePromotionPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 15/06/20.
//

#ifndef GamePromotionPopUp_h
#define GamePromotionPopUp_h

#include <stdio.h>
#include "PopUp.h"

class GamePromotionPopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    public:
     GamePromotionPopUp(std::function<void()> func);
    ~GamePromotionPopUp();
    
    void InstallCallBack();
    
    void OnButtonPressed(Ref *p_Sender);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);

};

#endif /* GamePromotionPopUp_hpp */
