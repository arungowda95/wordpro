#include "GameController.h"

GameController* GameController::gameObject = NULL;

void GameController::createScene(int screenIndex, int level /*= 0*/) {
}

void GameController::setBGMEnabled(bool enableMusic) {
	bgmEnabled = enableMusic;
	setBoolForKey("isBGMEnable", bgmEnabled);
}

void GameController::setSFXEnabled(bool enableSFX) {
	sfxEnabled = enableSFX;
	setBoolForKey("isSFXEnable", sfxEnabled);
}

void GameController::seVibrateEnabled(bool enableVibrate) {
	vibrateEnabled = enableVibrate;
	setBoolForKey("isVibrateEnable", isVibrateEnabled());
}
// For Music / SFX
bool GameController::isBGMEnabled()
{
    bgmEnabled = getBoolForKey("isBGMEnable");
    return bgmEnabled;
}
bool GameController::isSFXEnabled()
{
    sfxEnabled = getBoolForKey("isSFXEnable");
    return sfxEnabled;
}
void GameController::doVibrate() {
}

void GameController::playSFX(const char* index) {
	if (isSFXEnabled()) {
		SimpleAudioEngine::getInstance()->playEffect(index);
	}
}

void GameController::playBGM(const char* index, bool isLoop) {
	if (isBGMEnabled()) {
		SimpleAudioEngine::getInstance()->playBackgroundMusic(index, isLoop);
	}
}

void GameController::stopSounds() {
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->stopAllEffects();
}

void GameController::pauseSounds() {
	if (isBGMEnabled())
		SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	if (isSFXEnabled())
		SimpleAudioEngine::getInstance()->pauseAllEffects();
}

void GameController::resumeSounds() {
	if (isBGMEnabled())
		SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	if (isSFXEnabled())
		SimpleAudioEngine::getInstance()->resumeAllEffects();
}

long GameController::currentTimeMillis() {
	return (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count());
}

int GameController::getRandom(int lowerLimit, int upperLimit) {
	return (lowerLimit + (rand() % (upperLimit - lowerLimit + 1)));
}

int GameController::getRandom(int lowerLimit, int upperLimit, int gap) {
	int low = (lowerLimit / gap) + ((lowerLimit % gap == 0) ? 0 : 1);
	int high = upperLimit / gap;
	return ((low + (rand() % (high - low + 1))) * gap);
}

void GameController::initLastDay()
{
	time_t t = time(0);
	tm *today = localtime(&t);

	int year = getIntegerForKey("lastYear", today->tm_year - 1);
	int month = getIntegerForKey("lastMonth", today->tm_mon);
	int mday = getIntegerForKey("lastMDay", today->tm_mday);

	lastDay = dayStruct(mday, month, year);
    
      year = getIntegerForKey("firstYear", today->tm_year-1);
      month =  getIntegerForKey("firstMonth", today->tm_mon);
      mday = getIntegerForKey("firstMDay", today->tm_mday);

    firstDay = dayStruct(mday, month,year);
    
    
}

bool GameController::isDayHasPassed() {
	bool dayHasPassed = false;
	time_t t = time(0);
	tm *today = localtime(&t);

	if (today->tm_year >= lastDay.year) {
		if (today->tm_mon >= lastDay.month) {
			if (today->tm_mday > lastDay.mday)
				return true;
			if (today->tm_mon > lastDay.month)
				return true;
		}
		if (today->tm_year > lastDay.year)
			return true;
	}

	return dayHasPassed;
}

void GameController::setTodayAsLastDay()
{
	time_t t = time(0);
	tm *today = localtime(&t);

	setIntegerForKey("lastYear", today->tm_year);
	setIntegerForKey("lastMonth", today->tm_mon);
	setIntegerForKey("lastMDay", today->tm_mday);

	lastDay = dayStruct(today->tm_mday, today->tm_mon, today->tm_year);
}
int GameController::getDayDifferenceCount()
{
        int noOfDays = 0;
        time_t t = time(0);
        tm *today = localtime(&t);

        if (today->tm_year >= firstDay.year)
        {
            if (today->tm_mon >= firstDay.month)
            {
                if (today->tm_mday > firstDay.mday)
                {
                    noOfDays = today->tm_mday - firstDay.mday;
                    if (noOfDays>=2)
                    {
                        setTodayAsfirstDay();
                        return noOfDays;
                    }
                }
                if (today->tm_mon > firstDay.month)
                {
                    noOfDays = today->tm_mon - firstDay.month;
                    if (noOfDays>=1)
                    {
                        setTodayAsfirstDay();
                        noOfDays = 2;
                        return noOfDays;
                    }
                }
            }
            if (today->tm_year > firstDay.year)
            {
                noOfDays = today->tm_year - firstDay.year;
                if (noOfDays>=1)
               {
                   setTodayAsfirstDay();
                   noOfDays = 2;
                   return noOfDays;
               }
            }
        }
    return noOfDays;
}
void GameController::setTodayAsfirstDay()
{
    time_t t = time(0);
    tm *today = localtime(&t);

    setIntegerForKey("firstYear", today->tm_year);
    setIntegerForKey("firstMonth", today->tm_mon);
    setIntegerForKey("firstMDay", today->tm_mday);

    firstDay = dayStruct(today->tm_mday, today->tm_mon, today->tm_year);
        
}
void GameController::checkForRatePopUp()
{

    if(isDayHasPassed())
    {
                // this value will notify the no of times this game is opened
        GameController::getInstance()->setIntegerForKey("rateCounter", 1);
        GameController::getInstance()->setBoolForKey("showrate", false);
    }else{
    GameController::getInstance()->setIntegerForKey("rateCounter", GameController::getInstance()->getIntegerForKey("rateCounter") + 1);

     GameController::getInstance()->setIntegerForKey(LEVEL_RATE,0);
    }
}
bool GameController::getBoolForKey(const char* key, bool defaultValue /*= false*/) {
	std::string res = getStringForKey(key, (defaultValue) ? StringUtils::format("%strue", key).c_str() : StringUtils::format("%sfalse", key).c_str());
	if (res.compare(StringUtils::format("%strue", key)) == 0)
		return true;
	else
		return false;
}

void GameController::setBoolForKey(const char* key, bool value) {
	setStringForKey(key, (value) ? StringUtils::format("%strue", key).c_str() : StringUtils::format("%sfalse", key).c_str());
}

int GameController::getIntegerForKey(const char* key, int defaultValue /*= 0*/) {
	std::string res = getStringForKey(key, StringUtils::format("%d", defaultValue));
	int val;
	std::istringstream(res) >> val;
	return val; // return (std::stoi(res));
}

void GameController::setIntegerForKey(const char* key, int value) {
	setStringForKey(key, StringUtils::format("%d", value));
}

float GameController::getFloatForKey(const char* key, float defaultValue /*= 0.0f*/) {
	std::string res = getStringForKey(key, StringUtils::format("%f", defaultValue));
	return (std::atof(res.c_str()));
}

void GameController::setFloatForKey(const char* key, float value) {
	setStringForKey(key, StringUtils::format("%f", value));
}

double GameController::getDoubleForKey(const char* key, double defaultValue /*= 0*/) {
	std::string res = getStringForKey(key, StringUtils::format("%lf", defaultValue));
	return (std::atof(res.c_str()));
}

void GameController::setDoubleForKey(const char* key, double value) {
	setStringForKey(key, StringUtils::format("%lf", value));
}

// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
std::vector<std::string> &GameController::split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
std::vector<std::string> GameController::split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

std::string GameController::getCoinsFormat( int chips)
{
    bool bonus = false;
    std::string finalChips = "";
    if (std::strlen(StringUtils::format("%d", chips).c_str()) <= 4)//100
        finalChips = StringUtils::format("%d", chips);
    else
        finalChips = StringUtils::format("%d", chips);

//    if (std::strlen(StringUtils::format("%d", chips).c_str()) == 4)//1,000
//    {
//        std::string no = StringUtils::format("%d", chips).c_str(), str = "";
//        str = no.substr(0, 1).c_str();
//        str += ",";
//        str += no.substr(1, 3).c_str();
//        finalChips = str;
//    }
//    else
    if (std::strlen(StringUtils::format("%d", chips).c_str()) == 5)//10,000
    {
        std::string no = StringUtils::format("%d", chips).c_str(), str = "";
        str = no.substr(0, 2).c_str();
        if (!bonus)
        {
            str += ".";
            str += no.substr(1, 1).c_str();
            str += " K";
        }
        else
        {
            str += ",";
            str += no.substr(2, 3).c_str();
        }
        finalChips = str;
    }
    if (std::strlen(StringUtils::format("%d", chips).c_str()) == 6)//4,52,900
    {
        std::string no = StringUtils::format("%d", chips).c_str(), str = "";
        str = no.substr(0, 1).c_str();
        if (!bonus)
        {
            str += ".";
            str += no.substr(1, 1).c_str();
            str += " L";
        }
        else
        {
            str += ",";
            str += no.substr(1, 2).c_str();
            str += ",";
            str += no.substr(3, 3).c_str();
        }
        finalChips = str;
    }
    else if (std::strlen(StringUtils::format("%d", chips).c_str()) == 7)//10,00,000
    {
        std::string no = StringUtils::format("%d", chips).c_str(), str = "";
        str = no.substr(0, 2).c_str();
        if (!bonus)
        {
            str += ".";
            str += no.substr(2, 2).c_str();
            str += " L";
        }
        else
        {
            str += ",";
            str += no.substr(2, 2).c_str();
            str += ",";
            str += no.substr(4, 3).c_str();
        }
        finalChips = str;
    }
    else if (std::strlen(StringUtils::format("%d", chips).c_str()) == 8)//1,00,00,000
    {
        std::string no = StringUtils::format("%d", chips).c_str(), str = "";
        str = no.substr(0, 1).c_str();
        if (!bonus)
        {
            str += ".";
            str += no.substr(1, 2).c_str();
            str += " Cr";
        }
        else
        {
            str += ",";
            str += no.substr(1, 2).c_str();
            str += ",";
            str += no.substr(3, 2).c_str();
            str += ",";
            str += no.substr(5, 3).c_str();
        }
        finalChips = str;
    }
    else if (std::strlen(StringUtils::format("%d", chips).c_str()) >= 9)//10,00,00,000
    {
        std::string no = StringUtils::format("%d", chips).c_str(), str = "";
        str = no.substr(0, 2).c_str();
        if (!bonus)
        {
            str += ".";
            str += no.substr(2, 2).c_str();
            str += " Cr";
        }
        else
        {
            str += ",";
            str += no.substr(2, 2).c_str();
            str += ",";
            str += no.substr(4, 2).c_str();
            str += ",";
            str += no.substr(6, no.length() - 6).c_str();
        }
        finalChips = str;
    }
    //CCLOG("finalChips = %s, chips = %d", finalChips.c_str(), chips);
    return finalChips;
}
std::string GameController::getStringForKey(const char* key, std::string defaultValue /*= ""*/) {
#ifdef ENCRYPT_DATA
	xxtea_long retLength = 0;

	std::string revertStr = UserDefault::getInstance()->getStringForKey(key, defaultValue);

	if (revertStr.compare(defaultValue) == 0) {
		return defaultValue;
	}
	else {
		unsigned const char *gKey = reinterpret_cast<const unsigned char *> (getGameKey());
		xxtea_long gKeyLength = (xxtea_long)(sizeof(getGameKey()) - 1);

		unsigned char* output = NULL;
		int outLength = cocos2d::base64Decode((unsigned char*)revertStr.c_str(), (unsigned int)strlen(revertStr.c_str()), &output);
		char *decryptedData = reinterpret_cast<char*>(xxtea_decrypt(output, outLength, (unsigned char *)gKey, gKeyLength, &retLength));

		return decryptedData;
	}
#else
	return UserDefault::getInstance()->getStringForKey(key, defaultValue);
#endif
}

void GameController::setStringForKey(const char* key, std::string value) {
#ifdef ENCRYPT_DATA
	xxtea_long retLength = 0;

	unsigned const char *data = reinterpret_cast<const unsigned char *> (value.c_str());
	xxtea_long dataLength = (xxtea_long)(value.size());

	unsigned const char *gKey = reinterpret_cast<const unsigned char *> (getGameKey());
	xxtea_long gKeyLength = (xxtea_long)(sizeof(getGameKey()) - 1);

	unsigned const char *encryptedData = reinterpret_cast<unsigned const char*>(xxtea_encrypt((unsigned char *)data, dataLength, (unsigned char *)gKey, gKeyLength, &retLength));

	char* out = NULL;
	cocos2d::base64Encode(encryptedData, retLength, &out);
	std::string outStr(out);

	UserDefault::getInstance()->setStringForKey(key, outStr);
#else
	UserDefault::getInstance()->setStringForKey(key, value);
#endif
}

unsigned char* GameController::getGameKey() {
	return (unsigned char*)gameKey.c_str();
}

void GameController::setGameKey() {
}
std::string GameController::getBase64EncodeForFile(std::string filePath) {

	Data fileData = FileUtils::getInstance()->getDataFromFile(filePath);
	char* out = NULL;
	cocos2d::base64Encode(fileData.getBytes(), fileData.getSize(), &out);
	fileData.clear();

	return out;
}


void GameController::setLanguageId(std::string value)
{
	setStringForKey("languageId", value);
}

std::string GameController::getLanguageId()
{
	std::string lId;

	return lId;
}
void GameController::getLeaderboardData()
{
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->getLeaderBoardJson(me_Language);
    }
}
void GameController::getUserLanguageData()
{
    if(getStringForKey("connectAs").compare("facebook")==0)
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->getUserLanguageDataJson(getStringForKey("facebookID"));
    }
    else if(getStringForKey("connectAs").compare("Apple")==0)
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->getUserLanguageDataJson(getStringForKey("AppleId"));
    }
}
void GameController::updateUserDataToFireBase()
{
    int currentlevel = StatsManager::getInstance()->getLangPlayedlevelCount();

    if(getStringForKey("connectAs").compare("facebook")==0)
    {
        
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->pushDataToUserdata(getStringForKey("facebookID"), getStringForKey("facebookName"),getStringForKey("profileUrl"),me_Language,currentlevel,me_iCoinCount);
    
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->pushFirebaseUserLanguageData(getStringForKey("facebookID"),me_Language,currentlevel,me_iCoinCount);
    }
    else if(getStringForKey("connectAs").compare("Apple")==0)
    {
        ///  update data with the apple id
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->pushDataToUserdata(getStringForKey("AppleId"), getStringForKey("AppleIdName"),getStringForKey("profileUrl"),me_Language,currentlevel,me_iCoinCount);
    
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->pushFirebaseUserLanguageData(getStringForKey("AppleId"),me_Language,currentlevel,me_iCoinCount);
    }
}

void GameController::parseLanguageDataUserIds(std::string me_Strjson)
{
//    userIds
    log("userIds");
    rapidjson::Document doc;
    doc.Parse<0>(me_Strjson.c_str());
    if(doc.HasMember("languageData"))
    {
        if(doc["languageData"].IsArray())
        {
            size_t idsSize = doc["languageData"].GetArray().Size();
            for(int i=0; i<idsSize; i++)
            {
                userIds.push_back(doc["languageData"][i].GetString());
            }
        }
    }
}
void GameController::updateUserDataInGame(std::string userdata)
{
    
    if(getStringForKey("connectAs").compare("facebook")==0)
    {
        
        std::string  fullPath = FileUtils::getInstance()->fullPathForFilename("Sample_UserLangData.json");
        printf("\n Full path =%s\n",fullPath.c_str());
        
        char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(fullPath).c_str()) + sizeof(int) + 1];
        strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(fullPath).c_str());
        
        
        if (userdata.empty()) {
            userdata = getStringForKey("UserlangData");
            me_strUserDataJson = userdata;
        }
        
        rapidjson::Document userDoc;
        userDoc.Parse<0>(userdata.c_str());
        
        
        if(userDoc.HasParseError()) // Print parse error
        {
            CCLOG("GetParseError %u\n",userDoc.GetParseError());
            return;
        }
        
        std::string fbid = getStringForKey("facebookID");
        int Curlevel = 0;
        int coincount = 0;
        bool isupdate = false;
        
        if(userDoc.HasMember(fbid.c_str()))
        {
            const  rapidjson::Value& b = userDoc[fbid.c_str()];
            if(!b.IsNull())
            {
                log("crash_2");
                if(b.HasMember(me_Language.c_str()))
                {
                    log("crash_1");
                    if(b[me_Language.c_str()].HasMember("currentLevel"))
                        Curlevel = b[me_Language.c_str()]["currentLevel"].GetInt();
                    
                    if(b[me_Language.c_str()].HasMember("coinCount"))
                        coincount = b[me_Language.c_str()]["coinCount"].GetInt();
                }
            }
            
            if(Curlevel>StatsManager::getInstance()->getLangPlayedlevelCount())
            {
                isupdate = true;
                StatsManager::getInstance()->updateLangPlayedlevelCount(Curlevel,true);
                
                
                StatsManager::getInstance()->updateInGameLevelDataFromUserData(Curlevel);
            }
            
            if(me_iCoinCount<coincount)
            {
                isupdate = true;
                UserDefault::getInstance()->setIntegerForKey(COINS,coincount);
                me_iCoinCount = UserDefault::getInstance()->getIntegerForKey(COINS);
                
                Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
            }
        }
        if(isupdate) {
            updateUserDataToFireBase();
            getLeaderboardData();
        }
    }
    else if(getStringForKey("connectAs").compare("Apple")==0)
    {
        
        std::string  fullPath = FileUtils::getInstance()->fullPathForFilename("Sample_UserLangData.json");
        printf("\n Full path =%s\n",fullPath.c_str());
        
        char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(fullPath).c_str()) + sizeof(int) + 1];
        strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(fullPath).c_str());
        
        
        if (userdata.empty()) {
            userdata = getStringForKey("UserlangData");
            me_strUserDataJson = userdata;
        }
        
        rapidjson::Document userDoc;
        userDoc.Parse<0>(userdata.c_str());
        
        
        if(userDoc.HasParseError()) // Print parse error
        {
            CCLOG("GetParseError %u\n",userDoc.GetParseError());
            return;
        }
        
        std::string appleId = getStringForKey("AppleId");
        int Curlevel = 0;
        int coincount = 0;
        bool isupdate = false;
        
        if(userDoc.HasMember(appleId.c_str()))
        {
            const  rapidjson::Value& b = userDoc[appleId.c_str()];
            if(!b.IsNull())
            {
                log("crash_2");
                if(b.HasMember(me_Language.c_str()))
                {
                    log("crash_1");
                    if(b[me_Language.c_str()].HasMember("currentLevel"))
                        Curlevel = b[me_Language.c_str()]["currentLevel"].GetInt();
                    
                    if(b[me_Language.c_str()].HasMember("coinCount"))
                        coincount = b[me_Language.c_str()]["coinCount"].GetInt();
                }
            }
            
            if(Curlevel>StatsManager::getInstance()->getLangPlayedlevelCount())
            {
                isupdate = true;
                StatsManager::getInstance()->updateLangPlayedlevelCount(Curlevel,true);
                
                
                StatsManager::getInstance()->updateInGameLevelDataFromUserData(Curlevel);
            }
            
            if(me_iCoinCount<coincount)
            {
                isupdate = true;
                UserDefault::getInstance()->setIntegerForKey(COINS,coincount);
                me_iCoinCount = UserDefault::getInstance()->getIntegerForKey(COINS);
                
                Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
            }
        }
        if(isupdate) {
            updateUserDataToFireBase();
            getLeaderboardData();
        }
    }
    
}
bool GameController::checkIsFbIDAlreadyClaimedReward(std::string fbId)
{
    bool claimed = false;
    
    if(userIds.size()>0)
    {
        claimed =  std::find(userIds.begin(), userIds.end(), fbId.c_str()) != userIds.end();
    }
    return claimed;
}
#ifdef SDKBOX_ENABLED

#pragma mark SDKBOX_IAP
// ------- IPA Start---------
void GameController::onInitialized(bool ok) {
	CCLOG("onInitialized came... ok = %s", (ok) ? "true" : "false");
	setBoolForKey("isAdsRemoved", false);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	restoreAllProduct();
#endif
}

void GameController::onSuccess(const sdkbox::Product &p)
{
	if (STR_COMPARE(p.name, REMOVE_ADS))
    {
		setBoolForKey("isAdsRemoved", true);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_REMOVE_ADS_SUCCESS);
	}
	else if (STR_COMPARE(p.name, MEGA_DEAL))
    {
        std::string device = "", order_id = "", signature = "";
        std::string pur_data = "", plan_id = "";

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        device = "ios";
#else
        device = "android";
#endif

        pur_data = p.receipt;
        order_id = p.transactionID;
        plan_id = StringUtils::format("%s.%s", device.c_str(), p.id.c_str());

        CCLOG("IAP: Name: %s", p.name.c_str());
        CCLOG("IAP: ID: %s", p.id.c_str());
        CCLOG("IAP: Title: %s", p.title.c_str());
        CCLOG("IAP: Desc: %s", p.description.c_str());
        CCLOG("IAP: Price: %s", p.price.c_str());
        CCLOG("IAP: Price Value: %f", p.priceValue);
        CCLOG("IAP: Currency: %s", p.currencyCode.c_str());
        CCLOG("IAP: transactionID: %s", p.transactionID.c_str());
        CCLOG("IAP: receipt: %s", p.receipt.c_str());
        CCLOG("IAP: receipt data: %s", p.receiptCipheredPayload.c_str());

        signature = p.receiptCipheredPayload.c_str();
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_PURCHASE_SUCCESS);
    }
	else
	{
		for (int i = 0; i < inAppName.size(); i++)
		{
			if (STR_COMPARE(p.name, inAppName.at(i)))
			{
				std::string device = "", order_id = "", signature = "";
				std::string pur_data = "", plan_id = "";

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
				device = "ios";
#else
				device = "android";
#endif

				pur_data = p.receipt;
				order_id = p.transactionID;
				plan_id = StringUtils::format("%s.%s", device.c_str(), p.id.c_str());

				CCLOG("IAP: Name: %s", p.name.c_str());
				CCLOG("IAP: ID: %s", p.id.c_str());
				CCLOG("IAP: Title: %s", p.title.c_str());
				CCLOG("IAP: Desc: %s", p.description.c_str());
				CCLOG("IAP: Price: %s", p.price.c_str());
				CCLOG("IAP: Price Value: %f", p.priceValue);
				CCLOG("IAP: Currency: %s", p.currencyCode.c_str());
				CCLOG("IAP: transactionID: %s", p.transactionID.c_str());
				CCLOG("IAP: receipt: %s", p.receipt.c_str());
				CCLOG("IAP: receipt data: %s", p.receiptCipheredPayload.c_str());

				signature = p.receiptCipheredPayload.c_str();
                Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_PURCHASE_SUCCESS);
				break;
			}
		}
	}
	CCLOG("Purchase Success: %s", p.id.c_str());
}

void GameController::onFailure(const sdkbox::Product &p, const std::string &msg)
{
	CCLOG("Purchase Failed: %s", msg.c_str());
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_PURCHASE_FAILURE);
}

void GameController::onCanceled(const sdkbox::Product &p)
{
	CCLOG("Purchase Canceled: %s", p.id.c_str());
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_PURCHASE_FAILURE);
}

void GameController::onRestored(sdkbox::Product const& p) {
	
	CCLOG("name: %s", p.name.c_str());
    
    if(STR_COMPARE(p.name, REMOVE_ADS))
    {
        setBoolForKey("isAdsRemoved", true);
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_RESTORE_SUCCESS);
    }
}

void GameController::onProductRequestSuccess(std::vector<sdkbox::Product> const &products) {
	CCLOG("onProductRequestSuccess came...");
	int i = 0;
	inAppPrice.clear();
	inAppCoin.clear();
	inAppName.clear();
	inAppTitle.clear();
    
    CCLOG("Product_size: %lu", products.size());


	for (auto pro : products)
	{
		CCLOG("onProductRequestSuccess= %d", i);
        
        if(STR_COMPARE(pro.name, REMOVE_ADS))
        {
            CCLOG("Remove Ads price: %s", pro.price.c_str());
            inAppAdsPrice = pro.price.c_str();
            inAppAdsPriceVal = std::to_string(pro.priceValue);
        }
        if(STR_COMPARE(pro.name, "coins_deal"))
        {
            CCLOG("deal price: %s", pro.price.c_str());
            inAppLimitedSalePrice = pro.price.c_str();
        }
		else if (!STR_COMPARE(pro.name, REMOVE_ADS))
		{
			inAppPrice.push_back(pro.price);
			inAppName.push_back(pro.name);

			std::string tit = pro.title;
			std::string ans = "", ans1 = "";
			for (int i = 0; i < tit.size(); i++)
			{
				if (!STR_COMPARE(tit.substr(i, 1), "("))
				{
					ans += tit.substr(i, 1);
				}
				else if (STR_COMPARE(tit.substr(i, 1), "("))
				{
					break;
				}
			}

			for (int j = 0; j < tit.size(); j++)
			{
				if (!STR_COMPARE(tit.substr(j, 1), " ") && !STR_COMPARE(tit.substr(j, 1), ","))
				{
					ans1 += tit.substr(j, 1);
				}
				else if (STR_COMPARE(tit.substr(j, 1), " "))
				{
					break;
				}
			}
			inAppTitle.push_back(ans);
			inAppCoin.push_back(ans1);
            inAppPriceVal.push_back(std::to_string(pro.priceValue));

            currencycode = pro.currencyCode;
			CCLOG("=== %d ===", i + 1);
            CCLOG("name: %s", pro.name.c_str());
            CCLOG("title: %s", ans.c_str());
            CCLOG("coin: %s", ans1.c_str());
            CCLOG("price: %s", pro.price.c_str());
            CCLOG("currencyCode: %s", pro.currencyCode.c_str());
            CCLOG("priceValue: %f", pro.priceValue);
            CCLOG("description: %s", pro.description.c_str());
			i += 1;
		}
	}
}

void GameController::onProductRequestFailure(const std::string &msg) {
	CCLOG("onProductRequestFailure came...");
}

void GameController::onRestoreComplete(bool ok, const std::string & msg) {
	CCLOG("onRestoreComplete came...");
}

void GameController::purchaseThis(std::string productName)
{
    CCLOG("purchaseThis");
	sdkbox::IAP::purchase(productName);
}

void GameController::restoreAllProduct() {
	sdkbox::IAP::restore();
}
#pragma mark SDKBOX_ADMOB

static std::string kBannerAd     = "bannerAd";
static std::string kInterstialAd = "interstialAd";
static std::string kRewardedAd   = "rewardedAd";

void GameController::adViewDidReceiveAd(const std::string &name)
{
    cocos2d::log("adViewDidReceiveAd %s again", name.c_str());
    if(me_strScreenName=="MenuScreen"||me_strScreenName=="GameScreen")
    {
        if(name == kBannerAd&&!m_bNoAdsForSession&&!getBoolForKey("isAdsRemoved"))
            sdkbox::PluginAdMob::show(name);
    }
    
    if(name==kRewardedAd)
    {
        m_bVideoAdsAvailable = true;
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_VIDEO_ADS_AVAILABLE);
    }
}
void GameController::adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg)
{
        cocos2d::log("adViewDidFailToReceiveAdWithError %s", name.c_str());
    
        float delay = 6; // seconds
        cocos2d::Director::getInstance()->getScheduler()->schedule([name](float)
        {
            cocos2d::log("cache %s again", name.c_str());
            sdkbox::PluginAdMob::cache(name);
        }, this, 0, 0, delay, false, "once");
}
void GameController::adViewWillPresentScreen(const std::string &name)
{
    cocos2d::log("adViewWillPresentScreen %s ", name.c_str());
}
void GameController::adViewDidDismissScreen(const std::string &name)
{
    cocos2d::log("adViewDidDismissScreen %s ", name.c_str());
    
    
    if(name == kInterstialAd&&isBGMEnabled())
    {
        if(!SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) {
            SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        }
    }
    
    if(name == kRewardedAd)
    {
        m_bVideoAdsAvailable = false;
        if(adRewarded)
        {
            adRewarded = false;
            Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_VIDEO_ADS_SUCCEED);
        }else{
            Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_VIDEO_ADS_FAILED);
        }
    }

}
void GameController::adViewWillDismissScreen(const std::string &name)
{
    cocos2d::log("adViewWillDismissScreen %s ", name.c_str());
}
void GameController::adViewWillLeaveApplication(const std::string &name)
{
    cocos2d::log("adViewWillLeaveApplication %s ", name.c_str());
}
void GameController::reward(const std::string &name, const std::string &currency, double amount)
{

    cocos2d::log("adreward %s", name.c_str());
    if(name == kRewardedAd)
    {
        adRewarded = true;
    }
}

void GameController::cacheAds()
{
    sdkbox::PluginAdMob::cache(kBannerAd);
    sdkbox::PluginAdMob::cache(kInterstialAd);
    sdkbox::PluginAdMob::cache(kRewardedAd);
}
void GameController::showBanner()
{
    if(!m_bNoAdsForSession&&!getBoolForKey("isAdsRemoved"))
        sdkbox::PluginAdMob::show(kBannerAd);
}
void GameController::hideBanner()
{
    sdkbox::PluginAdMob::hide(kBannerAd);
}
void GameController::showInterstial()
{
    cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
        if (!m_bNoAdsForSession&&!getBoolForKey("isAdsRemoved"))
            sdkbox::PluginAdMob::show(kInterstialAd);
    });
}
void GameController::showVideoAd()
{
    sdkbox::PluginAdMob::show(kRewardedAd);
}
bool GameController::isVideoAdsAvailable()
{
    return m_bVideoAdsAvailable;
}
void GameController::disableAdsForThisSession()
{
    m_bNoAdsForSession = true;
    hideBanner();
}
#endif
std::string GameController::getMonthName(int num)
{
    std::string month = "";
    switch (num) {
        case 0:
            month = "JAN";
            break;
        case 1:
            month = "FEB";
            break;
        case 2:
            month = "MAR";
            break;
        case 3:
            month = "APR";
            break;
        case 4:
            month = "MAY";
            break;
        case 5:
            month = "JUN";
            break;
        case 6:
            month = "JUL";
            break;
        case 7:
            month = "AUG";
            break;
        case 8:
            month = "SEP";
            break;
        case 9:
            month = "OCT";
            break;
        case 10:
            month = "NOV";
            break;
        case 11:
            month = "DEC";
            break;
        default:
            break;
    }
    
    return month;
    
}
void GameController::doParticleEffect(Node *_node,Point p_Position,int noOfPartice)
{
    Color4F t_ColorStart = {1,1,1,1};
    Color4F t_ColorStartVar = {1,1,1,0};
    Color4F t_ColorEnd = {1,1,1,0.5f};
    Color4F t_ColorEndVar = {0,0,0,0};
    
    ParticleFireworks *t_ParticleFireworks = ParticleFireworks::createWithTotalParticles(noOfPartice);
    t_ParticleFireworks->setBlendAdditive(true);
    
    t_ParticleFireworks->setTexture(Director::getInstance()->getTextureCache()->addImage("Star.png"));
    t_ParticleFireworks->setEmissionRate(10/0.01);
    t_ParticleFireworks->setDuration(0.07);
    t_ParticleFireworks->setPosVar(Vec2(50,0.0f));
    t_ParticleFireworks->setAngle(90.0);
    t_ParticleFireworks->setAngleVar(153.59);
    t_ParticleFireworks->setEmitterMode(ParticleSystem::Mode::GRAVITY);
    t_ParticleFireworks->setSpeed(250);
    t_ParticleFireworks->setSpeedVar(150);
    t_ParticleFireworks->setGravity(Vec2(0,-250.00));
    t_ParticleFireworks->setRadialAccel(0);
    t_ParticleFireworks->setRadialAccelVar(0);
    t_ParticleFireworks->setTangentialAccel(0);
    t_ParticleFireworks->setTangentialAccelVar(0);
    t_ParticleFireworks->setLife(0.2);
    t_ParticleFireworks->setLifeVar(0.3);
    t_ParticleFireworks->setStartSize(30);
    t_ParticleFireworks->setStartSizeVar(5);
    t_ParticleFireworks->setEndSize(10);
    t_ParticleFireworks->setEndSizeVar(0.0);
    t_ParticleFireworks->setStartSpin(0);
    t_ParticleFireworks->setStartSpinVar(45);
    t_ParticleFireworks->setEndSpin(90);
    t_ParticleFireworks->setEndSpinVar(90);
    t_ParticleFireworks->setPositionType(ParticleSystem::PositionType::RELATIVE);
    t_ParticleFireworks->setStartColor(Color4F::WHITE);
    t_ParticleFireworks->setStartColorVar(Color4F(0,0,0,0));
    t_ParticleFireworks->setEndColor(Color4F(1,1,1,0));
    t_ParticleFireworks->setEndColorVar(Color4F(0,0,0,0));
    t_ParticleFireworks->setPosition(p_Position);
    _node->addChild(t_ParticleFireworks,5);
    t_ParticleFireworks->setAutoRemoveOnFinish(true);
}
void GameController::connectedToAppleId(std::string userId,std::string str2)
{
    setStringForKey("connectAs","Apple");
    
    userId.erase(remove(userId.begin(), userId.end(), '.'), userId.end());

    setStringForKey("AppleId", userId);
    
    setStringForKey("AppleIdName", str2);
    
    getStringForKey("profileUrl","");
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(APPLE_LOGIN_SUCCESS);

}
void GameController::FailedToConnectAppleId()
{
    setStringForKey("connectAs","");
    
    setStringForKey("AppleId","");
    
    setStringForKey("AppleIdName","");
    
    getStringForKey("profileUrl","");
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(APPLE_LOGIN_FAILURE);
    
}

void GameController::saveUserRank(int rankIndex)
{
    UserDefault::getInstance()->setIntegerForKey("UserIndex",rankIndex);
}
