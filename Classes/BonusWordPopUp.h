//
//  BonusWordPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 16/04/20.
//

#ifndef BonusWordPopUp_hpp
#define BonusWordPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class BonusWordPopUp:public PopUp
{
    
    MenuItemSprite *t_miCollectCoinBtn;
    MenuItemSprite *t_miCloseButton;
    int rewardArr[7] = {10,20,30,40,50,80,100};
    
    Point Pos;
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    Label *m_tCoinCount;
    int m_tCoins;
    int remCount;
public:
     
    BonusWordPopUp(std::function<void()> func);
    ~BonusWordPopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);

    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    void collectButtonCallBack(Ref *sender);
    
    void coinAddAnimation();
    void removeSprite(Ref *sender);

    void enableCollectButton();
    
    void coinAnimationDoneClosePopUp();
};
#endif /* BonusWordPopUp_hpp */
