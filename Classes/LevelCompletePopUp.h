//
//  LevelCompletePopUp.hpp
//  wordPro-mobile
//
//  Created by id on 16/03/20.
//

#ifndef LevelCompletePopUp_h
#define LevelCompletePopUp_h

#include <stdio.h>
#include "PopUp.h"

class LevelCompletePopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    Sprite *m_SprLocation;
    Label *m_LocationLabel;
    
    MenuItemSprite *t_NextButton,*t_DoubleCoinButton;
    Menu *m_menu;
    Sprite *m_LevelProgressBarBg;
    ProgressTimer *m_LevelProgressBar;
    
    Node *m_GiftNode;
    
    Label *giftRewardLbl;
    
    Sprite *sprBg2;
    Label *t_tCoinText;
    Sprite *sprCoin;
    
    Sprite *coinSpr,*giftIcon;
    Label *m_tCoinCount, *m_tGiftCount;
    
    Sprite *m_PrevLocationSpr,*m_CurLocationSpr,*m_NxtLocationSpr;
    Label  *m_PrevLocationLbl,*m_CurLocationLbl,*m_NxtLocationLbl;
    MenuItemSprite *t_miCloseButton;
    int m_tCoins;
    
    bool m_bPlayedAgain;
    
    bool m_bVideoSuccess;
    
    Vector<MenuItemSprite*> *moreitems;
    Node *m_gamesNode;
public:
    LevelCompletePopUp(std::function<void()> func);
    ~LevelCompletePopUp();
    
    void OnButtonPressed(Ref *p_Sender) override;
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);

    void createCoinsCollection(Point Pos,Sprite *spr);
    void createProgressBar(Point pos);
    
    void createLocationBar(Sprite*sprBar1);
    
    void registerVideoEvent();
    
    void doubleCoinBtnClick();
    
    void videoAdsAvailble();
    
    void onVideoAdSuccess();
    
    void onVideoAdFailure();
    
    void removeSprite(Ref *_sender);

    void coinAddAnimation(Point t_StartPos);
    
    void NextButtonCallBack();

    void coinAnimationDoneClosePopUp();
    
    void enableClaimButton();
    
    void claimButtonCallBack(Ref *_sender);
    
    void showGiftBox();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    void createMoregamesonLevelComplete();
    
    void moreGamesBtnCallback(Ref *sender);
    
    void animateButtons();
};

#endif /* LevelCompletePopUp_hpp */
