//
//  SettingPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 11/03/20.
//

#ifndef SettingPopUp_h
#define SettingPopUp_h

#include <stdio.h>
#include "cocos2d.h"
#include "PopUp.h"

class SettingPopUp:public PopUp
{
    
    Sprite* m_SprPopUpBg,*m_SprPopUpTop;
    Menu *m_mSettingMenu;
    MenuItemSprite *t_SoundButton,*t_MusicButton,*t_NotifyButton,*t_LangButton,*t_HtoPlayButton,*t_FeedbackButton,
                    *t_RateButton,*t_ShareButton;
    
    float txtWidth;
public:

    SettingPopUp(std::function<void()> func);
    ~SettingPopUp();
    
    void OnButtonPressed(Ref *p_Sender);
    
    Label *createButtonTextLabel(std::string txt,float fontsize);
    Sprite *createButtonSprite(std::string sprite,Point pos);

    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void onSoundButttonCallBack(Ref* sender);
    void musicButttonCallBack(Ref* sender);
    void notificationCallBack(Ref* sender);
    
    void languageButttonCallBack(Ref* sender);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
        
};
#endif /* SettingPopUp_hpp */
