//
//  UnlockPowerPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 05/05/20.
//

#ifndef UnlockPowerPopUp_hpp
#define UnlockPowerPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class UnlockPowerPopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    MenuItemSprite *t_OkayButton;
    
public:
     UnlockPowerPopUp(std::function<void()> func);
    ~UnlockPowerPopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
};

#endif /* UnlockPowerPopUp_hpp */
