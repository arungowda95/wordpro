//
//  LimitedSalePopUp.hpp
//  wordPro-mobile
//
//  Created by id on 23/03/20.
//

#ifndef LimitedSalePopUp_h
#define LimitedSalePopUp_h

#include <stdio.h>
#include "PopUp.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "CCActivityIndicator.h"
#endif


class LimitedSalePopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    Label *t_mSaleTimer;
    
    MenuItemSprite *t_miBuyBtn;
    
    time_t          m_SaleStart, m_SaleCurrent, m_start, m_Current;
    float           m_fSaleTimer;
    float           m_fSaleTimeDelay;

    float           m_fTimer;
    
    #if !(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        CCActivityIndicator * activityIndicator;
    #endif

public:
    LimitedSalePopUp(std::function<void()> func);
    ~LimitedSalePopUp();
    
    void OnButtonPressed(Ref *p_Sender) override;
    
    void buyBtnCallBack(Ref *p_Sender);
              
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void showlimitedSaleTimer();
    
    void UpdateLimitedSaleTimer();
    
    void OnUpdateFrame(float p_fDeltaTime);
    
    void onCoinPurchaseSuccess();
    
    void onCoinPurchaseFailure();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    void ShowIndicator();
    
    void HideIndicator();
};

#endif /* LimitedSalePopUp_hpp */
