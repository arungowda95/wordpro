//
//  AchievementPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 27/03/20.
//

#ifndef AchievementPopUp_hpp
#define AchievementPopUp_hpp

#include <stdio.h>
#include "PopUp.h"
#include "ui/UIListView.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;


class AchievementPopUp:public PopUp
{
    Point Pos;
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    Sprite *m_CompleteBg;
    ScrollView *m_ptrScrolView;
    
    MenuItemSprite *t_ClaimButton,*t_miCloseButton;
    
    int achivementsCount,m_iAchievementNum=1;
    
public:
    AchievementPopUp(std::function<void()> func);
    ~AchievementPopUp();
    
    
    void OnButtonPressed(Ref *p_Sender);
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    void claimButtonCallBack(Ref *p_Sender);
    
    void coinAddAnimation(int tagIndex);
    
    void removeSprite(Ref *_sender);
    
    
    void createClaimedItemsAtlast(Point Pos);
    
    std::string getIconsForAchievements(std::string);
    void getAchievementNum(std::string m_AchievementName);
};
#endif /* AchievementPopUp_hpp */
