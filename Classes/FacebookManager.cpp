//
//  FacebookManager.cpp
//  wordPro-mobile
//
//  Created by id on 30/06/20.
//

#include "FacebookManager.h"
#include "GameController.h"
#include "GameConstants.h"

FacebookManager* FacebookManager::fbManager = NULL;


void FacebookManager::login()
{
    
    std::vector<std::string> permissions = {sdkbox::FB_PERM_READ_PUBLIC_PROFILE,sdkbox::FB_PERM_READ_EMAIL};
    
    if(!sdkbox::PluginFacebook::isLoggedIn())
    {
        sdkbox::PluginFacebook::login(permissions);
    }
    else
    {
        sdkbox::FBAPIParam param;
        sdkbox::PluginFacebook::requestInvitableFriends(param);
    }
    //pushDataToLevelHighScoreTable
}
bool FacebookManager::isLoggedIn()
{
    if(sdkbox::PluginFacebook::isLoggedIn())
    {
        return true;
    }else{
        return false;
    }
}
void FacebookManager::callFacebookApi()
{

    CCLOG("GameController::callFacebookApi");
#ifdef SDKBOX_ENABLED
    GameController::getInstance()->setStringForKey("connectAs", "facebook");
     sdkbox::FBAPIParam paramFB;
    std::string graphRequest;
    std::stringstream sstm;
    sstm << "/me/?fields=name,id,picture.width("<<100<<").height("<<100<<")";
    graphRequest = sstm.str();
 
    sdkbox::PluginFacebook::api(graphRequest, "GET", paramFB, "UserInfo");
#endif
}


#ifdef SDKBOX_ENABLED
void FacebookManager::onLogin(bool isLogin, const std::string& msg) {
    CCLOG("Login = %s ::: %s", (isLogin) ? "true" : "false", msg.data());
    if (isLogin)
    {
        CCLOG("Login is Succesful ");
        callFacebookApi();

    }
    else
    {
        CCLOG("Login is Un Succesful ");
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FB_LOGIN_FAILURE);
    }
}

void FacebookManager::onPermission(bool isLogin, const std::string& msg){

    CCLOG(" my onPermission ");
    CCLOG("Login = %s ::: %s", (isLogin) ? "true" : "false", msg.data());
    //createScene(SCREEN_MENU_MAIN);
}

void FacebookManager::onAPI(const std::string& tag, const std::string& jsonData)
{
    CCLOG("##FB onAPI: tag -> %s, json -> %s", tag.c_str(), jsonData.c_str());
    
    if(strcmp(tag.c_str(),"UserInfo")==0)
    {
        rapidjson::Document doc;
        doc.Parse<0>(jsonData.c_str());
        if(doc.HasMember("id"))
        {
            facebookID = doc["id"].GetString();
            GameController::getInstance()->setStringForKey("facebookID",doc["id"].GetString());
            facebookID = GameController::getInstance()->getStringForKey("facebookID");
        }
        if(doc.HasMember("name"))
        {
            std::string fName = doc["name"].GetString();
            std::string name = fName;
            if (fName.length() > 11)
            {
                name = "";
                name = fName.substr(0, 11);
                name += "..";
            }
            facebookName = name;
            GameController::getInstance()->setStringForKey("facebookName",name);
            facebookName = GameController::getInstance()->getStringForKey("facebookName");
        }
        if(doc["picture"]["data"].HasMember("url"))
        {
            std::string url = doc["picture"]["data"]["url"].GetString();
            GameController::getInstance()->setStringForKey("profileUrl",url.c_str());
            CCLOG("picture's url = %s", url.data());
        }
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FB_LOGIN_SUCCESS);
    }
    if (sdkbox::PluginFacebook::isLoggedIn())
    {
        CCLOG("Call ConnectAsFB....!");
    }
    
    if(tag == "InvitableFriends")
    {
        CCLOG("Call ConnectAsFB....!");
    }

}

void FacebookManager::onSharedSuccess(const std::string& message) {

    CCLOG(" my onSharedSuccess ");
}

void FacebookManager::onSharedFailed(const std::string& message) {

    CCLOG(" my onSharedFailed ");
}

void FacebookManager::onSharedCancel() {

    CCLOG(" my onSharedCancel ");
}

void FacebookManager::onFetchFriends(bool ok, const std::string& msg) {

}

void FacebookManager::onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo& friends) {

    CCLOG(" my onRequestInvitableFriends ");
}

void FacebookManager::onInviteFriendsWithInviteIdsResult(bool result, const std::string& msg) {

    CCLOG(" my onInviteFriendsWithInviteIdsResult ");
}

void FacebookManager::onInviteFriendsResult(bool result, const std::string& msg) {
    CCLOG("%s ::: %s", (result) ? "true" : "false", msg.data());
}

void FacebookManager::onGetUserInfo(const sdkbox::FBGraphUser& userInfo) {

    CCLOG("user name =  %s ", userInfo.getName().c_str());
}

#endif

