//
//  AnswerGrid.hpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#ifndef AnswerGrid_h
#define AnswerGrid_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class AnswerWord;
class AnswerGrid:public Ref
{
    GameLayer      *m_ptrGameLayer;
    
    std::vector<std::string> *m_WordsArray;
    
    Vector<AnswerWord *> *m_ptrAnswerWordArray;
    
    std::vector<std::string> *m_BonusWordsArray;
    std::vector<std::string> *m_tempBonusArray;
    Point m_Position;
    float t_fGridScale;
    
    LayerColor *GridLayout;
    
    Size visibleSize;
    Vec2 Origin;
    Size t_fGridSize;
    
    bool m_bIsCoinReward;
public:
    
    int m_iAttemptNum;
    bool m_bAttempt;
    int t_iEmptyGrids;
    /// CONSTRUCTOR AND DESTRUCTOR
    AnswerGrid(GameLayer *p_ptrGameLayer);
    ~AnswerGrid();
    
    ///  calcutes Postion by size of the words array size
    /// @param wordsArray  size of Words array
    /// @param bonusWordsArray bonus words array
    void createAnswerGrid(std::vector<std::string> wordsArray,std::vector<std::string> bonusWordsArray);    
    
    ///  Creates Answer word Grid
    /// @param p_Word word
    /// @param t_Position  position of answer word
    /// @param t_iWordIndex Answer word obj index
    /// @param m_bCoinReward  boolean value of coin reward wheather this particular word reaward contains
    void createGrid(const std::string &p_Word,Point t_Position,int t_iWordIndex,bool m_bCoinReward);
    
    /// on Hint Button Pressed this will check for hint operation by checking empty  word tile
    bool doHint();
    
    /// Recieves Answered Letters Array Swiped by the user
    /// @param ansStrArr array of letters
    void recieveLetterFromOption(std::vector<std::string> ansStrArr);
    
    /// RETURN ANSWER GRID IS EMPTY OR NOT
    bool didAllGridsFill();
    
    /// checks for all the answer word's grids filled or not to complete level
    void checkForLevelComplete();
    
    /// returns Answerword's objects array
    Vector<AnswerWord*> *getAnswerWordArray();
    
    /// checks for Answer words grids fill
    void checkforGridsFill();
    
    /// checks wheather this answered word is last one or not
    void checkIsLastWord();
};

#endif /* AnswerGrid_hpp */
