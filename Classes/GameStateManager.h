//
//  GameStateManager.hpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#ifndef GameStateManager_h
#define GameStateManager_h

#include <stdio.h>
#include "cocos2d.h"

enum eGameState
{
    GAME_STATE_PLAY,
    GAME_STATE_PAUSE,
    GAME_STATE_OVER,
};

using namespace cocos2d;
using namespace std;

// SINGLETON CLASS
class GameStateManager
{
        eGameState  m_eGameState;
        static GameStateManager *m_ptrGameStateManager;
        GameStateManager();
    public:
        // RETURN GAME HANDLER
        static GameStateManager *getInstance();
    
        // SET GAME STATE
        void SetGameState(eGameState p_eGameState);
    
        // GET GAME STATE
        eGameState getGameState();
 
};
#endif /* GameStateManager_hpp */
