//
//  AchievementPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 27/03/20.
//

#include "AchievementPopUp.h"
#include "AchievementManager.h"

AchievementPopUp::AchievementPopUp(std::function<void()> func)
{
        m_fnSelector   = func;
        t_ClaimButton  = NULL;
        m_ptrScrolView = NULL;
    
        ResolutionSize = Director::getInstance()->getSafeAreaRect();
        visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
        origin = ResolutionSize.origin;
        
        m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
        
        m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
        m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()+0.05);
        m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
        this->addChild(m_SprPopUpBg,1);
        
        m_SprPopUpTop = Sprite::create("PopUp_Top.png");
        m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.3);
        m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
        this->addChild(m_SprPopUpTop,2);
        
        std::string t_Label = "Achievements";
        Label *t_tLabel = createLabelBasedOnLang(t_Label, 60);
        t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-64);
        t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+20);
        t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
        t_tLabel->setColor(Color3B(255,244,55));
        t_tLabel->enableOutline(Color4B(196,56,63,255),3);
         this->addChild(t_tLabel, 2);
    
         achivementsCount = (int)AchievementManager::getInstance()->GetAchievements().size();


        m_CompleteBg = Sprite::create("Progress_barBg.png");
    m_CompleteBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-10-m_CompleteBg->getBoundingBox().size.height/2));
        this->addChild(m_CompleteBg,2);
    
        int compltdCount = AchievementManager::getInstance()->getCompletedAchievementsCount();
       std::string t_CompText = "Completed";
        Label *t_tAchiveCompleted = createLabelBasedOnLang(t_CompText,26);
    t_tAchiveCompleted->setString(StringUtils::format("%s : %d/%d",t_tAchiveCompleted->getString().c_str(),compltdCount,achivementsCount));
        t_tAchiveCompleted->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        t_tAchiveCompleted->setWidth(m_CompleteBg->getContentSize().width-20);
        t_tAchiveCompleted->setHeight(t_tAchiveCompleted->getBoundingBox().size.height);
        t_tAchiveCompleted->setPosition(Vec2(m_CompleteBg->getBoundingBox().getMidX(),m_CompleteBg->getBoundingBox().getMidY()));
        t_tAchiveCompleted->setColor(Color3B::WHITE);
        this->addChild(t_tAchiveCompleted, 2);
       
        m_ptrScrolView = cocos2d::ui::ScrollView::create();
        m_ptrScrolView->setDirection(ui::ScrollView::Direction::VERTICAL);
        m_ptrScrolView->setTouchEnabled(true);
        m_ptrScrolView->setBounceEnabled(false);
        Size contentsize = Size(m_SprPopUpBg->getBoundingBox().size.width,m_CompleteBg->getBoundingBox().getMinY()-10-m_SprPopUpBg->getBoundingBox().getMinY()-110);
        m_ptrScrolView->setContentSize(Size(contentsize));
        m_ptrScrolView->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_CompleteBg->getBoundingBox().getMinY()-10-m_ptrScrolView->getBoundingBox().size.height/2));
        m_ptrScrolView->setScrollBarEnabled(false);
        m_ptrScrolView->setSwallowTouches(false);
        m_ptrScrolView->setAnchorPoint(Vec2(0.5f,0.5f));
    
        float totalNum = (float)achivementsCount;

    

        Sprite *path = Sprite::create("coins_popup_Bg.png");
        path->setScaleY(path->getScaleY()+0.05);

         float pathHeight = path->getBoundingBox().size.height+10;
         
         Size Containersize = Size(contentsize.width,pathHeight*(totalNum));
         m_ptrScrolView->setInnerContainerSize(Size(Containersize.width,Containersize.height));

    
    std::vector<Point> posArr;
    
         Point t_position = Vec2(m_ptrScrolView->getContentSize().width/2,Containersize.height-5);
         for (int index = 0; index<totalNum;index++)
         {
             
             std::string t_name = StringUtils::format("Achivement_%d",index+1);

             if(AchievementManager::getInstance()->checkIsAchievementClaimed(index)) {
                 continue;
             }
             Layout *layout = Layout::create();
             layout->setColor(Color3B::WHITE);
             layout->setContentSize(Size(path->getBoundingBox().size.width,path->getBoundingBox().size.height));
//             layout->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::SOLID);
             layout->setAnchorPoint(Vec2(0.5f,0.5f));
             layout->setPosition(Vec2(t_position.x,t_position.y-layout->getBoundingBox().size.height/2));
             layout->setTag(index);
             m_ptrScrolView->addChild(layout,1);
             
             posArr.push_back(layout->getPosition());
             
             Menu *t_ClaimMenu = Menu::create(NULL);
             t_ClaimMenu->setPosition(Vec2::ZERO);
             layout->addChild(t_ClaimMenu,3);

             Sprite *missionBg = Sprite::create("coins_popup_Bg.png");
             missionBg->setScaleY(path->getScaleY()+0.05);
             missionBg->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
             layout->addChild(missionBg,2);

             Sprite *missionIcon = Sprite::create("circle-image.png");
             missionIcon->setPosition(Vec2(missionIcon->getContentSize().width/2+20,layout->getContentSize().height/2+5));
             layout->addChild(missionIcon,2);

             std::string achivementTag = AchievementManager::getInstance()->GetAchievements().at(index)->m_KeyId;
             std::string icon = getIconsForAchievements(achivementTag);
             Sprite *t_AIcon = Sprite::create(icon.c_str());
             t_AIcon->setScale(0.85f);
             t_AIcon->setPosition(Vec2(missionIcon->getContentSize().width/2,missionIcon->getContentSize().height/2));
             missionIcon->addChild(t_AIcon,2);

             
             std::string achivementName = AchievementManager::getInstance()->GetAchievements().at(index)->m_AchievementName;
             getAchievementNum(achivementName);
             achivementName = StringUtils::format("Achievement%d",m_iAchievementNum);
             Label *t_tAchieveDisc = createLabelBasedOnLang(achivementName,25);
             t_tAchieveDisc->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
             t_tAchieveDisc->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
             t_tAchieveDisc->setColor(Color3B::WHITE);
             t_tAchieveDisc->enableOutline(Color4B(0,0,0,255),2);
             t_tAchieveDisc->setWidth(missionBg->getBoundingBox().size.width-300);
             t_tAchieveDisc->setHeight(t_tAchieveDisc->getBoundingBox().size.height);
             t_tAchieveDisc->setPosition(Vec2(missionIcon->getBoundingBox().getMaxX()+10,missionIcon->getBoundingBox().getMaxY()+5-t_tAchieveDisc->getContentSize().height/2));
             layout->addChild(t_tAchieveDisc, 2);
             
             //REWARD BUTTON
              int achiveReward = AchievementManager::getInstance()->GetAchievements().at(index)->m_iReward;
              Sprite *coinsbar = Sprite::create("coins-bar.png");
              coinsbar->setPosition(Vec2(layout->getContentSize().width/2+coinsbar->getContentSize().width-25,10+coinsbar->getContentSize().height/2));
              layout->addChild(coinsbar,2);
              
              std::string t_text = StringUtils::format("%d",achiveReward);
              Label *t_Coins = Label::createWithTTF(t_text, FONT_NAME,24, Size(0, 0));
              t_Coins->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
              t_Coins->setColor(Color3B::WHITE);             t_Coins->setPosition(Vec2(layout->getContentSize().width/2+coinsbar->getContentSize().width-15,17+t_Coins->getContentSize().height/2));
              layout->addChild(t_Coins, 2);

             
             bool t_completed = AchievementManager::getInstance()->GetAchievements().at(index)->m_bCompleted;
             if(t_completed)
             {

                 std::string t_name = StringUtils::format("Achivement_%d",index+1);
                 
                 t_ClaimButton = getButtonMade("Buy_button.png","Buy_button.png",CC_CALLBACK_1(AchievementPopUp::claimButtonCallBack,this));
                 t_ClaimButton->setScale(t_ClaimButton->getScale()-0.2f);
                    t_ClaimButton->setPosition(Vec2(missionBg->getBoundingBox().getMaxX()-t_ClaimButton->getBoundingBox().size.width/2-10,missionBg->getBoundingBox().getMidY()+20));
                 t_ClaimButton->setTag(index);
                 t_ClaimButton->setName(t_name);


                 Label *buy_label = createLabelBasedOnLang("Claim", 30);
                 buy_label->setColor(Color3B::WHITE);
                 buy_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
                 buy_label->setWidth(t_ClaimButton->getBoundingBox().size.width-10);
                 buy_label->setHeight(buy_label->getBoundingBox().size.height+20);
                 buy_label->enableOutline(Color4B(53,126,32,255),2);
                 buy_label->setPosition(Vec2(t_ClaimButton->getContentSize().width/2,t_ClaimButton->getContentSize().height/2+9.0f));
                 t_ClaimButton->addChild(buy_label,1);
                 
                 if(me_Language.compare(LANG_BANGLA)==0||me_Language.compare(LANG_TAMIL)==0)
                 {
                     buy_label->setSystemFontSize(30);
                 }
                 t_ClaimMenu->addChild(t_ClaimButton);
             }
             else
             {
                 Sprite *m_ProgressBarBg = Sprite::create("mission_bar.png");
                   m_ProgressBarBg->setPosition(layout->getContentSize().width-20-m_ProgressBarBg->getContentSize().width/2,layout->getBoundingBox().size.height/2+10);
                   layout->addChild(m_ProgressBarBg,2);
                 
                 int t_count = AchievementManager::getInstance()->GetAchievements().at(index)->m_iCurCount;
                 int t_TargetCount = AchievementManager::getInstance()->GetAchievements().at(index)->m_iTargetCount;
                 
                 if (t_count>=t_TargetCount)
                 {
                     t_count = t_TargetCount;
                 }
                 
                 std::string t_text1 = StringUtils::format("%d/%d",t_count,t_TargetCount);
                 Label *t_AchiveCount = Label::createWithTTF(t_text1, FONT_NAME,24, Size(0, 0));
                 t_AchiveCount->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
                 t_AchiveCount->setColor(Color3B::WHITE);
                 t_AchiveCount->setPosition(Vec2(m_ProgressBarBg->getPositionX(),m_ProgressBarBg->getBoundingBox().getMaxY()+t_AchiveCount->getContentSize().height/2));
                 layout->addChild(t_AchiveCount, 2);
                 
                 float percentage = (float(t_count)/float(t_TargetCount))*100;
                 
                 if (percentage>=100.0f)
                 {
                     percentage = 100.0f;
                 }
                 
                 ProgressTimer *m_ProgressBar = ProgressTimer::create(Sprite::create("mission_bar_Fill.png"));
                 m_ProgressBar->setType(ProgressTimer::Type::BAR);
                 m_ProgressBar->setBarChangeRate(Vec2(1,0));
                 m_ProgressBar->setMidpoint(Vec2(0.0f, 0.0f));
                 m_ProgressBar->setPosition(m_ProgressBarBg->getPosition());
                 m_ProgressBar->setPercentage(percentage);
                 layout->addChild(m_ProgressBar,3);
            }
             
             t_position .y -= layout->getBoundingBox().size.height+10;
         }
    
    this->addChild(m_ptrScrolView,2);
    
    
    createClaimedItemsAtlast(t_position);
    
    
    
        // CLOSE BUTTON
          t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(AchievementPopUp::OnButtonPressed,this));
          t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
          t_miCloseButton->setTag(0);

          Menu *m_Menu = Menu::create(t_miCloseButton,NULL);
          m_Menu->setPosition(Vec2::ZERO);
          this->addChild(m_Menu,2);
}

void AchievementPopUp::createClaimedItemsAtlast(Point t_position)
{
    Sprite *path = Sprite::create("coins_popup_Bg.png");
    path->setScaleY(path->getScaleY()+0.05);

             for (int index = 0; index<achivementsCount;index++)
             {
                 std::string t_name = StringUtils::format("Achivement_%d",index+1);

                 if(!AchievementManager::getInstance()->checkIsAchievementClaimed(index)) {
                     continue;
                 }

                 Layout *layout = Layout::create();
                 layout->setColor(Color3B::WHITE);
                 layout->setContentSize(Size(path->getBoundingBox().size.width,path->getBoundingBox().size.height));
    //             layout->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::SOLID);
                 layout->setAnchorPoint(Vec2(0.5f,0.5f));
                 layout->setPosition(Vec2(t_position.x,t_position.y-layout->getBoundingBox().size.height/2));
                 layout->setTag(index);
                 m_ptrScrolView->addChild(layout,1);
                 
 
                 Menu *t_ClaimMenu = Menu::create(NULL);
                 t_ClaimMenu->setPosition(Vec2::ZERO);
                 layout->addChild(t_ClaimMenu,3);

                 Sprite *missionBg = Sprite::create("coins_popup_Bg.png");
                 missionBg->setScaleY(path->getScaleY()+0.05);
                 missionBg->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
                 layout->addChild(missionBg,2);

                 Sprite *missionIcon = Sprite::create("circle-image.png");
                 missionIcon->setPosition(Vec2(missionIcon->getContentSize().width/2+20,layout->getContentSize().height/2+5));
                 layout->addChild(missionIcon,2);

                 std::string achivementTag = AchievementManager::getInstance()->GetAchievements().at(index)->m_KeyId;
                 std::string icon = getIconsForAchievements(achivementTag);
                 Sprite *t_AIcon = Sprite::create(icon.c_str());
                 t_AIcon->setScale(0.85f);
                 t_AIcon->setPosition(Vec2(missionIcon->getContentSize().width/2,missionIcon->getContentSize().height/2));
                 missionIcon->addChild(t_AIcon,2);

                 std::string achivementName = AchievementManager::getInstance()->GetAchievements().at(index)->m_AchievementName;
                 getAchievementNum(achivementName);
                 achivementName = StringUtils::format("Achievement%d",m_iAchievementNum);
                 Label *t_tAchieveDisc = createLabelBasedOnLang(achivementName,25);
                 t_tAchieveDisc->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
                 t_tAchieveDisc->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
                 t_tAchieveDisc->setColor(Color3B::WHITE);
                 t_tAchieveDisc->enableOutline(Color4B(0,0,0,255),2);
                 t_tAchieveDisc->setWidth(missionBg->getBoundingBox().size.width-300);
                 t_tAchieveDisc->setHeight(t_tAchieveDisc->getBoundingBox().size.height+20);
                 t_tAchieveDisc->setPosition(Vec2(missionIcon->getBoundingBox().getMaxX()+10,missionIcon->getBoundingBox().getMaxY()+5-t_tAchieveDisc->getContentSize().height/2));
                 layout->addChild(t_tAchieveDisc, 2);
                 
                 //REWARD BUTTON
                  int achiveReward = AchievementManager::getInstance()->GetAchievements().at(index)->m_iReward;
                  Sprite *coinsbar = Sprite::create("coins-bar.png");
                  coinsbar->setPosition(Vec2(layout->getContentSize().width/2+coinsbar->getContentSize().width-25,10+coinsbar->getContentSize().height/2));
                  layout->addChild(coinsbar,2);
                  
                  std::string t_text = StringUtils::format("%d",achiveReward);
                  Label *t_Coins = Label::createWithTTF(t_text, FONT_NAME,24, Size(0, 0));
                  t_Coins->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
                  t_Coins->setColor(Color3B::WHITE);             t_Coins->setPosition(Vec2(layout->getContentSize().width/2+coinsbar->getContentSize().width-15,17+t_Coins->getContentSize().height/2));
                  layout->addChild(t_Coins, 2);

                 
                 bool t_completed = AchievementManager::getInstance()->GetAchievements().at(index)->m_bCompleted;
                 if(t_completed)
                 {

                     std::string t_name = StringUtils::format("Achivement_%d",index+1);
                     
                     t_ClaimButton = getButtonMade("Buy_button.png","Buy_button.png",CC_CALLBACK_1(AchievementPopUp::claimButtonCallBack,this));
                     t_ClaimButton->setScale(t_ClaimButton->getScale()-0.2f);
                        t_ClaimButton->setPosition(Vec2(missionBg->getBoundingBox().getMaxX()-t_ClaimButton->getBoundingBox().size.width/2-10,missionBg->getBoundingBox().getMidY()+20));
                     t_ClaimButton->setTag(index);
                     t_ClaimButton->setName(t_name);

                     t_ClaimButton->setEnabled(false);
                     t_ClaimButton->setOpacity(150);

                     Label *buy_label = createLabelBasedOnLang("Claimed",30);
                     buy_label->setColor(Color3B::WHITE);
                     buy_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
                     buy_label->setWidth(t_ClaimButton->getBoundingBox().size.width-20);
                     buy_label->setHeight(150);
                     buy_label->enableOutline(Color4B(53,126,32,255),2);
                     buy_label->setPosition(Vec2(t_ClaimButton->getContentSize().width/2,t_ClaimButton->getContentSize().height/2+9.0f));
                     t_ClaimButton->addChild(buy_label,1);
                     
                     if(me_Language.compare(LANG_BANGLA)==0||me_Language.compare(LANG_TAMIL)==0)
                     {
                         buy_label->setSystemFontSize(23);
                     }
                     
                     t_ClaimMenu->addChild(t_ClaimButton);
                 }
                 else
                 {
                     Sprite *m_ProgressBarBg = Sprite::create("mission_bar.png");
                       m_ProgressBarBg->setPosition(layout->getContentSize().width-20-m_ProgressBarBg->getContentSize().width/2,layout->getBoundingBox().size.height/2+10);
                       layout->addChild(m_ProgressBarBg,2);
                     
                     int t_count = AchievementManager::getInstance()->GetAchievements().at(index)->m_iCurCount;
                     int t_TargetCount = AchievementManager::getInstance()->GetAchievements().at(index)->m_iTargetCount;
                     
                     if (t_count>=t_TargetCount)
                     {
                         t_count = t_TargetCount;
                     }
                     
                     std::string t_text1 = StringUtils::format("%d/%d",t_count,t_TargetCount);
                     Label *t_AchiveCount = Label::createWithTTF(t_text1, FONT_NAME,24, Size(0, 0));
                     t_AchiveCount->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
                     t_AchiveCount->setColor(Color3B::WHITE);
                     t_AchiveCount->setPosition(Vec2(m_ProgressBarBg->getPositionX(),m_ProgressBarBg->getBoundingBox().getMaxY()+t_AchiveCount->getContentSize().height/2));
                     layout->addChild(t_AchiveCount, 2);
                     
                     float percentage = (float(t_count)/float(t_TargetCount))*100;
                     
                     if (percentage>=100.0f)
                     {
                         percentage = 100.0f;
                     }
                     
                     ProgressTimer *m_ProgressBar = ProgressTimer::create(Sprite::create("mission_bar_Fill.png"));
                     m_ProgressBar->setType(ProgressTimer::Type::BAR);
                     m_ProgressBar->setBarChangeRate(Vec2(1,0));
                     m_ProgressBar->setMidpoint(Vec2(0.0f, 0.0f));
                     m_ProgressBar->setPosition(m_ProgressBarBg->getPosition());
                     m_ProgressBar->setPercentage(percentage);
                     layout->addChild(m_ProgressBar,3);
                }
                 
                 t_position .y -= layout->getBoundingBox().size.height+10;
             }
    
}
MenuItemSprite *AchievementPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
    
}


void AchievementPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
     MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
     
     int pButtonTag =  Button->getTag();

     switch (pButtonTag)
     {
         case 0:
         {
             status = (char*)"close";
         }break;
         case 1:
         {
             status = (char*)"No";
         }break;
         default:
             break;
     }
     
     if(m_fnSelector != NULL)
     {
         m_fnSelector();
     }
}
Label *AchievementPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
void AchievementPopUp::claimButtonCallBack(Ref *p_Sender)
{
    MenuItemSprite *button  = (MenuItemSprite*)p_Sender;
    int tag = button->getTag();
    std::string t_name = button->getName();
    AchievementManager::getInstance()->updateAchievementClaimed(tag);

    button->setOpacity(150);
    button->setEnabled(false);
    
    int coins = AchievementManager::getInstance()->GetAchievements().at(tag)->m_iReward;
    me_iCoinCount += coins;
    UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
    UserDefault::getInstance()->flush();

    

    coinAddAnimation(tag);
    
}
void AchievementPopUp::coinAddAnimation(int tagindex)
{
    Layout *layout =   (Layout*)m_ptrScrolView->getChildByTag(tagindex);
    Pos = layout->getPosition();
    
    Size contentsize = m_ptrScrolView->getInnerContainerSize();

    Point pos = layout->getWorldPosition();
    
    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    Point _startPosOppo = Vec2(pos.x+30,pos.y-30)-m_ZeroPosition;// Point(m_SprPopUpBg->getBoundingBox().getMidX()+30,m_SprPopUpBg->getBoundingBox().getMidY());
    for (int i=0; i< 20; i++)
    {
        Sprite *_spr = Sprite::create("coins.png");
        _spr->setScale(_spr->getScale()+0.2);
        _spr->setPosition(_startPosOppo);
        _spr->setOpacity(0);
        ccBezierConfig bezier;
        bezier.controlPoint_1 = _startPosOppo;
        bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
        bezier.endPosition =_endPos;
        BezierTo *_bez = BezierTo::create(.5f, bezier);
        this->addChild(_spr,5);
        FadeTo *_fade = FadeTo::create(.04*i,255);
        DelayTime *delay = DelayTime::create(0.3f);
        FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(AchievementPopUp::removeSprite,this));
        FiniteTimeAction*_seq = Sequence::create(_fade,_bez,delay,_call,NULL);
        _spr->runAction(_seq);
    }
    if(t_miCloseButton)
    {
        t_miCloseButton->setEnabled(false);
    }
    
    if(m_ptrScrolView)
        m_ptrScrolView->setTouchEnabled(false);
}

void AchievementPopUp::removeSprite(Ref *_sender)
{
    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);

    
    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
    

    if(t_miCloseButton)
    {
        t_miCloseButton->setEnabled(true);
    }

    if(m_ptrScrolView)
        m_ptrScrolView->setTouchEnabled(true);

}
// THIS RETURN DESCRIPTION ACCORDING TO NAME
void AchievementPopUp::getAchievementNum(std::string m_AchievementName)
{
    if(m_AchievementName.compare(ACHIEVED_30_GOOD_COMBOS)==0)
    {
        m_iAchievementNum = 1;
    }
    else if(m_AchievementName.compare(ACHIEVED_30_GREAT_COMBOS)==0)
    {
        m_iAchievementNum = 2;
    }
    else if(m_AchievementName.compare(ACHIEVED_30_AMAZING_COMBOS)==0)
    {
         m_iAchievementNum = 3;
    }
    else if(m_AchievementName.compare(ACHIEVED_30_AWESOME_COMBOS)==0)
    {
         m_iAchievementNum = 4;
    }
    else if(m_AchievementName.compare(ACHIEVED_100_WORDS_SPELL)==0)
    {
        m_iAchievementNum = 5;
    }
    else if(m_AchievementName.compare(ACHIEVED_25_TIMES_HINT)==0)
    {
        m_iAchievementNum = 6;
    }
    else if(m_AchievementName.compare(ACHIEVED_50_TIMES_HINT)==0)
    {
        m_iAchievementNum = 7;
    }
    else if(m_AchievementName.compare(ACHIEVED_25_TIMES_LETTER_A)==0)
    {
        m_iAchievementNum = 8;
    }
    else if(m_AchievementName.compare(ACHIEVED_50_TIMES_LETTER_A)==0)
    {
        m_iAchievementNum = 9;
    }
    else if(m_AchievementName.compare(ACHIEVED_10_GIFT_COLLECT)==0)
    {
        m_iAchievementNum = 10;
    }
    else if(m_AchievementName.compare(ACHIEVED_25_GIFT_COLLECT)==0)
    {
        m_iAchievementNum = 11;
    }
    else if(m_AchievementName.compare(ACHIEVED_15_DAYS_BONUS_COLLECT)==0)
    {
        m_iAchievementNum = 12;
    }
    else if(m_AchievementName.compare(ACHIEVED_50_DAYS_BONUS_COLLECT)==0)
    {
        m_iAchievementNum = 13;
    }
    else if(m_AchievementName.compare(ACHIEVED_50_LEVELS_WITHOUT_MISSPELL)==0)
    {
        m_iAchievementNum = 14;
    }
    else if(m_AchievementName.compare(ACHIEVED_25_BONUS_WORDS_COLLECT)==0)
    {
        m_iAchievementNum = 15;
    }
    else if(m_AchievementName.compare(ACHIEVED_50_BONUS_WORDS_COLLECT)==0)
    {
        m_iAchievementNum = 16;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_INDIA)==0)
    {
        m_iAchievementNum = 17;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_AMERICA)==0)
    {
        m_iAchievementNum = 18;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_AUSTRALIA)==0)
    {
        m_iAchievementNum = 19;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_ENGLAND)==0)
    {
        m_iAchievementNum = 20;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_RUSSIA)==0)
    {
        m_iAchievementNum = 21;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_INDONESIA)==0)
    {
        m_iAchievementNum = 22;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_JAPAN)==0)
    {
        m_iAchievementNum = 23;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_BRAZIL)==0)
    {
        m_iAchievementNum = 24;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_FRANCE)==0)
    {
        m_iAchievementNum = 25;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_CANADA)==0)
    {
        m_iAchievementNum = 26;
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_GERMANY)==0)
    {
        m_iAchievementNum = 27;
    }
}
std::string AchievementPopUp::getIconsForAchievements(std::string AchieveTag)
{

    std::string name = "circle-image";
    
    if(AchieveTag.compare("good")==0)
    {
        name = "Achievement_icons/good.png";
    }
   else if(AchieveTag.compare("great")==0) {
        name = "Achievement_icons/great.png";
    }
    else if(AchieveTag.compare("amazing")==0) {
         name = "Achievement_icons/amazing.png";
     }
    else if(AchieveTag.compare("awesome")==0) {
         name = "Achievement_icons/awesome.png";
     }
    else if(AchieveTag.compare("words")==0) {
         name = "Achievement_icons/words.png";
     }
    else if(AchieveTag.compare("hints")==0) {
         name = "Achievement_icons/hints.png";
     }
    else if(AchieveTag.compare("Letter_A")==0) {
         name = "Achievement_icons/Letter_A.png";
     }
    else if(AchieveTag.compare("giftbox")==0) {
         name = "Achievement_icons/giftbox.png";
     }
    else if(AchieveTag.compare("dailybonus")==0) {
         name = "Achievement_icons/dailybonus.png";
     }
    else if(AchieveTag.compare("misspell")==0) {
         name = "Achievement_icons/levels.png";
     }
    else if(AchieveTag.compare("bonusword")==0) {
         name = "Achievement_icons/bonusword.png";
     }
    else {
        name = "Achievement_icons/clear_country.png";
    }
    
    return name;
}
AchievementPopUp::~AchievementPopUp()
{
    
    if(m_ptrScrolView)
    {
        m_ptrScrolView->removeAllChildren();
        this->removeChild(m_ptrScrolView,true);
        m_ptrScrolView = NULL;
    }

    this->removeAllChildrenWithCleanup(true);
}
