//
//  GamePromotionPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 15/06/20.
//

#include "GamePromotionPopUp.h"
GamePromotionPopUp::GamePromotionPopUp(std::function<void()> func)
{
    m_fnSelector = func;
        
    ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.2f);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX());
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
//    this->addChild(m_SprPopUpTop,2);
    
    Sprite *GamePromoBg = Sprite::create("large_box.png");
    GamePromoBg->setScaleY(GamePromoBg->getScaleY()+0.6f);
    GamePromoBg->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()-30-GamePromoBg->getBoundingBox().size.height/2));
    this->addChild(GamePromoBg,2);
        

    std::string str = FileUtils::getInstance()->getWritablePath().append("popupPromoIcon.png");
    
    if(FileUtils::getInstance()->getStringFromFile(str).empty())
    {
        str = "pixel.png";
    }

    MenuItemSprite *ExitIcon = getButtonMade(str,str,CC_CALLBACK_0(GamePromotionPopUp::InstallCallBack,this));
    ExitIcon->setScale(2.0f,2.2f);
    ExitIcon->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),GamePromoBg->getBoundingBox().getMaxY()-30-ExitIcon->getBoundingBox().size.height/2));
    
    std::string str1 = GameController::getInstance()->m_PopupAppTitle;
    Label *t_GameTitle = Label::createWithTTF(GameController::getInstance()->m_PopupAppTitle,FONT_NAME, 35);
    t_GameTitle->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    t_GameTitle->setColor(Color3B::WHITE);
    t_GameTitle->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    t_GameTitle->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),ExitIcon->getBoundingBox().getMinY()-10-t_GameTitle->getBoundingBox().size.height/2));
    t_GameTitle->setColor(Color3B(255,244,55));
    t_GameTitle->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_GameTitle,3);
    
    Label *t_GameDesc = Label::createWithTTF(GameController::getInstance()->m_PopupAppDesc,FONT_NAME,27);
    t_GameDesc->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    t_GameDesc->setColor(Color3B::WHITE);
    t_GameDesc->enableOutline(Color4B(196,56,63,255),3);
    t_GameDesc->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    t_GameDesc->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_GameTitle->getBoundingBox().getMinY()-20-t_GameDesc->getBoundingBox().size.height/2));
    t_GameDesc->setWidth(GamePromoBg->getContentSize().width-50);
    t_GameDesc->setHeight(t_GameDesc->getBoundingBox().size.height);
    this->addChild(t_GameDesc,3);
    
    
    MenuItemSprite *t_miInstallBtn = getButtonMade("medium_button.png","medium_button.png",CC_CALLBACK_0(GamePromotionPopUp::InstallCallBack,this));
    t_miInstallBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()+10+t_miInstallBtn->getBoundingBox().size.height));
    t_miInstallBtn->setScale(0.9f);
    t_miInstallBtn->setTag(0);

    Label *installLabel = createLabelBasedOnLang("Install",40);
    installLabel->setWidth(t_miInstallBtn->getContentSize().width-10);
    installLabel->setHeight(150);
    installLabel->setColor(Color3B::WHITE);
    installLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    installLabel->enableOutline(Color4B(53,126,32,255),2);
    installLabel->setPosition(Vec2(t_miInstallBtn->getContentSize().width/2,t_miInstallBtn->getContentSize().height/2+10));
    t_miInstallBtn->addChild(installLabel,1);

    
    MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(GamePromotionPopUp::OnButtonPressed,this));
    t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-20,m_SprPopUpBg->getBoundingBox().getMaxY()-20));
    t_miCloseButton->setTag(0);

    Menu *menu = Menu::create(ExitIcon,t_miInstallBtn,t_miCloseButton,NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu,2);
        
}
void GamePromotionPopUp::InstallCallBack()
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    Application::getInstance()->openURL(GameController::getInstance()->m_PopupAppUrl);
    
}
Label *GamePromotionPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
MenuItemSprite *GamePromotionPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
    
}
void GamePromotionPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    int pButtonTag =  Button->getTag();

      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"close";
          }break;
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}

GamePromotionPopUp::~GamePromotionPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
