//
//  OptionGrid.cpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#include "OptionGrid.h"
#include "GameLayer.h"
#include "OptionLetterTile.h"
#include "GameStateManager.h"
#include "ShufflingClass.h"
#include "GameConstants.h"
#include "AnswerGrid.h"
#include "AchievementManager.h"
#include "DailyMissionManager.h"
#include "AnswerWord.h"
#include "StatsManager.h"
#include "GameController.h"
#include "HudLayer.h"
#include "LanguageTranslator.h"

OptionGrid::OptionGrid(GameLayer *p_ptrGameLayer)
{
    
    m_ptrGameLayer = p_ptrGameLayer;
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    Origin      = ResolutionSize.origin;
    
    m_OptionLetterTileArray = new Vector<OptionLetterTile*>;
    m_OptionArrayForBack = new Vector<OptionLetterTile*>;
    
    m_fDrawNode = NULL;
    m_fTempNode = NULL;
    m_AnswerText = NULL;
    nodeArray = new Vector<DrawNode*>;
    m_sprArr  = new Vector<Sprite*>();
    m_bTouchEnd   = true;
    m_bRevealed   = false;
    m_bTouchBegan = false;
    
    m_pStartPos  = Point(0,0);
    m_pEndPos    = Point(0,0);
    mb_touched   = 0;
    m_prevPos    = Point(0,0);
    m_fGridScale = 0.9f;
    
    m_strLevelWord = "";
    m_color = Color4F(Color3B(45,183,210), 1.0f);
    
    optBg = Sprite::create("options_Bg.png");
    optBg->setColor(Color3B(250,250,240));
    //    optBg->setScale(m_fScale);
    optBg->setPosition(Vec2(Origin.x+visibleSize.width/2,120+optBg->getBoundingBox().size.height/2));
    optBg->setOpacity(0);
    m_ptrGameLayer->addChild(optBg);
    
    createAnswerTextLabel();
    
    
    order = 2;
}
/// this creates answer  Text label to display when user swipes letters
void OptionGrid::createAnswerTextLabel()
{
    
    m_AnswerSpr = Sprite::create("Progress_barBg.png");
    m_AnswerSpr->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-10));
    // m_AnswerSpr->setScale(0);
    m_AnswerSpr->setOpacity(0);
    m_ptrGameLayer->addChild(m_AnswerSpr,1);
    
    if(me_Language.compare(LANG_ENGLISH)==0) {
        m_AnswerText = Label::createWithTTF("",FONT_NAME, 40,Size(0,100));
    }else{
        m_AnswerText = Label::createWithSystemFont("","fonts/arial.ttf", 35,Size(0,100));
    }
    
    m_AnswerText->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    m_AnswerText->setColor(Color3B::WHITE);
    m_AnswerText->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-10));
    m_ptrGameLayer->addChild(m_AnswerText,2);
    
}
void OptionGrid::createOptionLetterTiles(const std::vector<std::string> &p_Letters,std::map<std::string ,int> dictLetterCount)
{
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
    
    m_DictLetterCount = dictLetterCount;
    
    int totLetters = (int)p_Letters.size();
    std::string str = "";
    int count  = 0;
    for (int index = 0; index<totLetters; index++)
    {
        str = p_Letters.at(index);
        
        if(index==totLetters-1) {
            m_strLevelWord += str;
        }else
        {
            m_strLevelWord += str+"_";
        }
        
        if(dictLetterCount.size()>0)
            count = dictLetterCount.at(str);
        
        OptionLetterTile *_obj = new OptionLetterTile(m_ptrGameLayer, m_PositionArray.at(index),str,count,m_fGridScale);
        m_OptionLetterTileArray->pushBack(_obj);
        _obj->release();
        _obj = NULL;
    }
    
    StatsManager::getInstance()->setCurrentLevelLetters(m_strLevelWord);
}
#define GRID_SIZE 120
void OptionGrid::createPositionsForLetters(int noOfLeters)
{
    
    
    if(noOfLeters==4)
    {
        m_fGridScale = 0.88f;
    }
    else if(noOfLeters==5)
    {
        m_fGridScale = 0.8f;
    }
    else if(noOfLeters==6)
    {
        m_fGridScale = 0.7f;
    }
    else if(noOfLeters==7)
    {
        m_fGridScale = 0.7f;
    }
    
    
    
    Point _pos = Point(Origin.x+visibleSize.width/2,optBg->getBoundingBox().getMidY());
    
    double angle1 = 90;
    float x1,y1;
    
    std::vector<Point> _arryPositions;
    
    for (int i=0 ;i<noOfLeters;i++)
    {
        y1 = _pos.y+sin(CC_DEGREES_TO_RADIANS(angle1))*(GRID_SIZE+10);
        x1 =  _pos.x+cos(CC_DEGREES_TO_RADIANS(angle1))*(GRID_SIZE+10);
        m_PositionArray.push_back(Point(x1,y1));
        angle1-= 360.0f/noOfLeters;
    }
    
    printf("m_PositionArray->%lu\n",m_PositionArray.size());
    
}
// ON TOUCH BEGAN
void OptionGrid::onTouchBeganOptionGrid(Point p_Pos)
{
    
    for(int i=0; i<m_OptionLetterTileArray->size();i++)
    {
        OptionLetterTile *_optObj = NULL;
        _optObj = (OptionLetterTile *)m_OptionLetterTileArray->at(i);
        if(_optObj!=NULL)
        {
            if(_optObj->getLetterLabel()!=NULL)
            {
                if(_optObj->getLetterLabel()->getBoundingBox().containsPoint(p_Pos)&&!_optObj->m_bTouched&&!_optObj->m_bLock)
                {
                    Sprite *spr = Sprite::create("countEffect.png");
                    spr->setPosition(Vec2(_optObj->getLetterLabel()->getPosition().x,_optObj->getLetterLabel()->getPosition().y));
                    m_ptrGameLayer->addChild(spr,order+1);
                    m_sprArr->pushBack(spr);
                }
            }
        }
    }
    
    m_bTouchEnd = false;
    
}
// ON TOUCH MOVED
void OptionGrid::onTouchMoveOptionGrid(Point p_Location)
{
    
    for(int i=0; i<m_OptionLetterTileArray->size();i++)
    {
        OptionLetterTile *_optObj = NULL;
        _optObj = (OptionLetterTile *)m_OptionLetterTileArray->at(i);
        if(_optObj!=NULL)
        {
            
            if(_optObj->getLetterLabel()!=NULL&&_optObj->getLetterSprite()!= NULL)
            {
                if(_optObj->getLetterSprite()->getBoundingBox().containsPoint(p_Location)&!_optObj->m_bTouched&&!_optObj->m_bLock)
                {
                    _optObj->m_bTouched = true;
                    m_pStartPos = _optObj->getLetterLabel()->getPosition();
                    mb_touched++;
                    m_bTouchBegan = true;
                    _optObj->EnterTheKey();
                    m_OptionArrayForBack->pushBack(_optObj);
                    Sprite *spr = Sprite::create("countEffect.png");
                    spr->setPosition(Vec2(_optObj->getLetterLabel()->getPosition().x,_optObj->getLetterLabel()->getPosition().y));
                    m_ptrGameLayer->addChild(spr,order+1);
                    m_sprArr->pushBack(spr);
                    
                }
                else if(m_OptionArrayForBack->size()>1 && m_OptionArrayForBack->at(m_OptionArrayForBack->size()-2)->getLetterSprite()->getBoundingBox().containsPoint(p_Location))
                {
                    mb_touched=1;
                    m_pStartPos = m_OptionArrayForBack->at(m_OptionArrayForBack->size()-2)->getLetterLabel()->getPosition() ;
                    if(m_OptionArrayForBack->at(m_OptionArrayForBack->size()-1)->m_bTouched==true)
                    {
                        clearLastStringonTouchBack();
                        eraseLine();
                        m_OptionArrayForBack->at(m_OptionArrayForBack->size()-1)->m_bTouched=false;
                        m_OptionArrayForBack->erase(m_OptionArrayForBack->size()-1);
                        drawLineOnDrag(m_pStartPos,m_pEndPos);
                    }
                }
                else if (mb_touched==2)
                {
                    drawingLineonLetterReach(m_pStartPos,m_prevPos);
                    mb_touched--;
                }
                else
                {
                    m_prevPos = m_pStartPos;
                    m_pEndPos = p_Location;
                    if (m_bTouchBegan)
                        drawLineOnDrag(m_pStartPos,m_pEndPos);
                }
            }
        }
        
    }
}
// ON TOUCH ENDED
void OptionGrid::OnTouchEnded(Point p_Location)
{
    if(m_bTouchBegan==false)
    {
        m_bTouchEnd=true;
        removeEffectSpriteOnEnd();
        return;
    }
    
    m_bTouchBegan = false;
    bool _touched =false;
    for(int index=0; index<m_OptionLetterTileArray->size(); index++)
    {
        OptionLetterTile *_optObj = m_OptionLetterTileArray->at(index);
        if(_optObj->getLetterSprite()->getBoundingBox().containsPoint(p_Location)&&!_touched)
        {
            _touched = true;
        }
        else if (!_optObj->m_bLock)
        {
            _optObj->onTouchEnd();
        }
    }
    if(m_fDrawNode)
    {
        m_ptrGameLayer->removeChild(m_fDrawNode,true);
        m_fDrawNode = NULL;
    }
    if(nodeArray->size()>0)
    {
        for(int num=0;num<=nodeArray->size()-1;num++)
        {
            m_ptrGameLayer->removeChild(nodeArray->at(num),true);
        }
    }
    if(nodeArray->size()>0)
    {
        nodeArray->clear();
    }
    
    removeEffectSpriteOnEnd();
    
    if(m_OptionArrayForBack)
    {
        m_OptionArrayForBack->clear();
    }
    checkForValidation();
    onTouchEndOnGrids();
    mb_touched=0;
    m_prevPos = Point(0,0);
    m_bTouchEnd = true;
}
/// removes Touch effect sprite on touch end
void OptionGrid::removeEffectSpriteOnEnd()
{
    if(m_sprArr->size()>0)
    {
        for(int num=0;num<=m_sprArr->size()-1;num++)
        {
            m_ptrGameLayer->removeChild(m_sprArr->at(num),true);
        }
    }
    if(m_sprArr->size()>0)
    {
        m_sprArr->clear();
    }
}
/// on touch end of grids
void OptionGrid::onTouchEndOnGrids()
{
    for(int i = 0;i<m_OptionLetterTileArray->size();i++)
    {
        m_OptionLetterTileArray->at(i)->m_bTouched = false;
    }
    
    
}
/// clears last string when user swipes back touch on option tiles
void OptionGrid::clearLastStringonTouchBack()
{
    if(m_sprArr->size()>0)
    {
        m_ptrGameLayer->removeChild(m_sprArr->at(m_sprArr->size()-1),true);
        m_sprArr->erase(m_sprArr->size()-1);
    }
    
    if(m_AnswerText!=NULL)
    {
        std::string prevStr = m_AnswerText->getString();
        
        if (m_strLetterArray.size()!=0)
            m_strLetterArray.pop_back();
        
        prevStr = getCombinedString(m_strLetterArray);
        m_AnswerText->setString(prevStr);
        
        m_AnswerSpr->setScale(m_fScale);
        m_AnswerSpr->setOpacity(255);
    }
}
/// here we will recieve clicked String from option letter tile
/// @param str  string from tile
void OptionGrid::getClickedString(std::string curStr)
{
    
    if(m_AnswerText!=NULL)
    {
        std::string prevStr = m_AnswerText->getString();
        //        prevStr.append(curStr);
        
        m_strLetterArray.push_back(curStr);
        
        prevStr = getCombinedString(m_strLetterArray);
        
        m_AnswerText->setString(prevStr);
        m_AnswerText->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-10));
        
        m_AnswerSpr->setScale(m_fScale);
        m_AnswerSpr->setOpacity(255);
    }
}
///  gets the combiled string of swiped letters
/// @param str  array of swiped strings array
std::string OptionGrid::getCombinedString(std::vector<std::string> str)
{
    
    std::string tempstr = "";
    for (int i=0;i<str.size(); i++)
    {
        tempstr += str.at(i);
    }
    return tempstr;
}
/// checks for validation when user releases touch from option grids
void OptionGrid::checkForValidation()
{
    if (m_AnswerText!=NULL) {
        m_strAnswer = m_AnswerText->getString();
        
        if (m_ptrGameLayer->getAnswerGrid()!=NULL)
        {
            m_ptrGameLayer->getAnswerGrid()->recieveLetterFromOption(m_strLetterArray);
        }
        
        if (m_strLetterArray.size()>0) {
            m_strLetterArray.clear();
        }
    }
}
// ON SHUFFLING OPTION LETTERS
void OptionGrid::shuffleOptionLetters()
{
    OptionLetterTile *_optObj = NULL;
    std::vector<Point> t_PositionArray = ShufflingClass::getInstance()->randomShuffle(m_PositionArray);
    
    Point midPos = optBg->getPosition();
    for (int i=0;i<m_OptionLetterTileArray->size(); i++)
    {
        _optObj = m_OptionLetterTileArray->at(i);
        _optObj->shuffleOptionsTile(midPos,t_PositionArray.at(i));
    }
}
/// LINE CREATION TO DRAG
/// @param startPos Start position
/// @param endPos end position
void OptionGrid::drawLineOnDrag(Point startPos,Point endPos)
{
    
    if(m_fDrawNode)
    {
        m_ptrGameLayer->removeChild(m_fDrawNode,true);
        m_fDrawNode = NULL;
    }
    m_fDrawNode = DrawNode::create();
    
    m_fDrawNode->drawSegment(startPos,endPos,10.0f,m_color);
    m_ptrGameLayer->addChild(m_fDrawNode,order);
}
/// Draws line on letter reach from one letter grid to another
/// @param start start tile position
/// @param end  end tile postion
void OptionGrid::drawingLineonLetterReach(Point start,Point end)
{
    
    m_fTempNode= DrawNode::create();
    Color4F _clr = Color4F(Color3B(45,183,210), 1.0f);
    
    m_fTempNode->drawSegment(end,start , 10.0f,_clr);
    m_fTempNode->setOpacity(255);
    m_ptrGameLayer->addChild(m_fTempNode,order);
    
    nodeArray->pushBack(m_fTempNode);
}
/// checks for letter touch tutorial
void OptionGrid::checkforTutorialRemove()
{
    
    m_ptrGameLayer->removeTutorial();
    
    removeHandAnim();
    
    if(lineSprArr.size()>0)
    {
        if(lineSprArr.size()>0)
        {
            for(int num=0;num<lineSprArr.size();num++)
            {
                m_ptrGameLayer->removeChild(lineSprArr.at(num),true);
            }
        }
        
        if(lineSprArr.size()>0)
        {
            lineSprArr.clear();
        }
    }
    
    AnswerWord *word = m_ptrGameLayer->getAnswerGrid()->getAnswerWordArray()->at(0);
    std::string wordStr = word->getAnsWordString();
    std::string letter ="";
    
    order = 2;
    int size = (int)m_OptionLetterTileArray->size();
    
    std::vector<std::string> wordsarr = GameController::getInstance()->split(wordStr,',');
    
    Point origin;
    Point destination;
    int tag = 0;
    for(int i=0;i<size;i++)
    {
        OptionLetterTile *tile = m_OptionLetterTileArray->at(i);
        
        letter = tile->getLetterLabel()->getString();
        
        bool t_bFound =  std::find(wordsarr.begin(), wordsarr.end(), letter.c_str()) != wordsarr.end();
        
        if(t_bFound)
        {
            tile->getLetterSprite()->setLocalZOrder(1);
            tile->getLetterLabel()->setLocalZOrder(2);
            
            if (tag==0) {
                origin = tile->getLetterLabel()->getPosition();
            }
            else
            {
                destination = tile->getLetterLabel()->getPosition();
            }
            tag++;
        }
        else{
            tile->m_bLock = false;
        }
    }
    
    if(UserDefault::getInstance()->getIntegerForKey("firstTutNum")==0)
    {
        UserDefault::getInstance()->setIntegerForKey("firstTutNum",int(m_ptrGameLayer->getAnswerGrid()->getAnswerWordArray()->size()-1));
        
        DelayTime *delay = DelayTime::create(0.7f);
        CallFunc *callfunc = CallFunc::create([&,this]() {
            addThreelettersTutorial();
        });
        m_ptrGameLayer->runAction(Sequence::create(delay,callfunc, NULL));
    }
    else
    {
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_START,false);
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT,true);
    }
}
/// adds the tutorial for 3 letters
void OptionGrid::addThreelettersTutorial()
{
    m_ptrGameLayer->checkforFirstLevelTutorial();
    
    if(m_ptrGameLayer->getHudLayer()!=NULL)
    {
        m_ptrGameLayer->getHudLayer()->setEnableButtons(false);
        m_ptrGameLayer->getHudLayer()->m_bTutorialAvailble = true;
    }
}
/// sets the z order of tiles for tutorial
void OptionGrid::setLocalZorderForTutorial()
{
    int tutNum = UserDefault::getInstance()->getIntegerForKey("firstTutNum");
    order = 4;
    AnswerWord *word = m_ptrGameLayer->getAnswerGrid()->getAnswerWordArray()->at(tutNum);
    std::string wordStr = word->getAnsWordString();
    std::string letter ="";
    
    std::string str = m_ptrGameLayer->getwordslbl()->getString();
    
    std::string t_TempWord = wordStr;
    t_TempWord.erase(remove(t_TempWord.begin(), t_TempWord.end(), ','), t_TempWord.end());
    
    if (m_ptrGameLayer->getwordslbl()!= NULL)
    {
        std::string tempstr = StringUtils::format("%s '%s' ",str.c_str(),t_TempWord.c_str());
        m_ptrGameLayer->getwordslbl()->setString(tempstr);
    }
    
    int size = (int)m_OptionLetterTileArray->size();
    
    std::vector<std::string> wordsarr = GameController::getInstance()->split(wordStr,',');
    
    
    Point origin;
    Point destination;
    Point control1;
    int tag = 0;
    for(int i=0;i<size;i++)
    {
        OptionLetterTile *tile = m_OptionLetterTileArray->at(i);
        
        letter = tile->getLetterLabel()->getString();
        
        bool t_bFound =  std::find(wordsarr.begin(), wordsarr.end(), letter.c_str()) != wordsarr.end();
        
        if(t_bFound)
        {
            tile->getLetterSprite()->setLocalZOrder(4);
            tile->getLetterLabel()->setLocalZOrder(4);
            
            if (tag==0) {
                origin = tile->getLetterLabel()->getPosition();
            }
            else
            {
                destination = tile->getLetterLabel()->getPosition();
            }
            tag++;
        }
        else{
            tile->m_bLock = true;
        }
    }
    
    std::vector<Point> pointsArr;
    if(tutNum==0)
    {
        addHandAnimation(origin,destination);
    }
    else
    {
        for(int i=0;i<wordsarr.size();i++)
        {
            for(int j=0;j<size;j++)
            {
                OptionLetterTile *tile = m_OptionLetterTileArray->at(j);
                
                letter = tile->getLetterLabel()->getString();
                if(wordsarr.at(i)==letter)
                {
                    pointsArr.push_back(tile->getLetterLabel()->getPosition());
                    break;
                }
            }
        }
        
        for(int p =0; p<pointsArr.size()-1; p++)
        {
            Point pos = Point(pointsArr.at(p).getMidpoint(pointsArr.at(p+1)));
            
            Sprite *t_sprLine = Sprite::create("dotted-line.png");
            t_sprLine->setScale(m_fScale-0.3f,m_fScale);
            t_sprLine->setPosition(pos);
            m_ptrGameLayer->addChild(t_sprLine,4);
            
            lineSprArr.push_back(t_sprLine);
            
            if(pos.y>300)
            {
                if(pos.x<(Origin.x+visibleSize.width/2))
                    t_sprLine->setRotation(-60);
                else if(pos.x>(Origin.x+visibleSize.width/2))
                    t_sprLine->setRotation(+60);
            }
        }
        
        Point locate = pointsArr.at(0);
        
        sprHand = Sprite::create("hand.png");
        sprHand->setScale(0.5f);
        sprHand->setPosition(locate.x+sprHand->getBoundingBox().size.width/2,locate.y-sprHand->getBoundingBox().size.height/2);
        m_ptrGameLayer->addChild(sprHand,4);
        
        control1 = pointsArr.at(1);
        destination = pointsArr.at(2);
        if(me_Language.compare(LANG_ENGLISH)==0||me_Language.compare(LANG_GUJARATI)==0)
        {
            control1 = Vec2(control1.x+40,control1.y-50);
            destination = Vec2(destination.x+40,destination.y-50);
        }
        else if(me_Language.compare(LANG_KANNADA)==0||me_Language.compare(LANG_ODIA)==0)
        {
            control1 = Vec2(control1.x+50,control1.y-50);
            destination = Vec2(destination.x+40,destination.y-50);
        }
        else if(me_Language.compare(LANG_HINDI)==0||me_Language.compare(LANG_TELUGU)==0||me_Language.compare(LANG_MARATHI)==0)
        {
            control1 = Vec2(control1.x+40,control1.y-50);
            destination = Vec2(destination.x+50,destination.y-50);
        }
        else if(me_Language.compare(LANG_BANGLA)==0||me_Language.compare(LANG_MALAYALAM)==0||me_Language.compare(LANG_TAMIL)==0)
        {
            control1 = Vec2(control1.x+50,control1.y-50);
            destination = Vec2(destination.x+50,destination.y-50);
        }
        
        locate = sprHand->getPosition();
        
        DelayTime *delay = DelayTime::create(0.3f);
        MoveTo *move = MoveTo::create(1.5f, Vec2(control1.x,control1.y));
        MoveTo *move1 = MoveTo::create(1.5f, Vec2(destination.x,destination.y));
        FadeTo *fade = FadeTo::create(0.5f,0);
        Sequence *seq = Sequence::create(move,move1,fade,CallFuncN::create(std::bind(&OptionGrid::fadeoutLines,this )),delay,CallFuncN::create(std::bind(&OptionGrid::setHandPosition,this,locate)),NULL);
        RepeatForever *rep = RepeatForever::create(seq);
        sprHand->runAction(rep);
        
        
    }
}
/// fades out dotted line sprite
void OptionGrid::fadeoutLines()
{
    for (int t =0; t<lineSprArr.size(); t++)
    {
        FadeTo *fade = FadeTo::create(0.1f,0);
        lineSprArr.at(t)->runAction(fade);
    }
    
}
/// adding Hand animation for tutorial from start point and end point letters
/// @param origin  strt letter position
/// @param destination destination  letter position
void OptionGrid::addHandAnimation(Point origin,Point destination)
{
    sprHand = Sprite::create("hand.png");
    sprHand->setScale(m_fScale-0.2f);
    sprHand->setPosition(Vec2(origin.x+sprHand->getBoundingBox().size.width/2+30 ,origin.y-70));
    m_ptrGameLayer->addChild(sprHand,4);
    
    Point pos = Point(origin.getMidpoint(destination));
    
    sprLine = Sprite::create("dotted-line.png");
    sprLine->setScale(m_fScale-0.3f,m_fScale);
    sprLine->setPosition(pos);
    m_ptrGameLayer->addChild(sprLine,4);
    
    
    destination = Vec2(destination.x+40,destination.y-70);
    
    Point locate = Point(sprHand->getPositionX(),sprHand->getPositionY());
    
    
    MoveTo *move = MoveTo::create(1.5f, Vec2(destination.x,destination.y));
    FadeTo *fade = FadeTo::create(0.3f,0);
    Sequence *seq = Sequence::create(move,fade,CallFuncN::create(std::bind(&OptionGrid::setHandPosition,this,locate)),NULL);
    RepeatForever *rep = RepeatForever::create(seq);
    sprHand->runAction(rep);
    
    
}
/// set's hand position on swiping animation
/// @param pos  position of hand
void OptionGrid::setHandPosition(Point _pos)
{
    if(sprHand)
    {
        sprHand->setPosition(_pos);
        sprHand->setOpacity(255);
    }
    
    if(lineSprArr.size()>0)
    {
        for (int t =0; t<lineSprArr.size(); t++)
        {
            FadeTo *fade = FadeTo::create(0.1f,255.0f);
            lineSprArr.at(t)->runAction(fade);
        }
    }
    
}
/// remove hand animation after user drags letters
void OptionGrid::removeHandAnim()
{
    
    if (m_ptrGameLayer->getHudLayer()) {
        m_ptrGameLayer->getHudLayer()->m_bTutorialAvailble = false;
    }
    if(sprHand)
    {
        sprHand->stopAllActions();
        m_ptrGameLayer->removeChild(sprHand,true);
        sprHand = NULL;
    }
    
    if(sprLine)
    {
        m_ptrGameLayer->removeChild(sprLine,true);
        sprLine = NULL;
    }
    
    
}
/// erase line on reverse swipe
void OptionGrid::eraseLine()
{
    if(nodeArray->size()>0)
    {
        m_ptrGameLayer->removeChild(nodeArray->at(nodeArray->size()-1),true);
        nodeArray->erase(nodeArray->size()-1);
    }
}
/// display's number of strings to be used in level
void OptionGrid::showCountofLetters()
{
    
    OptionLetterTile *optionTile = NULL;
    
    for (int index = 0; index<m_OptionLetterTileArray->size(); index++)
    {
        optionTile = m_OptionLetterTileArray->at(index);
        
        if(optionTile!=NULL)
        {
            Sprite *spr = optionTile->getLetterSprite();
            Sprite *cntSpr = (Sprite*)spr->getChildByName("countSpr");
            if(cntSpr)
                cntSpr->setVisible(true);
            
            Label *cntLbl = (Label*)spr->getChildByName("countLabel");
            if(cntLbl)
                cntLbl->setVisible(true);
        }
    }
    
    m_bRevealed = true;
    
}
///  this is used to show animation on correct and wrong word
/// @param isValid  boolean value of  correct or wrong
void OptionGrid::validationAnimation(bool isValid)
{
    
    if(isValid)
    {
        if(m_AnswerSpr!=NULL)
        {
            m_AnswerText->setColor(Color3B(255,207,87));
            CallFunc *callfunc = CallFunc::create([&,this]()
                                                  {
                //                m_AnswerSpr->setColor(Color3B::BLACK);
                if(m_AnswerText!=NULL)
                {
                    m_AnswerText->setString("");
                    m_AnswerText->setColor(Color3B::WHITE);
                    //                    m_AnswerSpr->setScale(0.0f);
                    m_AnswerSpr->setOpacity(0);
                }
            });
            m_AnswerSpr->runAction(Sequence::create(DelayTime::create(0.3f),callfunc, NULL));
        }
        
        if(UserDefault::getInstance()->getBoolForKey(TUTORIAL_START))
            checkforTutorialRemove();
    }
    else
    {
        if(m_AnswerSpr!=NULL)
        {
            m_AnswerText->setColor(Color3B(242,95,92));
            CallFunc *callfunc = CallFunc::create([&,this]()
                                                  {
                //  m_AnswerSpr->setColor(Color3B::BLACK);
                if(m_AnswerText!=NULL)
                {
                    m_AnswerText->setString("");
                    m_AnswerText->setColor(Color3B::WHITE);
                    //                      m_AnswerSpr->setScale(0.0f);
                    m_AnswerSpr->setOpacity(0);
                }
            });
            
            m_AnswerSpr->runAction(Sequence::create(DelayTime::create(0.3f),callfunc, NULL));
        }
    }
}
/// returns map of  letters count
std::map<std::string,int> OptionGrid::getDictionaryLetter()
{
    return m_DictLetterCount;
}
///  updates dictionary of letter count after correct answer found to update letter count label
/// @param foundStr  founded string
void OptionGrid::updateOptionGridsAndDictionary(std::string foundStr)
{
    
    if(m_DictLetterCount.size()>0)
    {
        if (m_DictLetterCount.find(foundStr.c_str()) == m_DictLetterCount.end() )
        {
            // not found
            CCLOG("key not found");
            return;
        }
        else
        {
            // found
            m_DictLetterCount.at(foundStr) -= 1;
        }
    }
    
    m_ptrGameLayer->getAnswerGrid()->checkIsLastWord();
    int t_iWcount = m_ptrGameLayer->getAnswerGrid()->t_iEmptyGrids;
    
    OptionLetterTile *optionTile = NULL;
    int count = 0;
    std::string str = "";
    
    for (int index = 0; index<m_OptionLetterTileArray->size(); index++)
    {
        optionTile = m_OptionLetterTileArray->at(index);
        
        if(optionTile!=NULL)
        {
            Sprite *spr = optionTile->getLetterSprite();
            
            Label *Letter = optionTile->getLetterLabel();
            if (Letter!=NULL)
                str  = Letter->getString();
            
            if(m_DictLetterCount.size()>0)
                count = m_DictLetterCount.at(str);
            
            //             if(t_iWcount==1)
            //             {
            //                 if(count>=2) {
            //                     count = 1;
            //                 }
            //             }
            Label *cntLbl = (Label*)spr->getChildByName("countLabel");
            if(cntLbl)
            {
                cntLbl->setString(StringUtils::format("%d",count));
            }
        }
    }
    printf("string-->%s",foundStr.c_str());
}
Vector<OptionLetterTile*> *OptionGrid::getOptionTileArray()
{    
    return m_OptionLetterTileArray;
}
OptionGrid::~OptionGrid()
{
    if(m_OptionLetterTileArray != NULL)
    {
        m_OptionLetterTileArray->clear();
        delete m_OptionLetterTileArray;
        m_OptionLetterTileArray = NULL;
    }
    
    if(m_OptionArrayForBack != NULL)
    {
        m_OptionArrayForBack->clear();
        delete m_OptionArrayForBack;
        m_OptionArrayForBack = NULL;
    }
    
    if(m_sprArr != NULL)
    {
        m_sprArr->clear();
        delete m_sprArr;
        m_sprArr = NULL;
    }
    
    if(nodeArray != NULL)
    {
        nodeArray->clear();
        delete nodeArray;
        nodeArray = NULL;
    }
    if(m_PositionArray.size()>0)
    {
        m_PositionArray.clear();
    }
    
}
