//
//  CommonPopUp.hpp
//  wordPro-mobile
//
//  Created by Arun on 10/02/21.
//

#ifndef CommonPopUp_hpp
#define CommonPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class CommonPopUp :public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;

    MenuItemSprite *t_miOkBtn;
    
public:
    CommonPopUp(std::function<void()> func);
    ~CommonPopUp();
    
    void OnButtonPressed(Ref *p_Sender) override;
        
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);

    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};


#endif /* CommonPopUp_hpp */
