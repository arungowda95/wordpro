//
//  OptionLetterTile.cpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#include "OptionLetterTile.h"
#include "GameLayer.h"
#include "OptionGrid.h"
#include "GameStateManager.h"
#include "GameConstants.h"

OptionLetterTile::OptionLetterTile(GameLayer* t_ptrGameLayer,Point t_Position,std::string letterStr,int count,float scale)
{
    m_ptrGameLayer = t_ptrGameLayer;
    
    countLabel = NULL;
    //***** NEW CODE *****//
    m_bTouched = false;
    m_bLock = false;
    m_stringLetter = letterStr;
    m_iLetterCount = count;
    
    m_Sprite = Sprite :: create("text-box_tile.png");
    m_Sprite->setPosition(Vec2(t_Position));
    m_Sprite->setScale(scale);
    m_Sprite->setVisible(true);
    
    
    Sprite *countSpr = Sprite::create("number-box.png");
    countSpr->setPosition(Vec2(m_Sprite->getContentSize().width,m_Sprite->getBoundingBox().size.height+countSpr->getBoundingBox().size.height*0.5));
    countSpr->setName("countSpr");
    countSpr->setVisible(false);
    m_Sprite->addChild(countSpr,1);
    
    countLabel = Label::createWithTTF(StringUtils::format("%d",m_iLetterCount),FONT_NAME,26,Size(0,60));
    countLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    countLabel->setPosition(countSpr->getPosition());
    countLabel->setColor(Color3B::WHITE);
    countLabel->setName("countLabel");
    countLabel->setVisible(false);
    m_Sprite->addChild(countLabel,2);
    
    
    m_LetterLabel = Label::createWithTTF(letterStr, FONT_NAME,60,Size(0,60));
    
    if(me_Language.compare(LANG_ENGLISH)!=0){
        m_LetterLabel = Label::createWithSystemFont(letterStr,FONT_NAME,60,Size(0,150));
    }
    
    m_LetterLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    m_LetterLabel->setPosition(t_Position.x+10.5,t_Position.y+10.5);
    m_LetterLabel->setTag(1);
    m_LetterLabel->setColor(Color3B::WHITE);
    m_LetterLabel->enableBold();
    m_ptrGameLayer->addChild(m_LetterLabel,2);
    
    m_ptrGameLayer->addChild(m_Sprite,1);
    
    if(m_LetterLabel->getContentSize().width>(m_Sprite->getBoundingBox().size.width-25))
    {
        float scale = (m_Sprite->getBoundingBox().size.width-25)/(m_LetterLabel->getContentSize().width+5);
        
        m_LetterLabel->setSystemFontSize(60*scale);
        
        CCLOG("\nBIGGER_SIZE\n");
    }
    
}
// returns option letter label
Label *OptionLetterTile::getLetterLabel()
{
    return m_LetterLabel;
}
// returns option letter sprite
Sprite *OptionLetterTile::getLetterSprite()
{
    return m_Sprite;
}
void OptionLetterTile :: onTouch()
{
    m_bTouched = true;
    
}
void OptionLetterTile :: onTouchEnd()
{
    m_bTouched = false;
}
/// sends the clicked tile string
void OptionLetterTile ::EnterTheKey()
{
    if(GameStateManager :: getInstance()->getGameState()!=GAME_STATE_PLAY )
        return;
    
    std::string m_strAlphabet = m_LetterLabel->getString();
    
    m_ptrGameLayer->getOptionGrid()->getClickedString(m_strAlphabet);
    
}
/// Shuffles Option Tiles
/// @param t_midPos old position of grids
/// @param t_Position New Position of grids
void OptionLetterTile::shuffleOptionsTile(Point t_midPos,Point t_Position)
{
    if(m_Sprite!= NULL&&m_LetterLabel != NULL)
    {
        DelayTime *delay = DelayTime::create(0.2f);
        MoveTo *move1 = MoveTo::create(0.2f, Vec2(t_midPos.x,t_midPos.y));
        MoveTo *move2 = MoveTo::create(0.2f, Vec2(t_Position.x+10.5,t_Position.y+10.5));
        
        m_LetterLabel->runAction(Sequence::create(move1,delay,move2, NULL));
        
        MoveTo *move11 = MoveTo::create(0.2f, Vec2(t_midPos.x,t_midPos.y));
        MoveTo *move22 = MoveTo::create(0.2f, Vec2(t_Position.x,t_Position.y));
        
        m_Sprite->runAction(Sequence::create(move11,delay,move22, NULL));
    }
}
OptionLetterTile::~OptionLetterTile()
{
    
    if(countLabel != NULL)
    {
        m_Sprite->removeChild(countLabel, true);
        countLabel = NULL;
    }
    
    if(m_Sprite != NULL)
    {
        m_ptrGameLayer->removeChild(m_Sprite, true);
        m_Sprite = NULL;
    }
    
    if(m_LetterLabel != NULL)
    {
        m_ptrGameLayer->removeChild(m_LetterLabel, true);
        m_LetterLabel = NULL;
    }
}
