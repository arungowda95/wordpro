//
//  AchievementToasts.cpp
//  wordPro-mobile
//
//  Created by id on 04/05/20.
//

#include "AchievementToasts.h"
#include "AchievementManager.h"
#include "HudLayer.h"
#include "LanguageTranslator.h"
#include "GameConstants.h"
// CONSTRUCTOR
AchievementToasts::AchievementToasts(const std::string &p_AchievementName)
{
     m_iCointCount = 0;
     m_bReward = false;
     m_AchievementName = p_AchievementName;
     m_SprIconName = "";
    
     cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
     visibleSize = ResolutionSize.size;
     Origin = ResolutionSize.origin;

     m_SprToast = Sprite::create("coins_popup_Bg.png");
     m_SprToast->setScale(m_fScale+0.1f);
     m_SprToast->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height+100));
     this->addChild(m_SprToast,2);
    
     Sprite *missionIcon = Sprite::create("circle-image.png");
     missionIcon->setPosition(Vec2(missionIcon->getContentSize().width/2+20,m_SprToast->getContentSize().height/2-5));
     missionIcon->setScale(1.0f);
     m_SprToast->addChild(missionIcon,2);
    
     std::string t_Label = "AchievementComplete";
     Label *t_tLabel = createLabelBasedOnLang(t_Label,33);
     t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
     t_tLabel->setWidth(m_SprToast->getBoundingBox().size.width-64);
     t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height);
     t_tLabel->setPosition(Vec2( m_SprToast->getContentSize().width/2,m_SprToast->getContentSize().height-t_tLabel->getContentSize().height/2));
     t_tLabel->setColor(Color3B(255,244,55));
     t_tLabel->enableOutline(Color4B(196,56,63,255),3);
     m_SprToast->addChild(t_tLabel,2);
    
     std::string t_Descrip = getDescription();
     Label *t_tAchieveDisc =  createLabelBasedOnLang(StringUtils::format("Achievement%d",m_iAchievementNum),27);
     t_tAchieveDisc->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
     t_tAchieveDisc->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
     t_tAchieveDisc->setColor(Color3B::WHITE);
     t_tAchieveDisc->enableOutline(Color4B(0,0,0,255),2);
     t_tAchieveDisc->setWidth(m_SprToast->getBoundingBox().size.width-150);
     t_tAchieveDisc->setHeight(t_tAchieveDisc->getBoundingBox().size.height);
     t_tAchieveDisc->setPosition(Vec2(missionIcon->getBoundingBox().getMaxX()+30,t_tLabel->getBoundingBox().getMinY()-10-t_tAchieveDisc->getBoundingBox().size.height/2));
     m_SprToast->addChild(t_tAchieveDisc, 2);

     Sprite *t_AIcon = Sprite::create(m_SprIconName);
    if(t_AIcon==NULL)
        t_AIcon = Sprite::create("circle-image.png");

       t_AIcon->setPosition(Vec2(missionIcon->getContentSize().width/2,missionIcon->getContentSize().height/2));
     t_AIcon->setScale(0.85f);
     missionIcon->addChild(t_AIcon,2);

     // ANIMATE TOASTS
     AnimateToasts();
}
// THIS WILL ANIMTE TOASTS
void AchievementToasts::AnimateToasts()
{
    DelayTime *t_DelayTime = DelayTime::create(2.0f);
    MoveTo *t_MoveTo1 = MoveTo::create(0.5f, Vec2(m_SprToast->getPositionX(), m_SprToast->getPositionY() - m_SprToast->getContentSize().height-50));
    MoveTo *t_MoveTo2 = MoveTo::create(0.5f, Vec2(m_SprToast->getPositionX(), m_SprToast->getPositionY() + m_SprToast->getContentSize().height));
    EaseElasticIn *t_EaseCome = EaseElasticIn::create(t_MoveTo1, 0.85f);
    EaseElasticOut *t_EaseOut = EaseElasticOut::create(t_MoveTo2, 0.85f);
    Sequence *t_sequence = Sequence::create(t_EaseCome, t_DelayTime, t_EaseOut, NULL);
    m_SprToast->runAction(t_sequence);
}
Label *AchievementToasts::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

// THIS RETURN DESCRIPTION ACCORDING TO NAME
std::string AchievementToasts::getDescription()
{
    std::string t_Description = "";
    
    if(m_AchievementName.compare(ACHIEVED_30_GOOD_COMBOS)==0)
    {
        m_iAchievementNum = 1;
        t_Description = "Get 30 GOOD combos";
        m_SprIconName = "Achievement_icons/good.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_30_GREAT_COMBOS)==0)
    {
        m_iAchievementNum = 2;
        t_Description = "Get 30 GREAT combos";
        m_SprIconName = "Achievement_icons/great.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_30_AMAZING_COMBOS)==0)
    {
         m_iAchievementNum = 3;
         t_Description = "Get 30 Amazing combos";
         m_SprIconName = "Achievement_icons/amazing.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_30_AWESOME_COMBOS)==0)
    {
         m_iAchievementNum = 4;
         t_Description = "Get 30 Awesome combos";
         m_SprIconName = "Achievement_icons/awesome.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_100_WORDS_SPELL)==0)
    {
        m_iAchievementNum = 5;
        t_Description = "Spell 100 words";
        m_SprIconName = "Achievement_icons/words.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_25_TIMES_HINT)==0)
    {
        m_iAchievementNum = 6;
        t_Description = "Use Hint for 25 times";
        m_SprIconName = "Achievement_icons/hints.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_50_TIMES_HINT)==0)
    {
        m_iAchievementNum = 7;
        t_Description = "Use Hint for 50 times";
        m_SprIconName = "Achievement_icons/hints.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_25_TIMES_LETTER_A)==0)
    {
        m_iAchievementNum = 8;
        t_Description = "Use letter(A) count option for 25 times";
        m_SprIconName = "Achievement_icons/Letter_A.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_50_TIMES_LETTER_A)==0)
    {
        m_iAchievementNum = 9;
        t_Description = "Use letter(A) count option for 25 times";
        m_SprIconName = "Achievement_icons/Letter_A.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_10_GIFT_COLLECT)==0)
    {
        m_iAchievementNum = 10;
        t_Description = "Collect the Gift box items of level map for 10 times";
        m_SprIconName = "Achievement_icons/giftbox.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_25_GIFT_COLLECT)==0)
    {
        m_iAchievementNum = 11;
        t_Description = "Collect the Gift box items of level map for 25 times";
        m_SprIconName = "Achievement_icons/giftbox.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_15_DAYS_BONUS_COLLECT)==0)
    {
        m_iAchievementNum = 12;
        t_Description = "Collect daily Bonus for continuous 15 days";
        m_SprIconName = "Achievement_icons/dailybonus.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_50_DAYS_BONUS_COLLECT)==0)
    {
        m_iAchievementNum = 13;
        t_Description = "Collect daily Bonus for 50 times";
        m_SprIconName = "Achievement_icons/dailybonus.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_50_LEVELS_WITHOUT_MISSPELL)==0)
    {
        m_iAchievementNum = 14;
        t_Description = "Clear any 50 levels without misspelling";
        m_SprIconName = "Achievement_icons/levels.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_25_BONUS_WORDS_COLLECT)==0)
    {
        m_iAchievementNum = 15;
        t_Description = "Collect 25 Bonus word";
        m_SprIconName = "Achievement_icons/bonusword.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_50_BONUS_WORDS_COLLECT)==0)
    {
        m_iAchievementNum = 16;
        t_Description = "Collect 50 Bonus word";
        m_SprIconName = "Achievement_icons/bonusword.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_INDIA)==0)
    {
        m_iAchievementNum = 17;
        t_Description = "Complete all levels of India";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_AMERICA)==0)
    {
        m_iAchievementNum = 18;
        t_Description = "Complete all levels of America";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_AUSTRALIA)==0)
    {
        m_iAchievementNum = 19;
        t_Description = "Complete all levels of Australia";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_ENGLAND)==0)
    {
        m_iAchievementNum = 20;
        t_Description = "Complete all levels of England";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_RUSSIA)==0)
    {
        m_iAchievementNum = 21;
        t_Description = "Complete all levels of Russia";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_INDONESIA)==0)
    {
        m_iAchievementNum = 22;
        t_Description = "Complete all levels of Indonesia";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_JAPAN)==0)
    {
        m_iAchievementNum = 23;
        t_Description = "Complete all levels of Japan";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_BRAZIL)==0)
    {
        m_iAchievementNum = 24;
        t_Description = "Complete all levels of Brazil";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_FRANCE)==0)
    {
        m_iAchievementNum = 25;
        t_Description = "Complete all levels of France";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_CANADA)==0)
    {
        m_iAchievementNum = 26;
        t_Description = "Complete all levels of Canada";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    else if(m_AchievementName.compare(ACHIEVED_ALL_LEVELS_OF_GERMANY)==0)
    {
        m_iAchievementNum = 27;
        t_Description = "Complete all levels of Germany";
        m_SprIconName = "Achievement_icons/clear_country.png";
    }
    return t_Description;
    
}
std::string AchievementToasts::getIconsForAchievements(std::string AchieveTag)
{

    std::string name = "circle-image";
    
    if(AchieveTag.compare("good")==0)
    {
        name = "Achievement_icons/good.png";
    }
   else if(AchieveTag.compare("great")==0) {
        name = "Achievement_icons/great.png";
    }
    else if(AchieveTag.compare("amazing")==0) {
         name = "Achievement_icons/amazing.png";
     }
    else if(AchieveTag.compare("awesome")==0) {
         name = "Achievement_icons/awesome.png";
     }
    else if(AchieveTag.compare("words")==0) {
         name = "Achievement_icons/words.png";
     }
    else if(AchieveTag.compare("hints")==0) {
         name = "Achievement_icons/hints.png";
     }
    else if(AchieveTag.compare("Letter_A")==0) {
         name = "Achievement_icons/Letter_A.png";
     }
    else if(AchieveTag.compare("giftbox")==0) {
         name = "Achievement_icons/giftbox.png";
     }
    else if(AchieveTag.compare("dailybonus")==0) {
         name = "Achievement_icons/dailybonus.png";
     }
    else if(AchieveTag.compare("misspell")==0) {
         name = "Achievement_icons/levels.png";
     }
    else if(AchieveTag.compare("bonusword")==0) {
         name = "Achievement_icons/bonusword.png";
     }
    else {
        name = "Achievement_icons/clear_country.png";
    }
    
    return name;
}

// DESTRUCTOR
AchievementToasts::~AchievementToasts()
{
    if(m_SprToast != NULL)
    {
        m_SprToast->removeAllChildrenWithCleanup(true);
        this->removeChild(m_SprToast, true);
        m_SprToast = NULL;
    }
}
