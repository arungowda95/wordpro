//
//  IOSInterfaceManager.hpp
//  wordPro-mobile
//
//  Created by id on 31/03/20.
//

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#ifndef IOSInterfaceManager_h
#define IOSInterfaceManager_h

#include <stdio.h>
#include "cocos2d.h"
#include "InterfaceManager.h"

using namespace cocos2d;
class IOSInterfaceManager : public InterfaceManager
{
    
    public:
    
    void OpenUrl(std::string url);
    
    void OpenInStore();
    
    void ShareGame();
    
    void gameFeedBack();
    
    void ShowToastMessage(const std::string &p_Message);

    bool appInstalledOrNot(const std::string &package);
    
    bool isNetworkAvailable();
    
    void PostFirebaseAnalyticsEvent(const std::string &eventName,const std::string &param1,const std::string &paramVal_1,const std::string &param2,const std::string &paramVal_2);
    
    void PostFirebaseAnalyticsScreen(const std::string &p_ScreenName);
    
    std::string getCurrencySymbol(const std::string &p_CurrCode);
    
    void signInWithApple();
    
    void openFbInviteDialog();
    
    bool isFacebookLoggedIn();
    
    void pushDataToUserdata(const std::string &fbId,const std::string &username,const std::string &profileUrl,const std::string &language,int level,int coins);


    void pushFirebaseUserLanguageData(const std::string &fbId,const std::string &language,int currentLevel,int coins);
    
    void getLeaderBoardJson(const std::string &language);
    
    void getUserLanguageDataJson(const std::string &fbId);

    void getLanguageDataUserIds();

    void onUserLanguageDataSuccess(std::string stardata);
    
    void onlanguageDataUserIdsSuccess(std::string stardata);
    
    void onleaderBoardJsonSuccess(std::string strBuf);
    
    void OnLoginSuccessCallBack();
    void OnLoginFailedCallBack();
    
    void checkForGameUpdate();
    
    void initialize();
    
    void setUserFbId(const std::string &fbId);
    
    void getUserRank(const std::string &language);
};
#endif /* IOSInterfaceManager_hpp */
#endif
