//
//  ReportWordPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#ifndef ReportWordPopUp_h
#define ReportWordPopUp_h

#include <stdio.h>

#include "PopUp.h"
#include "ui/UIEditBox/UIEditBox.h"

class ReportWordPopUp:public PopUp, public ui::EditBoxDelegate
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    MenuItemSprite *t_ClaimButton;
    Node *m_EditBoxNode;
    float Posy;
    
    std::string m_StrName;
    std::string m_EnteredStr;
    
    ui::EditBox *t_EditBox;
    
public:
     ReportWordPopUp(std::function<void()> func);
    ~ReportWordPopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);

    void sendButtonCallBack();
    
    void editBoxEditingDidBegin(ui::EditBox* editBox);
    void editBoxTextChanged(ui::EditBox* editBox, const std::string& text);
    void editBoxReturn(ui::EditBox* editBox);
    void editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox);
    
    
    void closePopUpOnReportSuccess(bool success);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};
#endif /* ReportWordPopUp_hpp */
