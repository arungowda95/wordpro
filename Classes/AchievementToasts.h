//
//  AchievementToasts.hpp
//  wordPro-mobile
//
//  Created by id on 04/05/20.
//

#ifndef AchievementToasts_hpp
#define AchievementToasts_hpp

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;
class AchievementToasts : public Node
{
    Size visibleSize;
    Vec2 Origin;

        Sprite                   *m_SprToast;
        std::string               m_AchievementName;
        bool                      m_bReward;
        int                       m_iCointCount;
        std::string               m_SprIconName;
        int m_iAchievementNum;
    public:
        // CONSTRUCTOR AND DESTRUCTOR
        AchievementToasts(const std::string &p_AchievementName);
        ~AchievementToasts();
    
        // THIS WILL ANIMTE TOASTS
        void AnimateToasts();
    
        // THIS RETURN DESCRIPTION ACCORDING TO NAME
        std::string getDescription();
    
        std::string getIconsForAchievements(std::string AchieveTag);
    
        Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};

#endif /* AchievementToasts_hpp */
