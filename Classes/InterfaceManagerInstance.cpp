//
//  InterfaceManagerInstance.cpp
//  wordPro-mobile
//
//  Created by id on 31/03/20.
//

#include "InterfaceManagerInstance.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "IOSInterfaceManager.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AndroidInterfaceManager.h"
#endif

static InterfaceManagerInstance *m_Instance = NULL;
// RETURN SINGELTON CLASS
InterfaceManagerInstance *InterfaceManagerInstance::getInstance()
{
    if(m_Instance == NULL)
    {
        m_Instance = new InterfaceManagerInstance();
    }
    return m_Instance;
}

InterfaceManagerInstance::InterfaceManagerInstance()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    m_InterfaceManager = (InterfaceManager*)new IOSInterfaceManager();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    m_InterfaceManager = (InterfaceManager*)new AndroidInterfaceManager();
#endif
}

InterfaceManager* InterfaceManagerInstance::getInterfaceManager()
{
    return m_InterfaceManager;
}
