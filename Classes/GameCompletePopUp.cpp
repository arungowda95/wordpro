//
//  GameCompletePopUp.cpp
//  wordPro-mobile
//
//  Created by id on 19/05/20.
//

#include "GameCompletePopUp.h"
GameCompletePopUp::GameCompletePopUp(std::function<void()> func)
{
    m_fnSelector = func;
        
    ResolutionSize = Director::getInstance()->getSafeAreaRect();
    visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.3f);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.2);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
//    this->addChild(m_SprPopUpTop,2);
    
    std::string t_subtext = "MorePuzzle";
    Label *t_tSubLabel = createLabelBasedOnLang(t_subtext,45);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,244,55));
    t_tSubLabel->enableOutline(Color4B(76,33,14,255),3);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-t_tSubLabel->getBoundingBox().size.height/2));
    this->addChild(t_tSubLabel, 2);

t_NextButton = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_1(GameCompletePopUp::OnButtonPressed,this));
   t_NextButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()+30+t_NextButton->getBoundingBox().size.height));
   t_NextButton->setTag(0);
   
    Label *NxtLabel = createLabelBasedOnLang("CheckOut",35);
   NxtLabel->setColor(Color3B::WHITE);
   NxtLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
   NxtLabel->setDimensions(t_NextButton->getContentSize().width-10,150);
   NxtLabel->enableOutline(Color4B(53,126,32,255),2);
   NxtLabel->setPosition(Vec2(t_NextButton->getBoundingBox().size.width/2,t_NextButton->getBoundingBox().size.height/2+15));
   t_NextButton->addChild(NxtLabel,1);

   
   // CLOSE BUTTON
     MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(GameCompletePopUp::OnButtonPressed,this));
     t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
     t_miCloseButton->setTag(0);

     Menu *m_Menu = Menu::create(t_NextButton,NULL);
     m_Menu->setPosition(Vec2::ZERO);
     this->addChild(m_Menu,2);

}
MenuItemSprite *GameCompletePopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
      
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;

}
void GameCompletePopUp::OnButtonPressed(Ref *p_Sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    int pButtonTag =  Button->getTag();
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"GameComplete";
          }break;
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}

Label *GameCompletePopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

GameCompletePopUp::~GameCompletePopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
