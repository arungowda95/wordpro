//
//  DailyMissionManager.cpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#include "DailyMissionManager.h"
#include "InterfaceManagerInstance.h"
#include "InterfaceManager.h"
#include "GameConstants.h"
#include "GameStateManager.h"
#include "GameController.h"
#include "DailyMissionToasts.h"
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
#include <memory>

using namespace rapidjson;

// Implement Manager
DailyMissionManager* DailyMissionManager::m_PtrDailyMissionManager = NULL;
// CONSTRUCTOR
DailyMissionManager::DailyMissionManager()
{
     m_iMissionTag = 0;
     m_MissionName = "";
     if(!UserDefault::getInstance()->getBoolForKey("DailyMissionAvailable"))
     {
         CheckDailyMissionAvailability();
     }
     checkForDailyMissionDay();

     if(UserDefault::getInstance()->getBoolForKey("DailyMissionAvailable"))
     {
         clearPreviousDayMissionsData();
         
         UserDefault::getInstance()->setBoolForKey(NEW_TASK, true);
         UserDefault::getInstance()->setBoolForKey("DailyMissionAvailable", false);
         UserDefault::getInstance()->flush();

         time_t  m_StartTime;
         time(&m_StartTime);
         UserDefault::getInstance()->setStringForKey(MISSION_START_TIME, ctime(&m_StartTime));
     }
}
// Return Achievement Manager
DailyMissionManager* DailyMissionManager::getInstance()
{
    if(!m_PtrDailyMissionManager)
    {
        m_PtrDailyMissionManager = new DailyMissionManager();
    }
    return m_PtrDailyMissionManager;
}
// CREATE ACHIEVEMENT TOASTS WITH ACHIEVEMENT NAME
void DailyMissionManager::createMissionToast(const std::string &p_MissionName)
{
    DailyMissionToasts *t_PtrAchievementToasts = new DailyMissionToasts(p_MissionName);
    t_PtrAchievementToasts->autorelease();
    Director::getInstance()->getRunningScene()->addChild(t_PtrAchievementToasts,3);
}
// THIS INITIALIZE ACHIEVEMENT LIST
void DailyMissionManager::LoadDailyMissionsJson()
{
    // SIMPLY INTIALIZE
     rapidjson::Document t_Document;
     std::string fullPath = FileUtils::getInstance()->getWritablePath().append("DailyEventsData.json");
 //    ssize_t bufferSize = 0;
 //    char *p_Buffer = new char[sizeof(FileUtils::getInstance()->getFileData(fullPath.c_str(), "r", &bufferSize))];
 //    p_Buffer = (char*)FileUtils::getInstance()->getFileData(fullPath.c_str(), "r", &bufferSize);
     if(FileUtils::getInstance()->getStringFromFile(fullPath).empty())
     {
         //CCLOG("INTERNAL FILE NOT FOUND");
         fullPath = FileUtils::getInstance()->fullPathForFilename("DailyEventsData.json");
     }
     
     char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(fullPath).c_str()) + sizeof(int) + 1];
     strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(fullPath).c_str());
     
     t_Document.Parse<kParseStopWhenDoneFlag>(p_Buffer);
     
     if (t_Document.HasParseError())  // Print parse error
     {
         CCLOG("GetParseError %u\n", t_Document.GetParseError());
         
         delete [] p_Buffer;
         p_Buffer = NULL;
         
         return;
     }
     
     
     printf("\nDay-->%s\n",m_StrDay.c_str());
     if(t_Document.HasMember(m_StrDay.c_str()))
     {
         if(t_Document[m_StrDay.c_str()].IsArray())
         {
             rapidjson::Document::AllocatorType& allocator = t_Document.GetAllocator();
             for(int i = 0; i<t_Document[m_StrDay.c_str()].Size(); i++)
             {
                 rapidjson::Value objValue;
                 objValue.SetObject();
                 
                 // ACHIEVEMENT NAME
                 std::string t_String = t_Document[m_StrDay.c_str()][i]["Name"].GetString();
                 objValue.AddMember("Name", rapidjson::Value().SetString(t_String.c_str(), allocator), allocator);
                 
                 // Key Tag
                 std::string t_StrKeyTag = t_Document[m_StrDay.c_str()][i]["KeyTag"].GetString();
                 objValue.AddMember("KeyTag", rapidjson::Value().SetString(t_StrKeyTag.c_str(),allocator), allocator);

                 // CurrCount
                 int t_iCurCount  = t_Document[m_StrDay.c_str()][i]["CurrCount"].GetInt();
                 objValue.AddMember("CurrCount", rapidjson::Value().SetInt(t_iCurCount), allocator);

                 // Target
                 int t_iTarget  = t_Document[m_StrDay.c_str()][i]["Target"].GetInt();
                 objValue.AddMember("Target", rapidjson::Value().SetInt(t_iTarget), allocator);

                 // REWARD COUNT
                 int t_iReward = t_Document[m_StrDay.c_str()][i]["Reward"].GetInt();
                 objValue.AddMember("Reward", rapidjson::Value().SetInt(t_iReward), allocator);
                 
                 // REWARD CLAIMED
                 bool t_bRewardClaimed = t_Document[m_StrDay.c_str()][i]["RewardClaimed"].GetBool();
                 objValue.AddMember("RewardClaimed", rapidjson::Value().SetBool(t_bRewardClaimed), allocator);

                 // COMPLETED
                 bool t_bCompleted = t_Document[m_StrDay.c_str()][i]["Status"].GetBool();
                 objValue.AddMember("Status", rapidjson::Value().SetBool(t_bCompleted), allocator);


                 rapidjson::StringBuffer strbuf;
                 rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
                 objValue.Accept(writer);

                 //CCLOG("LIST_%d %s", i, strbuf.GetString());
                 DailyMissionObserver *t_MissionObserver = new DailyMissionObserver(strbuf.GetString());
                 m_ObserverArray.push_back(t_MissionObserver);
                 //delete t_MissionObserver;
             }
         }
     }
     
     delete [] p_Buffer;
     p_Buffer = NULL;

}
// THIS UPDATE ACHIEVEMENT
void DailyMissionManager::OnDailyMissionUpdate(const std::string &p_Type, int p_iCount)
{
    if(m_ObserverArray.size()>0)
    {
        for (int i = 0; i<m_ObserverArray.size(); i++)
        {
            DailyMissionObserver *t_MissionObserver = (DailyMissionObserver *)m_ObserverArray.at(i);
            if(!t_MissionObserver->IsCompleted())
                t_MissionObserver->UpdateMission(p_Type, p_iCount);
        }
    }
}

// THIS SAVE MISSIONS JSON
void DailyMissionManager::SaveMissions()
{
    rapidjson::Document t_doc;
    t_doc.SetObject();
    rapidjson::Document::AllocatorType& allocator = t_doc.GetAllocator();
    
    rapidjson::Value t_DailyMissionData(rapidjson::kArrayType);
    for(int i = 0 ; i < m_ObserverArray.size(); i++)
    {
        DailyMissionObserver *t_MissionObserver = (DailyMissionObserver*)m_ObserverArray.at(i);
        rapidjson::Value t_MissionObserverValue = t_MissionObserver->GetJsonData(allocator);
        t_DailyMissionData.PushBack(t_MissionObserverValue, allocator);
    }
    
    t_doc.AddMember(rapidjson::Value().SetString(m_StrDay.c_str(), allocator), t_DailyMissionData, allocator);
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    t_doc.Accept(writer);
    
    FILE *m_File = NULL;
    std::string path = FileUtils::getInstance()->getWritablePath().append("DailyEventsData.json");
    //CCLOG("%s",path.c_str());
    m_File = fopen(path.c_str(), "w+");
    if (!m_File)
    {
        //CCLOG("File Not Found");
        return;
    }
    
    std::string buffer = strbuf.GetString();
    fwrite(buffer.c_str(), 1,strlen(buffer.c_str()), m_File);
    fclose(m_File);
}
// THIS CLEAR ALL CURRENT MISSIONS
void DailyMissionManager::ClearAllCurrentMissions()
{
    m_ObserverArray.clear();
}

// ON GAME STATE CHANGED
void DailyMissionManager::GameStateChanged()
{
    switch (GameStateManager::getInstance()->getGameState())
    {
        case GAME_STATE_OVER:
            for (int i = 0; i<m_ObserverArray.size(); i++)
            {
                DailyMissionObserver *t_MissionObserver = (DailyMissionObserver*)m_ObserverArray.at(i);
                t_MissionObserver->OnGameOver();
            }
            break;
        default:
            break;
    }
}
// ON APPLICATION QUIT
void DailyMissionManager::OnApplicationQuit()
{
    SaveMissions();
}
// THIS CHECK AND UNLOCK ACHIEVEMNET
void DailyMissionManager::CheckAndUnlockDailyMission(const std::string &p_Name)
{
    SaveMissions();
    createMissionToast(p_Name);
}
// THIS CHECK ALL ACHIEVEMENTS COMPLETED OR NOT
bool DailyMissionManager::CheckAllDailyMissionsComplete()
{
    bool t_bAllComplete = true;
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        if(m_ObserverArray.at(i)->IsCompleted() == false)
        {
            t_bAllComplete = false;
        }
    }
    return t_bAllComplete;
}
// THIS RETURN MISSIONS ARRAY
std::vector<DailyMissionObserver*> DailyMissionManager::GetDailyMissions()
{
    return m_ObserverArray;
}
// RETURN MISSION TAG
int DailyMissionManager::getMissionTag()
{
    return m_iMissionTag;
}
int DailyMissionManager::getCompletedDailyMissionsCount()
{
    int count = 0;
    
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        if(m_ObserverArray.at(i)->IsCompleted() == true)
        {
            count++;
        }
    }
    return count;
}
int DailyMissionManager::getCountOfDailyMissionsToClaim()
{
    int count = 0;
    
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        if(m_ObserverArray.at(i)->IsCompleted() == true&&!m_ObserverArray.at(i)->getIsRewardClaimed())
        {
            count++;
        }
    }
    return count;
}

/// CHECK FOR ALL DAILY MISSIONS CLAIMED OR NOT
bool DailyMissionManager::checkAllDailyMissionsClaimed()
{
    bool value = false;

    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        value = m_ObserverArray.at(i)->getIsRewardClaimed();
        if(!value)
            break;
    }
    return value;
}
// THIS CHECK AND UNLOCK MISSION
bool DailyMissionManager::checkIsDailyMissionClaimed(int index)
{
    bool value = false;
    if(m_ObserverArray.size()>0)
    {
        {
            DailyMissionObserver *t_AchievementObserver = (DailyMissionObserver *)m_ObserverArray.at(index);
                value = t_AchievementObserver->getIsRewardClaimed();
        }
    }
    return value;
}
// THIS CHECK AND UNLOCK MISSIONS
void DailyMissionManager::updateDailyMissionClaimed(int index)
{
    if(m_ObserverArray.size()>0)
    {
        {
            DailyMissionObserver *t_AchievementObserver = (DailyMissionObserver *)m_ObserverArray.at(index);
                t_AchievementObserver->updateRewardClaimed();
            SaveMissions();
        }
    }
}
void DailyMissionManager::clearPreviousDayMissionsData()
{
    std::string fullPath = FileUtils::getInstance()->getWritablePath().append("DailyEventsData.json");
    if(!FileUtils::getInstance()->getStringFromFile(fullPath).empty())
    {
        //CCLOG("INTERNAL FILE NOT FOUND");
        FileUtils::getInstance()->removeFile(fullPath);
    }
}
// RETURN MISSION NAME
std::string DailyMissionManager::getLastUnlockDailyMissionName()
{
    return m_MissionName;
}
void DailyMissionManager::checkForDailyMissionDay()
{

    int day = UserDefault::getInstance()->getIntegerForKey(MISSION_DAY);

    
    if (day>5)
    {
        UserDefault::getInstance()->setIntegerForKey(MISSION_DAY, 1);
    }
    
    day = UserDefault::getInstance()->getIntegerForKey(MISSION_DAY);
    
    switch (day)
    {
        case 1:
        {
            m_StrDay = DAY_1;
        }break;
        case 2:
        {
            m_StrDay = DAY_2;
        }break;
        case 3:
        {
            m_StrDay = DAY_3;
        }break;
        case 4:
        {
            m_StrDay = DAY_4;
        }break;
        case 5:
        {
            m_StrDay = DAY_5;
        }break;
        default:
            m_StrDay = DAY_1;
            break;
    }
}
std::string DailyMissionManager::getDayString()
{

    std::string _day;
    int day = UserDefault::getInstance()->getIntegerForKey(MISSION_DAY);

    
    if (day>5)
    {
        UserDefault::getInstance()->setIntegerForKey(MISSION_DAY, 1);
    }
    
    day = UserDefault::getInstance()->getIntegerForKey(MISSION_DAY);
    
    switch (day)
    {
        case 1:
        {
            _day = DAY_1;
        }break;
        case 2:
        {
            _day = DAY_2;
        }break;
        case 3:
        {
            _day = DAY_3;
        }break;
        case 4:
        {
            _day = DAY_4;
        }break;
        case 5:
        {
            _day = DAY_5;
        }break;
        default:
            _day = DAY_1;
            break;
    }
    
    return _day;
}
// THIS CHECK FOR DAILY BONUS
void DailyMissionManager::CheckDailyMissionAvailability()
{
    float t_fTimeDelay = 24*60*60;
    time_t  m_start, m_Current;
    // GET BEGIN TIME
    std::string strTime = UserDefault::getInstance()->getStringForKey(MISSION_START_TIME);
    struct tm target;
    strptime(strTime.c_str(),"%c", &target);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    m_start = timelocal(&target);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    target.tm_isdst = -1;
    m_start = mktime(&target);
#endif
    
    // GET END TIME
    time(&m_Current);
    
    float diff1 = m_Current - m_start;
    if(diff1>t_fTimeDelay && diff1<=2*t_fTimeDelay)
    {
        // MISSION AVIALBLE TO SHOW
        UserDefault::getInstance()->setBoolForKey("DailyMissionAvailable", true);
        int day = UserDefault::getInstance()->getIntegerForKey(MISSION_DAY);

        UserDefault::getInstance()->setIntegerForKey(MISSION_DAY, day+1);
        UserDefault::getInstance()->flush();
    }
    else if (diff1>2*t_fTimeDelay)
    {
        // RESET MISSION LOOP
        UserDefault::getInstance()->setBoolForKey("DailyMissionAvailable", true);
        UserDefault::getInstance()->setBoolForKey(DAY_1, true);
        UserDefault::getInstance()->setBoolForKey(DAY_2, false);
        UserDefault::getInstance()->setBoolForKey(DAY_3, false);
        UserDefault::getInstance()->setBoolForKey(DAY_4, false);
        UserDefault::getInstance()->setBoolForKey(DAY_5, false);
        UserDefault::getInstance()->flush();
    }
    else
    {
        UserDefault::getInstance()->setBoolForKey("DailyMissionAvailable", false);
        UserDefault::getInstance()->flush();
    }
}
// DESTRUCTOR
DailyMissionManager::~DailyMissionManager()
{
    for (int i = 0; i<m_ObserverArray.size(); i++)
    {
        delete m_ObserverArray[i];
        m_ObserverArray.erase(m_ObserverArray.begin()+i);
    }
    m_ObserverArray.clear();
}
