//
//  BonusWordPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 16/04/20.
//

#include "BonusWordPopUp.h"

BonusWordPopUp::BonusWordPopUp(std::function<void()> func)
{
    m_fnSelector = func;
    
    t_miCollectCoinBtn = NULL;
    
    remCount = 0;
    
     ResolutionSize = Director::getInstance()->getSafeAreaRect();
     visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
     origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.4f);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.25);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);

    std::string t_Label = "bonuswords";
    Label *t_tLabel = createLabelBasedOnLang(t_Label, 55);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+20);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(t_tLabel, 2);
    
    std::string t_SubText = "bonusMsg";
    Label *t_tSubLabel = createLabelBasedOnLang(t_SubText,35);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,255,255));
    t_tSubLabel->enableOutline(Color4B(196,56,63,255),2);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-30-t_tSubLabel->getBoundingBox().size.height/2));
    this->addChild(t_tSubLabel, 2);

    int count = StatsManager::getInstance()->getLangBonusWordsCount();

    
    if(me_Language.compare(LANG_TAMIL)==0)
    {
        t_tLabel->setSystemFontSize(50);
        t_tSubLabel->setSystemFontSize(30);
        t_tSubLabel->setHeight(0);
        t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+20);
        t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-30-t_tSubLabel->getBoundingBox().size.height/2));
    }
    
     Sprite *m_LevelProgressBarBg = Sprite::create("Progress_barBg.png");
     m_LevelProgressBarBg->setPosition(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()-30);
     this->addChild(m_LevelProgressBarBg,2);

     ProgressTimer *m_LevelProgressBar = ProgressTimer::create(Sprite::create("Progress_bar_filler.png"));
     m_LevelProgressBar->setType(ProgressTimer::Type::BAR);
     m_LevelProgressBar->setBarChangeRate(Vec2(1,0));
     m_LevelProgressBar->setMidpoint(Vec2(0.0f, 0.0f));
     m_LevelProgressBar->setPosition(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()-30);
     m_LevelProgressBar->setPercentage(0);
     this->addChild(m_LevelProgressBar,2);
    
    
    if(count>10)
    {
        remCount = count - 10;
        
        count = 10;
    }
    
    float percentage = (float(count)/10)*100;

    
    if (percentage>=100) {
        percentage = 100.0f;
    }
    
    std::string t_Count = StringUtils::format("%d/10",count);
    Label *t_tCountLabel = createLabelBasedOnLang(t_Count,40);
    t_tCountLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCountLabel->setColor(Color3B::WHITE);
    t_tCountLabel->enableOutline(Color4B(53,126,32,255),2);
    t_tCountLabel->setPosition(Vec2(m_LevelProgressBarBg->getPositionX(),m_LevelProgressBar->getPositionY()));
    this->addChild(t_tCountLabel,3);

    
    Sprite *coinSpr = Sprite::create("big-coins.png");
    coinSpr->setPosition(Vec2(m_LevelProgressBarBg->getBoundingBox().getMaxX()+coinSpr->getBoundingBox().size.width/2,m_LevelProgressBarBg->getPositionY()));
    this->addChild(coinSpr,1);
    
    Sprite *coincountBg = Sprite::create("circle.png");
    coincountBg->setScale(coincountBg->getScale()-0.25);
    coincountBg->setColor(Color3B::RED);
    coincountBg->setPosition(Vec2(coinSpr->getContentSize().width-25,coinSpr->getContentSize().height-10));
    coinSpr->addChild(coincountBg,1);
    
    m_tCoins = 20;
    m_tCoinCount = Label::createWithTTF(StringUtils::format("%d",m_tCoins), FONT_NAME, 30);
    m_tCoinCount->setPosition(Vec2(coincountBg->getContentSize().width/2,coincountBg->getContentSize().height/2));
    m_tCoinCount->setColor(Color3B::WHITE);
    coincountBg->addChild(m_tCoinCount,1);
    
    Sprite *coinShadow = Sprite::create("shadow.png");
    coinShadow->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    coinShadow->setPosition(Vec2(coinSpr->getContentSize().width/2,15));
    coinSpr->addChild(coinShadow,-1);

    t_miCollectCoinBtn = getButtonMade("medium_button.png","medium_button.png",CC_CALLBACK_1(BonusWordPopUp::collectButtonCallBack,this));
    t_miCollectCoinBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()+t_miCollectCoinBtn->getBoundingBox().size.height));
    t_miCollectCoinBtn->setTag(0);
    t_miCollectCoinBtn->setEnabled(false);
    t_miCollectCoinBtn->setOpacity(150);
    
    std::string t_CText = "Collect";
    Label *t_tCollectLabel = createLabelBasedOnLang(t_CText, 40);
    t_tCollectLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCollectLabel->setColor(Color3B::WHITE);
    t_tCollectLabel->setWidth(t_miCollectCoinBtn->getContentSize().width);
    t_tCollectLabel->setHeight(150);
    t_tCollectLabel->setName("Collect");
    t_tCollectLabel->enableOutline(Color4B(53,126,32,255),2);
    t_tCollectLabel->setPosition(Vec2(t_miCollectCoinBtn->getBoundingBox().size.width/2-10 ,t_miCollectCoinBtn->getBoundingBox().size.height/2+15.0f));
    t_miCollectCoinBtn->addChild(t_tCollectLabel,1);

    
    ProgressFromTo *t_ProgressfromTo = ProgressFromTo::create(1.5f, m_LevelProgressBar->getPercentage(),percentage);
    EaseSineInOut *t_EaseSineInOut = EaseSineInOut::create(t_ProgressfromTo);
    
    FiniteTimeAction *_call = NULL;
    if(percentage==100)
       _call = CallFuncN::create(CC_CALLBACK_0(BonusWordPopUp::enableCollectButton,this));
    
    m_LevelProgressBar->runAction(Sequence::create(t_EaseSineInOut,_call, NULL));

    
    // CLOSE BUTTON
      t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(BonusWordPopUp::OnButtonPressed,this));
      t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
      t_miCloseButton->setTag(0);

      Menu *m_Menu = Menu::create(t_miCloseButton,t_miCollectCoinBtn,NULL);
      m_Menu->setPosition(Vec2::ZERO);
      this->addChild(m_Menu,2);

}
void BonusWordPopUp::enableCollectButton()
{
    {
        t_miCollectCoinBtn->setEnabled(true);
        t_miCollectCoinBtn->setOpacity(255);
    }
}
Label *BonusWordPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr);
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

MenuItemSprite *BonusWordPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
      
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}
void BonusWordPopUp::OnButtonPressed(Ref *p_Sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
      
    int pButtonTag =  Button->getTag();
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"close";
          }break;
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}
void BonusWordPopUp::coinAddAnimation()
{
    Point  _endPos = Vec2(origin.x+visibleSize.width-40,origin.y+visibleSize.height)-m_ZeroPosition;
    Point _startPosOppo = Point(t_miCollectCoinBtn->getPositionX()-110,t_miCollectCoinBtn->getPositionY()+10);
    for (int i=0; i< 20; i++)
    {
        Sprite *_spr = Sprite::create("coins.png");
        _spr->setScale(_spr->getScale()+0.2);
        _spr->setPosition(_startPosOppo);
        _spr->setOpacity(0);
        ccBezierConfig bezier;
        bezier.controlPoint_1 = _startPosOppo;
        bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
        bezier.endPosition =_endPos;
        BezierTo *_bez = BezierTo::create(.5f, bezier);
        this->addChild(_spr,5);
        FadeTo *_fade = FadeTo::create(.04*i,255);
        FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(BonusWordPopUp::removeSprite,this));
        FiniteTimeAction*_seq = Sequence::create(_fade,_bez,_call,NULL);
        _spr->runAction(_seq);
    }
    if(t_miCloseButton)
    {
        t_miCloseButton->setEnabled(false);
    }
}

void BonusWordPopUp::removeSprite(Ref *_sender)
{
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);

    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
    
    if(t_miCloseButton)
    {
        t_miCloseButton->setEnabled(true);
    }
    
    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);

}

void BonusWordPopUp::collectButtonCallBack(Ref *sender)
{
    me_iCoinCount += m_tCoins;
    UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
    UserDefault::getInstance()->flush();
    
 
    t_miCollectCoinBtn->setEnabled(false);
    t_miCollectCoinBtn->setOpacity(150);

    StatsManager::getInstance()->updateLangBonusWordsCount(0);
    
    FiniteTimeAction *_call2 = CallFuncN::create(CC_CALLBACK_0(BonusWordPopUp::coinAddAnimation, this));
    DelayTime *_delay = DelayTime::create(1.8f);
    FiniteTimeAction *_call3 = CallFuncN::create(CC_CALLBACK_0(BonusWordPopUp::coinAnimationDoneClosePopUp, this));
    FiniteTimeAction *_seq  = Sequence::create(_call2,_delay,_call3, NULL);
    this->runAction(_seq);

}
void BonusWordPopUp::coinAnimationDoneClosePopUp()
{
    status = (char*)"close";
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
    
    GameController::getInstance()->showInterstial();

}
BonusWordPopUp::~BonusWordPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
