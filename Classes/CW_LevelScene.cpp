//
//  CW_LevelScene.cpp
//  wordPro-mobile
//
//  Created by id on 03/04/20.
//

#include "CW_LevelScene.h"
#include "GameConstants.h"
#include "GameController.h"
#include "MainMenuScene.h"
#include "LevelScene.h"
#include "StatsManager.h"
#include "LanguageTranslator.h"

CW_LevelScene::CW_LevelScene()
{
//     me_strScreenName = "LevelScreen";
     
     LayerColor *colorLayer = LayerColor::create(Color4B(0,0,0,255));
     this->addChild(colorLayer);

     m_ptrLayer = new CW_LevelLayer();
     m_ptrLayer->autorelease();
     this->addChild(m_ptrLayer);
}
CW_LevelScene::~CW_LevelScene()
{
    this->removeAllChildrenWithCleanup(true);
}
#pragma mark CHALLENGE_WORLD LAYER
CW_LevelLayer::CW_LevelLayer()
{
    m_ptrScrolView = NULL;
    m_BackButton = NULL;
    m_iCountriesCount = 0;
    
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    origin = ResolutionSize.origin;
    
    GameController::getInstance()->showBanner();

    auto KeyListener = EventListenerKeyboard::create();
    KeyListener->setEnabled(true);
    KeyListener->onKeyPressed = CC_CALLBACK_2(CW_LevelLayer::onKeyPressed,this);
    KeyListener->onKeyReleased = CC_CALLBACK_2(CW_LevelLayer::onKeyReleased,this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(KeyListener, this);
    
    LayerColor *colorLayer = LayerColor::create(Color4B(47, 52, 56,255));
    colorLayer->setOpacity(130);
    this->addChild(colorLayer,1);
    
    Sprite *m_SprSplashBg = Sprite::create("Splash-bg.jpg");
    m_SprSplashBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2));
    m_SprSplashBg->setScale(m_fScale);
    this->addChild(m_SprSplashBg);

//    AddAlphaBg();
    
    //Back Button
        m_BackButton = getButtonMade("more-games-button.png","more-games-button.png",CC_CALLBACK_1(CW_LevelLayer::backBtnCallback,this));
        m_BackButton->setScale(m_fScale); m_BackButton->setPosition(Vec2(5+origin.x+m_BackButton->getBoundingBox().size.width/2,origin.y+visibleSize.height-10-m_BackButton->getBoundingBox().size.height/2));

        Sprite *backIcon = Sprite::create("back.png");
        backIcon->setPosition(Vec2(m_BackButton->getContentSize().width/2,m_BackButton->getContentSize().height/2+15));
        m_BackButton->addChild(backIcon,1);

    CW_Sprite = Sprite::create("Challenge World.png");
    CW_Sprite->setScale(m_fScale);
    CW_Sprite->setPosition(Vec2(origin.x+visibleSize.width/2,m_BackButton->getBoundingBox().getMinY()+25-CW_Sprite->getBoundingBox().size.height/2));
    this->addChild(CW_Sprite,2);
    
    

    m_miUpButton =getButtonMade("up-arrow_Btn.png","up-arrow_Btn.png",CC_CALLBACK_1(CW_LevelLayer::ArrowBtnsCallback,this));
    m_miUpButton->setScale(m_fScale);
    m_miUpButton->setPosition(Vec2(origin.x+visibleSize.width/2,CW_Sprite->getBoundingBox().getMinY()-m_miUpButton->getBoundingBox().size.height/2));
    m_miUpButton->setName("TopArrow");
    
    // DOWN BUTTON
        m_miDownButton =getButtonMade("down-arrow_Btn.png","down-arrow_Btn.png",CC_CALLBACK_1(CW_LevelLayer::ArrowBtnsCallback,this));
        m_miDownButton->setScale(m_fScale);
        m_miDownButton->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+110+m_miDownButton->getBoundingBox().size.height/2));
        m_miDownButton->setName("DownArrow");

   
    
    Menu *backMenu = Menu::create(m_BackButton,m_miUpButton,m_miDownButton,NULL);
    backMenu->setPosition(Vec2::ZERO);
    this->addChild(backMenu,2);

    ParseLevelMapData();
    
    createChallengeLevelMap();
}
///  Create Level Map Scroll View
void CW_LevelLayer::createChallengeLevelMap()
{
    
       m_ptrScrolView = cocos2d::ui::ScrollView::create();
       m_ptrScrolView->setDirection(ui::ScrollView::Direction::VERTICAL);
       m_ptrScrolView->setTouchEnabled(true);
       m_ptrScrolView->setBounceEnabled(false);
       Size contentsize = Size(origin.x+visibleSize.width,m_miUpButton->getBoundingBox().getMinY()-20-m_miDownButton->getBoundingBox().getMaxY());
       m_ptrScrolView->setContentSize(Size(contentsize));
       m_ptrScrolView->setPosition(Vec2(origin.x+visibleSize.width/2,m_miUpButton->getBoundingBox().getMinY()-10-m_ptrScrolView->getBoundingBox().size.height/2));
       m_ptrScrolView->addEventListener(CC_CALLBACK_2(CW_LevelLayer::onScrolling,this));
       m_ptrScrolView->setScrollBarEnabled(false);
       m_ptrScrolView->setSwallowTouches(false);
       m_ptrScrolView->setAnchorPoint(Vec2(0.5f,0.5f));
       
 
       
       float totalNum = m_iCountriesCount;

    
        Sprite *path = Sprite::create("coins_popup_Bg.png");
        path->setScale(m_fScale);
        path->setScaleY(path->getScaleY()+0.6);
 
          float pathHeight = path->getBoundingBox().size.height+5;
          
          Size Containersize = Size(contentsize.width,pathHeight*(totalNum));
          m_ptrScrolView->setInnerContainerSize(Size(Containersize.width,Containersize.height));

          Point t_position = Vec2(m_ptrScrolView->getContentSize().width/2,Containersize.height-5);
       
       std::string curLang = UserDefault::getInstance()->getStringForKey(LANGUAGE);
       
    
    int unloclcount = StatsManager::getInstance()->getLangUnlockedCountryCount();
    
    if (unloclcount<m_CountriesArray.size()) {
        StatsManager::getInstance()->setNextUnlockCountry(m_CountriesArray.at(unloclcount));
    }
    
    std::vector<std::string> m_playedArray;
    for(int i = 1; i<unloclcount; i++)
    {
        m_playedArray.push_back(m_CountriesArray.at(i-1));
    }
    StatsManager::getInstance()->setplayedCountriesArray(m_playedArray);
    
       for(int index = 1; index<=totalNum; index++)
       {
           
           Layout *layout = Layout::create();
           layout->setColor(Color3B::WHITE);
           layout->setContentSize(Size(path->getBoundingBox().size.width, path->getBoundingBox().size.height));
//           layout->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::SOLID);
           layout->setAnchorPoint(Vec2(0.5f,0.5f));
           layout->setPosition(Vec2(t_position.x,t_position.y-layout->getBoundingBox().size.height/2));
           m_ptrScrolView->addChild(layout,1);

           
           std::string str = m_CountriesArray.at(index-1);

           
           Sprite *t_ImageView = Sprite::create("coins_popup_Bg.png");
           if (index>unloclcount) {
               t_ImageView = Sprite::create("popup-box_black.png");
           }
           t_ImageView->setScale(m_fScale);
           t_ImageView->setScaleX(t_ImageView->getScaleX()+0.05);
           t_ImageView->setScaleY(t_ImageView->getScaleY()+0.6);
           t_ImageView->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
           layout->addChild(t_ImageView);
           
           
           std::string placestr ="Countries/"+m_CountriesArray.at(index-1);

           Sprite *t_ImagePlace = Sprite::create(placestr.append(".png"));
           if (t_ImagePlace==NULL) {
               t_ImagePlace = Sprite::create("Countries/India.png");
           }
           float hieght = t_ImageView->getBoundingBox().size.height-40;
           float scale = hieght/t_ImagePlace->getBoundingBox().size.height;
           t_ImagePlace->setScale(scale);
           t_ImagePlace->setPosition(Vec2(t_ImageView->getBoundingBox().getMaxX()-20-t_ImagePlace->getBoundingBox().size.width/2,t_ImageView->getBoundingBox().getMidY()+10));
           layout->addChild(t_ImagePlace,1);


           
           
           if (index>unloclcount)
           {
               Sprite *sprLock = Sprite::create("lock-image.png");
             sprLock->setScale(m_fScale);
             sprLock->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
             sprLock->setPosition(Vec2(t_ImageView->getBoundingBox().getMinX()+sprLock->getBoundingBox().size.width,t_ImageView->getBoundingBox().getMidY()));
             layout->addChild(sprLock,1);
               
               std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(str);
               Label * m_tCountry= NULL;
               if(me_Language.compare(LANG_ENGLISH)==0) {
                   std::transform(langstr.begin(), langstr.end(), langstr.begin(), ::toupper);
                   m_tCountry = Label::createWithTTF(langstr,FONT_NAME,45);
               }else{
                   m_tCountry = Label::createWithSystemFont(langstr,SYS_FONT_NAME, 45);
               }
               m_tCountry->setWidth(m_tCountry->getBoundingBox().size.width+10);
               m_tCountry->setHeight(m_tCountry->getBoundingBox().size.height);;
               m_tCountry->setColor(Color3B::WHITE);
               m_tCountry->enableOutline(Color4B(0,0,0,255),2);
               m_tCountry->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
                m_tCountry->setPosition(Vec2(sprLock->getBoundingBox().getMidX(),t_ImageView->getBoundingBox().getMidY()+m_tCountry->getContentSize().height/2));
               layout->addChild(m_tCountry,2);

               
               float diff = (t_ImageView->getBoundingBox().getMinX()+10);

               float diff1 = (m_tCountry->getBoundingBox().getMinX());

               if(m_tCountry->getBoundingBox().getMinX()<(t_ImageView->getBoundingBox().getMinX()+10))
               {
                   log("less");
                   float diff = t_ImageView->getBoundingBox().getMinX() - (m_tCountry->getBoundingBox().getMinX()-10);
                   m_tCountry->setPositionX(m_tCountry->getPositionX()+diff+10);
               }
           }
            else
            {
                              
               std::string placeName = m_CountriesArray.at(index-1);
               ui::Button *Playbtn = Button::create("Buy_button.png","Buy_button.png");
               Playbtn->setScale(m_fScale);
               Playbtn->addClickEventListener(CC_CALLBACK_1(CW_LevelLayer::levelMapBtnCallBack, this));
               Playbtn->setPosition(Vec2(t_ImageView->getBoundingBox().getMinX()+Playbtn->getBoundingBox().size.width-20,t_ImageView->getBoundingBox().getMinY()+Playbtn->getBoundingBox().size.height*0.8));
               layout->addChild(Playbtn,2);
               Playbtn->setName(placeName);
               Playbtn->setTag(index-1);
                                
               Label *play_label = createLabelBasedOnLang("Play",42);
               play_label->setColor(Color3B::WHITE);
               play_label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
               play_label->setWidth(Playbtn->getContentSize().width-20);
               play_label->setHeight(150);
               play_label->enableOutline(Color4B(53,126,32,255),2);
               play_label->setPosition(Vec2(Playbtn->getContentSize().width/2,Playbtn->getContentSize().height/2+10.0f));
               Playbtn->addChild(play_label,1);
               
                if(me_Language.compare(LANG_MALAYALAM)==0)
                {
                    play_label->setSystemFontSize(30);
                }else if (me_Language.compare(LANG_TAMIL)==0)
                {
                   play_label->setSystemFontSize(27);
                }
                
                
                std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(str);
                Label * m_tCountry= NULL;
                if(me_Language.compare(LANG_ENGLISH)==0) {
                    std::transform(langstr.begin(), langstr.end(), langstr.begin(), ::toupper);
                    m_tCountry = Label::createWithTTF(langstr,FONT_NAME,45);
                }else{
                    m_tCountry = Label::createWithSystemFont(langstr,SYS_FONT_NAME, 45);
                }
                m_tCountry->setWidth(m_tCountry->getBoundingBox().size.width+10);
                m_tCountry->setHeight(m_tCountry->getBoundingBox().size.height+10);
                m_tCountry->setColor(Color3B::WHITE);
                m_tCountry->enableOutline(Color4B(0,0,0,255),2);
                m_tCountry->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
                m_tCountry->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
                  m_tCountry->setPosition(Vec2(t_ImageView->getBoundingBox().getMinX()+30,Playbtn->getBoundingBox().getMaxY() +m_tCountry->getContentSize().height/2));
                layout->addChild(m_tCountry,2);

           }
           t_position.y -= layout->getBoundingBox().size.height+5;
       }

       
 
     this->addChild(m_ptrScrolView,2);
    
    DelayTime *delay = DelayTime::create(0.0f);
    CallFuncN *callfunc = CallFuncN::create(std::bind(&CW_LevelLayer::checkForCurrentCountryScroll,this));
    this->runAction(Sequence::create(delay,callfunc,NULL));
    
}
/// Creates Label Based on current selected language
/// @param labelStr String tag for translation
/// @param fontsize font size current label string
/// @return returns label
Label *CW_LevelLayer::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
/// this is to update the scroll to current country unlocked
void CW_LevelLayer::checkForCurrentCountryScroll()
{
    
    int unloclcount = StatsManager::getInstance()->getLangUnlockedCountryCount();

    
    float val = (float)(float(unloclcount-1)/(11))*100;

    if(unloclcount>1)
    {
        m_ptrScrolView->scrollToPercentVertical(val,0.0f,true);
        
        if(unloclcount>=9)
        {
            m_ptrScrolView->scrollToPercentVertical(100.0f,0.0f,true);

        }
    }
    
    
    float percentage = val;
     if(percentage<=1.0f)
     {
         m_miUpButton->setEnabled(false);
         m_miDownButton->setEnabled(true);
     }
    else if(percentage>=10.0f)
     {
         m_miUpButton->setEnabled(true);
         m_miDownButton->setEnabled(true);
         
         if (percentage>=85)
         {
             m_miUpButton->setEnabled(true);
             m_miDownButton->setEnabled(false);
         }
    }

    
}
void CW_LevelLayer::AddAlphaBg()
{
     float xscale = visibleSize.width/64.0f;
     float yscale = visibleSize.height/64.0f;
     Sprite *m_SprBgAlpha = Sprite::create("pixel.png");
     m_SprBgAlpha->setScale(xscale,yscale);
     m_SprBgAlpha->setColor(Color3B(47, 52, 56)); //setColor(Color3B::BLACK);
     m_SprBgAlpha->setOpacity(130);
     m_SprBgAlpha->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2));
     this->addChild(m_SprBgAlpha,1);
}
MenuItemSprite *CW_LevelLayer::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);

    Sprite *_disableSpr = Sprite::create(normalSpr);
    _disableSpr->setOpacity(150);

    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr,_disableSpr,func);
    return Button;
}
void CW_LevelLayer::backBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    Director::getInstance()->purgeCachedData();
    MainMenuScene *scene = new MainMenuScene();
    scene->autorelease();
    Director::getInstance()->replaceScene(TransitionSlideInL::create(0.4f,scene));
    scene = NULL;
    GameController::getInstance()->playSFX(SFX_SCREEN_MOVE);

}
/// onclick of play button
void CW_LevelLayer::levelMapBtnCallBack(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    Button *levelBtn = (Button*)sender;
    std::string countryname = levelBtn->getName();
    
    int index = levelBtn->getTag();
    StatsManager::getInstance()->setcurrentCountryindex(index);
    
    StatsManager::getInstance()->setCurrentCountry(countryname);
    
    LevelScene *scene = new LevelScene();
    scene->autorelease();
    Director::getInstance()->replaceScene(TransitionSlideInR::create(0.4f,scene));
    scene = NULL;

}
void CW_LevelLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* unused_event)
{
    
    
}
void CW_LevelLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* unused_event)
{
    if(keyCode==EventKeyboard::KeyCode::KEY_BACK)
    {
        backBtnCallback(NULL);
    }
}

void CW_LevelLayer::onScrolling(Ref* sender, ui::ScrollView::EventType type)
{
    if (type == ui::ScrollView::EventType::SCROLLING_ENDED)
     {
         printf("\nScroll_percent-->%f\n",m_ptrScrolView->getScrolledPercentVertical());
         
         float percent = m_ptrScrolView->getScrolledPercentVertical();
         
         
         
         if(percent<=5.0f)
         {
             m_miUpButton->setEnabled(false);
             m_miDownButton->setEnabled(true);
         }
        else if(percent<=70.0f)
         {
             m_miUpButton->setEnabled(true);
             m_miDownButton->setEnabled(true);
         }
         else if (percent>=85)
         {
             m_miDownButton->setEnabled(false);
             m_miUpButton->setEnabled(true);
         }
     }
    
}
/// arrow buttons callback to move up and down for scroll
void CW_LevelLayer::ArrowBtnsCallback(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    MenuItemSprite *arrowBtn = (MenuItemSprite*) p_Sender;
    std::string btnName = arrowBtn->getName();
    
    if(btnName.compare("TopArrow")==0)
    {
        m_ptrScrolView->scrollToTop(1.0f, true);
        arrowBtn->setEnabled(false);
        
        m_miDownButton->setEnabled(true);
    }
    else if (btnName.compare("DownArrow")==0)
    {
      m_ptrScrolView->scrollToBottom(1.0f, true);
      arrowBtn->setEnabled(false);

      m_miUpButton->setEnabled(true);
    }
}
/// parse Json level map data
void CW_LevelLayer::ParseLevelMapData()
{
    
    std::string country = "India";
    std::string t_fullPath = FileUtils::getInstance()->fullPathForFilename("Regions_Data.json");
     
     char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str()) + sizeof(int) + 1];
     strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str());
     
     rapidjson::Document t_Regiondoc;
     t_Regiondoc.Parse<kParseStopWhenDoneFlag>(p_Buffer);
     
     if(t_Regiondoc.HasParseError()) // Print parse error
     {
         CCLOG("GetParseError %u\n",t_Regiondoc.GetParseError());
        delete [] p_Buffer;
        p_Buffer = NULL;
        
        return;
     }
     std::vector<std::string> t_StatesArray;
     if(!t_Regiondoc.IsNull())
     {
         if(t_Regiondoc.HasMember("Countries"))
         {
             if(t_Regiondoc["Countries"].IsArray())
             {
                 rapidjson::SizeType size = t_Regiondoc["Countries"].Size();
                 
                 m_iCountriesCount = (int)size;
                 
                 for(int index = 0; index<size; index++)
                 {
                     std::string ContName = t_Regiondoc["Countries"][index].GetString();
                     m_CountriesArray.push_back(ContName);
                 }
             }
         }
         if(t_Regiondoc.HasMember("Cntry_States"))
         {
             for(int index = 0; index<m_CountriesArray.size(); index++)
             {
                 std::string country = m_CountriesArray.at(index);

                 int countrylevelsize = (int)t_Regiondoc["Cntry_States"][country.c_str()].Size();
                 
                 if(country=="India")
                 {
                     countrylevelsize = countrylevelsize*5;
                 }
                 m_CountrieslevelCntMap[country] = countrylevelsize;
             }
         }
     }
    
    std::string strtcntry = getLangBasedStartingCountry();
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        auto itr =  std::find(m_CountriesArray.begin(),m_CountriesArray.end(), strtcntry);
        m_CountriesArray.erase(itr);
        
        m_CountriesArray.push_back(strtcntry);
    }
    
    printf("\n StatesCount = %d\n",m_iCountriesCount);
    printf("\n StatesArrCount = %lu\n",m_CountriesArray.size());
    
    
}
/// This will return The Starting Country According to language
/// @return returns  country name string
std::string CW_LevelLayer::getLangBasedStartingCountry()
{
    std::string t_StrPlace = "India";
      
      if(me_Language.compare(LANG_ENGLISH)==0)
      {
         t_StrPlace = "India";
      }else{
          t_StrPlace = "India";
      }
    return t_StrPlace;
     if(me_Language.compare(LANG_KANNADA)==0)
      {
          t_StrPlace = "Karnataka";
      }
      else if(me_Language.compare(LANG_HINDI)==0)
      {
          t_StrPlace = "Delhi";
      }
      else if(me_Language.compare(LANG_TELUGU)==0)
      {
          t_StrPlace = "";
      }
      else if(me_Language.compare(LANG_MALAYALAM)==0)
      {
          t_StrPlace = "Kerala";
      }
      else if(me_Language.compare(LANG_MARATHI)==0)
      {
          t_StrPlace = "Maharastra";
      }
      else if(me_Language.compare(LANG_ODIA)==0)
      {
          t_StrPlace = "Orissa";
      }
      else if(me_Language.compare(LANG_TAMIL)==0)
      {
          t_StrPlace = "Tamil Nadu";
      }
      else if(me_Language.compare(LANG_BANGLA)==0)
      {
          t_StrPlace = "WestBengal";
      }
      else if(me_Language.compare(LANG_GUJARATI)==0)
      {
          t_StrPlace = "Gujarat";
      }
      return t_StrPlace;
}
CW_LevelLayer::~CW_LevelLayer()
{
    if(m_ptrScrolView)
    {
        m_ptrScrolView->removeAllChildren();
        this->removeChild(m_ptrScrolView,true);
        m_ptrScrolView = NULL;
    }
    
    if(m_CountriesArray.size()>0)
    {
        m_CountriesArray.clear();
    }

    this->removeAllChildrenWithCleanup(true);
}
