//
//  AchievementManager.hpp
//  wordPro-mobile
//
//  Created by id on 20/04/20.
//

#ifndef AchievementManager_h
#define AchievementManager_h

#include <stdio.h>
#include "cocos2d.h"
#include "AchievementObserver.h"


using namespace cocos2d;
class AchievementManager
{
        // CONSTRUCTOR
        AchievementManager();
        /**
         *  The singleton pointer of AchievementManager.
         */
        static AchievementManager* m_PtrAchievementManager;
    
        
        std::vector<AchievementObserver*> m_ObserverArray;
        std::vector<AchievementObserver*> m_CompletedObserverArray;
        rapidjson::Document m_JsonDocument;
    
        std::string m_AchievementName;
        int m_iAchievementTag;
    
    public:
        /**
         *  Gets the instance of AchievementManager.
         */
        static AchievementManager* getInstance();
    
        // DESTRUCTOR
        virtual ~AchievementManager();
    
        // THIS INITIALIZE ACHIEVEMENT LIST
        void LoadAchievementJson();
    
        // CREATE ACHIEVEMENT TOASTS WITH ACHIEVEMENT NAME
        void createAchievementToast(const std::string &p_AchievementName);
        
        // THIS UPDATE ACHIEVEMENT
        void OnAchievementUpdate(const std::string &p_Type, int p_iCount);
    
        // THIS SAVE ACHIEVEMENT JSON
        void SaveAchievements();
        
        // THIS CLEAR ALL CURRENT ACHIEVEMENTS
        void ClearAllCurrentAchievements();
        
        // ON GAME STATE CHANGED
        void GameStateChanged();
        
        // ON APPLICATION QUIT
        void OnApplicationQuit();
    
        // THIS CHECK AND UNLOCK ACHIEVEMNET
        void CheckAndUnlockAchievement(const std::string &p_Name);
    
        // RETURN ACHIEVEMENT TAG
        int getAchievementTag();
    
        // RETURN ACHIEVEMENT NAME
        std::string getLastUnlockAchievementName();
    
        // THIS CHECK ALL ACHIEVEMENTS COMPLETED OR NOT
        bool CheckAllAchievementsComplete();
    
        // THIS RETURN ACHIEVEMENTS ARRAY
        std::vector<AchievementObserver*> GetAchievements();
    
    
        int getCompletedAchievementsCount();
    
        bool checkIsAchievementClaimed(int index);
    
        void updateAchievementClaimed(int index);
    
        int getCountOfAchievementsToClaim();
    
        bool checkAllAchievementsClaimed();

};
#endif /* AchievementManager_hpp */
