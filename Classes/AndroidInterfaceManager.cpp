//
//  AndroidInterfaceManager.cpp
//  wordPro-mobile
//
//  Created by id on 31/03/20.
//

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include "AndroidInterfaceManager.h"
#include "InterfaceManagerInstance.h"
#include "GameConstants.h"
#include "GameController.h"

using namespace std;

#define  CLASS_NAME_FIREBASE "org/cocos2dx/cpp/Firebase/FirebaseDatabaseService"


AndroidInterfaceManager::AndroidInterfaceManager()
{

}
void AndroidInterfaceManager::OpenUrl(std::string url)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz,"openUrl","(Ljava/lang/String;)V");
    jstring jurl = JniHelper::getEnv()->NewStringUTF(url.c_str());
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID, jurl);
}
void AndroidInterfaceManager::OpenInStore()
{
    CallVoidMethod((char *) "OpenInStore");
}
void AndroidInterfaceManager::gameFeedBack()
{
     CallVoidMethod((char *) "gameFeedBack");
}
void AndroidInterfaceManager::ShareGame()
{
     CallVoidMethod((char *) "shareGame");
}
void AndroidInterfaceManager::ShowToastMessage(const std::string &p_Message)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "showToastMessage","(Ljava/lang/String;)V");
    jstring jmsg = JniHelper::getEnv()->NewStringUTF(p_Message.c_str());
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID, jmsg);
}
bool AndroidInterfaceManager::appInstalledOrNot(const std::string &package)
{

    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "appInstalledOrNot", "(Ljava/lang/String;)Z");
    jstring jpackage = JniHelper::getEnv()->NewStringUTF(package.c_str());
    return JniHelper::getEnv()->CallBooleanMethod(JniHelper::getActivity(),methodID,jpackage);
}
std::string AndroidInterfaceManager::getCurrencySymbol(const std::string &p_CurrCode)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "getCurrencySymbol", "(Ljava/lang/String;)Ljava/lang/String;");
    jstring JCurrCode = JniHelper::getEnv()->NewStringUTF(p_CurrCode.c_str());
    jstring jstrRet = (jstring) JniHelper::getEnv()->CallObjectMethod(JniHelper::getActivity(),methodID,JCurrCode);
    return JniHelper::jstring2string(jstrRet);
}
void AndroidInterfaceManager::CallVoidMethod(char *method)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, method, "()V");
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID);
}
void AndroidInterfaceManager::PostFirebaseAnalyticsEvent(const std::string &eventName,const std::string &param1,const std::string &paramVal_1,const std::string &param2,const std::string &paramVal_2)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "postFirebaseAnalyticsEvent","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

    jstring jEvent = JniHelper::getEnv()->NewStringUTF(eventName.c_str());
    jstring jParam1 = JniHelper::getEnv()->NewStringUTF(param1.c_str());
    jstring jparamVal_1 = JniHelper::getEnv()->NewStringUTF(paramVal_1.c_str());
    jstring jParam2 = JniHelper::getEnv()->NewStringUTF(param2.c_str());
    jstring jparamVal_2 = JniHelper::getEnv()->NewStringUTF(paramVal_2.c_str());
    
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID, jEvent, jParam1,jparamVal_1, jParam2,jparamVal_2);
}
void AndroidInterfaceManager::PostFirebaseAnalyticsScreen(const std::string &p_ScreenName)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "postFirebaseAnalyticsScreen","(Ljava/lang/String;)V");
    jstring jScreenName = JniHelper::getEnv()->NewStringUTF(p_ScreenName.c_str());
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID, jScreenName);
}

bool AndroidInterfaceManager::isNetworkAvailable()
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "isNetworkAvailable", "()Z");
    return JniHelper::getEnv()->CallBooleanMethod(JniHelper::getActivity(), methodID);
}

void AndroidInterfaceManager::openFbInviteDialog()
{
    CallVoidMethod((char *) "openFBInviteDialog");
}
void AndroidInterfaceManager::loginWithFaceBook()
{
    CallVoidMethod((char *) "loginWithFaceBook");
}
bool AndroidInterfaceManager::isFacebookLoggedIn()
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "isFacebookLoggedIn", "()Z");
    return JniHelper::getEnv()->CallBooleanMethod(JniHelper::getActivity(), methodID);
}
void AndroidInterfaceManager::getLeaderBoardJson(const std::string &language)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz, "getLeaderBoardjson","(Ljava/lang/String;)V");
    jstring jId = JniHelper::getEnv()->NewStringUTF(language.c_str());
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID, jId);

}

void AndroidInterfaceManager::pushDataToUserdata(const std::string &fbId,const std::string &username,const std::string &profileUrl,const std::string &language,int level,int coins)
{
    log("android_interface");
    JniMethodInfo t;

    if(JniHelper::getStaticMethodInfo(t,CLASS_NAME_FIREBASE, "pushFirebaseLeaderBoardData", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(fbId.c_str());
        jstring stringArg2 = t.env->NewStringUTF(username.c_str());
        jstring stringArg3 = t.env->NewStringUTF(profileUrl.c_str());
        jstring stringArg4 = t.env->NewStringUTF(language.c_str());

        t.env->CallStaticVoidMethod(t.classID,t.methodID,stringArg1,stringArg2,stringArg3,stringArg4,level,coins);
        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(stringArg2);
        t.env->DeleteLocalRef(stringArg3);
        t.env->DeleteLocalRef(stringArg4);
    }
    t.env->DeleteLocalRef(t.classID);
}
void AndroidInterfaceManager::pushFirebaseUserLanguageData(const std::string &fbId,const std::string &language,int currentLevel,int coins)
{
    JniMethodInfo t;
    if(JniHelper::getStaticMethodInfo(t,CLASS_NAME_FIREBASE,"pushFirebaseUserLanguageData","(Ljava/lang/String;Ljava/lang/String;II)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(fbId.c_str());
        jstring stringArg2 = t.env->NewStringUTF(language.c_str());

        t.env->CallStaticVoidMethod(t.classID,t.methodID,stringArg1,stringArg2,currentLevel,coins);
        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(stringArg2);
    }
    t.env->DeleteLocalRef(t.classID);

}
void AndroidInterfaceManager::getUserLanguageDataJson(const std::string &fbId)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz,"getUserLanguageDataJson","(Ljava/lang/String;)V");
    jstring jfbId = JniHelper::getEnv()->NewStringUTF(fbId.c_str());
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID,jfbId);
}
void AndroidInterfaceManager::getLanguageDataUserIds()
{
    CallVoidMethod((char *) "getLanguageDataUserIds");
}
void AndroidInterfaceManager::checkForGameUpdate()
{
    CallVoidMethod((char *) "checkforAppUpdate");
}
void AndroidInterfaceManager::initialize()
{
    CallVoidMethod((char *) "initialize");
}
void AndroidInterfaceManager::setUserFbId(const std::string &fbId)
{
    JniMethodInfo t;
    if(JniHelper::getStaticMethodInfo(t,CLASS_NAME_FIREBASE,"saveUserFbId","(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(fbId.c_str());
        t.env->CallStaticVoidMethod(t.classID,t.methodID,stringArg1);
        t.env->DeleteLocalRef(stringArg1);
    }
    t.env->DeleteLocalRef(t.classID);
}
void AndroidInterfaceManager::getUserRank(const std::string &language)
{
    jclass clazz = JniHelper::getEnv()->GetObjectClass(JniHelper::getActivity());
    jmethodID methodID = JniHelper::getEnv()->GetMethodID(clazz,"getCurrentUserRank","(Ljava/lang/String;)V");
    jstring jfbId = JniHelper::getEnv()->NewStringUTF(language.c_str());
    JniHelper::getEnv()->CallVoidMethod(JniHelper::getActivity(), methodID,jfbId);
}
AndroidInterfaceManager::~AndroidInterfaceManager()
{

}
extern "C"
{
void Java_org_cocos2dx_cpp_AppActivity_onFaceBookLoginSuccess(JNIEnv *env, jobject thiz,int result)
{

    log("Android login success");
    cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
          if(result == 1)
          {
              Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FB_LOGIN_SUCCESS);
        }else{
            Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(FB_LOGIN_FAILURE);
        }
    });
}

void Java_org_cocos2dx_cpp_NativeCall_callUserAction(JNIEnv *env, jclass thiz,int actiontype)
{
    UserDefault::getInstance()->setBoolForKey("Notification", true);
    UserDefault::getInstance()->setIntegerForKey("openNotification", actiontype);
}
void Java_org_cocos2dx_cpp_NativeCall_setNotificationGiftType(JNIEnv *env, jclass thiz,jstring type,int result)
{

}
void Java_org_cocos2dx_cpp_NativeCall_setUserPredictionType(JNIEnv *env, jclass thiz,int actiontype,int coins)
{
    UserDefault::getInstance()->setBoolForKey("UserPredict", true);

    UserDefault::getInstance()->setIntegerForKey(USER_PREDICT_TYPE,actiontype);
    UserDefault::getInstance()->setIntegerForKey(PREDICTION_COINS,coins);
}
void Java_org_cocos2dx_cpp_Firebase_FirebaseDatabaseService_onleaderBoardJsonSuccess(JNIEnv *env, jobject thiz,jstring jsonString,jint userIndex)
{

    const char *jsonText = env->GetStringUTFChars(jsonString, 0);

    //Do something with the nativeString

    int length = strlen(jsonText);
    char buf[length + 1];
    strcpy(buf, jsonText);


    me_strLeaderBoardJson = buf;

    UserDefault::getInstance()->setStringForKey("leaderboardJson",me_strLeaderBoardJson.c_str());
    
    me_strLeaderBoardJson = UserDefault::getInstance()->getStringForKey("leaderboardJson","");

    log("android_interface_1->%s",me_strLeaderBoardJson.c_str());

    UserDefault::getInstance()->setIntegerForKey("UserIndex",userIndex);

    env->ReleaseStringUTFChars(jsonString, jsonText);


}
void Java_org_cocos2dx_cpp_Firebase_FirebaseDatabaseService_onlanguageDataUserIdsSuccess(JNIEnv *env, jobject thiz,jstring jsonString)
{

    const char *jsonText = env->GetStringUTFChars(jsonString, 0);

    //Do something with the nativeString

    int length = strlen(jsonText);
    char buf[length + 1];
    strcpy(buf, jsonText);

    me_strUserIdsJson = buf;

    GameController::getInstance()->setStringForKey("langDataUserIds",me_strUserIdsJson.c_str());
    
    me_strUserIdsJson = GameController::getInstance()->getStringForKey("langDataUserIds","");

    GameController::getInstance()->parseLanguageDataUserIds(me_strUserIdsJson);

    env->ReleaseStringUTFChars(jsonString, jsonText);
    



}
void Java_org_cocos2dx_cpp_Firebase_FirebaseDatabaseService_onUserLanguageDataSuccess(JNIEnv *env, jobject thiz,jstring jsonString)
{

    const char *jsonText = env->GetStringUTFChars(jsonString, 0);

    //Do something with the nativeString

    int length = strlen(jsonText);
    char buf[length + 1];
    strcpy(buf, jsonText);

    me_strUserDataJson = buf;

    log("android_interface_3->%s",me_strUserDataJson.c_str());
    
    GameController::getInstance()->setStringForKey("UserlangData",me_strUserDataJson.c_str());
    
    GameController::getInstance()->updateUserDataInGame(me_strUserDataJson);

    env->ReleaseStringUTFChars(jsonString, jsonText);


}


}
#endif
