//
//  LanguagePopUp.hpp
//  wordPro-mobile
//
//  Created by id on 11/03/20.
//

#ifndef LanguagePopUp_hpp
#define LanguagePopUp_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "PopUp.h"
#include "ui/UIListView.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;

using namespace cocos2d;

class LanguagePopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    Menu *t_miLanguageMenu;
    MenuItemSprite *t_miUpButton,*t_miDownButton;
    ScrollView *m_ptrScrolView;
    
    std::string t_strLanguage = "";
     std::vector<std::string> LangStrArr = {LANG_ENGLISH,LANG_HINDI,LANG_KANNADA,LANG_TELUGU,LANG_TAMIL,LANG_MALAYALAM,LANG_MARATHI,LANG_BANGLA,LANG_ODIA,LANG_GUJARATI};
//    std::string LangStrArr[10] = {LANG_ENGLISH,"हिंदी","ಕನ್ನಡ","తెలుగు","தமிழ்","മലയാളം","मराठी","বাংলা","ଓଡ଼ିଆ","ગુજરાતી"};//
public:
    LanguagePopUp(std::function<void()> func);
    ~LanguagePopUp();
    
    /// BUTTON PRESSED FUNCTIONS RETURN CALL BACK TO RESPECTIVE LAYER FROM IT CALLED
    void OnButtonPressed(Ref *p_Sender);
    
    void addLanguageButton(Point Pos,const std::string &name);
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void languageBtnCallback(Ref *p_Sender);
    
    void arrowbtnpressed(Ref *sender);
    
    void onScrolling(Ref* sender, ui::ScrollView::EventType type);
    
    std::string getLanguageString(int tag);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};

#endif /* LanguagePopUp_hpp */
