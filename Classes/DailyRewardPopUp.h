//
//  DailyRewardPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 24/03/20.
//

#ifndef DailyRewardPopUp_h
#define DailyRewardPopUp_h

#include <stdio.h>
#include "PopUp.h"

enum RewardDay
{
    REWARD_DAY_NONE,
    DAY_1_REWARD,
    DAY_2_REWARD,
    DAY_3_REWARD,
    DAY_4_REWARD,
    DAY_5_REWARD,
    DAY_6_REWARD,
    DAY_7_REWARD,
};
 class DailyRewardPopUp:public PopUp
{
    
    RewardDay m_eRewardDay;
    Menu *m_mClaimMenu;
    int   m_iCoinCount;
    
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    MenuItemSprite *t_miDoubleCoinBtn,*t_miCollectCoinBtn;
    MenuItemSprite *t_miCloseButton;
    
    Vector<Sprite*> *BonusItemArray;
    
    int rewardArr[7] = {10,20,30,40,50,80,100};
    
    Point Pos;
    bool m_bVideoSuccess = false;
public:
     
    DailyRewardPopUp(std::function<void()> func);
    ~DailyRewardPopUp();

    void createDailyBonusItems(Point pos);
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);
    
    /// THIS UPDATE BONUS CYCLE
    void UpdateDailyBonusCycle();
    
    /// THIS RETURN BONUS DAY TYPE
    void UpdateBonusDay();
    
    /// ON COLLECT BUTTON PRESSED
    void OnCollectButtonPressed();
    
    void checkForDailyReward();
    
    void updateRewardClaimedItems();
    
    void registerVideoEvent();
    void doublecoinBtnCallback();

    void onVideoAdSuccess();
    
    void onVideoAdFailure();
    
    void videoAdsAvailble();

    // COIN COLLECT ANIMATION
    void coinAddAnimation();
    
    void removeSprite(Ref *_sender);
    
    void coinAnimationDoneClosePopUp();
    
    void setEnableButtons(bool p_Enable);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};
#endif /* DailyRewardPopUp_hpp */
