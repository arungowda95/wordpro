//
//  DailyMissionObserver.cpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#include "DailyMissionObserver.h"
#include "DailyMissionManager.h"
#pragma mark  OBSERVER
//+++++++++++++++++++++++++++++++++++ OBSERVER ++++++++++++++++++++++++++++++++++++++++//

//CONSTRUCTOR
DailyMissionObserver::DailyMissionObserver(const std::string &p_JsonBuffer)
{
    m_bCompleted = false;
    m_JsonDocument.Parse<kParseStopWhenDoneFlag>(p_JsonBuffer.c_str());
    
    if(m_JsonDocument.HasMember("Name"))
    {
        m_MissionName = m_JsonDocument["Name"].GetString();
    }
    
    if(m_JsonDocument.HasMember("KeyTag"))
    {
        m_KeyId = m_JsonDocument["KeyTag"].GetString();
    }
    
    if(m_JsonDocument.HasMember("CurrCount"))
    {
        m_iCurCount = m_JsonDocument["CurrCount"].GetInt();
    }
    
    if(m_JsonDocument.HasMember("Target"))
    {
        m_iTargetCount = m_JsonDocument["Target"].GetInt();
    }

    if(m_JsonDocument.HasMember("Reward"))
    {
        m_iReward = m_JsonDocument["Reward"].GetInt();
    }
    
    if(m_JsonDocument.HasMember("RewardClaimed"))
    {
        m_bRewardClaimed = m_JsonDocument["RewardClaimed"].GetBool();
    }
    
    if(m_JsonDocument.HasMember("Status"))
    {
        m_bCompleted = m_JsonDocument["Status"].GetBool();
    }    
}
// ON GAME OVER
void DailyMissionObserver::OnGameOver()
{
    
}
// CHECK CONDITION SATISFIED OR NOT
bool DailyMissionObserver::ConditionSatisfied()
{
    
    if(m_iCurCount >= m_iTargetCount)
    return true;
    else
    return  false;
}
// THIS UPDATE MISSION
void DailyMissionObserver::UpdateMission(const std::string &p_Type, int p_iCount)
{
      if(m_KeyId.compare(p_Type) == 0)
      {
          m_iCurCount += p_iCount;
      }
}

// CHECK MISSION COMPLETED OR NOT
bool DailyMissionObserver::IsCompleted()
{
    if(!m_bCompleted)
    {
        bool approveConditions = true;
        
        
        if(ConditionSatisfied() == false)
        {
            approveConditions = false;
        }
        if(approveConditions)
        {
            m_bCompleted = true;
            DailyMissionManager::getInstance()->CheckAndUnlockDailyMission(m_MissionName);
        }


    }
    return m_bCompleted;
}
// RETURN MISSION JSON DATA
rapidjson::Value DailyMissionObserver::GetJsonData(rapidjson::Document::AllocatorType& allocator)
{
    rapidjson::Value objValue;
    objValue.SetObject();
    //rapidjson::Document::AllocatorType& allocator = objValue.GetAllocator();
    
    objValue.AddMember("Name", rapidjson::Value().SetString(m_MissionName.c_str(), allocator), allocator);


    objValue.AddMember("CurrCount", rapidjson::Value().SetInt(m_iCurCount), allocator);

    objValue.AddMember("KeyTag", rapidjson::Value().SetString(m_KeyId.c_str(),allocator), allocator);

    objValue.AddMember("Target", rapidjson::Value().SetInt(m_iTargetCount), allocator);
        
    objValue.AddMember("Reward", rapidjson::Value().SetInt(m_iReward), allocator);

    objValue.AddMember("RewardClaimed", rapidjson::Value().SetBool(m_bRewardClaimed), allocator);

    objValue.AddMember("Status", rapidjson::Value().SetBool(m_bCompleted), allocator);

    return objValue;
}
void DailyMissionObserver::updateRewardClaimed()
{
    m_bRewardClaimed = true;
}
bool DailyMissionObserver::getIsRewardClaimed()
{
    return m_bRewardClaimed;
}
// RETURN MISSION NAME CONDITION
std::string DailyMissionObserver::getDailyMissionName()
{
    return m_MissionName;
}
// DESTRUCTOR
DailyMissionObserver::~DailyMissionObserver()
{

}
