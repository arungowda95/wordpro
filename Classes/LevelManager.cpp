//
//  LevelManager.cpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#include "LevelManager.h"
#include "GameLayer.h"
#include "OptionGrid.h"
#include "AnswerGrid.h"
#include "GameConstants.h"
#include "GameController.h"
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"
#include "StatsManager.h"

using namespace rapidjson;
LevelManager::LevelManager(GameLayer *p_ptrGameLayer)
{
    m_ptrGameLayer = p_ptrGameLayer;
    
    m_ptrOptionGrid = m_ptrGameLayer->getOptionGrid();
    m_ptrAnswerGrid = m_ptrGameLayer->getAnswerGrid();
    
    
    int curlevel = UserDefault::getInstance()->getIntegerForKey(CURR_LEVEL);
    if(curlevel>=0)
    {
        m_iIndex = curlevel;
    }else{
        m_iIndex = StatsManager::getInstance()->getLangPlayedlevelCount();
    }
    
    
    loadLevel();
    
    
    
}
/// loads current level data
void LevelManager::loadLevel()
{
    std::string country = StatsManager::getInstance()->getCurrentCountry();
    
    std::vector<std::string> t_PlayedArray = StatsManager::getInstance()->getplayedCountriesArray();
    
    int levelnum = 0;
    
    int index = StatsManager::getInstance()->getCurrentCountryIndex();
    
    if(index>0)
    {
        for(int i= 0; i<index; i++)
        {
            std::string str = t_PlayedArray.at(i);
            levelnum += StatsManager::getInstance()->getPlayedLevelCountForCountries(str);
        }
        
        levelnum += m_iIndex;
        
        m_iIndex = levelnum;
    }
    else if(index==0)
    {
        levelnum += m_iIndex;
        
        m_iIndex = levelnum;
    }
    CCLOG("\n levelnum->%d \n",m_iIndex);
    parseJson();
    
    if(GameController::getInstance()->getStringForKey("connectAs").compare("facebook")==0)
    {
        std::string strlang = StringUtils::format("update_level_%s",me_Language.c_str());
        if(!UserDefault::getInstance()->getBoolForKey(strlang.c_str()))
        {
            UserDefault::getInstance()->setBoolForKey(strlang.c_str(),true);
            
            if(m_iIndex>StatsManager::getInstance()->getLangPlayedlevelCount())
            {
                StatsManager::getInstance()->updateLangPlayedlevelCount(m_iIndex,true);
                GameController::getInstance()->updateUserDataToFireBase();
                CCLOG("\n levelnum->%d \n",m_iIndex);
            }
        }
    }
}
void LevelManager::checkJson(std::string t_fullPath)
{
    char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str()) + sizeof(int) + 1];
    strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str());
    
    rapidjson::Document t_doc;
    t_doc.Parse<kParseStopWhenDoneFlag>(p_Buffer);
    
    if(t_doc.HasParseError()) // Print parse error
    {
        CCLOG("GetParseError %u\n",t_doc.GetParseError());
        delete [] p_Buffer;
        p_Buffer = NULL;
        
        return;
    }
    
    for(int i=0; i<t_doc.Size();i++)
    {
        
        if(t_doc[i].HasMember("L"))
        {
            std::string t_Word = t_doc[i]["L"].GetString();
            m_LettersArray = split(t_Word,',');
        }
        
        // Words
        if(t_doc[i].HasMember("W"))
        {
            for (int j = 0; j<t_doc[i]["W"].Size(); j++)
            {
                const std::string t_Word = t_doc[i]["W"][j].GetString();
                m_WordsArray.push_back(t_Word);
            }
            
            if(m_WordsArray.size()<=1) {
                printf("\n empty_words-->%d\n",i);
            }
            
            
            
            for (auto t_word : m_WordsArray)
            {
                std::vector<std::string> t_TempArray = split(t_word,',');
                
                for(auto letter : t_TempArray)
                {
                    bool Found = std::find(m_LettersArray.begin(), m_LettersArray.end(), letter)!= m_LettersArray.end();
                    
                    if(Found)
                    {
                        
                    }
                    else
                    {
                        printf("\n level-->%d\n",i);
                        printf("\n letter-->%s\n",letter.c_str());
                        break;
                    }
                }
            }
            
        }
        m_WordsArray.clear();
    }
    return;
}

/// PARSE JSON FILE
void LevelManager::parseJson()
{
    std::string t_fullPath = FileUtils::getInstance()->fullPathForFilename("Data/English_Data.json");
    
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_fullPath="Data/English_Data.json";
    }
    else if(me_Language.compare(LANG_KANNADA)==0)
    {
        t_fullPath="Data/Kannada_Data.json";
    }
    else if (me_Language.compare(LANG_HINDI)==0)
    {
        t_fullPath="Data/Hindi_Data.json";
    }
    else if (me_Language.compare(LANG_MALAYALAM)==0)
    {
        t_fullPath="Data/Malayalam_Data.json";
    }
    else if (me_Language.compare(LANG_MARATHI)==0)
    {
        t_fullPath="Data/Marathi_Data.json";
    }
    else if (me_Language.compare(LANG_ODIA)==0)
    {
        t_fullPath="Data/Odia_Data.json";
    }
    else if (me_Language.compare(LANG_TAMIL)==0)
    {
        t_fullPath="Data/Tamil_Data.json";
    }
    else if (me_Language.compare(LANG_TELUGU)==0)
    {
        t_fullPath="Data/Telugu_Data.json";
    }
    else if (me_Language.compare(LANG_GUJARATI)==0)
    {
        t_fullPath="Data/Gujarati_Data.json";
    }
    else if (me_Language.compare(LANG_BANGLA)==0)
    {
        t_fullPath="Data/Bengali_Data.json";
    }
    
    checkJson(t_fullPath);
    
    char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str()) + sizeof(int) + 1];
    strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str());
    
    rapidjson::Document t_doc;
    t_doc.Parse<kParseStopWhenDoneFlag>(p_Buffer);
    
    if(t_doc.HasParseError()) // Print parse error
    {
        CCLOG("GetParseError %u\n",t_doc.GetParseError());
        delete [] p_Buffer;
        p_Buffer = NULL;
        
        return;
    }
    
    if (!t_doc.IsNull())
    {
        
        if(m_iIndex >= t_doc.Size())
        {
            StatsManager::getInstance()->InitializeLevelCountForLanguages();
            m_iIndex = 1;//StatsManager::getInstance()->getLangPlayedlevelCount();
        }
        
        if(t_doc[m_iIndex].HasMember("L"))
        {
            std::string t_Word = t_doc[m_iIndex]["L"].GetString();
            m_LettersArray = split(t_Word,',');
            
            m_ptrOptionGrid->createPositionsForLetters(int(m_LettersArray.size()));
        }
        
        // Bonus Words
        if(t_doc[m_iIndex].HasMember("B"))
        {
            rapidjson::Value& val = t_doc[m_iIndex]["B"];
            
            if (val.Size()>0)
            {
                for (int i = 0; i<val.Size(); i++)
                {
                    std::string t_Word = val[i].GetString();
                    m_BonusWordsArray.push_back(t_Word);
                }
            }
        }
        
        // Words
        if(t_doc[m_iIndex].HasMember("W"))
        {
            for (int i = 0; i<t_doc[m_iIndex]["W"].Size(); i++)
            {
                const std::string t_Word = t_doc[m_iIndex]["W"][i].GetString();
                m_WordsArray.push_back(t_Word);
            }
        }
        
        // Sort the words based on size
        sortWordsLowtoHigh(m_WordsArray);
        
        findCountOfString(m_LettersArray,m_WordsArray);
        
        m_ptrOptionGrid->createOptionLetterTiles(m_LettersArray,dictLetterCount);// Option grids
        m_ptrAnswerGrid->createAnswerGrid(m_WordsArray,m_BonusWordsArray);// Answer Grids
    }
    
    
    
    printf("t_LettersArray->%lu\n",m_LettersArray.size());
    printf("t_WordsArray->%lu\n",m_WordsArray.size());
    
}
/// Sorts words Based on length of word
/// @param t_WordsArray Array of words
void LevelManager::sortWordsLowtoHigh(std::vector<std::string> t_WordsArray)
{
    std::vector<std::vector<std::string>> sTempArray;
    
    if(t_WordsArray.size()>0)
    {
        for (auto t_word : t_WordsArray)
        {
            std::vector<std::string> t_TempArray = split(t_word,',');
            
            if(t_TempArray.size()>1)
            {
                sTempArray.push_back(t_TempArray);
            }
        }
        
        
        int n = sTempArray.size();
        //sorting - ASCENDING ORDER
        std::vector<std::string> temp;
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(sTempArray.at(i).size()>sTempArray.at(j).size())
                {
                    temp = sTempArray.at(i);
                    sTempArray.at(i) = sTempArray.at(j);
                    sTempArray.at(j) = temp;
                }
            }
        }
        
        // is not working if array is already sorted
        //        bool sorted = std::is_sorted(sTempArray.begin(), sTempArray.end());
        //        if(!sorted)
        //        {
        //            std::sort(sTempArray.begin(), sTempArray.end());
        //        }
        
        if(m_WordsArray.size()>0)
        {
            m_WordsArray.clear();
        }
        
        if(sTempArray.size()>0)
        {
            std::string res = "";
            
            for(auto it : sTempArray)
            {
                for(auto str : it)
                {
                    if(!res.empty()) {
                        res.append(",");
                    }
                    res.append(str);
                }
                //                printf("\n %s\n",res.c_str());
                m_WordsArray.push_back(res);
                res = "";
            }
        }
        
        //        printf("\n After sort-->%lu\n",m_WordsArray.size());
        
    }
    
}
///To count num of string
void LevelManager::findCountOfString(std::vector<std::string> t_LettersArray,std::vector<std::string> t_WordsArray)
{
    
    //    std::sort(t_LettersArray.begin(), t_LettersArray.end());
    //    t_LettersArray.erase(it, t_LettersArray.end());
    
    std::vector<std::string> s;
    std::vector<std::string> dupStr;
    
    m_strRepString = "";
    bool dupFound = false;
    
    for (int i = 0; i<t_LettersArray.size(); i++)
    {
        dupFound = std::find(s.begin(), s.end(), t_LettersArray.at(i))!= s.end();
        
        if (dupFound)
        {
            dupStr.push_back(t_LettersArray.at(i));
        }
        else
            s.push_back(t_LettersArray.at(i));
        
    }
    
    std::map<std::string ,std::string> dict;
    
    for(int i=0;i<t_LettersArray.size(); i++)
    {
        dict[t_LettersArray.at(i)] = t_LettersArray.at(i);
        dictLetterCount[t_LettersArray.at(i)] = 0;
    }
    
    // tabel tennis
    //
    std::string str = "";
    
    for(int i=0; i<t_WordsArray.size(); i++)
    {
        const std::string &word = t_WordsArray.at(i);
        std::vector<std::string> t_TempArray = split(word,',');
        for (int j=0; j<t_TempArray.size(); j++)
        {
            str = t_TempArray.at(j);
            
            if (dict.find(str.c_str()) == dict.end() )
            {
                // not found
            }
            else
            {
                // found
                if (dict.at(str)==str)
                {
                    dictLetterCount[str] +=1;
                }
            }
            
        }
    }
    
    
    printf("Size-->%lu",dictLetterCount.size());
}
// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
std::vector<std::string> &LevelManager::split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
// TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
std::vector<std::string> LevelManager::split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
LevelManager::~LevelManager()
{
    if(m_LettersArray.size()>0)
    {
        m_LettersArray.clear();
    }
    if(m_WordsArray.size()>0)
    {
        m_WordsArray.clear();
    }
    if(m_BonusWordsArray.size()>0)
    {
        m_BonusWordsArray.clear();
    }
}
// spantinous
// perstint ->
// sensitive ->sadness quickly
// spantionus -> perstint
