//
//  MainMenuScene.cpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#include "MainMenuScene.h"
#include "GameScene.h"
#include "GameScene.h"
#include "LevelScene.h"
#include "GameConstants.h"
#include "GameController.h"
#include "PopUpManager.h"
#include "InterfaceManagerInstance.h"
#include "CW_LevelScene.h"
#include "FacebookManager.h"
#include "LanguageTranslator.h"
#include "TutorialManager.h"

MainMenuScene::MainMenuScene()
{
    me_strScreenName = "MenuScreen";
    
    LayerColor *colorLayer = LayerColor::create(Color4B(0,0,0,255));
    this->addChild(colorLayer);
    
    if(UserDefault::getInstance()->getBoolForKey("AppUpdateTime",false))
    {
        time_t  m_StartTime;
        time(&m_StartTime);
        UserDefault::getInstance()->setStringForKey(APP_UPDATE_TIME, ctime(&m_StartTime));
        UserDefault::getInstance()->setBoolForKey("AppUpdateTime",true);
    }
    
    m_ptrMenuLayer = new MenuLayer();
    m_ptrMenuLayer->autorelease();
    this->addChild(m_ptrMenuLayer, 1);
}
MainMenuScene::~MainMenuScene()
{
    this->removeAllChildrenWithCleanup(true);
}

#pragma mark MENU LAYER
MenuLayer::MenuLayer()
{
    signInApple = NULL;
    m_ptrPopUpManager = NULL;
    moreGameArr = NULL;
    m_bIsEventCreated = false;
    m_tSaleTimerLbl = NULL;
    m_SpSale = NULL;
    m_rateButton  = NULL;
    m_shareButton = NULL;
    m_TutManager  = NULL;
    // DAILY BONUS TIMER
    m_fTimer = 0.0f;
    m_fSaleTimer = 0.0f;
    m_fSaleTimeDelay = 12*60*60;
    
    if(UserDefault::getInstance()->getBoolForKey("FROM_LAUNCH"))
    {
        CCLOG("start Bg Sound");
        GameController::getInstance()->playBGM(BGM_GAME,true);
    }
    
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsScreen("MenuScreen");
    
    if(!me_bDailyRewardAvailable)
        CheckDailyRewardAvailability();
    
    auto KeyListener = EventListenerKeyboard::create();
    KeyListener->setEnabled(true);
    KeyListener->onKeyPressed = CC_CALLBACK_2(MenuLayer::onKeyPressed, this);
    KeyListener->onKeyReleased = CC_CALLBACK_2(MenuLayer::onKeyReleased, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(KeyListener, this);
    
    
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    Origin = ResolutionSize.origin;
    
    Sprite *m_SprSplashBg = Sprite::create("Splash-bg.jpg");
    m_SprSplashBg->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
    m_SprSplashBg->setScale(m_fScale);
    this->addChild(m_SprSplashBg);
    
    // Settings Button
    m_SettingsButton = getButtonMade("setting-button.png","setting-button.png",CC_CALLBACK_1(MenuLayer::settingsBtnCallback, this));
    m_SettingsButton->setScale(m_fScale);
    m_SettingsButton->setPosition(Vec2(Origin.x+20+m_SettingsButton->getBoundingBox().size.width/2,Origin.y+visibleSize.height-20-m_SettingsButton->getBoundingBox().size.height/2));
    
    // Achievement Button
    m_AchievementButton = getButtonMade("achievements-button.png","achievements-button.png",CC_CALLBACK_1(MenuLayer::achievementBtnCallback, this));
    m_AchievementButton->setScale(m_fScale);
    
    float xpos = m_SettingsButton->getBoundingBox().getMaxX();
    m_AchievementButton->setPosition(Vec2(xpos+20+m_AchievementButton->getBoundingBox().size.width/2,Origin.y+visibleSize.height-20-m_AchievementButton->getBoundingBox().size.height/2));
    
    Sprite *t_SprClaimIcon = Sprite::create("Claim.png");
    t_SprClaimIcon->setScale(0.9f);
    t_SprClaimIcon->setPosition(Vec2(m_AchievementButton->getContentSize().width/2,t_SprClaimIcon->getBoundingBox().size.height/2));
    t_SprClaimIcon->setName("tag");
    t_SprClaimIcon->setVisible(false);
    m_AchievementButton->addChild(t_SprClaimIcon,2);
    
    //Shop Button
    m_ShopButton = getButtonMade("shop-button.png","shop-button.png",CC_CALLBACK_1(MenuLayer::shopBtnCallback, this));
    m_ShopButton->setScale(m_fScale);
    
    xpos = m_AchievementButton->getBoundingBox().getMaxX();
    m_ShopButton->setPosition(Vec2(xpos+20+m_ShopButton->getBoundingBox().size.width/2,Origin.y+visibleSize.height-20-m_ShopButton->getBoundingBox().size.height/2));
    
    m_SpSale = Sprite::create("sale_image.png");
    m_SpSale->setPosition(Vec2(m_ShopButton->getContentSize().width/2,m_SpSale->getContentSize().height/2));
    m_ShopButton->addChild(m_SpSale,1);
    
    
    //Coin Button
    m_CoinsButton = getButtonMade("add-dollar-button.png","add-dollar-button.png",CC_CALLBACK_1(MenuLayer::addCoinBtnCallback,this));
    m_CoinsButton->setScale(m_fScale);
    m_CoinsButton->setPosition(Vec2(Origin.x+visibleSize.width-20-m_CoinsButton->getBoundingBox().size.width/2,m_ShopButton->getPositionY()));
    
    
    std::string coinStr = GameController::getInstance()->getCoinsFormat(me_iCoinCount);
    m_CoinCount = Label::createWithTTF(coinStr,FONT_NAME,20);
    m_CoinCount->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    m_CoinCount->enableOutline(Color4B(30, 30, 11, 255),3);
    m_CoinCount->setColor(Color3B::WHITE);
    if (me_iCoinCount<=50) {
        m_CoinCount->setColor(Color3B::RED);
    }
    m_CoinCount->setPosition(Vec2(m_CoinsButton->getContentSize().width/2-10,m_CoinsButton->getContentSize().height/2));
    m_CoinsButton->addChild(m_CoinCount,2);
    
    Menu *topMenu = Menu::create(m_SettingsButton,m_AchievementButton,m_ShopButton,m_CoinsButton,NULL);
    topMenu->setPosition(Vec2::ZERO);
    this->addChild(topMenu);
    
    //GAME WORD LOGO
    Sprite *m_SprLogo = Sprite::create("word-pro_logo.png");
    m_SprLogo->setScale(m_fScale);
    m_SprLogo->setPosition(Vec2(Origin.x+visibleSize.width/2-10,Origin.y+visibleSize.height*0.7));
    this->addChild(m_SprLogo,1);
    
    ScaleTo *scale = ScaleTo::create(0.7f,m_SprLogo->getScale()+0.02f);
    ScaleTo *scale1 = ScaleTo::create(0.7f,m_SprLogo->getScale());
    Sequence *seq = Sequence::create(scale,scale1,NULL);
    RepeatForever *rep = RepeatForever::create(seq);
    m_SprLogo->runAction(rep);
    
    // Daily Button
    m_DailyEventBtn = getButtonMade("Daily-Event.png","Daily-Event.png",CC_CALLBACK_1(MenuLayer::dailyEventBtnCallback, this));
    m_DailyEventBtn->setScale(m_fScale);
    m_DailyEventBtn->setPosition(Vec2(Origin.x+10+m_DailyEventBtn->getBoundingBox().size.width/2,m_SprLogo->getBoundingBox().getMinY()-m_DailyEventBtn->getBoundingBox().size.height/2));
    
    
    time_t t = time(0);
    tm *today = localtime(&t);
    int month = today->tm_mon;
    int day = today->tm_mday;
    
    std::string monthname = GameController::getInstance()->getMonthName(month);
    Label *monthLabel = Label::createWithTTF(monthname, FONT_NAME,25);
    monthLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    monthLabel->setPosition(Vec2(m_DailyEventBtn->getContentSize().width/2,m_DailyEventBtn->getContentSize().height-monthLabel->getContentSize().height));
    monthLabel->setColor(Color3B(254,253,56));
    monthLabel->setName("month");
    m_DailyEventBtn->addChild(monthLabel,1);
    
    Label *dayLabel = Label::createWithTTF(StringUtils::format("%d",day), FONT_NAME,40);
    dayLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    dayLabel->setPosition(Vec2(m_DailyEventBtn->getContentSize().width/2,m_DailyEventBtn->getContentSize().height/2-5));
    dayLabel->setColor(Color3B(72,52,26));
    dayLabel->setName("day");
    m_DailyEventBtn->addChild(dayLabel,1);
    
    Sprite *SprTaskImage = Sprite::create();
    SprTaskImage->setPosition(Vec2(m_DailyEventBtn->getContentSize().width/2,m_DailyEventBtn->getContentSize().height*-0.14));
    SprTaskImage->setName("tag");
    SprTaskImage->setVisible(false);
    m_DailyEventBtn->addChild(SprTaskImage,2);
    
    if(UserDefault::getInstance()->getBoolForKey(NEW_TASK))
    {
        SprTaskImage->setTexture("New_Task.png");
        SprTaskImage->setVisible(true);
    }
    
    // Mega Deal Button
    m_MegaDealButton = getButtonMade("mega-deal-image.png","mega-deal-image.png",CC_CALLBACK_1(MenuLayer::megaDealBtnCallback, this));
    m_MegaDealButton->setScale(m_fScale);
    m_MegaDealButton->setVisible(false);
    m_MegaDealButton->setPosition(Vec2(Origin.x+10+m_MegaDealButton->getBoundingBox().size.width/2,m_DailyEventBtn->getBoundingBox().getMinY()-m_DailyEventBtn->getBoundingBox().size.height*0.8));
    
    std::string t_SubText = "11:58:25";
    m_tSaleTimerLbl = Label::createWithTTF(t_SubText, FONT_NAME,25,Size(0, 0));
    m_tSaleTimerLbl->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    m_tSaleTimerLbl->setColor(Color3B::WHITE);
    m_tSaleTimerLbl->enableOutline(Color4B::RED,2);
    m_tSaleTimerLbl->setHeight(m_tSaleTimerLbl->getBoundingBox().size.height);
    m_tSaleTimerLbl->setVisible(false);
    m_tSaleTimerLbl->setPosition(Vec2(m_MegaDealButton->getContentSize().width/2,-m_tSaleTimerLbl->getBoundingBox().size.height/2));
    
    m_MegaDealButton->addChild(m_tSaleTimerLbl,2);
    
    // Play Button
    m_PlayButton = getButtonMade("play-button.png","play-button.png",CC_CALLBACK_1(MenuLayer::playBtnCallback, this));
    m_PlayButton->setScale(m_fScale);
    m_PlayButton->setPosition(Vec2(Origin.x+visibleSize.width/2,m_DailyEventBtn->getPositionY()-m_PlayButton->getContentSize().height/2));
    
    /// Rate button
    m_rateButton = getButtonMade("rate_button.png","rate_button.png",CC_CALLBACK_0(MenuLayer::rateButtonCallBack,this));
    m_rateButton->setScale(m_fScale);
    m_rateButton->setPosition(Vec2(Origin.x+visibleSize.width-20-m_rateButton->getBoundingBox().size.width/2,m_DailyEventBtn->getPositionY()+50.0f));
    
    /// SHARE  button
    m_shareButton = getButtonMade("share_button.png","share_button.png",CC_CALLBACK_0(MenuLayer::shareButtonCallBack,this));
    m_shareButton->setScale(m_fScale);
    m_shareButton->setPosition(Vec2(Origin.x+visibleSize.width-20-m_shareButton->getBoundingBox().size.width/2,m_rateButton->getBoundingBox().getMinY()-10-m_shareButton->getBoundingBox().size.height/2));
    
    std::string str = "Fb_Login.png";
    
    m_FacebookButton = getButtonMade(str,str,CC_CALLBACK_1(MenuLayer::FacebookLoginCallback,this));
    m_FacebookButton->setScale(m_fScale);
    m_FacebookButton->setPosition(Vec2(Origin.x+visibleSize.width/2,m_PlayButton->getBoundingBox().getMinY()-(30+m_FacebookButton->getBoundingBox().size.height/2)));
    m_FacebookButton->setVisible(false);
    
    m_FBInfoButton = getButtonMade("Fb_info.png","Fb_info.png",CC_CALLBACK_1(MenuLayer::FacebookLoginCallback,this));
    //    m_FBInfoButton->setScale(m_fScale);
    m_FBInfoButton->setPosition(Vec2(Origin.x+visibleSize.width/2+m_FacebookButton->getBoundingBox().size.width/2,m_FacebookButton->getPositionY()+m_FacebookButton->getBoundingBox().size.height/2-10));
    m_FBInfoButton->setName("Info");
    m_FBInfoButton->setVisible(false);
    
    m_LeaderBoardButton = getButtonMade("Leaderboard_button.png","Leaderboard_button.png",CC_CALLBACK_0(MenuLayer::leaderboardBtnCallback,this));
    m_LeaderBoardButton->setScale(m_fScale);
    m_LeaderBoardButton->setPosition(Vec2(Origin.x+visibleSize.width-m_LeaderBoardButton->getBoundingBox().size.width/2,m_MegaDealButton->getPositionY()-30.0f));
    m_LeaderBoardButton->setEnabled(false);
    
    Menu *t_BottomMenu = Menu::create(m_PlayButton,m_DailyEventBtn,m_MegaDealButton,m_FacebookButton,m_rateButton,m_shareButton,m_LeaderBoardButton,m_FBInfoButton,NULL);
    t_BottomMenu->setPosition(Vec2::ZERO);
    this->addChild(t_BottomMenu);
    
    m_ptrPopUpManager = new PopUpManager(this,SCREEN_MENU);
    m_ptrPopUpManager->autorelease();
    this->addChild(m_ptrPopUpManager,3);
    
    m_TutManager = TutorialManager::create();
    m_TutManager->setPosition(Vec2::ZERO);
    this->addChild(m_TutManager,5);
    
    GameController::getInstance()->showBanner();
    
    checkForMoreGames();
    
    getLevelFinishMoreGames(LEVEL_FINISH_GAMES_URL,"Json");
    
    /// download exit popup promotion data and check for game promotion popup
    checkForExitPromotion();
    
    /// check for limited sale availabality
    checkforLimitedSaleAvailability();
    
    checkforDailyRewardPopUp();
    
    checkForDailyMissionUpdates();
    
    checkForAchievementUpdates();
    
    registerCustomEvents();
    
    this->schedule(schedule_selector(MenuLayer::OnUpdateFrame));
    
    
    if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
    {
        m_FacebookButton->setPosition(Vec2(Origin.x+visibleSize.width/2,m_PlayButton->getBoundingBox().getMinY()-(10+m_FacebookButton->getBoundingBox().size.height/2)));
        
        signInApple = getButtonMade("signInApple.png","signInApple.png",CC_CALLBACK_1(MenuLayer::signInWithApple,this));
        //        signInApple->setScale(m_fScale);
        signInApple->setPosition(Vec2(Origin.x+visibleSize.width/2,m_FacebookButton->getBoundingBox().getMinY()-(5+signInApple->getBoundingBox().size.height/2)));
        
        Menu *t_IosMenu = Menu::create(signInApple,NULL);
        t_IosMenu->setPosition(Vec2::ZERO);
        this->addChild(t_IosMenu);
        
        
        if(strcmp(GameController::getInstance()->getStringForKey("connectAs").c_str(),"Apple")==0)
        {
            m_FacebookButton->setVisible(false);
            m_FBInfoButton->setVisible(false);
            
            signInApple->setVisible(false);
        }
    }
    //    checkforJson();
    
    GameController::getInstance()->getLeaderboardData();
    
    GameController::getInstance()->getUserLanguageData();
    
    //checks for game update for 3 days once
    checkForGameUpdate();
    
    checkForChallengeAndboardTut();
    
    if(FacebookManager::getInstance()->isLoggedIn())
    {
        m_FacebookButton->setVisible(false);
        m_FBInfoButton->setVisible(false);
        
        std::string fbId = GameController::getInstance()->getStringForKey("facebookID");
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->setUserFbId(fbId);
        
        if(signInApple)
            signInApple->setVisible(false);
    }
    
}
void MenuLayer::checkForChallengeAndboardTut()
{
    Label *m_tDisableLbl = Label::createWithTTF("?", FONT_NAME,60,Size(0, 0));
    m_tDisableLbl->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    m_tDisableLbl->setColor(Color3B::BLACK);
    m_tDisableLbl->setVisible(true);
    m_tDisableLbl->setPosition(Vec2(m_LeaderBoardButton->getContentSize().width/2,m_LeaderBoardButton->getContentSize().height/2));
    m_LeaderBoardButton->addChild(m_tDisableLbl,2);
    
    if(signInApple)
        signInApple->setVisible(false);
    
    
    if(FacebookManager::getInstance()->isLoggedIn())
    {
        UserDefault::getInstance()->setBoolForKey("leaderboard",true);
    }
    
    if(!UserDefault::getInstance()->getBoolForKey("dailyTask", false))
    {
        if(StatsManager::getInstance()->getLangPlayedlevelCount()>=7)
        {
            checkforLeaderBoardTutorial(1);
        }
    }
    
    if(!UserDefault::getInstance()->getBoolForKey("leaderboard", false))
    {
        if(StatsManager::getInstance()->getLangPlayedlevelCount()>=10)
        {
            m_FacebookButton->setVisible(true);
            m_FBInfoButton->setVisible(true);
            
            if(signInApple)
                signInApple->setVisible(true);
            
            m_tDisableLbl->setVisible(false);
            m_LeaderBoardButton->setEnabled(true);
            
            if(!m_bIsTutorial)
            {
                DelayTime *t_DelayeTime = DelayTime::create(0.5f);
                CallFuncN *t_CallFunc = CallFuncN::create(std::bind(&MenuLayer::checkforLeaderBoardTutorial,this,2));
                this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
            }
        }
    }
    else
    {
        if(signInApple)
            signInApple->setVisible(true);
        
        m_FacebookButton->setVisible(true);
        m_FBInfoButton->setVisible(true);
        
        m_LeaderBoardButton->setEnabled(true);
        m_tDisableLbl->setVisible(false);
        
        checkforLeaderBoardTutorial(3);
    }
    
}
void MenuLayer::checkforJson()
{
    
    string jsonBufferstr =  "{\"Achievements\": [{\"Name\": \"Get 30 GOOD combos\",\"KeyTag\":\"good\",\"CurrCount\":0,\"Target\": 30,\"Reward\": 25,\"RewardClaimed\": false,\"Status\": false},{\"Name\": \"Get 30 GREAT combos\",\"KeyTag\":\"great\",\"CurrCount\":0,\"Target\": 30,\"Reward\": 30,\"RewardClaimed\": false,\"Status\": false}]}";
    
    string jsonBuffer ="{ \"people\": [{\"id\": 1, \"name\":\"MIKE\",\"surname\":\"TAYLOR\"}, {\"id\": 2, \"name\":\"TOM\",\"surname\":\"JERRY\"} ]}";
    rapidjson::Document m_JsonDocument;
    m_JsonDocument.Parse<rapidjson::kParseStopWhenDoneFlag>(jsonBuffer.c_str());
    
    if (m_JsonDocument.HasParseError())  // Print parse error
    {
        printf("GetParseError %u\n",m_JsonDocument.GetParseError());
        return;
    }
    
    
    if(m_JsonDocument.HasMember("people"))
    {
        int size = m_JsonDocument["people"].Size();
        printf("Size-->%d\n",m_JsonDocument["people"].Size());
        for(int i=0;i<size;i++)
        {
            printf("\n id--> %d\n",m_JsonDocument["people"][i]["id"].GetInt());
            printf("\n name--> %s\n",m_JsonDocument["people"][i]["name"].GetString());
            printf("\n surname--> %s\n",m_JsonDocument["people"][i]["surname"].GetString());
        }
    }
}
/// check for limited sale availablity
void MenuLayer::checkforLimitedSaleAvailability()
{
    if(StatsManager::getInstance()->getLangPlayedlevelCount()<15)
    {
        if(!UserDefault::getInstance()->getBoolForKey(LIMITED_SALE_AVAILABLE))
        {
            UserDefault::getInstance()->setBoolForKey(LIMITED_SALE_AVAILABLE, false);
            m_MegaDealButton->setVisible(false);
        }
        return;
    }
    
    if(!UserDefault::getInstance()->getBoolForKey("FIRST_TIME_SALE"))
    {
        time_t m_StartTime;
        time(&m_StartTime);
        UserDefault::getInstance()->setStringForKey(SALE_START_TIME, ctime(&m_StartTime));
        UserDefault::getInstance()->setBoolForKey(LIMITED_SALE_AVAILABLE,true);
        UserDefault::getInstance()->setBoolForKey("FIRST_TIME_SALE",true);
        GameController::getInstance()->setTodayAsfirstDay();
    }
    
    if(UserDefault::getInstance()->getBoolForKey(LIMITED_SALE_AVAILABLE))
    {
        int tot_S = (int)m_fSaleTimer%60;
        int tot_M = (int)m_fSaleTimer/60;
        int minute = tot_M%60;
        int tot_H = (int)tot_M/60;
        int hour = tot_H%60;
        std::string  buffer = StringUtils::format("%02d:%02d:%02d",hour,minute,tot_S) ;
        m_MegaDealButton->setVisible(true);
        m_tSaleTimerLbl->setString(buffer);
        //        m_tSaleTimerLbl->setVisible(true);
    }
    
    if (GameController::getInstance()->getDayDifferenceCount()>=2)
    {
        time_t  m_StartTime;
        time(&m_StartTime);
        UserDefault::getInstance()->setStringForKey(SALE_START_TIME, ctime(&m_StartTime));
        UserDefault::getInstance()->setBoolForKey(LIMITED_SALE_AVAILABLE,true);
        UserDefault::getInstance()->setBoolForKey("SALE_POPUP_SHOWN",false);
        checkforLimitedSaleAvailability();
    }
}
/// check for on launch popup's language daily reward  promotion
void MenuLayer::checkforDailyRewardPopUp()
{
    
    if(!UserDefault::getInstance()->getBoolForKey("FIRST_LANG"))
    {
        LoadOtherPopUpWithDelay(0.5f,"Language");
        UserDefault::getInstance()->setBoolForKey("FROM_LAUNCH",false);
    }
    else if(me_bDailyRewardAvailable&&UserDefault::getInstance()->getBoolForKey("FROM_LAUNCH"))
    {
        m_bIsDailyReward = true;
        LoadOtherPopUpWithDelay(0.5f,"DailyReward");
        UserDefault::getInstance()->setBoolForKey("FROM_LAUNCH",false);
    }
}
///  CREATE CUSTOM EVENTS FOR PURCHASE AND VIDEO ADS SUCCESS
void MenuLayer::registerCustomEvents()
{
    auto t_FbLoginSuccess = EventListenerCustom::create(FB_LOGIN_SUCCESS, CC_CALLBACK_0(MenuLayer::FbLoginSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_FbLoginSuccess,this);
    
    auto t_FbLoginFailure = EventListenerCustom::create(FB_LOGIN_FAILURE, CC_CALLBACK_0(MenuLayer::FbLoginFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_FbLoginFailure,this);
    
    auto t_AppleLoginSuccess = EventListenerCustom::create(APPLE_LOGIN_SUCCESS, CC_CALLBACK_0(MenuLayer::AppleSignInSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_AppleLoginSuccess,this);
    
    auto t_AppleLoginFailure = EventListenerCustom::create(APPLE_LOGIN_FAILURE, CC_CALLBACK_0(MenuLayer::AppleSignInFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_AppleLoginFailure,this);
    
}
/// UPDATE FRAME CHECK FOR TIME
void MenuLayer::OnUpdateFrame(float p_fDeltaTime)
{
    {
        m_fTimer += p_fDeltaTime;
        if(m_fTimer >= 1.0f)
        {
            m_fTimer = 0.0f;
            
            if(UserDefault::getInstance()->getBoolForKey(LIMITED_SALE_AVAILABLE))
            {
                checkSaleTimer();
            }
            
            if(UserDefault::getInstance()->getBoolForKey("UserPredict",false))
            {
                userRewardsBasedPrediction();
            }
            
            if(UserDefault::getInstance()->getBoolForKey("Notification", false))
            {
                openNotificationAction();
            }
        }
    }
}
/// check for more games json data
void MenuLayer::checkForMoreGames()
{
    
    if(GameController::getInstance()->getBoolForKey("isMoreGameLoaded") == true)
    {
        createMoreGames();
        return;
    }
    
    HttpRequest *request = new (std::nothrow) HttpRequest();
    request->setUrl(MORE_GAMES_URL);
    
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(MenuLayer::onFetchCompleted, this));
    HttpClient::getInstance()->send(request);
    httpRequest.push_back(request);
    
}
/// on success of more games data download
void MenuLayer::onFetchCompleted(HttpClient *sender, HttpResponse *response)
{
    if (!response || !response->isSucceed()) {
        CCLOG("response failed! error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    unsigned char *data = (unsigned char *)&(buffer->front());
    std::string jsonDataString(reinterpret_cast<char*>(data), buffer->size());
    
    CCLOG("Full server response data : %s \n", jsonDataString.c_str());
    
    
    rapidjson::Document doc;
    doc.Parse<0>(jsonDataString.c_str());
    
    if (doc.HasMember("isMoreGamesVisible")) {
        GameController::getInstance()->isMoreGamesVisible = doc["isMoreGamesVisible"].GetBool();
        if (GameController::getInstance()->isMoreGamesVisible == true)
        {
            GameController::getInstance()->moreGameAppUrl.clear();
            GameController::getInstance()->moreGameImageUrl.clear();
        }
    }
    
    if (doc.HasMember("appList")) {
        const  rapidjson::Value& b = doc["appList"];
        if (!b.IsNull())
        {
            CCLOG("appList size = %d", (int)b.Size());
            for (rapidjson::SizeType j = 0; j < (int)b.Size(); j++)
            {
                const  rapidjson::Value& c = b[j];
                if (c.HasMember("isVisible"))
                {
                    if (c["isVisible"].GetBool() == true)
                    {
                        if (c.HasMember("appUrl"))
                            GameController::getInstance()->moreGameAppUrl.push_back(c["appUrl"].GetString());
                        
                        if (c.HasMember("imageUrl"))
                            GameController::getInstance()->moreGameImageUrl.push_back(c["imageUrl"].GetString());
                    }
                }
            }
        }
    }
    
    if (GameController::getInstance()->isMoreGamesVisible == true)
    {
        createMoreGames();
    }
    
}
/// CREATE MORE GAMES BUTTONS
void MenuLayer::createMoreGames()
{
    moreGameArr = new Vector<MenuItemSprite*>();
    
    GameController::getInstance()->setBoolForKey("isMoreGameLoaded", true);
    int noOfGames = (int)GameController::getInstance()->moreGameImageUrl.size();
    
    photoIndex = -1;
    
    for (int p = 0;p < noOfGames;p++)
    {
        std::string str = FileUtils::getInstance()->getWritablePath();
        str += StringUtils::format("moreGamesIcon%d.png", p);
        
        if (FileUtils::getInstance()->isFileExist(str) == false)
        {
            photoIndex = p;
            break;
        }
    }
    
    
    Point m_Position = Point(Origin.x+visibleSize.width/2 - (noOfGames*162*m_fScale)/2.0f,Origin.y+100);
    
    for(int i=0;i<noOfGames; i++)
    {
        m_Position.x += (162*m_fScale)/2;
        
        std::string str = FileUtils::getInstance()->getWritablePath();
        str += StringUtils::format("moreGamesIcon%d.png", i);
        
        MenuItemSprite *iconBtn = getButtonMade("more-games-button.png", "more-games-button.png", CC_CALLBACK_1(MenuLayer::moreGamesBtnCallback, this));
        iconBtn->setScale(m_fScale);
        iconBtn->setPosition(Vec2(m_Position.x,m_Position.y+iconBtn->getBoundingBox().size.height/2));
        iconBtn->setTag(i);
        
        Sprite *giftcountBg = Sprite::create("gift_countBg.png");
        giftcountBg->setColor(Color3B::RED);
        giftcountBg->setPosition(Vec2(iconBtn->getContentSize().width/2,iconBtn->getContentSize().height*0.25));
        iconBtn->addChild(giftcountBg,6);
        
        Label *m_tAdLabel = Label::createWithTTF("Ad", FONT_NAME,25);
        m_tAdLabel->setPosition(Vec2(giftcountBg->getContentSize().width/2,giftcountBg->getContentSize().height/2));
        m_tAdLabel->setColor(Color3B::WHITE);
        giftcountBg->addChild(m_tAdLabel,1);
        
        Sprite *sprGameIcon;
        if (FileUtils::getInstance()->isFileExist(str))
        {
            sprGameIcon = Sprite::create(str);
        }
        else
        {
            sprGameIcon = Sprite::create("pixel.png");
            sprGameIcon->setColor(Color3B(134,235,20));
        }
        
        DrawNode*m_Draw = DrawNode::create();
        m_Draw->drawSolidCircle(Vec2(0.0f, 0.0f),52.0f, 0.0f, 32.0f, Color4F::GREEN);
        
        ClippingNode *clipper = ClippingNode::create();
        clipper->setStencil(m_Draw);
        clipper->setPosition(Vec2(iconBtn->getContentSize().width/2,iconBtn->getContentSize().height/2+5.5f));
        // add the clipping node to the main node
        iconBtn->addChild(clipper,5,1);
        
        sprGameIcon->setOpacity(220);
        clipper->setName("GameIcon");
        clipper->addChild(sprGameIcon,1);
        
        moreGameArr->pushBack(iconBtn);
        Menu *menu = Menu::create(iconBtn, nullptr);
        menu->setPosition(Vec2::ZERO);
        this->addChild(menu);
        
        m_Position.x += (162*m_fScale)/2;
    }
    
    
    if (photoIndex>-1) {
        downloadMoreGamesIcon(photoIndex);
    }
    
    
}
/// download more games icons
void MenuLayer::downloadMoreGamesIcon(int index)
{
    if (index >= (int)GameController::getInstance()->moreGameImageUrl.size())
        return;
    
    CCLOG("Menulayer::downloadMoreGamesIcon for %d", index);
    HttpRequest* request = new (std::nothrow) HttpRequest();
    request->setUrl(GameController::getInstance()->moreGameImageUrl.at(index));
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(MenuLayer::onMoreGamesPhotoDownloaded, this));
    HttpClient::getInstance()->send(request);
    httpRequest.push_back(request);
}
/// on success callback for more games icons download
void MenuLayer::onMoreGamesPhotoDownloaded(HttpClient *sender, HttpResponse *response)
{
    
    if (!response || !response->isSucceed()) {
        CCLOG("response failed! error buffer: %s", response->getErrorBuffer());
        downloadMoreGamesIcon(photoIndex);
        return;
    }
    
    photoIndex += 1;
    std::vector<char> *buffer = response->getResponseData();
    Image *img = new Image();
    img->initWithImageData((const unsigned char *)&(buffer->front()), buffer->size());
    
    if (img != nullptr)
    {
        Texture2D *adTexture = new Texture2D();
        adTexture->initWithImage(img);
        
        std::string str = FileUtils::getInstance()->getWritablePath();
        str += StringUtils::format("moreGamesIcon%d.png", photoIndex - 1);
        
        MenuItemSprite *iconBtn = moreGameArr->at(photoIndex-1);
        iconBtn->removeChildByName("GameIcon");
        
        Sprite *sprGameIcon = Sprite::createWithTexture(adTexture);
        
        DrawNode*m_Draw = DrawNode::create();
        m_Draw->drawSolidCircle(Vec2(0.0f, 0.0f),52.0f, 0.0f, 32.0f, Color4F::GREEN);
        
        ClippingNode *clipper = ClippingNode::create();
        clipper->setStencil(m_Draw);
        clipper->setPosition(Vec2(iconBtn->getContentSize().width/2,iconBtn->getContentSize().height/2+5.5f));
        // add the clipping node to the main node
        iconBtn->addChild(clipper,5,1);
        
        clipper->setName("GameIcon");
        sprGameIcon->setOpacity(220);
        clipper->addChild(sprGameIcon,1);
        
        
        img->saveToFile(str, true);
    }
    
    for (int p = photoIndex;p < (int)GameController::getInstance()->moreGameImageUrl.size();p++)
    {
        std::string str = FileUtils::getInstance()->getWritablePath();
        str += StringUtils::format("moreGamesIcon%d.png", p);
        
        if (FileUtils::getInstance()->isFileExist(str) == false)
        {
            if (photoIndex < (int)moreGameArr->size())
            {
                photoIndex = p;
                downloadMoreGamesIcon(p);
                break;
            }
        }
    }
    
}
/// CUSTOM BUTTON MADE
MenuItemSprite * MenuLayer :: getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    Sprite *_disabledSpr = Sprite::create(selSpr);
    _disabledSpr->setOpacity(150);
    
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr,_disabledSpr ,func);
    return Button;
}
///  PLAY BUTTON CALLBACK
void MenuLayer::playBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    {
        CW_LevelScene *scene = new CW_LevelScene();
        scene->autorelease();
        Director::getInstance()->replaceScene(TransitionMoveInR::create(0.5f, scene));
        scene = NULL;
    }
    GameController::getInstance()->playSFX(SFX_SCREEN_MOVE);
    
    GameController::getInstance()->setBoolForKey("GAME_ENTER",true);
}
/// settings button callback
void MenuLayer::settingsBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_SETTINGS);
    setEnableButtons(false);
}
/// achievement button callback
void MenuLayer::achievementBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_ACHIEVEMENT);
    setEnableButtons(false);
}
/// shop coins button callback
void MenuLayer::shopBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_BUY_COINS);
    setEnableButtons(false);
}
/// add coins button callback
void MenuLayer::addCoinBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_BUY_COINS);
    setEnableButtons(false);
}
/// more games icons button callback
void MenuLayer::moreGamesBtnCallback(Ref *pSender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    MenuItemSprite *tag = (MenuItemSprite*)pSender;
    CCLOG("MenuLoginMode::moreGamesIconCallBack, tag = %d", tag->getTag());
    //    GameController::getInstance()->playSFX(SFX_BTN_PRESS);
    
    std::string item = StringUtils::format("moregames_item_%d",tag->getTag()+1);
    //    GameController::getInstance()->getExtras()->postFirebaseAnalyticsEvent(item);
    
    Application::getInstance()->openURL(GameController::getInstance()->moreGameAppUrl.at(tag->getTag()).c_str());
    
}
/// daily event missions callaback
void MenuLayer::dailyEventBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_DAILY_MISSION);
    setEnableButtons(false);
    
    if(m_bIsTutorial){
        UserDefault::getInstance()->setBoolForKey("dailyTask", true);
        m_bIsTutorial = false;
        if (m_TutManager) {
            m_TutManager->removeTuorial();
        }
    }
}
/// limited sale button callback
void MenuLayer::megaDealBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    m_ptrPopUpManager->LoadPopUp(POPUP_LIMITED_SALE);
    setEnableButtons(false);
}
///  UPDATE COIN VALUE ON  COIN SUCCESS
void MenuLayer::UpdateCoins()
{
    std::string coinStr = GameController::getInstance()->getCoinsFormat(me_iCoinCount);
    
    if(m_CoinCount)
    {
        m_CoinCount->setString(coinStr);
        m_CoinCount->setColor(Color3B::WHITE);
        if (me_iCoinCount<=50) {
            m_CoinCount->setColor(Color3B::RED);
        }
        CoinLabelAnimation();
    }
}
/// indicate animation of coin label on coins adding
void MenuLayer::CoinLabelAnimation()
{
    if(m_CoinCount)
    {
        ScaleTo *scale1 = ScaleTo::create(0.4f,1.2f,1.2f);
        ScaleTo *scale2 = ScaleTo::create(0.4f,1.0f,1.0f);
        m_CoinCount->runAction(Sequence::create(scale1,scale2, NULL));
    }
}
/// set enable and disable actions of buttons
void MenuLayer::setEnableButtons(bool p_bEnable)
{
    m_SettingsButton->setEnabled(p_bEnable);
    m_AchievementButton->setEnabled(p_bEnable);
    m_ShopButton->setEnabled(p_bEnable);
    m_CoinsButton->setEnabled(p_bEnable);
    
    m_DailyEventBtn->setEnabled(p_bEnable);
    m_MegaDealButton->setEnabled(p_bEnable);
    m_PlayButton->setEnabled(p_bEnable);
    m_rateButton->setEnabled(p_bEnable);
    m_shareButton->setEnabled(p_bEnable);
    m_FacebookButton->setEnabled(p_bEnable);
    
    if(StatsManager::getInstance()->getLangPlayedlevelCount()>=10)
        m_LeaderBoardButton->setEnabled(p_bEnable);
    
    if (moreGameArr==NULL) {
        return;
    }
    
    if(moreGameArr->size()==0)
        return;
    
    {
        for(int i=0;i<moreGameArr->size(); i++)
        {
            moreGameArr->at(i)->setEnabled(p_bEnable);
        }
    }
}
/// THIS WILL REMOVE CURRENT ONE AND CALL ANOTHER POPUP WAIT FOR ANIMATION OF CURRENT POPUP
void MenuLayer::LoadOtherPopUpWithDelay(float p_fDelayTime, const std::string &p_PopUpName)
{
    DelayTime *t_DelayeTime = DelayTime::create(p_fDelayTime);
    CallFuncN *t_CallFunc = CallFuncN::create(std::bind(&MenuLayer::LoadOtherPopUp, this, this, p_PopUpName));
    this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
}

/// THIS LOAD OTHER POP UP BY COMPARING WITH STRING
void MenuLayer::LoadOtherPopUp(Ref *p_Sender, const std::string &p_String)
{
    if(p_String.compare("DailyReward")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_DAILY_REWARD);
    }
    else if(p_String.compare("Language")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_LANGUAGE);
    }
    else if(p_String.compare("RemoveAds")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_REMOVE_ADS);
    }
    else if(p_String.compare("Rate")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_RATE_GAME);
    }
    else if(p_String.compare("FreeCoins")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_FREE_COINS);
    }
    else if(p_String.compare("PromotionPopUp")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_GAME_PROMOTION);
    }
    else if(p_String.compare("LimitedSale")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_LIMITED_SALE);
    }
    else if(p_String.compare("dailyTask")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_DAILY_MISSION);
    }
    else if(p_String.compare("Leaderboard")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_GAME_LEADERBOARD);
    }
    else if(p_String.compare("buttonTut")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_COMMON_TUT);
    }
    else if(p_String.compare("userGift")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_GIFT_CLAIM);
    }
}
/// THIS CREATE UPDATE COIN EVENT
void MenuLayer::CreateUpdateCoinEvent()
{
    if(!m_bIsEventCreated)
    {
        m_bIsEventCreated = true;
        auto t_UpdateCoins = EventListenerCustom::create(EVENT_UPDATE_COINS, CC_CALLBACK_0(MenuLayer::UpdateCoins, this));
        Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_UpdateCoins, this);
    }
}
/// THIS SHOW TIMER
void MenuLayer::checkSaleTimer()
{
    // GET BEGIN TIME
    std::string strTime = UserDefault::getInstance()->getStringForKey(SALE_START_TIME);
    struct tm target;
    strptime(strTime.c_str(),"%c", &target);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    m_SaleStart = timelocal(&target);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    // THIS SET DAYLIGHT SET TIME STATUS UNKOWN
    target.tm_isdst = -1;
    //CCLOG("DST %d", target.tm_isdst);
    m_SaleStart = mktime(&target);
    //CCLOG("START TIME %s",ctime(&m_start));
#endif
    // GET END TIME
    time(&m_SaleCurrent);
    
    //CCLOG("CURRENT TIME %s",ctime(&m_Current));
    
    double diff1 = difftime(m_SaleCurrent, m_SaleStart);
    
    //CCLOG("TIME DIFFERENCE %lf",diff1);
    
    if(diff1>m_fSaleTimeDelay)
    {
        // UPDATE SALE AVAILABLE
        UserDefault::getInstance()->setBoolForKey(LIMITED_SALE_AVAILABLE, false);
        UserDefault::getInstance()->flush();
        m_MegaDealButton->setVisible(false);
    }
    
    m_fSaleTimer = m_fSaleTimeDelay-(int)diff1;
    
    if(diff1<0)
        diff1*=-1;
    
    // CHECK IF TIME_DIFF IS LESS THAN ASSIGN_TIME_DELAY THAN SHOW TIMER
    if(m_fSaleTimer>=0 && diff1>0)
    {
        UpdateLimitedSaleTimer();
    }
}
/// THIS UPDATE TIMER
void MenuLayer::UpdateLimitedSaleTimer()
{
    if(m_tSaleTimerLbl != NULL)
    {
        int tot_S = (int)m_fSaleTimer%60;
        int tot_M = (int)m_fSaleTimer/60;
        int minute = tot_M%60;
        int tot_H = (int)tot_M/60;
        int hour = tot_H%60;
        std::string  buffer = StringUtils::format(" %02d:%02d:%02d",hour,minute,tot_S) ;
        m_tSaleTimerLbl->setString(buffer);
        m_tSaleTimerLbl->setVisible(true);
    }
}
/// THIS CHECK FOR DAILY REWARD
void MenuLayer::CheckDailyRewardAvailability()
{
    float t_fTimeDelay = 24*60*60;
    time_t  m_start, m_Current;
    // GET BEGIN TIME
    std::string strTime = UserDefault::getInstance()->getStringForKey(REWARD_START_TIME);
    struct tm target;
    strptime(strTime.c_str(),"%c", &target);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    m_start = timelocal(&target);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    target.tm_isdst = -1;
    m_start = mktime(&target);
#endif
    
    // GET END TIME
    time(&m_Current);
    
    float diff1 = m_Current - m_start;
    if(diff1>t_fTimeDelay && diff1<=2*t_fTimeDelay)
    {
        // BONUS AVIALBLE TO SHOW
        me_bDailyRewardAvailable = true;
        UserDefault::getInstance()->setBoolForKey(DAILY_REWARD_AVAILABLE, me_bDailyRewardAvailable);
        UserDefault::getInstance()->flush();
    }
    else if (diff1>2*t_fTimeDelay)
    {
        // RESET BONUS LOOP
        me_bDailyRewardAvailable = true;
        UserDefault::getInstance()->setBoolForKey(DAILY_REWARD_AVAILABLE, me_bDailyRewardAvailable);
        UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, true);
        UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, false);
        UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, false);
        UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, false);
        UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, false);
        UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, false);
        UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, false);
        UserDefault::getInstance()->flush();
    }
    else
    {
        me_bDailyRewardAvailable = false;
        UserDefault::getInstance()->setBoolForKey(DAILY_REWARD_AVAILABLE, me_bDailyRewardAvailable);
        UserDefault::getInstance()->flush();
    }
}
///****************************KEYPAD FUNCTIONS********************/
void MenuLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event *event)
{
    
    
}
void MenuLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *unused_event)
{
    if(keyCode==EventKeyboard::KeyCode::KEY_BACK)
    {
        m_ptrPopUpManager->OnKeyButtonPressed();
    }
}
/// this check for exit popup game promotion data and popup promotion on daily basis
void MenuLayer::checkForExitPromotion()
{
    if(!UserDefault::getInstance()->getBoolForKey("exit_promo_app"))
    {
        UserDefault::getInstance()->setBoolForKey("EXIT_PROMO",true);
        UserDefault::getInstance()->setBoolForKey("exit_promo_app",true);
    }
    
    if(!UserDefault::getInstance()->getBoolForKey("popup_promo_app"))
    {
        UserDefault::getInstance()->setBoolForKey("popup_promo_app",true);
        UserDefault::getInstance()->setBoolForKey("PROMO_POPUP",true);
    }
    
    if(GameController::getInstance()->isDayHasPassed())
    {
        CCLOG("DAY_PASSED");
        UserDefault::getInstance()->setBoolForKey("EXIT_PROMO",true);
        UserDefault::getInstance()->setBoolForKey("PROMO_POPUP",true);
        GameController::getInstance()->setTodayAsLastDay();
    }else{
        UserDefault::getInstance()->setBoolForKey("EXIT_PROMO",false);
    }
    
    if(UserDefault::getInstance()->getBoolForKey("PROMO_POPUP")) {
        checkForGamePromotionData();
    }else{
        parsePromotionData("");
    }
    
    if(UserDefault::getInstance()->getBoolForKey("EXIT_PROMO")||m_bJsonError)
    {
        HttpRequest* request = new (std::nothrow) HttpRequest();
        request->setUrl(EXIT_PROMOTION_URL);
        request->setRequestType(HttpRequest::Type::GET);
        request->setResponseCallback(CC_CALLBACK_2(MenuLayer::httpDataSuccessCallback, this));
        HttpClient::getInstance()->send(request);
        httpRequest.push_back(request);
    }
    else
    {
        parseExitData("");
    }
}
/// success callback for exit popup promotion data
void MenuLayer::httpDataSuccessCallback(HttpClient* client, HttpResponse* response)
{
    
    if(response->getResponseCode()!=200||!response || !response->isSucceed())
    {
        GameController::getInstance()->setBoolForKey("ExitPromotionAvailable", false);
        return;
    }
    std::vector<char> *buffer = response->getResponseData();
    unsigned char *data = (unsigned char *)&(buffer->front());
    std::string jsonDataString(reinterpret_cast<char*>(data), buffer->size());
    
    FILE *m_File = NULL;
    std::string path = FileUtils::getInstance()->getWritablePath().append("ExitPromoData.json");
    //CCLOG("%s",path.c_str());
    m_File = fopen(path.c_str(), "w+");
    if (!m_File)
    {
        CCLOG("File Not Found");
        return;
    }
    
    fwrite(jsonDataString.c_str(), 1,strlen(jsonDataString.c_str()), m_File);
    fclose(m_File);
    
    parseExitData(jsonDataString);
}
/// parse exit popup app promotion data
void MenuLayer::parseExitData(std::string jsonStr)
{
    std::string fullPath = "";
    char *p_Buffer;
    if (jsonStr.empty())
    {
        fullPath = FileUtils::getInstance()->getWritablePath().append("ExitPromoData.json");
        p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(fullPath).c_str()) + sizeof(int) + 1];
        strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(fullPath).c_str());
        
        
        if (FileUtils::getInstance()->getStringFromFile(fullPath).empty()) {
            GameController::getInstance()->setBoolForKey("ExitPromotionAvailable", false);
            m_bJsonError = true;
            checkForExitPromotion();
            return;
        }
    }
    else
    {
        p_Buffer = new char[strlen(jsonStr.c_str()) + sizeof(int) + 1];
        strcpy(p_Buffer,jsonStr.c_str());
    }
    
    rapidjson::Document m_JsonDocument;
    m_JsonDocument.Parse<0>(p_Buffer);
    CCLOG("Full server response data : %s \n", jsonStr.c_str());
    
    if (m_JsonDocument.HasParseError())  // Print parse error
    {
        CCLOG("GetParseError %u\n",m_JsonDocument.GetParseError());
        checkForExitPromotion();
        return;
    }
    
    if(m_JsonDocument.HasMember("exit_promotion"))
    {
        const  rapidjson::Value& bValue = m_JsonDocument["exit_promotion"];
        if (!bValue.IsNull())
        {
            std::string appPackage = "";
            
            for(int index=0;index<bValue.Size();index++)
            {
                if(m_JsonDocument["exit_promotion"][index].HasMember("appPackage"))
                    appPackage = m_JsonDocument["exit_promotion"][index]["appPackage"].GetString();
                
                
                if(!InterfaceManagerInstance::getInstance()->getInterfaceManager()->appInstalledOrNot(appPackage))
                {
                    
                    if(m_JsonDocument["exit_promotion"][index].HasMember("title"))
                        GameController::getInstance()->m_strAppTitle = m_JsonDocument["exit_promotion"][index]["title"].GetString();
                    
                    if(m_JsonDocument["exit_promotion"][index].HasMember("desc"))
                        GameController::getInstance()->m_strDesc = m_JsonDocument["exit_promotion"][index]["desc"].GetString();
                    
                    if(m_JsonDocument["exit_promotion"][index].HasMember("url_app"))
                        GameController::getInstance()->m_strAppUrl = m_JsonDocument["exit_promotion"][index]["url_app"].GetString();
                    
                    if(m_JsonDocument["exit_promotion"][index].HasMember("url_image"))
                        GameController::getInstance()->m_appIconUrl = m_JsonDocument["exit_promotion"][index]["url_image"].GetString();
                    
                    if(GameController::getInstance()->getStringForKey("APP_PACKAGE")!=appPackage)
                    {
                        GameController::getInstance()->setStringForKey("APP_PACKAGE", appPackage);
                        
                        std::string IconStr = FileUtils::getInstance()->getWritablePath().append("ExitPromoIcon.png");
                        
                        FileUtils::getInstance()->removeFile(IconStr);
                        
                        downloadExitPromoGameIcon(0,GameController::getInstance()->m_appIconUrl);
                    }
                    else
                    {
                        GameController::getInstance()->setBoolForKey("ExitPromotionAvailable",true);
                    }
                    
                    break;
                }
            }
        }
    }
}
/// download exit popup game icon with url
void MenuLayer::downloadExitPromoGameIcon(int index,std::string url)
{
    HttpRequest* request = new (std::nothrow) HttpRequest();
    request->setUrl(url.c_str());
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(MenuLayer::onExitPhotoDownloaded, this));
    HttpClient::getInstance()->send(request);
    //request->release();
    httpRequest.push_back(request);
}
/// success callback for exit popup on icon download
void MenuLayer::onExitPhotoDownloaded(HttpClient *sender, HttpResponse *response)
{
    if (!response || !response->isSucceed()) {
        CCLOG("response failed! error buffer: %s", response->getErrorBuffer());
        downloadExitPromoGameIcon(0,GameController::getInstance()->m_appIconUrl);
        return;
    }
    std::vector<char> *buffer = response->getResponseData();
    Image *img = new Image();
    img->initWithImageData((const unsigned char *)&(buffer->front()), buffer->size());
    
    if (img != nullptr) {
        Texture2D *adTexture = new Texture2D();
        adTexture->initWithImage(img);
        FILE *m_File = NULL;
        std::string str = FileUtils::getInstance()->getWritablePath().append("ExitPromoIcon.png");
        
        //CCLOG("%s",path.c_str());
        m_File = fopen(str.c_str(), "w+");
        if (!m_File)
        {
            CCLOG("File Not Found");
            return;
        }
        
        fwrite(str.c_str(), 1,strlen(str.c_str()), m_File);
        fclose(m_File);
        img->saveToFile(str, true);
        
        GameController::getInstance()->setBoolForKey("ExitPromotionAvailable", true);
    }
    
}
/// THIS UPDATE  DATA on POUP's change
void MenuLayer::UpdateMenuData()
{
    switch (m_ptrPopUpManager->getPopUpType())
    {
        case POPUP_DAILY_MISSION:
        {
            checkForDailyMissionUpdates();
            checkForChallengeAndboardTut();
        }break;
        case POPUP_DAILY_REWARD:
        {
            m_bIsDailyReward = false;
            checkForChallengeAndboardTut();
        }break;
        case POPUP_ACHIEVEMENT:
        {
            checkForAchievementUpdates();
        }break;
        case POPUP_LANGUAGE:
        {
            StatsManager::getInstance()->InitializeLaunchData();
            LanguageTranslator::getInstance()->LoadTranslationFile();
            
            GameController::getInstance()->updateUserDataToFireBase();
            GameController::getInstance()->getLeaderboardData();
            GameController::getInstance()->updateUserDataInGame(me_strUserDataJson);
            
        }break;
        default:
            break;
    }
}
/// check for  DAILY MISSION button tags update on achivement's complete
void MenuLayer::checkForDailyMissionUpdates()
{
    if(m_DailyEventBtn)
    {
        Sprite* SprTask = (Sprite*)m_DailyEventBtn->getChildByName("tag");
        if(!UserDefault::getInstance()->getBoolForKey(NEW_TASK))
        {
            SprTask->setVisible(false);
        }
        
        if(DailyMissionManager::getInstance()->checkAllDailyMissionsClaimed())
        {
            SprTask->setTexture("Task_Completed.png");
            SprTask->setVisible(true);
            return;
        }
        
        if(DailyMissionManager::getInstance()->getCountOfDailyMissionsToClaim()>=1)
        {
            SprTask->setTexture("Claim.png");
            SprTask->setVisible(true);
        }
        else if(!UserDefault::getInstance()->getBoolForKey(NEW_TASK))
        {
            SprTask->setVisible(false);
        }
        
    }
}
/// check for  achievement button tags update on achivement's complete
void MenuLayer::checkForAchievementUpdates()
{
    if(m_AchievementButton)
    {
        Sprite* SprTask = (Sprite*)m_AchievementButton->getChildByName("tag");
        
        if(AchievementManager::getInstance()->checkAllAchievementsClaimed())
        {
            SprTask->setTexture("Task_Completed.png");
            SprTask->setVisible(true);
            return;
        }
        
        if(AchievementManager::getInstance()->getCountOfAchievementsToClaim()>=1)
        {
            SprTask->setTexture("Claim.png");
            SprTask->setVisible(true);
        }
        else
        {
            SprTask->setVisible(false);
        }
        
    }
}
///  check for popup promotion data
void MenuLayer::checkForGamePromotionData()
{
    HttpRequest* request = new (std::nothrow) HttpRequest();
    request->setUrl(POPUP_PROMOTION_URL);
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(MenuLayer::httpPromotionSuccessCallback, this));
    HttpClient::getInstance()->send(request);
    request->setTag("Promojson");
    httpRequest.push_back(request);
    
}
/// on success callback for promotion data http  request
void MenuLayer::httpPromotionSuccessCallback(HttpClient* client, HttpResponse* response)
{
    
    if(response->getResponseCode()!=200||!response || !response->isSucceed())
    {
        GameController::getInstance()->setBoolForKey("PopUpPromotionAvailable", false);
        return;
    }
    if(strcmp(response->getHttpRequest()->getTag(),"popupPromotion_ICON")==0)
    {
        std::vector<char> *buffer = response->getResponseData();
        Image *img = new Image();
        img->initWithImageData((const unsigned char *)&(buffer->front()), buffer->size());
        
        if(img != nullptr)
        {
            Texture2D *adTexture = new Texture2D();
            adTexture->initWithImage(img);
            FILE *m_File = NULL;
            std::string str = FileUtils::getInstance()->getWritablePath().append("popupPromoIcon.png");
            
            //CCLOG("%s",path.c_str());
            m_File = fopen(str.c_str(), "w+");
            if (!m_File)
            {
                CCLOG("File Not Found");
                return;
            }
            
            fwrite(str.c_str(), 1,strlen(str.c_str()), m_File);
            fclose(m_File);
            img->saveToFile(str, true);
            GameController::getInstance()->setBoolForKey("PopUpPromotionAvailable", true);
        }
    }
    else
    {
        std::vector<char> *buffer = response->getResponseData();
        unsigned char *data = (unsigned char *)&(buffer->front());
        std::string jsonDataString(reinterpret_cast<char*>(data), buffer->size());
        
        FILE *m_File = NULL;
        std::string path = FileUtils::getInstance()->getWritablePath().append("promotionData.json");
        //CCLOG("%s",path.c_str());
        m_File = fopen(path.c_str(), "w+");
        if (!m_File)
        {
            CCLOG("File Not Found");
            return;
        }
        
        fwrite(jsonDataString.c_str(), 1,strlen(jsonDataString.c_str()), m_File);
        fclose(m_File);
        
        parsePromotionData(jsonDataString);
    }
}
/// parse popup promotion data
void MenuLayer::parsePromotionData(std::string jsonStr)
{
    std::string fullPath = "";
    char *p_Buffer;
    if (jsonStr.empty())
    {
        fullPath = FileUtils::getInstance()->getWritablePath().append("promotionData.json");
        p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(fullPath).c_str()) + sizeof(int) + 1];
        strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(fullPath).c_str());
        
        
        if (FileUtils::getInstance()->getStringFromFile(fullPath).empty()) {
            GameController::getInstance()->setBoolForKey("PopUpPromotionAvailable", false);
            return;
        }
    }
    else
    {
        p_Buffer = new char[strlen(jsonStr.c_str()) + sizeof(int) + 1];
        strcpy(p_Buffer,jsonStr.c_str());
    }
    
    rapidjson::Document m_JsonDocument;
    m_JsonDocument.Parse<0>(p_Buffer);
    
    if (m_JsonDocument.HasParseError())  // Print parse error
    {
        CCLOG("GetParseError %u\n",m_JsonDocument.GetParseError());
        GameController::getInstance()->setBoolForKey("PopUpPromotionAvailable", false);
        return;
    }
    
    if(m_JsonDocument.HasMember("promotion_array"))
    {
        const  rapidjson::Value& bValue = m_JsonDocument["promotion_array"];
        if (!bValue.IsNull())
        {
            std::string appPackage = "";
            
            for(int index=0;index<bValue.Size();index++)
            {
                if(m_JsonDocument["promotion_array"][index].HasMember("appPackage"))
                    appPackage = m_JsonDocument["promotion_array"][index]["appPackage"].GetString();
                
                
                if(!InterfaceManagerInstance::getInstance()->getInterfaceManager()->appInstalledOrNot(appPackage))
                {
                    
                    if(m_JsonDocument["promotion_array"][index].HasMember("title"))
                        GameController::getInstance()->m_PopupAppTitle = m_JsonDocument["promotion_array"][index]["title"].GetString();
                    
                    if(m_JsonDocument["promotion_array"][index].HasMember("desc"))
                        GameController::getInstance()->m_PopupAppDesc = m_JsonDocument["promotion_array"][index]["desc"].GetString();
                    
                    if(m_JsonDocument["promotion_array"][index].HasMember("url_app"))
                        GameController::getInstance()->m_PopupAppUrl = m_JsonDocument["promotion_array"][index]["url_app"].GetString();
                    
                    if(m_JsonDocument["promotion_array"][index].HasMember("url_image"))
                        GameController::getInstance()->m_PopupIconUrl = m_JsonDocument["promotion_array"][index]["url_image"].GetString();
                    
                    if(GameController::getInstance()->getStringForKey("Promo_package")!=appPackage)
                    {
                        GameController::getInstance()->setStringForKey("Promo_package", appPackage);
                        
                        std::string IconStr = FileUtils::getInstance()->getWritablePath().append("popupPromoIcon.png");
                        
                        FileUtils::getInstance()->removeFile(IconStr);
                        
                        HttpRequest* request = new (std::nothrow) HttpRequest();
                        request->setUrl(GameController::getInstance()->m_PopupIconUrl.c_str());
                        request->setRequestType(HttpRequest::Type::GET);
                        request->setResponseCallback(CC_CALLBACK_2(MenuLayer::httpPromotionSuccessCallback, this));
                        request->setTag("popupPromotion_ICON");
                        HttpClient::getInstance()->send(request);
                        httpRequest.push_back(request);
                    }
                    else
                    {
                        GameController::getInstance()->setBoolForKey("PopUpPromotionAvailable",true);
                    }
                    break;
                }else{
                    GameController::getInstance()->setBoolForKey("PopUpPromotionAvailable",false);
                }
            }
        }
    }
}
/// show app promotion popup
void MenuLayer::showPromotionPopUp()
{
    if(UserDefault::getInstance()->getBoolForKey("PROMO_POPUP")&&GameController::getInstance()->getBoolForKey("GAME_ENTER")&&GameController::getInstance()->getBoolForKey("PopUpPromotionAvailable"))
    {
        UserDefault::getInstance()->setBoolForKey("PROMO_POPUP",false);
        LoadOtherPopUpWithDelay(0.4f,"PromotionPopUp");
        GameController::getInstance()->setBoolForKey("GAME_ENTER",false);
    }
}
void MenuLayer::getLevelFinishMoreGames(std::string url,std::string tag)
{
    if(GameController::getInstance()->getBoolForKey("isLevelfinishGamesLoaded") == true)
    {
        return;
    }
    
    CCLOG("\n Url-->%s tag-->%s\n",url.c_str(),tag.c_str());
    HttpRequest *request = new (std::nothrow) HttpRequest();
    request->setUrl(url);
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(MenuLayer::onJsonFetchCompletd, this));
    request->setTag(tag);
    HttpClient::getInstance()->send(request);
    httpRequest.push_back(request);
}
void MenuLayer::onJsonFetchCompletd(HttpClient* client, HttpResponse* response)
{
    if (!response || !response->isSucceed())
    {
        if(strcmp(response->getHttpRequest()->getTag(),"Json")==0)
        {
            getLevelFinishMoreGames(LEVEL_FINISH_GAMES_URL,"Json");
        }
        CCLOG("response failed! error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    if(strcmp(response->getHttpRequest()->getTag(),"Json")==0)
    {
        std::vector<char> *buffer = response->getResponseData();
        unsigned char *data = (unsigned char *)&(buffer->front());
        std::string jsonDataString(reinterpret_cast<char*>(data), buffer->size());
        
        rapidjson::Document doc;
        doc.Parse<0>(jsonDataString.c_str());
        
        if (doc.HasMember("isMoreGamesVisible")) {
            GameController::getInstance()->isLevelCompGamesVisible = doc["isMoreGamesVisible"].GetBool();
            if (GameController::getInstance()->isLevelCompGamesVisible == true)
            {
                GameController::getInstance()->t_LevelmoreGameAppUrl.clear();
                GameController::getInstance()->t_levelmoreGameImageUrl.clear();
            }
        }
        
        if (doc.HasMember("appList")) {
            const  rapidjson::Value& b = doc["appList"];
            if (!b.IsNull())
            {
                CCLOG("appList size = %d", (int)b.Size());
                for (rapidjson::SizeType j = 0; j < (int)b.Size(); j++)
                {
                    const  rapidjson::Value& c = b[j];
                    if (c.HasMember("isVisible"))
                    {
                        if (c["isVisible"].GetBool() == true)
                        {
                            if (c.HasMember("appUrl"))
                                GameController::getInstance()->t_LevelmoreGameAppUrl.push_back(c["appUrl"].GetString());
                            
                            if (c.HasMember("imageUrl"))
                                GameController::getInstance()->t_levelmoreGameImageUrl.push_back(c["imageUrl"].GetString());
                        }
                    }
                }
            }
        }
        
        if (GameController::getInstance()->isLevelCompGamesVisible == true)
        {
            
            for (int p = 0;p < (int)GameController::getInstance()->t_levelmoreGameImageUrl.size();p++)
            {
                std::string str = FileUtils::getInstance()->getWritablePath();
                str += StringUtils::format("gamesIcon%d.png", p);
                if (FileUtils::getInstance()->isFileExist(str) == false)
                {
                    std::string iconUrl = GameController::getInstance()->t_levelmoreGameImageUrl.at(p);
                    getLevelFinishMoreGames(iconUrl,to_string(p+1));
                }
            }
        }
    }
    else
    {
        std::vector<char> *buffer = response->getResponseData();
        Image *img = new Image();
        try
        {
            if(img->initWithImageData(reinterpret_cast<const unsigned char*>(&(buffer->front())), buffer->size()))
            {
                
            }
            else
                img = nullptr;
        }
        catch(const std::exception& e){
            CCLOG(" ERROR%s",e.what());
        }
        
        if(img != nullptr)
        {
            FILE *m_File = NULL;
            std::string index = response->getHttpRequest()->getTag();
            
            std::string size = to_string(GameController::getInstance()->t_levelmoreGameImageUrl.size());
            if(index==size) {
                GameController::getInstance()->setBoolForKey("isLevelfinishGamesLoaded", true);
            }
            
            
            std::string str = FileUtils::getInstance()->getWritablePath().append("gamesIcon").append(index).append(".png");
            CCLOG("\n Icon-->%s\n",str.c_str());
            m_File = fopen(str.c_str(), "w+");
            if (!m_File)
            {
                CCLOG("File Not Found");
                return;
            }
            
            fwrite(str.c_str(), 1,strlen(str.c_str()), m_File);
            fclose(m_File);
            img->saveToFile(str, true);
        }
    }
}
/// Facebook Login CallBack
void MenuLayer::FacebookLoginCallback(Ref *sender)
{
    MenuItemSprite *tag = (MenuItemSprite*)sender;
    
    if (strcmp(tag->getName().c_str(),"Info")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_FB_INFO);
        return;
    }
    
    if(InterfaceManagerInstance::getInstance()->getInterfaceManager()->isNetworkAvailable())
    {
        if(!FacebookManager::getInstance()->isLoggedIn())
        {
            FacebookManager::getInstance()->login();
            m_FacebookButton->setEnabled(false);
            m_FBInfoButton->setEnabled(false);
            InterfaceManagerInstance::getInstance()->getInterfaceManager()->getLanguageDataUserIds();
        }
    }
    else
    {
        std::string strtrans = LanguageTranslator::getInstance()->getTranslatorStringWithTag("NoInternet");
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage(strtrans);
    }
}
void MenuLayer::signInWithApple(Ref *sender)
{
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->signInWithApple();
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->getLanguageDataUserIds();
}
void MenuLayer::AppleSignInSuccess()
{
    if(m_FacebookButton!=NULL && signInApple!=NULL)
    {
        log("Name-->%s",GameController::getInstance()->getStringForKey("AppleIdName").c_str());
        log("AppleId-->%s",GameController::getInstance()->getStringForKey("AppleId").c_str());
        
        m_FacebookButton->setVisible(false);
        m_FBInfoButton->setVisible(false);
        
        signInApple->setVisible(false);
        GameController::getInstance()->getUserLanguageData();
    }
}
void MenuLayer::AppleSignInFailure()
{
    
}

void MenuLayer::FbLoginSuccess()
{
    if(m_FacebookButton!=NULL)
    {
        std::string fbId = GameController::getInstance()->getStringForKey("facebookID");
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->setUserFbId(fbId);
        log("Name-->%s",FacebookManager::getInstance()->facebookName.c_str());
        log("FbId-->%s",FacebookManager::getInstance()->facebookID.c_str());
        
        m_FacebookButton->setVisible(false);
        m_FBInfoButton->setVisible(false);
        
        if(signInApple)
            signInApple->setVisible(false);
        
        if(!GameController::getInstance()->checkIsFbIDAlreadyClaimedReward(GameController::getInstance()->getStringForKey("facebookID")))
        {
            me_iCoinCount += 2000;
            UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
            UpdateCoins();
        }
        
        GameController::getInstance()->getUserLanguageData();
    }
}
void MenuLayer::FbLoginFailure()
{
    m_FacebookButton->setEnabled(true);
    m_FBInfoButton->setEnabled(true);
}
void MenuLayer::leaderboardBtnCallback()
{
    if(InterfaceManagerInstance::getInstance()->getInterfaceManager()->isNetworkAvailable())
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_GAME_LEADERBOARD);
        
        if (m_bIsTutorial) {
            m_bIsTutorial = false;
            UserDefault::getInstance()->setBoolForKey("leaderboard", true);
            
            if (m_TutManager) {
                m_TutManager->removeTuorial();
            }
        }
    }
    else
    {
        std::string strtrans = LanguageTranslator::getInstance()->getTranslatorStringWithTag("NoInternet");
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage(strtrans);
    }
}
void MenuLayer::rateButtonCallBack()
{
    setEnableButtons(false);
    m_ptrPopUpManager->LoadPopUp(POPUP_RATE_GAME);
    //#FFC107
    //    InterfaceManagerInstance::getInstance()->getInterfaceManager()->OpenInStore();
}
void MenuLayer::shareButtonCallBack()
{
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShareGame();
}
void MenuLayer::checkForGameUpdate()
{
    // GET BEGIN TIME
    double m_fTimeDelay = 72*60*60;
    std::string strTime = UserDefault::getInstance()->getStringForKey(APP_UPDATE_TIME);
    struct tm target;
    strptime(strTime.c_str(),"%c", &target);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    m_start = timelocal(&target);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    // THIS SET DAYLIGHT SET TIME STATUS UNKOWN
    target.tm_isdst = -1;
    //CCLOG("DST %d", target.tm_isdst);
    m_start = mktime(&target);
    //CCLOG("START TIME %s",ctime(&m_start));
#endif
    // GET END TIME
    time(&m_Current);
    //CCLOG("CURRENT TIME %s",ctime(&m_Current));
    
    double diff1 = difftime(m_Current, m_start);
    
    CCLOG("TIME DIFFERENCE %lf",diff1);
    
    if(diff1>=m_fTimeDelay)
    {
        time_t  m_StartTime;
        time(&m_StartTime);
        UserDefault::getInstance()->setStringForKey(APP_UPDATE_TIME, ctime(&m_StartTime));
        
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->checkForGameUpdate();
    }
    
}
void MenuLayer::openNotificationAction()
{
    int notifyType = UserDefault::getInstance()->getIntegerForKey("openNotification",0);
    
    switch (notifyType)
    {
        case 0:
        {
            UserDefault::getInstance()->setBoolForKey("Notification", false);
        }break;
        case 1:
        {
            UserDefault::getInstance()->setBoolForKey("Notification", false);
            //            InterfaceManagerInstance::getInstance()->getInterfaceManager()->OpenUrl("");
        }break;
        default:
            break;
    }
    
}
void MenuLayer::checkforLeaderBoardTutorial(int type)
{
    if(m_bIsDailyReward) {
        return;
    }
    if(!UserDefault::getInstance()->getBoolForKey("dailyTask", false)&&type==1)
    {
        m_bIsTutorial = true;
        //        LoadOtherPopUpWithDelay(0.3f,"buttonTut");
        Sprite *sprTouch = Sprite::create("Daily-Event.png");
        sprTouch->setScale(m_fScale);
        sprTouch->setPosition(m_DailyEventBtn->getPosition());
        m_TutManager->addChild(sprTouch,4);
        
        time_t t = time(0);
        tm *today = localtime(&t);
        int month = today->tm_mon;
        int day = today->tm_mday;
        
        std::string monthname = GameController::getInstance()->getMonthName(month);
        Label *monthLabel = Label::createWithTTF(monthname, FONT_NAME,25);
        monthLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        monthLabel->setPosition(Vec2(sprTouch->getContentSize().width/2,sprTouch->getContentSize().height-monthLabel->getContentSize().height));
        monthLabel->setColor(Color3B(254,253,56));
        monthLabel->setName("month");
        sprTouch->addChild(monthLabel,1);
        
        Label *dayLabel = Label::createWithTTF(StringUtils::format("%d",day), FONT_NAME,40);
        dayLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        dayLabel->setPosition(Vec2(sprTouch->getContentSize().width/2,sprTouch->getContentSize().height/2-5));
        dayLabel->setColor(Color3B(72,52,26));
        dayLabel->setName("day");
        sprTouch->addChild(dayLabel,1);
        
        Point Pos  = Vec2(m_DailyEventBtn->getPositionX(),m_DailyEventBtn->getBoundingBox().getMinY());
        m_TutManager->showTutorialForDailyTaskButton(Pos);
    }
    else if(!UserDefault::getInstance()->getBoolForKey("leaderboard", false)&& type ==2)
    {
        if(FacebookManager::getInstance()->isLoggedIn())
        {
            return;
        }
        m_bIsTutorial = true;
        
        Sprite *sprTouch = Sprite::create("Leaderboard_button.png");
        sprTouch->setPosition(m_LeaderBoardButton->getPosition());
        m_TutManager->addChild(sprTouch,4);
        
        Point Pos  = Vec2(m_LeaderBoardButton->getPositionX(),m_LeaderBoardButton->getBoundingBox().getMaxY());
        m_TutManager->showTutorialForLeaderboard(Pos);
    }
    else if(UserDefault::getInstance()->getBoolForKey(LIMITED_SALE_AVAILABLE)&&!UserDefault::getInstance()->getBoolForKey("SALE_POPUP_SHOWN"))
    {
        UserDefault::getInstance()->setBoolForKey("FROM_LAUNCH",false);
        LoadOtherPopUpWithDelay(0.3f,"LimitedSale");
        UserDefault::getInstance()->setBoolForKey("SALE_POPUP_SHOWN",true);
    }
    else if(!me_bDailyRewardAvailable)
    {
        UserDefault::getInstance()->setBoolForKey("FROM_LAUNCH",false);
        showPromotionPopUp();
    }
    
    
}
void MenuLayer::userRewardsBasedPrediction()
{
    UserDefault::getInstance()->setBoolForKey("UserPredict",false);
    
    int notifyType = UserDefault::getInstance()->getIntegerForKey(USER_PREDICT_TYPE,0);
    switch (notifyType)
    {
        case 1:
        {
            //            "churn"
            LoadOtherPopUpWithDelay(0.5f,"userGift");
        }break;
        case 2:
        {
            //            "purchase"
            time_t m_StartTime;
            time(&m_StartTime);
            UserDefault::getInstance()->setStringForKey(SALE_START_TIME, ctime(&m_StartTime));
            UserDefault::getInstance()->setBoolForKey(LIMITED_SALE_AVAILABLE,true);
            LoadOtherPopUpWithDelay(0.3f,"LimitedSale");
        }break;
        default:
            break;
    }
}
/// DESTRUCTOR
MenuLayer::~MenuLayer()
{
    
    
}

void MenuLayer::onExit()
{
    
    if(m_bIsEventCreated)
    {
        Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_UPDATE_COINS);
    }
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(FB_LOGIN_SUCCESS);
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(FB_LOGIN_FAILURE);
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(APPLE_LOGIN_SUCCESS);
    
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(APPLE_LOGIN_FAILURE);
    
    
    for (int i = 0;i < (int)httpRequest.size();i++)
    {
        httpRequest.at(i)->setResponseCallback(nullptr);
        httpRequest.at(i)->release();
    }
    
    if(m_ptrPopUpManager)
    {
        this->removeChild(m_ptrPopUpManager,true);
        m_ptrPopUpManager = NULL;
    }
    
    this->removeAllChildrenWithCleanup(true);
    Layer::onExit();
}
