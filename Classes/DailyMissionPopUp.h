//
//  DailyMissionPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 24/03/20.
//

#ifndef DailyMissionPopUp_hpp
#define DailyMissionPopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class DailyMissionPopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;

    MenuItemSprite *t_ClaimButton;
    float m_fTimer;

    time_t          m_MissionStart, m_MissionCurrent, m_start, m_Current;

    Label *t_mMissionTimer;
    
    float           m_fMissionTimer;
    float           m_fMissionTimeDelay;
    
    std::string  m_MissionStr;

public:
    DailyMissionPopUp(std::function<void()> func);
    ~DailyMissionPopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);

    void claimButtonCallBack(Ref *sender);
    
    std::string getIconsForMissions(std::string MissionTag);
    
    void coinAddAnimation(Point t_Position);
    
    void removeSprite(Ref *_sender);
    
    void coinAnimationDoneClosePopUp();
    
    void OnUpdateFrame(float p_fDeltaTime);
    
    void UpdateTimer();
    
    void showdayTimer();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    std::string getMissionNum(std::string str);
};

#endif /* DailyMissionPopUp_hpp */
