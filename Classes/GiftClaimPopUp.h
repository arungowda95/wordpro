//
//  GiftClaimPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 23/04/20.
//

#ifndef GiftClaimPopUp_h
#define GiftClaimPopUp_h

#include <stdio.h>
#include "PopUp.h"

class GiftClaimPopUp:public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;
    
    MenuItemSprite *t_ClaimButton;
    
    float Posy;
    int m_tCoinsCount;
public:
     GiftClaimPopUp(std::function<void()> func);
    ~GiftClaimPopUp();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void OnButtonPressed(Ref *p_Sender);
    
    void coinAddAnimation();

    void removeSprite(Ref *_sender);
        
    void claimButtonPressed();
    
    void coinAnimationDoneClosePopUp();
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
};

#endif /* GiftClaimPopUp_h */
