//
//  StatsManager.cpp
//  wordPro-mobile
//
//  Created by id on 01/04/20.
//

#include "StatsManager.h"
#include "GameConstants.h"

using namespace rapidjson;

// Implement Achievement Manager
StatsManager* StatsManager::m_PtrStatsManager = NULL;
// CONSTRUCTOR
StatsManager::StatsManager()
{
    
}
// Return Stats Manager
StatsManager* StatsManager::getInstance()
{
    if(!m_PtrStatsManager)
    {
        m_PtrStatsManager = new StatsManager();
    }
    return m_PtrStatsManager;
}
StatsManager::~StatsManager()
{
    
}
void StatsManager::setCurrentCountry(std::string Loc)
{
    
   std::string str = StringUtils::format(COUNTRY,me_Language.c_str());
   UserDefault::getInstance()->setStringForKey(str.c_str(), Loc);
    
   m_strCurCountry = UserDefault::getInstance()->getStringForKey(str.c_str());
}
std::string StatsManager::getCurrentCountry()
{
    std::string str = StringUtils::format(COUNTRY,me_Language.c_str());
 
    m_strCurCountry = UserDefault::getInstance()->getStringForKey(str.c_str());
    return m_strCurCountry;
}

void StatsManager::setNextUnlockCountry(std::string Loc)
{
   std::string str = StringUtils::format(NEXT_COUNTRY,me_Language.c_str());
   UserDefault::getInstance()->setStringForKey(str.c_str(), Loc);
    
   m_strCurCountry = UserDefault::getInstance()->getStringForKey(str.c_str());
}
std::string StatsManager::getNextUnlockCountry()
{
    std::string str = StringUtils::format(NEXT_COUNTRY,me_Language.c_str());
 
    m_strCurCountry = UserDefault::getInstance()->getStringForKey(str.c_str());
    return m_strCurCountry;
}

void StatsManager::setCurrentState(std::string Loc)
{
    m_strCurState = Loc;
}
std::string StatsManager::getCurrentState()
{
    return m_strCurState;
}
void StatsManager::setLocations(std::string prevLoc,std::string curLoc,std::string nextLoc)
{
    m_strPrevLocation = prevLoc;
    m_strCurLocation = curLoc;
    m_strNxtLocation = nextLoc;
}
std::string StatsManager::getPrevLocation()
{
    return m_strPrevLocation;
}
std::string StatsManager::getCurLocation()
{
    return m_strCurLocation;
}
#pragma mark Bonus Words
void StatsManager::updateLangBonusWordsCount(int count)
{
    std::string str = StringUtils::format(BonusWords,me_Language.c_str());
    int totcount = UserDefault::getInstance()->getIntegerForKey(str.c_str());
    
    if(count==0)
    {
        count = totcount - 10;
        UserDefault::getInstance()->setIntegerForKey(str.c_str(),count);
        UserDefault::getInstance()->flush();
        return;
    }
    
    UserDefault::getInstance()->setIntegerForKey(str.c_str(),UserDefault::getInstance()->getIntegerForKey(str.c_str())+1);
    UserDefault::getInstance()->flush();
}
int StatsManager::getLangBonusWordsCount()
{
    std::string str = StringUtils::format(BonusWords,me_Language.c_str());
    int totcount = UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();
    return totcount;
}
#pragma mark Lang Level Count
void StatsManager::updateLangPlayedlevelCount(int count,bool isUpdate)
{
    std::string str = StringUtils::format(LEVEL_NUM,me_Language.c_str());
    UserDefault::getInstance()->getIntegerForKey(str.c_str());
    
    if(isUpdate)
    {
        UserDefault::getInstance()->setIntegerForKey(str.c_str(),count);
    }
    else
    {
        UserDefault::getInstance()->setIntegerForKey(str.c_str(),UserDefault::getInstance()->getIntegerForKey(str.c_str())+1);
    }
    
    UserDefault::getInstance()->flush();
}
int StatsManager::getLangPlayedlevelCount()
{
    std::string str = StringUtils::format(LEVEL_NUM,me_Language.c_str());
    int totcount = UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();
    return totcount;
}
std::string StatsManager::getNxtLocation()
{
    return m_strNxtLocation;
}

int StatsManager::getTotalLevelCountForCountries(std::string countryname)
{
    std::string str = StringUtils::format(CountriesTotLevelCount,countryname.c_str(),me_Language.c_str());
    int count =UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();

    return count;
}
void StatsManager::updatePlayedLevelCountForCountries(std::string countryname, int count)
{
    std::string str = StringUtils::format(CountriesPlayedLevelCount,countryname.c_str(),me_Language.c_str());
    UserDefault::getInstance()->setIntegerForKey(str.c_str(),UserDefault::getInstance()->getIntegerForKey(str.c_str())+1);
    count = UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();
}
int StatsManager::getPlayedLevelCountForCountries(std::string countryname)
{
    std::string str = StringUtils::format(CountriesPlayedLevelCount,countryname.c_str(),me_Language.c_str());
    int count =UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();

    return count;
}

void StatsManager::updateIsCountryPlayed(std::string countryname)
{
    std::string str = StringUtils::format(IsCountryPlayed,countryname.c_str(),me_Language.c_str());
    UserDefault::getInstance()->setBoolForKey(str.c_str(),true);
    UserDefault::getInstance()->flush();
}
bool StatsManager::checkAllCountriesPlayed()
{
    bool complete = true;
    for(auto strcountry : m_CountriesArray)
    {
        if(!checkIsCountryPlayed(strcountry))
        {
            complete = false;
            break;
        }
    }
    return complete;
}
bool StatsManager::checkIsCountryPlayed(std::string countryname)
{
    std::string str = StringUtils::format(IsCountryPlayed,countryname.c_str(),me_Language.c_str());
    bool Isplayed =UserDefault::getInstance()->getBoolForKey(str.c_str());
    UserDefault::getInstance()->flush();

    return Isplayed;
}

 void StatsManager::setplayedCountriesArray(std::vector<std::string> playedArr)
{
     m_PlayedCountries = playedArr;
}

std::vector<std::string> StatsManager::getplayedCountriesArray()
{
    return m_PlayedCountries;
}

#pragma mark initialize lang LevelCount
// THIS RESET ALL ACTIVE SAVE GAMES OF PUZZLE
void StatsManager::InitializeLevelCountForLanguages()
{
    
}
// THIS RESET ALL ACTIVE SAVE GAMES OF PUZZLE
void StatsManager::InitialUnlockedCountryForLanguages()
{
    
    std::string india = "India";
    std::string other = "America";
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_ENGLISH).c_str(),other);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_KANNADA).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_HINDI).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_TELUGU).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_MALAYALAM).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_MARATHI).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_ODIA).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_TAMIL).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_BANGLA).c_str(),india);
    UserDefault::getInstance()->setStringForKey(StringUtils::format(CountriesUnlocked,LANG_GUJARATI).c_str(),india);
    UserDefault::getInstance()->flush();
    
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_ENGLISH).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_KANNADA).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_HINDI).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_TELUGU).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_MALAYALAM).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_MARATHI).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_ODIA).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_TAMIL).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_BANGLA).c_str(),1);
    UserDefault::getInstance()->setIntegerForKey(StringUtils::format(CountriesUnlockedCount,LANG_GUJARATI).c_str(),1);
    UserDefault::getInstance()->flush();
}
int StatsManager::getLangUnlockedCountryCount()
{
    std::string str = StringUtils::format(CountriesUnlockedCount,me_Language.c_str());
    int totcount = UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();
    return totcount;
}

void StatsManager::LoadLevelMapData()
{
    
    std::string country = "India";
    std::string t_fullPath = FileUtils::getInstance()->fullPathForFilename("Regions_Data.json");
     
     char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str()) + sizeof(int) + 1];
     strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(t_fullPath).c_str());
     
     rapidjson::Document t_Regiondoc;
     t_Regiondoc.Parse<kParseStopWhenDoneFlag>(p_Buffer);
     
     if(t_Regiondoc.HasParseError()) // Print parse error
     {
         CCLOG("GetParseError %u\n",t_Regiondoc.GetParseError());
        delete [] p_Buffer;
        p_Buffer = NULL;
        
        return;
     }
     std::vector<std::string> t_StatesArray;
     if(!t_Regiondoc.IsNull())
     {
         if(t_Regiondoc.HasMember("Countries"))
         {
             if(t_Regiondoc["Countries"].IsArray())
             {
                 rapidjson::SizeType size = t_Regiondoc["Countries"].Size();
                 for(int index = 0; index<size; index++)
                 {
                     std::string ContName = t_Regiondoc["Countries"][index].GetString();
                     m_CountriesArray.push_back(ContName);
                 }
             }
         }
         if(t_Regiondoc.HasMember("Cntry_States"))
         {
             for(int index = 0; index<m_CountriesArray.size(); index++)
             {
                 std::string country = m_CountriesArray.at(index);

                 int countrylevelsize = (int)t_Regiondoc["Cntry_States"][country.c_str()].Size();
                 
                 if(country=="India")
                 {
                     countrylevelsize = countrylevelsize*5;
                 }
                 m_CountrieslevelCntMap[country] = countrylevelsize;
             }
         }
     }
    initializeTotLevelCountForCountries();
//    setTotalGiftCountInCountriesForLanguages();
    
}
void StatsManager::initializeTotLevelCountForCountries()
{
    for(int index = 0; index<m_CountriesArray.size(); index++)
    {
        std::string countryname = m_CountriesArray.at(index);
        int totcount = m_CountrieslevelCntMap.at(countryname);
        
        for(int i=0; i<10; i++)
        {
            std::string strLanguage = LangStrArr[i];
            {
                std::string str = StringUtils::format(CountriesTotLevelCount,countryname.c_str(),strLanguage.c_str());
                UserDefault::getInstance()->setIntegerForKey(str.c_str(),totcount);
            }
            
            //setTotalGiftCountInCountriesForLanguages
            {
                int count = totcount/5;
            
                std::string str = StringUtils::format(CountriesTotGiftsCount,countryname.c_str(),strLanguage.c_str());
                UserDefault::getInstance()->setIntegerForKey(str.c_str(),count);
                int count1 = UserDefault::getInstance()->getIntegerForKey(str.c_str());
                printf("\n setTotalGiftCountInCountriesForLanguages ->%d\n",count1);
            }
        }
//        setTotalLevelCountForCountries(countryname, count);
    }
    UserDefault::getInstance()->flush();

}
void StatsManager::InitializeLaunchData()
{
        if(!UserDefault::getInstance()->getBoolForKey("once_for_data"))
        {
            UserDefault::getInstance()->setBoolForKey("once_for_data",true);
            
            initialLevelCountForCountriesforLanguages();
    //        InitializeIsCountryPlayed();
    //        initialGiftUnlockCountForCountriesInLanguages();
    //        initialGiftClaimedStateForCountriesInLanguages();
        }
}

void StatsManager::initialLevelCountForCountriesforLanguages()
{

    for(int index = 0; index<m_CountriesArray.size(); index++)
    {
        std::string strtcntry = m_CountriesArray.at(index);
        for(int i=0; i<10; i++)
        {
            std::string strLanguage = LangStrArr[i];
            {
                std::string str = StringUtils::format(CountriesPlayedLevelCount,strtcntry.c_str(),strLanguage.c_str());
                UserDefault::getInstance()->setIntegerForKey(str.c_str(),0);
            }
            
            //InitializeIsCountryPlayed
            {
                std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),false);
            }
            
            //initialGiftUnlockCountForCountriesInLanguages
            {
                std::string str1 = StringUtils::format(CountriesGiftsUnlockCount,strtcntry.c_str(),strLanguage.c_str());
                UserDefault::getInstance()->setIntegerForKey(str1.c_str(),0);
            }
            
            {// initialGiftClaimedStateForCountriesInLanguages
                    int count = m_CountrieslevelCntMap.at(strtcntry);
                    count = count/5;
                    for(int j=0; j<count; j++)
                    {
                        std::string str = StringUtils::format(CountriesGiftsClaimed,strtcntry.c_str(),strLanguage.c_str(),j);
                        UserDefault::getInstance()->setBoolForKey(str.c_str(),false);
                    }
            }
        }
    }
    UserDefault::getInstance()->flush();
}


void StatsManager::updateUnlockedGiftsCountInLangForCountries(std::string countryname, int count)
{
    std::string str = StringUtils::format(CountriesGiftsUnlockCount,countryname.c_str(),me_Language.c_str());
    UserDefault::getInstance()->setIntegerForKey(str.c_str(),count);
    count = UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();
}
int StatsManager::getUnlockedGiftsCountInLangForCountries(std::string countryname)
{
    std::string str = StringUtils::format(CountriesGiftsUnlockCount,countryname.c_str(),me_Language.c_str());
    int count =UserDefault::getInstance()->getIntegerForKey(str.c_str());
    UserDefault::getInstance()->flush();

    return count;
}

void StatsManager::updateIsClaimedGiftInLangForCountries(std::string countryname, int tag)
{
    std::string str = StringUtils::format(CountriesGiftsClaimed,countryname.c_str(),me_Language.c_str(),tag);
    UserDefault::getInstance()->setBoolForKey(str.c_str(),true);
    UserDefault::getInstance()->flush();
}
bool StatsManager::getClaimedGiftInLangForCountries(std::string countryname,int tag)
{
    std::string str = StringUtils::format(CountriesGiftsClaimed,countryname.c_str(),me_Language.c_str(),tag);
    bool claimed = UserDefault::getInstance()->getBoolForKey(str.c_str());
    UserDefault::getInstance()->flush();
    return claimed;
}
void StatsManager::setCurrentLevelLetters(std::string strword)
{
    m_strLevelWord = strword;
}
std::string StatsManager::getCurrentLevelLetters()
{
    return  m_strLevelWord;
}
void StatsManager::setcurrentCountryindex(int index)
{
    m_index = index;
}
int StatsManager::getCurrentCountryIndex()
{
    return m_index;
}
void StatsManager::updateInGameLevelDataFromUserData(int level)
{

    
    if(strcmp(me_Language.c_str(),LANG_ENGLISH)==0)
    {
        if(level<10)
        {
            std::string strtcntry = "India";
            
            if(me_Language.compare(LANG_ENGLISH)==0)
            {
                auto itr =  std::find(m_CountriesArray.begin(),m_CountriesArray.end(), strtcntry);
                m_CountriesArray.erase(itr);
                
                m_CountriesArray.push_back(strtcntry);
            }
             strtcntry = m_CountriesArray.at(0);
             std::string strLanguage = me_Language;
            {
                {
                    std::string str = StringUtils::format(CountriesPlayedLevelCount,strtcntry.c_str(),strLanguage.c_str());
                    UserDefault::getInstance()->setIntegerForKey(str.c_str(),level);
                }

                //InitializeIsCountryPlayed
                {
                    std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                    UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),false);
                }

                if(level>0)
                {
                  int count = level/5;
                    
                    {
                        int giftunclk = count;
                        int t_iGiftTag = giftunclk;
                        for (int i=0; i<t_iGiftTag; i++) {
                            StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(strtcntry,i);
                        }

                        StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(strtcntry, giftunclk);
                    }
                }
                
                
                if(level<=0)
                {
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                }
                else if(level<=2)
                {
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                }
                else if(level<=6)
                {
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE, false);
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                }
                else if(level>7)
                {
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_REVEAL, false);
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE, false);
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
                    UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                }

                UserDefault::getInstance()->setIntegerForKey("level",level);
            }
        }
        else if(level>=10)
        {
            int unlckcnt = level/10;
            
            std::string strtcntry = "India";
            
            if(unlckcnt>=11)
            {
                unlckcnt = 10;
            }
            
            if(me_Language.compare(LANG_ENGLISH)==0)
            {
                auto itr =  std::find(m_CountriesArray.begin(),m_CountriesArray.end(), strtcntry);
                m_CountriesArray.erase(itr);
                
                m_CountriesArray.push_back(strtcntry);
            }
             std::string strLanguage = me_Language;

            
            std::string str = StringUtils::format(CountriesUnlockedCount,me_Language.c_str());
            UserDefault::getInstance()->setIntegerForKey(str.c_str(),unlckcnt+1);

            
            for(int i = 0; i<unlckcnt; i++)
            {
                strtcntry = m_CountriesArray.at(i);
                
                int cntlevel = 10;

                  std::string str = StringUtils::format(CountriesPlayedLevelCount,strtcntry.c_str(),strLanguage.c_str());
                  UserDefault::getInstance()->setIntegerForKey(str.c_str(),cntlevel);

              //InitializeIsCountryPlayed
                  std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                  UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),true);

                  if(cntlevel>0)
                  {
                    int count = cntlevel/5;
                      
                        {
                          int giftunclk = count;
                          int t_iGiftTag = giftunclk;
                          for (int i=0; i<t_iGiftTag; i++) {
                              StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(strtcntry,i);
                          }
                          StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(strtcntry, giftunclk);
                      }
                  }
             }
            
            
            int remLevel = level%10;
            
            unlckcnt = level/10;

            if(unlckcnt>=11)
            {
                unlckcnt = 10;
                
                remLevel = level - 100;
            }
            
//            5
            if(remLevel>1)
            {
                 strtcntry = m_CountriesArray.at(unlckcnt);
                 std::string strLanguage = me_Language;
                {
                    {
                        std::string str = StringUtils::format(CountriesPlayedLevelCount,strtcntry.c_str(),strLanguage.c_str());
                        UserDefault::getInstance()->setIntegerForKey(str.c_str(),remLevel);
                    }

                    //InitializeIsCountryPlayed
                    {
                        std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                        UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),false);
                    }

                    if(remLevel>0)
                    {
                      int count = remLevel/5;
                        
                        {
                            int giftunclk = count;
                            int t_iGiftTag = giftunclk;
                            for (int i=0; i<t_iGiftTag; i++) {
                                StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(strtcntry,i);
                            }
                            StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(strtcntry, giftunclk);
                        }
                    }
                }
            }
            
            UserDefault::getInstance()->setBoolForKey(TUTORIAL_REVEAL, false);
            UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE, false);
            UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
            UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
            UserDefault::getInstance()->setIntegerForKey("level",level);

        }
    }
    else
    {
           if(level<90)
           {
               std::string strtcntry = "India";

                strtcntry = m_CountriesArray.at(0);
                std::string strLanguage = me_Language;
               {
                   {
                       std::string str = StringUtils::format(CountriesPlayedLevelCount,strtcntry.c_str(),strLanguage.c_str());
                       UserDefault::getInstance()->setIntegerForKey(str.c_str(),level);
                   }

                   //InitializeIsCountryPlayed
                   {
                       std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                       UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),false);
                   }

                   if(level>0)
                   {
                     int count = level/5;
                       
                       {
                           int giftunclk = count;
                           int t_iGiftTag = giftunclk;
                           
                           for (int i=0; i<t_iGiftTag; i++) {
                               StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(strtcntry,i);
                           }
                           
                           StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(strtcntry, giftunclk);
                       }
                   }
                   
                   
                   if(level<=0)
                   {
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                   }
                   else if(level<=2)
                   {
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                   }
                   else if(level<=6)
                   {
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE, false);
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                   }
                   else if(level>7)
                   {
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_REVEAL, false);
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE, false);
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
                       UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
                   }
                    
                   UserDefault::getInstance()->setIntegerForKey("level",level);
               }
           }
           else if(level>=90)
           {
               int unlckcnt = (level-90)/10;
               
               if (unlckcnt==0) {
                   unlckcnt =1;
               }

               std::string strtcntry = "India";
               std::string strLanguage = me_Language;

               
               std::string str = StringUtils::format(CountriesUnlockedCount,me_Language.c_str());
               UserDefault::getInstance()->setIntegerForKey(str.c_str(),unlckcnt+1);

               
               for(int i = 0; i<unlckcnt; i++)
               {
                   strtcntry = m_CountriesArray.at(i);
                   
                   int cntlevel = 10;
                   
                   if (i==0) {
                       cntlevel = 90;
                   }

                     std::string str = StringUtils::format(CountriesPlayedLevelCount,strtcntry.c_str(),strLanguage.c_str());
                     UserDefault::getInstance()->setIntegerForKey(str.c_str(),cntlevel);

                 //InitializeIsCountryPlayed
                     std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                     UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),true);

                     if(cntlevel>0)
                     {
                       int count = cntlevel/5;
                         
                           {
                             int giftunclk = count;
                             int t_iGiftTag = giftunclk;
                             for (int i=0; i<t_iGiftTag; i++) {
                                 StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(strtcntry,i);
                             }
                             StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(strtcntry, giftunclk);
                         }
                     }
                }
               
               int totlevel = level - 90;
               
               int remLevel = totlevel%10;
               
               unlckcnt = totlevel/10;
   //            5
               
               if (unlckcnt==0) {
                   unlckcnt =1;
               }
               
               if(remLevel>1)
               {
                    strtcntry = m_CountriesArray.at(unlckcnt);
                    std::string strLanguage = me_Language;
                   {
                       {
                           std::string str = StringUtils::format(CountriesPlayedLevelCount,strtcntry.c_str(),strLanguage.c_str());
                           UserDefault::getInstance()->setIntegerForKey(str.c_str(),remLevel);
                       }

                       //InitializeIsCountryPlayed
                       {
                           std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                           UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),false);
                       }

                       if(remLevel>0)
                       {
                         int count = remLevel/5;
                           
                           {
                               int giftunclk = count;
                               int t_iGiftTag = giftunclk;
                               for (int i=0; i<t_iGiftTag; i++) {
                                   StatsManager::getInstance()->updateIsClaimedGiftInLangForCountries(strtcntry,i);
                               }
                               StatsManager::getInstance()->updateUnlockedGiftsCountInLangForCountries(strtcntry, giftunclk);
                           }
                       }
                   }
               }
               
               UserDefault::getInstance()->setBoolForKey(TUTORIAL_REVEAL, false);
               UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE, false);
               UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT, false);
               UserDefault::getInstance()->setBoolForKey(TUTORIAL_START, false);
               UserDefault::getInstance()->setIntegerForKey("level",level);

            }
    }
 /*
    {
        std::string strtcntry = m_CountriesArray.at(index);
        {
            std::string strLanguage = LangStrArr[i];
            
            //InitializeIsCountryPlayed
            {
                std::string strplayed = StringUtils::format(IsCountryPlayed,strtcntry.c_str(),strLanguage.c_str());
                UserDefault::getInstance()->setBoolForKey(strplayed.c_str(),false);
            }
            
            //initialGiftUnlockCountForCountriesInLanguages
            {
                std::string str1 = StringUtils::format(CountriesGiftsUnlockCount,strtcntry.c_str(),strLanguage.c_str());
                UserDefault::getInstance()->setIntegerForKey(str1.c_str(),0);
            }
            
            {// initialGiftClaimedStateForCountriesInLanguages
                    int count = m_CountrieslevelCntMap.at(strtcntry);
                    count = count/5;
                    for(int j=0; j<count; j++)
                    {
                        std::string str = StringUtils::format(CountriesGiftsClaimed,strtcntry.c_str(),strLanguage.c_str(),j);
                        UserDefault::getInstance()->setBoolForKey(str.c_str(),false);
                    }
            }
        }
    }*/
    UserDefault::getInstance()->flush();
}

