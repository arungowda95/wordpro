//
//  RatePopUp.cpp
//  wordPro-mobile
//
//  Created by id on 08/05/20.
//

#include "RatePopUp.h"

RatePopUp::RatePopUp(std::function<void()> func)
{
 
   m_fnSelector = func;

   Rect Res = Director::getInstance()->getSafeAreaRect();
   visibleSize = Size(Res.size.width,Res.size.height);
   origin = Res.origin;
           
   m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
 
   m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
   m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()-0.25);
   m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
   this->addChild(m_SprPopUpBg,1);
   
   m_SprPopUpTop = Sprite::create("PopUp_Top.png");
   m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleY()+0.15);
   m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
   this->addChild(m_SprPopUpTop,2);

    
    std::string t_Label = "Rate";
    Label *t_tLabel = createLabelBasedOnLang(t_Label, 60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+10);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMidY()));
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tLabel, 2);
    
    
    std::string t_Label1 = "RateInfo";
    t_tSubRateLabel = createLabelBasedOnLang(t_Label1,40);
    t_tSubRateLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubRateLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubRateLabel->setHeight(t_tSubRateLabel->getBoundingBox().size.height+10);
    t_tSubRateLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-30-t_tSubRateLabel->getBoundingBox().size.height/2));
    
    t_tSubRateLabel->setColor(Color3B(255,255,255));
    t_tSubRateLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tSubRateLabel, 2);

//    if(me_strScreenName=="MenuScreen")
    {
        CreatePopupForRateButton();
        
        // CLOSE BUTTON
        MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(RatePopUp::OnButtonPressed,this));
        t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
        t_miCloseButton->setTag(2);

        Menu *m_Menu = Menu::create(t_miCloseButton,NULL);
        m_Menu->setPosition(Vec2::ZERO);
        this->addChild(m_Menu,2);

        return;
    }
    std::string t_subLabel = "RateMsg";
    Label *t_tSubLabel = createLabelBasedOnLang(t_subLabel, 40);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,244,55));
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+10);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()+30));
    t_tSubLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tSubLabel, 2);

     // YES BUTTON
    float posy = (m_SprPopUpBg->getBoundingBox().getMinY());
    t_miRateYesBtn = getButtonMade("empty_smallButton.png", "empty_smallButton.png", CC_CALLBACK_1(RatePopUp::OnButtonPressed,this));
      t_miRateYesBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX()-5-t_miRateYesBtn->getBoundingBox().size.width/2,posy+30+t_miRateYesBtn->getBoundingBox().size.height));
      t_miRateYesBtn->setTag(0);

    Label *t_tYesLabel = createLabelBasedOnLang("Yes",45);
    t_tYesLabel->setColor(Color3B::WHITE);
    t_tYesLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    t_tYesLabel->enableOutline(Color4B(53,126,32,255),2);
    t_tYesLabel->setWidth(t_miRateYesBtn->getContentSize().width);
    t_tYesLabel->setHeight(t_tYesLabel->getBoundingBox().size.height);
    t_tYesLabel->setPosition(Vec2(t_miRateYesBtn->getBoundingBox().size.width/2,t_miRateYesBtn->getBoundingBox().size.height/2+15));
    t_miRateYesBtn->addChild(t_tYesLabel,1);

    
      // NO BUTTON
      t_miRateNoBtn = getButtonMade("empty_smallButton.png", "empty_smallButton.png", CC_CALLBACK_1(RatePopUp::OnButtonPressed,this));
    t_miRateNoBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX()+5+t_miRateNoBtn->getBoundingBox().size.width/2,posy+30+t_miRateNoBtn->getBoundingBox().size.height));
         t_miRateNoBtn->setTag(1);
      
      Label *t_NoLabel = createLabelBasedOnLang("No",45);
      t_NoLabel->setColor(Color3B::WHITE);
      t_NoLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
      t_NoLabel->enableOutline(Color4B(53,126,32,255),2);
      t_NoLabel->setWidth(t_miRateNoBtn->getContentSize().width);
      t_NoLabel->setHeight(t_NoLabel->getBoundingBox().size.height);
      t_NoLabel->setPosition(Vec2(t_miRateNoBtn->getBoundingBox().size.width/2,t_miRateNoBtn->getBoundingBox().size.height/2+15));
      t_miRateNoBtn->addChild(t_NoLabel,1);

    
       Menu *m_Menu = Menu::create(t_miRateYesBtn,t_miRateNoBtn,NULL);
       m_Menu->setPosition(Vec2::ZERO);
       this->addChild(m_Menu,2);

}
void RatePopUp::OnButtonPressed(Ref *p_Sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    
    int pButtonTag =  Button->getTag();
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    switch (pButtonTag)
    {
        case 0:
        {
            status = (char*)"Rate";
        }break;
        case 1:
        {
            status = (char*)"No";
        }break;
        case 2:
        {
            status = (char*)"close";
        }break;
        default:
            break;
    }
    
    if(m_fnSelector != NULL)
    {
        m_fnSelector();
    }
}
Label *RatePopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
        t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
MenuItemSprite *RatePopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
      Sprite *_SelectedSpr = Sprite::create(selSpr);
      _SelectedSpr->setOpacity(150);
    
    Sprite *_DisableSpr = Sprite::create(selSpr);
    _DisableSpr->setOpacity(150);

      MenuItemSprite *Button = MenuItemSprite::create(_normalSpr,_SelectedSpr,_DisableSpr,func);
      return Button;
}
void RatePopUp::CreatePopupForRateButton()
{
    
    t_starsArr = new Vector<MenuItemSprite*>();

    Point m_Position = Point(m_SprPopUpBg->getBoundingBox().getMidX() - (5*110)/2.0f,m_SprPopUpBg->getBoundingBox().getMidY()-40);

    for(int i=0;i<5; i++)
    {
        m_Position.x += (110)/2;
        
        std::string str = FileUtils::getInstance()->getWritablePath();
        str += StringUtils::format("moreGamesIcon%d.png", i);
        
        MenuItemSprite *starBtn = getButtonMade("star_rate_norm.png", "star_rate_norm.png", CC_CALLBACK_1(RatePopUp::starButtonCallback, this));
        starBtn->setPosition(Vec2(m_Position.x,m_Position.y+starBtn->getBoundingBox().size.height/2));
        starBtn->setName("empty");
        starBtn->setTag(i);
      
        t_starsArr->pushBack(starBtn);
        Menu *menu = Menu::create(starBtn, nullptr);
        menu->setPosition(Vec2::ZERO);
        this->addChild(menu,3);
        
        m_Position.x += (110)/2;
    }
    
    t_miRateButton = getButtonMade("large_button_1.png","large_button_1.png",CC_CALLBACK_1(RatePopUp::rateButtonCallback,this));
    t_miRateButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinX()+t_miRateButton->getContentSize().height*0.8));
    t_miRateButton->setName("notNow");
    
    t_rateLabel = createLabelBasedOnLang("NotNow",40);
    t_rateLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    t_rateLabel->setColor(Color3B::WHITE);
    t_rateLabel->enableBold();
    t_rateLabel->enableOutline(Color4B(53,126,32,255),2);
    t_rateLabel->setWidth(t_miRateButton->getContentSize().width-20);
    t_rateLabel->setHeight(t_miRateButton->getContentSize().height-10);
    t_rateLabel->setPosition(Vec2(t_miRateButton->getContentSize().width/2,t_miRateButton->getContentSize().height/2+13));
    t_miRateButton->addChild(t_rateLabel,1);
    
    Menu *rateMenu = Menu::create(t_miRateButton,NULL);
    rateMenu->setPosition(Vec2::ZERO);
    this->addChild(rateMenu,3);
    
}
void RatePopUp::starButtonCallback(Ref*sender)
{
    MenuItemSprite *star = (MenuItemSprite*)sender;
    int pButtonTag =  star->getTag();
    
    std::string selname = star->getName();
    for(int i=0; i<t_starsArr->size();i++)
    {
        if(i<=pButtonTag)
        {
            Sprite *spr = Sprite::create("star_rate_select.png");
            spr->setColor(Color3B(255,193,7));
            
            Sprite *spr1 = Sprite::create("star_rate_select.png");
            spr1->setColor(Color3B(255,193,7));

            t_starsArr->at(i)->setNormalImage(spr);
            t_starsArr->at(i)->setSelectedImage(spr1);
            t_starsArr->at(i)->setName("fill");
        }
        else
        {
            Sprite *spr1 = Sprite::create("star_rate_norm.png");
            Sprite *spr2 = Sprite::create("star_rate_norm.png");

            t_starsArr->at(i)->setNormalImage(spr1);
            t_starsArr->at(i)->setSelectedImage(spr2);
            t_starsArr->at(i)->setName("empty");
        }
    }
    
    setStringForLabelBasedOnStar(pButtonTag+1);
}
void RatePopUp::setStringForLabelBasedOnStar(int starNum)
{
    float offy = 0;
    switch (starNum) {
        case 1:
        {
            std::string str = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateInfo_1");
            t_tSubRateLabel->setString(str);
            
            std::string str1 = LanguageTranslator::getInstance()->getTranslatorStringWithTag("sendFeedback");
            t_rateLabel->setString(str1);
            t_miRateButton->setName("Feedback");
        }
        break;
        case 2:
        {
            std::string str = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateInfo_1");
            t_tSubRateLabel->setString(str);
            
            std::string str1 = LanguageTranslator::getInstance()->getTranslatorStringWithTag("sendFeedback");
            t_rateLabel->setString(str1);
            t_miRateButton->setName("Feedback");
        }
        break;
        case 3:
        {
            std::string str = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateInfo_2");
            t_tSubRateLabel->setString(str);
            
            std::string str1 = LanguageTranslator::getInstance()->getTranslatorStringWithTag("sendFeedback");
            t_rateLabel->setString(str1);
            t_miRateButton->setName("Feedback");
        }
        break;
        case 4:
        {
            offy = 50;
            std::string str = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateInfo_3");
            t_tSubRateLabel->setString(str);
            
            std::string str1 = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateNow");
            t_rateLabel->setString(str1);
            t_miRateButton->setName("Ratenow");
        }
        break;
        case 5:
        {
            offy = 50;
            std::string str = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateInfo_4");
            t_tSubRateLabel->setString(str);
            
            std::string str1 = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateNow");
            t_rateLabel->setString(str1);
            t_miRateButton->setName("Ratenow");
        }
        break;
        default:
            break;
    }
    
    
    if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
    {
        offy = 50;
        std::string str = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateInfo_4");
        t_tSubRateLabel->setString(str);
        
        std::string str1 = LanguageTranslator::getInstance()->getTranslatorStringWithTag("RateNow");
        t_rateLabel->setString(str1);
        t_miRateButton->setName("Ratenow");
    }
    else
    {
        t_tSubRateLabel->setHeight(0);
        t_tSubRateLabel->setHeight(t_tSubRateLabel->getBoundingBox().size.height+10);
        t_tSubRateLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-20-t_tSubRateLabel->getBoundingBox().size.height/2-offy));
    }

    
}
void RatePopUp::rateButtonCallback(Ref *sender)
{
    MenuItemSprite *star = (MenuItemSprite*)sender;
    std::string selname = star->getName();
    
    if(strcmp(selname.c_str(),"notNow")==0)
    {
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    }
    else if(strcmp(selname.c_str(),"Feedback")==0)
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->gameFeedBack();
        status = (char*)"close";
        if (m_fnSelector!=NULL) {
            m_fnSelector();
        }
    }
    else if(strcmp(selname.c_str(),"Ratenow")==0)
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->OpenInStore();
        GameController::getInstance()->setBoolForKey("rate", true);
    }
    
}
RatePopUp::~RatePopUp()
{
    this->removeAllChildrenWithCleanup(true);
}

