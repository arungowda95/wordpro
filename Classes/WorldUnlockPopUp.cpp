//
//  WorldUnlockPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 22/04/20.
//

#include "WorldUnlockPopUp.h"
WorldUnlockPopUp::WorldUnlockPopUp(std::function<void()> func)
{
    
    m_fnSelector = func;
    
     ResolutionSize = Director::getInstance()->getSafeAreaRect();
     visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    
    std::string t_Label = "Complete";
    Label *t_tTopLabel = createLabelBasedOnLang(t_Label,60);
    t_tTopLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tTopLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-70);
    t_tTopLabel->setHeight(150);
    t_tTopLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    t_tTopLabel->setColor(Color3B(255,244,55));
    t_tTopLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tTopLabel, 2);

    
    std::string t_Label1 = "CountryUnlock";
    Label *t_tLabel = createLabelBasedOnLang(t_Label1,50);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+20);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-30-t_tLabel->getContentSize().height/2));
    t_tLabel->setColor(Color3B(255,244,55));
    this->addChild(t_tLabel, 2);

    
    
    std::string t_NameLabel = StatsManager::getInstance()->getNextUnlockCountry();
    Label *t_tCountryLabel = Label::createWithTTF(t_NameLabel, FONT_NAME,70, Size(0, 100));
    t_tCountryLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCountryLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()));
    t_tCountryLabel->setColor(Color3B(255,244,55));
    t_tCountryLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tCountryLabel, 2);
    float scale = t_tCountryLabel->getScale();
    t_tCountryLabel->runAction(RepeatForever::create(Sequence::create(ScaleTo::create(0.3f, scale+0.15f),ScaleTo::create(0.3f, scale),NULL)));

    std::string path = "Countries/"+t_NameLabel+".png";
    Sprite *m_SprIcon = Sprite::create(path.c_str());
    m_SprIcon->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()));
    if(m_SprIcon->getContentSize().width>m_SprPopUpBg->getBoundingBox().size.width)
    {
        m_SprIcon->setScale(0.8f);  }
    this->addChild(m_SprIcon,1);

    

    Sprite *m_SprGlow = Sprite::create("glow.png");
    m_SprGlow->setPosition(Vec2(t_tCountryLabel->getPosition()));
    this->addChild(m_SprGlow);
    m_SprGlow->runAction(RepeatForever::create(RotateBy::create(1.0f, 80.0f)));
    
    t_NextButton = getButtonMade("empty_smallButton.png","empty_smallButton.png",CC_CALLBACK_1(WorldUnlockPopUp::OnButtonPressed,this));
    t_NextButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()+30+t_NextButton->getBoundingBox().size.height));
    t_NextButton->setTag(1);
    
    Label *NxtLabel = createLabelBasedOnLang("CheckOut",35);
    NxtLabel->setWidth(t_NextButton->getBoundingBox().size.width-15);
    NxtLabel->setHeight(150);
    NxtLabel->setColor(Color3B::WHITE);
    NxtLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    NxtLabel->enableOutline(Color4B(53,126,32,255),2);
    NxtLabel->setPosition(Vec2(t_NextButton->getBoundingBox().size.width/2,t_NextButton->getBoundingBox().size.height/2+15));
    t_NextButton->addChild(NxtLabel,1);

    
    // CLOSE BUTTON
      MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(WorldUnlockPopUp::OnButtonPressed,this));
      t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
      t_miCloseButton->setTag(0);

      Menu *m_Menu = Menu::create(t_NextButton,NULL);
      m_Menu->setPosition(Vec2::ZERO);
      this->addChild(m_Menu,2);

    
}
MenuItemSprite *WorldUnlockPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
      
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}
Label *WorldUnlockPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

void WorldUnlockPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
     MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
       
       int pButtonTag =  Button->getTag();

       switch (pButtonTag)
       {
           case 0:
           {
               status = (char*)"close";
           }break;
           case 1:
           {
               status = (char*)"Check_Out";
           }break;
            default:
         break;
       }
     
     if (m_fnSelector!=NULL) {
         m_fnSelector();
     }
}
WorldUnlockPopUp::~WorldUnlockPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
