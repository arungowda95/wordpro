//
//  OptionLetterTile.hpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

#ifndef OptionLetterTile_h
#define OptionLetterTile_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class OptionLetterTile:public Ref
{
    GameLayer *m_ptrGameLayer;
    Sprite    *m_Sprite;
    Label     *m_LetterLabel;
    std::string m_stringLetter;
    Label *countLabel;
public:
    bool m_bTouched;
    bool m_bLock;
    
    int m_iLetterCount;
    
    OptionLetterTile(GameLayer* t_ptrGameLayer,Point t_Position,std::string letterStr,int count,float scale);
    ~OptionLetterTile();
    
    // returns option letter label
    Label  *getLetterLabel();
    
    // returns option letter sprite
    Sprite *getLetterSprite();
    
    /// on touch and touch end of option tile
    void onTouch();
    void onTouchEnd();
    
    /// sends the clicked tile string
    void EnterTheKey();
    
    /// Shuffles Option Tiles
    /// @param t_midPos old position of grids
    /// @param t_Position New Position of grids
    void shuffleOptionsTile(Point t_midPos,Point t_Position);
};
#endif /* OptionLetterTile_hpp */
