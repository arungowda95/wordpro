//
//  DailyMissionToasts.cpp
//  wordPro-mobile
//
//  Created by id on 05/05/20.
//

#include "DailyMissionToasts.h"
#include "DailyMissionManager.h"
#include "HudLayer.h"
#include "GameConstants.h"
#include "LanguageTranslator.h"
// CONSTRUCTOR
DailyMissionToasts::DailyMissionToasts(const std::string &p_MissionName)
{
     m_iCointCount = 0;
     m_bReward = false;
     m_MissionName = p_MissionName;
     m_SprIconName = "";
    
     cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
     visibleSize = ResolutionSize.size;
     Origin = ResolutionSize.origin;

     m_SprToast = Sprite::create("coins_popup_Bg.png");
     m_SprToast->setScale(m_fScale+0.1f);
     m_SprToast->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height+100));
     this->addChild(m_SprToast,2);
    
     Sprite *missionIcon = Sprite::create("circle-image.png");
     missionIcon->setPosition(Vec2(missionIcon->getContentSize().width/2+20,m_SprToast->getContentSize().height/2-5));
     missionIcon->setScale(1.0f);
     m_SprToast->addChild(missionIcon,2);
    
     std::string t_Label = "MissionComplete";
     Label *t_tLabel = createLabelBasedOnLang(t_Label, 33);
     t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
     t_tLabel->setWidth(m_SprToast->getBoundingBox().size.width-64);
     t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height);
     t_tLabel->setPosition(Vec2( m_SprToast->getContentSize().width/2+5.0f,m_SprToast->getContentSize().height-t_tLabel->getContentSize().height/2));
     t_tLabel->setColor(Color3B(255,244,55));
     t_tLabel->enableOutline(Color4B(196,56,63,255),3);
     m_SprToast->addChild(t_tLabel,2);
    
     std::string t_Descrip = getDescription();
     Label *t_tMissionDisc =  createLabelBasedOnLang(StringUtils::format("Mission%d",m_iMissionNum), 27);
     t_tMissionDisc->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
     t_tMissionDisc->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
     t_tMissionDisc->setColor(Color3B::WHITE);
     t_tMissionDisc->enableOutline(Color4B(0,0,0,255),2);
     t_tMissionDisc->setWidth(m_SprToast->getBoundingBox().size.width-150);
     t_tMissionDisc->setHeight(t_tMissionDisc->getBoundingBox().size.height);
     t_tMissionDisc->setPosition(Vec2(missionIcon->getBoundingBox().getMaxX()+30,t_tLabel->getBoundingBox().getMinY()-30));
     m_SprToast->addChild(t_tMissionDisc, 2);

     Sprite *t_AIcon = Sprite::create(m_SprIconName);
    if(t_AIcon==NULL)
        t_AIcon = Sprite::create("circle-image.png");
     t_AIcon->setPosition(Vec2(missionIcon->getContentSize().width/2,missionIcon->getContentSize().height/2));
     t_AIcon->setScale(0.85f);
     missionIcon->addChild(t_AIcon,2);

     // ANIMATE TOASTS
     AnimateToasts();
}
// THIS WILL ANIMTE TOASTS
void DailyMissionToasts::AnimateToasts()
{
    DelayTime *t_DelayTime = DelayTime::create(2.0f);
    MoveTo *t_MoveTo1 = MoveTo::create(0.5f, Vec2(m_SprToast->getPositionX(), m_SprToast->getPositionY() - m_SprToast->getContentSize().height-50));
    MoveTo *t_MoveTo2 = MoveTo::create(0.5f, Vec2(m_SprToast->getPositionX(), m_SprToast->getPositionY() + m_SprToast->getContentSize().height));
    EaseElasticIn *t_EaseCome = EaseElasticIn::create(t_MoveTo1, 0.85f);
    EaseElasticOut *t_EaseOut = EaseElasticOut::create(t_MoveTo2, 0.85f);
    Sequence *t_sequence = Sequence::create(t_EaseCome, t_DelayTime, t_EaseOut, NULL);
    m_SprToast->runAction(t_sequence);
}
// THIS RETURN DESCRIPTION ACCORDING TO NAME
std::string DailyMissionToasts::getDescription()
{
    std::string t_Description = "";
    
    if(m_MissionName.compare("Spell 25 words")==0)
    {
        t_Description = "Spell 25 words";
        m_SprIconName = "Achievement_icons/words.png";//
        m_iMissionNum = 1;
    }
    else if(m_MissionName.compare("Clear 25 levels")==0)
    {
        t_Description = "Clear 25 levels";
        m_SprIconName = "Achievement_icons/levels.png";//
        m_iMissionNum = 2;
    }
    else if(m_MissionName.compare("Get 25 GOOD combos")==0)
    {
         t_Description = "Get 25 GOOD combos";
         m_SprIconName = "Achievement_icons/good.png";//
         m_iMissionNum = 3;
    }
    else if(m_MissionName.compare("Get 25 GREAT combos")==0)
    {
         t_Description = "Get 25 GREAT combos";
         m_SprIconName = "Achievement_icons/great.png";//
         m_iMissionNum = 4;
    }
    else if(m_MissionName.compare("Spell 50 words")==0)
    {
        t_Description = "Spell 100 words";
        m_SprIconName = "Achievement_icons/words.png";//
        m_iMissionNum = 5;
    }
    else if(m_MissionName.compare("Use Hint for 10 times")==0)
    {
        t_Description = "Use Hint for 10 times";
        m_SprIconName = "Achievement_icons/hints.png";//
        m_iMissionNum = 6;
    }
    else if(m_MissionName.compare("Get 25 Amazing combos")==0)
    {
        t_Description = "Get 25 Amazing combos";
        m_SprIconName = "Achievement_icons/amazing.png";//
        m_iMissionNum = 7;
    }
    else if(m_MissionName.compare("Get 25 Awesome combos")==0)
    {
        t_Description = "Get 25 Awesome combos";
        m_SprIconName = "Achievement_icons/awesome.png";//
        m_iMissionNum = 8;
    }
    else if(m_MissionName.compare("Spell 25 words")==0)
    {
        t_Description = "Spell 25 words";
        m_SprIconName = "Achievement_icons/words.png";//
        m_iMissionNum = 9;
    }
    else if(m_MissionName.compare("Use letter(A) count option for 10 times")==0)
    {
        t_Description = "Use letter(A) count option for 10 times";
        m_SprIconName = "Achievement_icons/Letter_A.png";//
        m_iMissionNum = 10;
    }
    else if(m_MissionName.compare("Collect the Gift box items of level map for 5 times")==0)
    {
        t_Description = "Collect the Gift box items of level map for 5 times";
        m_SprIconName = "Achievement_icons/giftbox.png";//
        m_iMissionNum = 11;
    }
    else if(m_MissionName.compare("Clear any 10 levels without misspelling")==0)
    {
        t_Description = "Clear any 10 levels without misspelling";
        m_SprIconName = "Achievement_icons/levels.png";//
        m_iMissionNum = 12;
    }
    else if(m_MissionName.compare("Spell 50 words")==0)
    {
        t_Description = "Spell 50 words";
        m_SprIconName = "Achievement_icons/words.png";//
        m_iMissionNum = 13;
    }
    else if(m_MissionName.compare("Collect 10 Bonus word")==0)
    {
        t_Description = "Collect 10 Bonus word";//
        m_SprIconName = "Achievement_icons/bonusword.png";
        m_iMissionNum = 14;
    }
    else if(m_MissionName.compare("Get 25 Good combos")==0)
    {
        t_Description = "Get 25 Good combos";
        m_SprIconName = "Achievement_icons/good.png";//
        m_iMissionNum = 15;
    }
    else if(m_MissionName.compare("Use letter(A) count option for 10 times")==0)
    {
        t_Description = "Use letter(A) count option for 10 times";
        m_SprIconName = "Achievement_icons/Letter_A.png";//
        m_iMissionNum = 16;
    }
    else if(m_MissionName.compare("Spell 25 words")==0)
    {
        t_Description = "Spell 25 words";
        m_SprIconName = "Achievement_icons/words.png";//
        m_iMissionNum = 17;
    }
    else if(m_MissionName.compare("Use Hint for 10 times")==0)
    {
        t_Description = "Use Hint for 10 times";
        m_SprIconName = "Achievement_icons/hints.png";//
        m_iMissionNum = 18;
    }
    else if(m_MissionName.compare("Collect 10 Bonus word")==0)
    {
        t_Description = "Collect 10 Bonus word";//
        m_SprIconName = "Achievement_icons/bonusword.png";
        m_iMissionNum = 19;
    }
    else if(m_MissionName.compare("Clear any 10 levels without misspelling")==0)
    {
        t_Description = "Clear any 10 levels without misspelling";
        m_SprIconName = "Achievement_icons/levels.png";//
        m_iMissionNum = 20;
    }
   
    return t_Description;
    
}
Label *DailyMissionToasts::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        if(langstr.compare("MissionComplete")==0) {
            std::transform(langstr.begin(), langstr.end(), langstr.begin(), ::toupper);
        }
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}
std::string DailyMissionToasts::getIconsForMissions(std::string AchieveTag)
{

    std::string name = "circle-image";
    
    return name;
}

// DESTRUCTOR
DailyMissionToasts::~DailyMissionToasts()
{
    if(m_SprToast != NULL)
    {
        m_SprToast->removeAllChildrenWithCleanup(true);
        this->removeChild(m_SprToast, true);
        m_SprToast = NULL;
    }
}
