//
//  AnswerLetterTile.hpp
//  wordPro-mobile
//
//  Created by id on 25/02/20.
//

#ifndef AnswerLetterTile_hpp
#define AnswerLetterTile_hpp

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class AnswerLetterTile:public Ref
{
    
    GameLayer *m_ptrGameLayer;
    
    Point         m_Position;
    
    Label            *m_tLetter;
    Sprite           *m_Sprite;
    Sprite           *m_CoinSpr;
    
    bool         m_bHasContent;
    bool         m_bIsHint;
    bool         m_bReward;
public:
    std::string   m_StringLetter;
    AnswerLetterTile(GameLayer *t_ptrGamelayer,Point p_Position, const std::string &p_LetterStr,float scale,bool reward);
    ~AnswerLetterTile();
    
    /// RETURN CONTAIN STATUS
    bool HasContain();
    
    /// this Sets letter visible on Right Word
    void setLetterVisible(float p_fDelayTime,bool visible);
    
    /// THIS SET LETTER VISIBLE on HINT
    void onHintLetterVisible(float p_fDelayTime,bool m_isHint);
    
    /// animates Grids on wrong ans moves left and right tiles
    void animateGrids();
    
    /// RETURN'S LETTER LABEL
    Label *getLetterLabel();
    
    /// RETURN'S  LETTER SPRITE
    Sprite *getAnsSprite();
    
    /// Coin Animation on finding Coin Reward Word in HUD Layer by passing ans letter tile position
    void CoinAnimation(Point pos);
    
};
#endif /* AnswerLetterTile_hpp */
