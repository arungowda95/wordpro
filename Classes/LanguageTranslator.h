//
//  LanguageTranslator.hpp
//  wordPro-mobile
//
//  Created by id on 27/03/20.
//

#ifndef LanguageTranslator_h
#define LanguageTranslator_h

#include "cocos2d.h"
#include <stdio.h>
#include "json/document.h"
#include "json/writer.h"
#include "json/stringbuffer.h"
#include "json/reader.h"
#include "json/rapidjson.h"


using namespace rapidjson;

using namespace cocos2d;
using namespace std;

// SINGELTON CLASS
class LanguageTranslator
{
        rapidjson::Document m_JsonDocument;
        rapidjson::Document m_EngDocument;
    
        // CONSTRUCTOR
        LanguageTranslator();
    
        static LanguageTranslator *m_PtrLanguageTranslator;
    public:
    
        // RETURN SINGELTON INSTANCE
        static LanguageTranslator *getInstance();
    
        // THIS LOAD TRANSLATION
        void LoadTranslationFile();
    
        // THIS RETURN TRANSLATED STRING
        std::string getTranslatorStringWithTag(const std::string &p_StringTag);
};

#endif /* LanguageTranslator_hpp */
