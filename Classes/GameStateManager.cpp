//
//  GameStateManager.cpp
//  wordPro-mobile
//
//  Created by id on 20/02/20.
//

//  THIS CLASS SET GAME STATE IN THE GAME (SIGELETON CLASS REMAIN THROUGH OUT THE GAME)

#include "GameStateManager.h"

//SINGELTON STUFF
GameStateManager* GameStateManager::m_ptrGameStateManager = NULL;

// RETURN GAME HANDLER
GameStateManager* GameStateManager::getInstance()
{
    if(!m_ptrGameStateManager)
    {
        m_ptrGameStateManager = new GameStateManager();
    }
    return m_ptrGameStateManager;
}
// CONSTRUCTOR
GameStateManager::GameStateManager()
{

}
// SET GAME STATE
void GameStateManager::SetGameState(eGameState p_eGameState)
{
    m_eGameState = p_eGameState;
}
// GET GAME STATE
eGameState GameStateManager::getGameState()
{
    return m_eGameState;
}
