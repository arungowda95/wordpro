//
//  IOSInterfaceManager.cpp
//  wordPro-mobile
//
//  Created by id on 31/03/20.
//
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "IOSInterfaceManager.h"
#include "GameConstants.h"
#include "InterfaceManagerInstance.h"
#include "InterfaceManager.h"
#import  "Reachability.h"
#include "AppController.h"
#include "FirebaseViewController.h"
#include "GameController.h"
#include "AuthController.h"
//
void IOSInterfaceManager::OpenUrl(std::string url)
{
    
}
//
void IOSInterfaceManager::OpenInStore()
{
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate openInAppStore];
}
//
void IOSInterfaceManager::ShareGame()
{
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate shareGame];
}
void IOSInterfaceManager::gameFeedBack()
{
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate sendGameFeedBack];
}
void IOSInterfaceManager::ShowToastMessage(const std::string &p_Message)
{
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate displayToastMessage:[NSString stringWithUTF8String:p_Message.c_str()]];
}
//INTERNET CONNECTION CHECKING
bool IOSInterfaceManager::isNetworkAvailable()
{
   bool m_bIsInternetConnected = true;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    if([reachability currentReachabilityStatus])
    {
        m_bIsInternetConnected =true;
    }else{
        m_bIsInternetConnected =false;
    }

    return m_bIsInternetConnected;
}

void IOSInterfaceManager::PostFirebaseAnalyticsEvent(const std::string &eventName,const std::string &param1,const std::string &paramVal_1,const std::string &param2,const std::string &paramVal_2)
{
    printf("\nPostFirebaseAnalyticsEvent-->%s->%s->%s->%s ->%s\n",eventName.c_str(),param1.c_str(),paramVal_1.c_str(),param2.c_str(),paramVal_2.c_str());
    AppController *appDelegate  = (AppController *)[[UIApplication sharedApplication] delegate];

    NSString* eveName = [NSString stringWithUTF8String:eventName.c_str()];
    NSString* para1 = [NSString stringWithUTF8String:param1.c_str()];
    NSString* parVal1 = [NSString stringWithUTF8String:paramVal_1.c_str()];
    NSString* para2 = [NSString stringWithUTF8String:param2.c_str()];
    NSString* paraVal2 = [NSString stringWithUTF8String:paramVal_2.c_str()];

    [appDelegate postFirebaseAnalyticsEvent:eveName param1:para1 paramVal:parVal1 param2:para2 paramVal2:paraVal2];
}
void IOSInterfaceManager::PostFirebaseAnalyticsScreen(const std::string &p_ScreenName)
{
    AppController *appDelegate  = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate postFirebaseAnalyticsScreen:[NSString stringWithUTF8String:p_ScreenName.c_str()]];
}

//
bool IOSInterfaceManager::appInstalledOrNot(const std::string &package)
{
    AppController *appDelegate  = (AppController *)[[UIApplication sharedApplication] delegate];
    return [appDelegate appInstalledOrNot:[NSString stringWithUTF8String:package.c_str()]];
}
std::string IOSInterfaceManager::getCurrencySymbol(const std::string &p_CurrCode)
{
//    return "@"EUR"";
    AppController *appDelegate  = (AppController *)[[UIApplication sharedApplication] delegate];
    return [[appDelegate getCurrencySymbol:[NSString stringWithUTF8String:p_CurrCode.c_str()]] UTF8String];
}
/// push firebase leaderboard data
void IOSInterfaceManager::pushDataToUserdata(const std::string &fbId,const std::string &username,const std::string &profileUrl,const std::string &language,int level,int coins)
{

    NSString* fbID = [NSString stringWithUTF8String:fbId.c_str()];
    NSString* name = [NSString stringWithUTF8String:username.c_str()];
    NSString* url = [NSString stringWithUTF8String:profileUrl.c_str()];
    NSString* lang = [NSString stringWithUTF8String:language.c_str()];

    NSNumber *lvl = [NSNumber numberWithInt:level];
    NSNumber *coin = [NSNumber numberWithInt:coins];

    [[FirebaseViewController sharedInstance] pushFirebaseLeaderBoardData:fbID userName:name profileUrl:url language:lang currLevel:lvl coinCount:coin];
    
}
void IOSInterfaceManager::pushFirebaseUserLanguageData(const std::string &fbId,const std::string &language,int currentLevel,int coins)
{

    NSString* fbID = [NSString stringWithUTF8String:fbId.c_str()];
    NSString* lang = [NSString stringWithUTF8String:language.c_str()];

    NSNumber *level = [NSNumber numberWithInt:currentLevel];
    NSNumber *coin = [NSNumber numberWithInt:coins];

    [[FirebaseViewController sharedInstance] updateUserLanguageData:fbID language:lang currlevel:level coinCount:coin];
}

void IOSInterfaceManager::getLeaderBoardJson(const std::string &language)
{
    [[FirebaseViewController sharedInstance] getLeaderBoardData:[NSString stringWithUTF8String:language.c_str()]     completion:^(NSString *str) {
        std::string bar = std::string([str UTF8String]);
        onleaderBoardJsonSuccess(bar);
    }];
}
void IOSInterfaceManager::getUserLanguageDataJson(const std::string &fbId)
{
     [[FirebaseViewController sharedInstance] getUserLanguageData:[NSString stringWithUTF8String:fbId.c_str()]     completion:^(NSString *str) {
         std::string bar = std::string([str UTF8String]);
         onUserLanguageDataSuccess(bar);
     }];
}
/// number of users ID's response
void IOSInterfaceManager::getLanguageDataUserIds()
{
    [[FirebaseViewController sharedInstance] getLanguageDataUserIds:^(NSString *str) {
        std::string bar = std::string([str UTF8String]);
        onlanguageDataUserIdsSuccess(bar);
    }];
}
void IOSInterfaceManager::onlanguageDataUserIdsSuccess(std::string strbuff)
{
    me_strUserIdsJson = strbuff;

    log("IOS_interface->%s",me_strUserIdsJson.c_str());

    GameController::getInstance()->parseLanguageDataUserIds(me_strUserIdsJson);

}
void IOSInterfaceManager::onUserLanguageDataSuccess(std::string strBuf)
{
    me_strUserDataJson = strBuf;
    log("IOS_interface->%s",me_strUserDataJson.c_str());

    GameController::getInstance()->setStringForKey("UserlangData",me_strUserDataJson.c_str());

    GameController::getInstance()->updateUserDataInGame(me_strUserDataJson);
}
void IOSInterfaceManager::onleaderBoardJsonSuccess(std::string strBuf)
{
    me_strLeaderBoardJson = strBuf;
}

// NOT USED
void IOSInterfaceManager::signInWithApple()
{
    // NOT USED
    [[AuthController sharedInstance] handleAuthrization];

}
void IOSInterfaceManager::OnLoginSuccessCallBack()
{
    
}
void IOSInterfaceManager::OnLoginFailedCallBack()
{
    
}

void IOSInterfaceManager::openFbInviteDialog()
{
    // NOT USED
}
bool IOSInterfaceManager::isFacebookLoggedIn()
{
    // NOT USED
    return true;
}
void IOSInterfaceManager::checkForGameUpdate()
{
    
}
void IOSInterfaceManager::initialize()
{
    
}
void IOSInterfaceManager::setUserFbId(const std::string &fbId)
{
    NSString* fbID = [NSString stringWithUTF8String:fbId.c_str()];
    [[FirebaseViewController sharedInstance] setUserfbId:fbID];

}
void IOSInterfaceManager::getUserRank(const std::string &language)
{
    
}
#endif
