//
//  InterfaceManager.hpp
//  wordPro-mobile
//
//  Created by id on 31/03/20.
//

#ifndef InterfaceManager_hpp
#define InterfaceManager_hpp

#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;

class InterfaceManager
{
    
    protected:

    
    public:
    virtual void OpenUrl(std::string url);
       
    virtual void OpenInStore();
    
    virtual void ShareGame();
    
    virtual void gameFeedBack();
    
    virtual void ShowToastMessage(const std::string &p_Message);
    
    virtual bool appInstalledOrNot(const std::string &package);
    
    virtual void PostFirebaseAnalyticsEvent(const std::string &eventName,const std::string &param1,const std::string &paramVal_1,const std::string &param2,const std::string &paramVal_2);
    
    virtual void PostFirebaseAnalyticsScreen(const std::string &p_ScreenName);

    virtual bool isNetworkAvailable(){return true;};
    
    virtual std::string getCurrencySymbol(const std::string &p_CurrCode){return "$";};
    
    virtual void openFbInviteDialog();
    
    virtual void signInWithApple();
    
    virtual bool isFacebookLoggedIn();
    
    virtual void pushDataToUserdata(const std::string &fbId,const std::string &username,const std::string &profileUrl,const std::string &language,int level,int coins);

    virtual void getLeaderBoardJson(const std::string &language);
    
    virtual void pushFirebaseUserLanguageData(const std::string &fbId,const std::string &language,int currentLevel,int coins);
    
    virtual void getUserLanguageDataJson(const std::string &fbId);

    virtual void getLanguageDataUserIds();
    
    virtual void checkForGameUpdate();
    
    virtual void initialize();

    virtual void setUserFbId(const std::string &fbId);
    
    virtual void getUserRank(const std::string &language);

};
#endif /* InterfaceManager_hpp */
