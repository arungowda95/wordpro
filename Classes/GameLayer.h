//
//  GameLayer.hpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#ifndef GameLayer_h
#define GameLayer_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class HudLayer;
class OptionGrid;
class AnswerGrid;
class LevelManager;
class GameLayer:public Layer
{
    
    Size visibleSize;
    Vec2 Origin;
    HudLayer       *m_ptrHudLayer;
    OptionGrid     *m_ptrOptionGrid;
    AnswerGrid     *m_ptrAnswerGrid;
    LevelManager   *m_ptrLevelManager;
    
    Point m_pStartPos,m_pEndPos;
    
    CC_SYNTHESIZE(Node*, m_tutorNode, TutorNode);
    
    CC_SYNTHESIZE(Sprite*, gameBg, gameBg);
    
    CC_SYNTHESIZE(Sprite*, placeBg, placeBg);
    CC_SYNTHESIZE(Label*, placeLbl, placeLbl);
    
    CC_SYNTHESIZE(Sprite*, AnswersBg, AnswersBg);
    
    CC_SYNTHESIZE(Label*, wordslbl, wordslbl);
    
    CC_SYNTHESIZE(Sprite*, m_SprTutorBox, tutorBox);
    
public:
    
    Sprite *m_SprBgAlpha;
    
    GameLayer();
    ~GameLayer();
    
    /*****************TOUCH DELEGATES FUNCTIONS****************/
    virtual bool onTouchBegan(Touch *p_Touch, Event *p_Event);
    virtual void onTouchMoved(Touch *p_Touch, Event *p_Event);
    virtual void onTouchEnded(Touch *p_Touch, Event *p_Event);
    
    /// SET AND GET METHODS
    void setHudLayer(Layer *p_PtrHudlayer);
    
    /// RETURN HUDLAYER OBJECT
    HudLayer* getHudLayer();
    
    /// RETURN OptionGrid OBJECT
    OptionGrid *getOptionGrid();
    
    /// RETURN ANSWERGRID OBJECT
    AnswerGrid *getAnswerGrid();
    
    /// Returns Level Manager Object
    LevelManager *getLevelManager();
    
    /// shows tutorial for First level of game
    void checkforFirstLevelTutorial();
    
    /// removes First level tutorial Node
    void removeTutorial();
};

#endif /* GameLayer_hpp */
