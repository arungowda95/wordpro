//
//  DailyRewardPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 24/03/20.
//

#include "DailyRewardPopUp.h"

DailyRewardPopUp::DailyRewardPopUp(std::function<void()> func)
{
    m_fnSelector = func;
    
    BonusItemArray = NULL;
    t_miCollectCoinBtn = NULL;
    t_miDoubleCoinBtn  = NULL;
    t_miCollectCoinBtn = NULL;
    BonusItemArray = new Vector<Sprite*>();
    
     ResolutionSize = Director::getInstance()->getSafeAreaRect();
     visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
     origin = ResolutionSize.origin;
    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setScaleY(m_SprPopUpBg->getScaleY()+0.12);
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.25);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    std::string t_Label = "DailyReward";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,60);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setWidth(m_SprPopUpTop->getBoundingBox().size.width-64);
    t_tLabel->setHeight(t_tLabel->getBoundingBox().size.height+10);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    this->addChild(t_tLabel, 2);
    
    
    if(me_Language.compare(LANG_KANNADA)==0)
    {
        t_tLabel->setSystemFontSize(50);
    }
    
    std::string t_congrats = "Congrats";
    Label *t_tCongratLabel = createLabelBasedOnLang(t_congrats,45);
    t_tCongratLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCongratLabel->setColor(Color3B::WHITE);
    t_tCongratLabel->enableOutline(Color4B(76,33,14,255),3);
    t_tCongratLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tCongratLabel->setHeight(t_tCongratLabel->getBoundingBox().size.height+10);
    t_tCongratLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-10-t_tCongratLabel->getBoundingBox().size.height/2));
    this->addChild(t_tCongratLabel, 2);
    
    std::string t_subtext = "DailyRewardMsg";
    Label *t_tSubLabel = createLabelBasedOnLang(t_subtext, 35);
    t_tSubLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tSubLabel->setColor(Color3B(255,244,55));
    t_tSubLabel->enableOutline(Color4B(76,33,14,255),3);
    t_tSubLabel->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
    t_tSubLabel->setHeight(t_tSubLabel->getBoundingBox().size.height+10);
    t_tSubLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tCongratLabel->getBoundingBox().getMinY()-10-t_tSubLabel->getBoundingBox().size.height/2));
    this->addChild(t_tSubLabel, 2);

    Sprite *sprite = Sprite::create("large_box.png");
    sprite->setScaleY(sprite->getScaleY()+0.3);
    sprite->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_tSubLabel->getBoundingBox().getMinY()-20-sprite->getBoundingBox().size.height/2));
    this->addChild(sprite,2);
    
    Point pos = Point(sprite->getBoundingBox().getMinX(),sprite->getBoundingBox().getMaxY());
    createDailyBonusItems(pos);
    
    checkForDailyReward();
    
    float width = (m_fScale*90)/2;
    t_miCollectCoinBtn = getButtonMade("large_button.png","large_button.png",CC_CALLBACK_0(DailyRewardPopUp::OnCollectButtonPressed,this));
    t_miCollectCoinBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),sprite->getBoundingBox().getMinY()-10-t_miCollectCoinBtn->getBoundingBox().size.height/2));
    t_miCollectCoinBtn->setTag(0);
    
    
    std::string t_CText = LanguageTranslator::getInstance()->getTranslatorStringWithTag("Collect");
    t_CText.append(StringUtils::format(" %d",m_iCoinCount));
    Label *t_tCollectLabel;
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tCollectLabel = Label::createWithTTF(t_CText, FONT_NAME,35, Size(0, 0));
    }
    else
    {
       t_tCollectLabel = Label::createWithSystemFont(t_CText, SYS_FONT_NAME,35, Size(0, 0));
       t_tLabel->enableBold();
    }
    t_tCollectLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tCollectLabel->setColor(Color3B::WHITE);
    t_tCollectLabel->setName("Collect");
    t_tCollectLabel->enableOutline(Color4B(53,126,32,255),2);
    t_tCollectLabel->setWidth(t_tCollectLabel->getBoundingBox().size.width+10);
    t_tCollectLabel->setHeight(150);
    t_tCollectLabel->setPosition(Vec2(width+t_miCollectCoinBtn->getBoundingBox().size.width/2-10 ,t_miCollectCoinBtn->getBoundingBox().size.height/2+15.0f));
    t_miCollectCoinBtn->addChild(t_tCollectLabel,1);
    
    Sprite *CollectIcon = Sprite::create("big-coins.png");
    CollectIcon->setScale(CollectIcon->getScale()-0.1);
    CollectIcon->setPosition(Vec2(CollectIcon->getBoundingBox().size.width/2+20,t_miCollectCoinBtn->getBoundingBox().size.height/2+10.0f));
    CollectIcon->setName("Icon");
    t_miCollectCoinBtn->addChild(CollectIcon,2);
    
    Pos = Vec2(t_miCollectCoinBtn->getBoundingBox().getMinX()+30,t_miCollectCoinBtn->getPositionY());
    
    t_miDoubleCoinBtn = getButtonMade("medium_button.png","medium_button.png",CC_CALLBACK_0(DailyRewardPopUp::doublecoinBtnCallback,this));
    t_miDoubleCoinBtn->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),t_miCollectCoinBtn->getBoundingBox().getMinY()-10-t_miDoubleCoinBtn->getBoundingBox().size.height/2));
    t_miDoubleCoinBtn->setTag(0);

    Label *dbCoinLabel = createLabelBasedOnLang("doubleReward",30);
    dbCoinLabel->setColor(Color3B::WHITE);
    dbCoinLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    dbCoinLabel->enableOutline(Color4B(53,126,32,255),2);
    dbCoinLabel->setWidth(dbCoinLabel->getBoundingBox().size.width+10);
    dbCoinLabel->setHeight(150);
    dbCoinLabel->setPosition(Vec2(t_miDoubleCoinBtn->getBoundingBox().size.width/2,t_miDoubleCoinBtn->getBoundingBox().size.height/2+10));
    t_miDoubleCoinBtn->addChild(dbCoinLabel,1);

    if(!GameController::getInstance()->isVideoAdsAvailable())
    {
        t_miDoubleCoinBtn->setEnabled(false);
    }
    
     // CLOSE BUTTON
     t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_0(DailyRewardPopUp::OnCollectButtonPressed,this));
     t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
     t_miCloseButton->setTag(0);

     Menu *m_Menu = Menu::create(t_miCloseButton,t_miCollectCoinBtn,t_miDoubleCoinBtn,NULL);
     m_Menu->setPosition(Vec2::ZERO);
     this->addChild(m_Menu,2);

     registerVideoEvent();
}
void DailyRewardPopUp::createDailyBonusItems(Point pos)
{
        
    Point position = Point(pos.x+15,pos.y-30);
    int count = 4;
    int day = 1;
    for (int i = 0; i<2; i++)
    {
          for (int j = 0; j<count; j++)
          {
           
              Sprite *t_miRewardSprbg = Sprite::create("daily_reward_Norm.png"); t_miRewardSprbg->setPosition(Vec2(position.x+t_miRewardSprbg->getBoundingBox().size.width/2,position.y-t_miRewardSprbg->getBoundingBox().size.height/2));
              this->addChild(t_miRewardSprbg,2);
              
              Sprite *CoinIcon = Sprite::create("big-coins.png");
              CoinIcon->setScale(CoinIcon->getScale()-0.1);
              CoinIcon->setPosition(Vec2(t_miRewardSprbg->getBoundingBox().size.width/2,t_miRewardSprbg->getBoundingBox().size.height/2+CoinIcon->getBoundingBox().size.height/2-10));
              CoinIcon->setName("Icon");
              t_miRewardSprbg->addChild(CoinIcon,2);

              
              Label *rewardLabel = Label::createWithTTF(StringUtils::format("%d",rewardArr[day-1]),FONT_NAME,40,Size(0,0));
              rewardLabel->setColor(Color3B::WHITE);
              rewardLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
              rewardLabel->enableOutline(Color4B(0,0,0,255),2);
              rewardLabel->setName("reward");
              rewardLabel->setHeight(rewardLabel->getBoundingBox().size.height);
              rewardLabel->setPosition(Vec2(t_miRewardSprbg->getBoundingBox().size.width/2,t_miRewardSprbg->getBoundingBox().size.height/2-5-rewardLabel->getContentSize().height/2));
              t_miRewardSprbg->addChild(rewardLabel,2);
 
              
              std::string t_Cday = LanguageTranslator::getInstance()->getTranslatorStringWithTag("Day");
              t_Cday.append(StringUtils::format("%d",day));
              Label *dayLabel;
              if(me_Language.compare(LANG_ENGLISH)==0)
              {
                  dayLabel = Label::createWithTTF(t_Cday, FONT_NAME,35);
              }
              else
              {
                 dayLabel = Label::createWithSystemFont(t_Cday, SYS_FONT_NAME,35);
                 dayLabel->enableBold();
              }
              dayLabel->setColor(Color3B::WHITE);
              dayLabel->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
              dayLabel->enableOutline(Color4B(0,0,0,255),2);
              dayLabel->setWidth(dayLabel->getBoundingBox().size.width+20);
              dayLabel->setHeight(dayLabel->getBoundingBox().size.height+20);
              dayLabel->setPosition(Vec2(t_miRewardSprbg->getBoundingBox().size.width/2,-30));
              t_miRewardSprbg->addChild(dayLabel,1);
              
              BonusItemArray->pushBack(t_miRewardSprbg);
              
              position.x += t_miRewardSprbg->getBoundingBox().size.width+5;
              day++;
          }
        position.x = pos.x+((134*m_fScale)/2)+20;
        position.y = pos.y-(134*m_fScale)-30-80;
        count = 3;
      }
}
Label *DailyRewardPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

void DailyRewardPopUp::checkForDailyReward()
{
     UpdateBonusDay();
    
     updateRewardClaimedItems();
    
    switch (m_eRewardDay)
    {
        case DAY_1_REWARD:
        {
            m_iCoinCount = rewardArr[0];
            Sprite *rewardBtn = BonusItemArray->at(0);
            
            if (rewardBtn) {
                rewardBtn->setTexture("daily_reward_Select.png");
            }
            else{
                return;
            }
            Sprite *CoinIcon = (Sprite*)rewardBtn->getChildByName("Icon");
            Label *CoinLabel = (Label*)rewardBtn->getChildByName("reward");

            CoinLabel->enableOutline(Color4B(23,117,150,255),2);

            Sprite *Coinglow = Sprite::create("coins_glow.png");
            
            Coinglow->setPosition(Vec2(rewardBtn->getBoundingBox().size.width/2,CoinIcon->getPositionY()));
            Coinglow->setName("coinglow");
            rewardBtn->addChild(Coinglow,1);
            Coinglow->runAction(RepeatForever::create(RotateBy::create(3.0f, 360)));
        }
            break;
        case DAY_2_REWARD:
        {
            m_iCoinCount = rewardArr[1];
            Sprite *rewardBtn = BonusItemArray->at(1);
            
            if (rewardBtn) {
                rewardBtn->setTexture("daily_reward_Select.png");
            }
            else{
                return;
            }
            Sprite *CoinIcon = (Sprite*)rewardBtn->getChildByName("Icon");
            Label *CoinLabel = (Label*)rewardBtn->getChildByName("reward");

            CoinLabel->enableOutline(Color4B(23,117,150,255),2);

            Sprite *Coinglow = Sprite::create("coins_glow.png");
            
            Coinglow->setPosition(Vec2(rewardBtn->getBoundingBox().size.width/2,CoinIcon->getPositionY()));
            Coinglow->setName("coinglow");
            rewardBtn->addChild(Coinglow,1);
            Coinglow->runAction(RepeatForever::create(RotateBy::create(3.0f, 360)));
        }break;
        case DAY_3_REWARD:
        {
            m_iCoinCount = rewardArr[2];
            Sprite *rewardBtn = BonusItemArray->at(2);
            
            if (rewardBtn) {
                rewardBtn->setTexture("daily_reward_Select.png");
            }
            else{
                return;
            }
            Sprite *CoinIcon = (Sprite*)rewardBtn->getChildByName("Icon");
            Label *CoinLabel = (Label*)rewardBtn->getChildByName("reward");

            CoinLabel->enableOutline(Color4B(23,117,150,255),2);

            Sprite *Coinglow = Sprite::create("coins_glow.png");
            
            Coinglow->setPosition(Vec2(rewardBtn->getBoundingBox().size.width/2,CoinIcon->getPositionY()));
            Coinglow->setName("coinglow");
            rewardBtn->addChild(Coinglow,1);
            Coinglow->runAction(RepeatForever::create(RotateBy::create(3.0f, 360)));
        }break;
        case DAY_4_REWARD:
        {
            m_iCoinCount = rewardArr[3];
            Sprite *rewardBtn = BonusItemArray->at(3);
            
            if (rewardBtn) {
                rewardBtn->setTexture("daily_reward_Select.png");
            }
            else{
                return;
            }
            Sprite *CoinIcon = (Sprite*)rewardBtn->getChildByName("Icon");
            Label *CoinLabel = (Label*)rewardBtn->getChildByName("reward");

            CoinLabel->enableOutline(Color4B(23,117,150,255),2);

            Sprite *Coinglow = Sprite::create("coins_glow.png");
            
            Coinglow->setPosition(Vec2(rewardBtn->getBoundingBox().size.width/2,CoinIcon->getPositionY()));
            Coinglow->setName("coinglow");
            rewardBtn->addChild(Coinglow,1);
            Coinglow->runAction(RepeatForever::create(RotateBy::create(3.0f, 360)));
        }break;
        case DAY_5_REWARD:
        {
            m_iCoinCount = rewardArr[4];
            Sprite *rewardBtn = BonusItemArray->at(4);
            
            if (rewardBtn) {
                rewardBtn->setTexture("daily_reward_Select.png");
            }
            else{
                return;
            }
            Sprite *CoinIcon = (Sprite*)rewardBtn->getChildByName("Icon");
            Label *CoinLabel = (Label*)rewardBtn->getChildByName("reward");

            CoinLabel->enableOutline(Color4B(23,117,150,255),2);

            Sprite *Coinglow = Sprite::create("coins_glow.png");
            Coinglow->setPosition(Vec2(rewardBtn->getBoundingBox().size.width/2,CoinIcon->getPositionY()));
            Coinglow->setName("coinglow");
            rewardBtn->addChild(Coinglow,1);
            Coinglow->runAction(RepeatForever::create(RotateBy::create(3.0f, 360)));
        }break;
        case DAY_6_REWARD:
        {
            m_iCoinCount = rewardArr[5];
            Sprite *rewardBtn = BonusItemArray->at(5);
            
            if (rewardBtn) {
                rewardBtn->setTexture("daily_reward_Select.png");
            }
            else{
                return;
            }
            Sprite *CoinIcon = (Sprite*)rewardBtn->getChildByName("Icon");
            Label *CoinLabel = (Label*)rewardBtn->getChildByName("reward");

            CoinLabel->enableOutline(Color4B(23,117,150,255),2);

            Sprite *Coinglow = Sprite::create("coins_glow.png");
            Coinglow->setPosition(Vec2(rewardBtn->getBoundingBox().size.width/2,CoinIcon->getPositionY()));
            Coinglow->setName("coinglow");
            rewardBtn->addChild(Coinglow,1);
            Coinglow->runAction(RepeatForever::create(RotateBy::create(3.0f, 360)));
        }break;
        case DAY_7_REWARD:
        {
            m_iCoinCount = rewardArr[6];
            Sprite *rewardBtn = BonusItemArray->at(6);
            
            if (rewardBtn) {
                rewardBtn->setTexture("daily_reward_Select.png");
            }
            else{
                return;
            }
            Sprite *CoinIcon = (Sprite*)rewardBtn->getChildByName("Icon");
            Label *CoinLabel = (Label*)rewardBtn->getChildByName("reward");

            CoinLabel->enableOutline(Color4B(23,117,150,255),2);

            Sprite *Coinglow = Sprite::create("coins_glow.png");
            Coinglow->setPosition(Vec2(rewardBtn->getBoundingBox().size.width/2,CoinIcon->getPositionY()));
            Coinglow->setName("coinglow");
            rewardBtn->addChild(Coinglow,1);
            Coinglow->runAction(RepeatForever::create(RotateBy::create(3.0f, 360)));
        }break;
        default:
            break;
    }
    
}
void DailyRewardPopUp::setEnableButtons(bool p_Enable)
{
    t_miDoubleCoinBtn->setEnabled(p_Enable);
    t_miCollectCoinBtn->setEnabled(p_Enable);
    t_miCloseButton->setEnabled(p_Enable);
}
void DailyRewardPopUp::updateRewardClaimedItems()
{
    
    int day = m_eRewardDay;

    for(int i=0; i<day-1; i++)
    {
         Sprite *rewardBtn = BonusItemArray->at(i);
           if(rewardBtn)
           {
               rewardBtn->setOpacity(150);
               Sprite *CollctedTick = Sprite::create("right-image.png");
               CollctedTick->setPosition(Vec2(rewardBtn->getPositionX(),rewardBtn->getPositionY()));
               CollctedTick->setName("coinglow");
               CollctedTick->setOpacity(255);
               this->addChild(CollctedTick,2);
           }
           else{
               continue;
           }
    }
}
MenuItemSprite *DailyRewardPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
      
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
      
    Sprite *_DisabledSpr = Sprite::create(normalSpr);
    _DisabledSpr->setOpacity(150);

    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr,_DisabledSpr,func);
    return Button;
}
void DailyRewardPopUp::registerVideoEvent()
{
    auto t_VideoAdAvailble = EventListenerCustom::create(EVENT_VIDEO_ADS_AVAILABLE,CC_CALLBACK_0(DailyRewardPopUp::videoAdsAvailble,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdAvailble, this);
    
    auto t_VideoAdSuccess = EventListenerCustom::create(EVENT_VIDEO_ADS_SUCCEED, CC_CALLBACK_0(DailyRewardPopUp::onVideoAdSuccess,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdSuccess, this);
    
    auto t_VideoAdFailure = EventListenerCustom::create(EVENT_VIDEO_ADS_FAILED, CC_CALLBACK_0(DailyRewardPopUp::onVideoAdFailure,this));
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_VideoAdFailure, this);
}

void DailyRewardPopUp::OnButtonPressed(Ref *p_Sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    int pButtonTag =  Button->getTag();
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

      switch (pButtonTag)
      {
          case 0:
          {
              status = (char*)"close";
          }break;
           default:
        break;
      }
    
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
}
void DailyRewardPopUp::doublecoinBtnCallback()
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameController::getInstance()->showVideoAd();
    
    setEnableButtons(false);
}
void DailyRewardPopUp::onVideoAdSuccess()
{
    if(t_miCollectCoinBtn!=NULL)
    {
        m_iCoinCount = m_iCoinCount*2;
        Label *t_tCollectLabel = (Label*)t_miCollectCoinBtn->getChildByName("Collect");
        t_tCollectLabel->setString("");
        t_tCollectLabel->setString(StringUtils::format("Collect %d",m_iCoinCount));
        ScaleTo *scale1 = ScaleTo::create(0.4f,t_tCollectLabel->getScale()+0.2f);
        ScaleTo *scale2 = ScaleTo::create(0.4f,t_tCollectLabel->getScale());
        t_tCollectLabel->runAction(Sequence::create(scale1,scale2, NULL));
    }
    m_bVideoSuccess = true;
    setEnableButtons(true);
    
    if (t_miDoubleCoinBtn!=NULL) {
          t_miDoubleCoinBtn->setEnabled(false);
      }
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_AVAILABLE);
}
void DailyRewardPopUp::onVideoAdFailure()
{
    setEnableButtons(true);
    
    if(!GameController::getInstance()->isVideoAdsAvailable())
    {
        t_miDoubleCoinBtn->setEnabled(false);
    }
}
void DailyRewardPopUp::videoAdsAvailble()
{
      if(t_miDoubleCoinBtn!=NULL&&!m_bVideoSuccess)
      {
            t_miDoubleCoinBtn->setEnabled(true);
        }
}
/// ON COLLECT BUTTON PRESSED
void DailyRewardPopUp::OnCollectButtonPressed()
{
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);

    me_bDailyRewardAvailable = false;
     me_iCoinCount += m_iCoinCount;
    UserDefault::getInstance()->setIntegerForKey(COINS, me_iCoinCount);
    UserDefault::getInstance()->setBoolForKey(DAILY_REWARD_AVAILABLE, me_bDailyRewardAvailable);
    UserDefault::getInstance()->flush();
    
    setEnableButtons(false);
    
    FiniteTimeAction *_call2 = CallFuncN::create(CC_CALLBACK_0(DailyRewardPopUp::coinAddAnimation, this));
    DelayTime *_delay = DelayTime::create(1.8f);
    FiniteTimeAction *_call3 = CallFuncN::create(CC_CALLBACK_0(DailyRewardPopUp::coinAnimationDoneClosePopUp,this));
    FiniteTimeAction *_seq  = Sequence::create(_call2,_delay,_call3, NULL);
    this->runAction(_seq);
    
//    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    time_t  m_StartTime;
    time(&m_StartTime);
    UserDefault::getInstance()->setStringForKey(REWARD_START_TIME, ctime(&m_StartTime));
//    InterfaceManagerInstance::getInstance()->getInterfaceManager()->DailyBonusStarted(24);
    UpdateDailyBonusCycle();
    AchievementManager::getInstance()->OnAchievementUpdate("dailybonus",1);
    AchievementManager::getInstance()->CheckAllAchievementsComplete();
}
/// THIS RETURN BONUS DAY TYPE
void DailyRewardPopUp::UpdateBonusDay()
{
    bool t_bBonusDay1 = UserDefault::getInstance()->getBoolForKey(REWARD_DAY_1);
    bool t_bBonusDay2 = UserDefault::getInstance()->getBoolForKey(REWARD_DAY_2);
    bool t_bBonusDay3 = UserDefault::getInstance()->getBoolForKey(REWARD_DAY_3);
    bool t_bBonusDay4 = UserDefault::getInstance()->getBoolForKey(REWARD_DAY_4);
    bool t_bBonusDay5 = UserDefault::getInstance()->getBoolForKey(REWARD_DAY_5);
    bool t_bBonusDay6 = UserDefault::getInstance()->getBoolForKey(REWARD_DAY_6);
    bool t_bBonusDay7 = UserDefault::getInstance()->getBoolForKey(REWARD_DAY_7);
    
    if(t_bBonusDay1)
        m_eRewardDay = DAY_1_REWARD;
    else if (t_bBonusDay2)
        m_eRewardDay = DAY_2_REWARD;
    else if (t_bBonusDay3)
        m_eRewardDay = DAY_3_REWARD;
    else if (t_bBonusDay4)
        m_eRewardDay = DAY_4_REWARD;
    else if (t_bBonusDay5)
        m_eRewardDay = DAY_5_REWARD;
    else if (t_bBonusDay6)
        m_eRewardDay = DAY_6_REWARD;
    else if (t_bBonusDay7)
        m_eRewardDay = DAY_7_REWARD;
    else
        m_eRewardDay = DAY_1_REWARD;

}
/// THIS UPDATE BONUS CYCLE
void DailyRewardPopUp::UpdateDailyBonusCycle()
{
    switch (m_eRewardDay)
    {
        case DAY_1_REWARD:
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, true);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, false);
            break;
        case DAY_2_REWARD:
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, true);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, false);
            break;
        case DAY_3_REWARD:
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, true);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, false);
            break;
        case DAY_4_REWARD:
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, true);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, false);
            break;
        case DAY_5_REWARD:
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, true);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, false);
            break;
        case DAY_6_REWARD:
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, true);
            break;
        case DAY_7_REWARD:
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_1, true);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_2, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_3, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_4, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_5, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_6, false);
            UserDefault::getInstance()->setBoolForKey(REWARD_DAY_7, false);
            break;
        default:
            break;
    }
    UserDefault::getInstance()->flush();
}
void DailyRewardPopUp::coinAddAnimation()
{
 
    this->runAction(CallFunc::create([&,this]()
    {
        Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
        Point _startPosOppo = Point(Pos.x,Pos.y);
        for (int i=0; i< 20; i++)
        {
            Sprite *_spr = Sprite::create("coins.png");
            _spr->setScale(_spr->getScale()+0.2);
            _spr->setPosition(_startPosOppo);
            _spr->setOpacity(0);
            ccBezierConfig bezier;
            bezier.controlPoint_1 = _startPosOppo;
            bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
            bezier.endPosition =_endPos;
            BezierTo *_bez = BezierTo::create(.5f, bezier);
            this->addChild(_spr,5);
            FadeTo *_fade = FadeTo::create(.04*i,255);
            FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(DailyRewardPopUp::removeSprite,this));
            FiniteTimeAction*_seq = Sequence::create(_fade,_bez,_call,NULL);
            _spr->runAction(_seq);
        }
            if(t_miCloseButton)
            {
                t_miCloseButton->setEnabled(false);
            }
    }));

}
void DailyRewardPopUp::removeSprite(Ref *_sender)
{
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);
    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
    
    
    Point  _endPos = Vec2(origin.x+visibleSize.width-150,origin.y+visibleSize.height)-m_ZeroPosition;
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
    
    Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(EVENT_UPDATE_COINS);
    
}
void DailyRewardPopUp::coinAnimationDoneClosePopUp()
{
    status = (char*)"close";
    if (m_fnSelector!=NULL) {
        m_fnSelector();
    }
    if(t_miCloseButton)
    {
        t_miCloseButton->setEnabled(true);
    }
    
    GameController::getInstance()->showInterstial();
}

DailyRewardPopUp::~DailyRewardPopUp()
{
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_AVAILABLE);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_SUCCEED);
    Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_VIDEO_ADS_FAILED);
    this->removeAllChildrenWithCleanup(true);
}
