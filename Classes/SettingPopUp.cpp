//
//  SettingPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 11/03/20.
//

#include "SettingPopUp.h"


SettingPopUp::SettingPopUp(std::function<void()> func)
{
    m_fnSelector = func;
    
    Rect ResolutionSize = Director::getInstance()->getSafeAreaRect();
    Size visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    Point origin = ResolutionSize.origin;

    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    float width = (m_fScale*90)/2;
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    
    std::string t_Label = "Settings";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,50);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tLabel->setWidth(m_SprPopUpTop->getContentSize().width-70);
    t_tLabel->setHeight(150);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(t_tLabel, 2);

    
    //Sound Button
    Point pos = Point(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY());
    t_SoundButton = getButtonMade("small_button.png", "small_button.png", CC_CALLBACK_1(SettingPopUp::onSoundButttonCallBack,this));
    t_SoundButton->setPosition(Vec2(pos.x-5-t_SoundButton->getBoundingBox().size.width/2,pos.y-t_SoundButton->getBoundingBox().size.height*0.7));
    t_SoundButton->setTag(0);
    
     txtWidth = 130*t_SoundButton->getScaleY();

    Label *SoundLabel = createButtonTextLabel("Sound",35);
    SoundLabel->setPosition(Vec2(width+t_SoundButton->getBoundingBox().size.width/2,t_SoundButton->getBoundingBox().size.height/2+15.0f));
    t_SoundButton->addChild(SoundLabel,1);

    Sprite *soundIcon = Sprite::create("sound-on.png");
    soundIcon->setPosition(Vec2(soundIcon->getBoundingBox().size.width-5,t_SoundButton->getBoundingBox().size.height/2+10.0f));
    soundIcon->setName("Icon");
    t_SoundButton->addChild(soundIcon,2);
    
    Sprite *soundoff = Sprite::create("off-image.png");
    soundoff->setPosition(Vec2(soundIcon->getBoundingBox().size.width/2,soundIcon->getBoundingBox().size.height/2));
    soundoff->setName("off");
    soundIcon->addChild(soundoff,2);
    
      if(GameController::getInstance()->isSFXEnabled())
      {
          soundoff->setVisible(false);
      }
      else
      {
          soundoff->setVisible(true);
      }
    
    //Music Button
     t_MusicButton = getButtonMade("small_button.png", "small_button.png", CC_CALLBACK_1(SettingPopUp::musicButttonCallBack,this));
    t_MusicButton->setPosition(Vec2(pos.x+5+t_MusicButton->getBoundingBox().size.width/2,pos.y-t_MusicButton->getBoundingBox().size.height*0.7));
    t_MusicButton->setTag(1);
    
    Label *musicLabel = createButtonTextLabel("Music",35);
    musicLabel->setPosition(Vec2(width+t_MusicButton->getBoundingBox().size.width/2,t_MusicButton->getBoundingBox().size.height/2+15.0f));
    t_MusicButton->addChild(musicLabel,1);
    
    Sprite *musicIcon = Sprite::create("music-on.png");
    musicIcon->setPosition(Vec2(musicIcon->getBoundingBox().size.width-5,t_MusicButton->getBoundingBox().size.height/2+10.0f));
    musicIcon->setName("Icon");
    t_MusicButton->addChild(musicIcon,2);
    
    Sprite *musicoff = Sprite::create("off-image.png");
    musicoff->setPosition(Vec2(musicIcon->getBoundingBox().size.width/2,musicIcon->getBoundingBox().size.height/2));
    musicoff->setName("off");
    musicIcon->addChild(musicoff,2);

    if(GameController::getInstance()->isBGMEnabled())
    {
        musicoff->setVisible(false);
    }
    else
    {
        musicoff->setVisible(true);
    }

    //Notification  Button
     pos = Point(m_SprPopUpBg->getBoundingBox().getMidX(),t_MusicButton->getBoundingBox().getMinY());
    t_NotifyButton = getButtonMade("large_button.png", "large_button.png", CC_CALLBACK_1(SettingPopUp::notificationCallBack,this));
    t_NotifyButton->setPosition(Vec2(pos.x,pos.y-t_NotifyButton->getBoundingBox().size.height*0.7));
    t_NotifyButton->setTag(1);
    
    txtWidth = 240*t_NotifyButton->getScaleY();
    
    Label *NotifyLabel = createButtonTextLabel("Notification",35);
    NotifyLabel->setPosition(Vec2(width+t_NotifyButton->getBoundingBox().size.width*0.5 ,t_NotifyButton->getBoundingBox().size.height/2+15.0f));
    t_NotifyButton->addChild(NotifyLabel,1);
    
    Sprite *NotifyIcon = Sprite::create("notification-on.png");
    NotifyIcon->setPosition(Vec2(NotifyIcon->getBoundingBox().size.width-5,t_NotifyButton->getBoundingBox().size.height/2+10.0f));
    NotifyIcon->setName("Icon");
    t_NotifyButton->addChild(NotifyIcon,2);

    Sprite *NotifyOff = Sprite::create("off-image.png");
    NotifyOff->setPosition(Vec2(NotifyIcon->getBoundingBox().size.width/2,NotifyIcon->getBoundingBox().size.height/2));
    NotifyOff->setName("off");
    NotifyIcon->addChild(NotifyOff,2);
    
    if(me_bNotification){
        NotifyOff->setVisible(false);
    }else{
        NotifyOff->setVisible(true);
    }
    
    //Language Button
    pos.y = t_NotifyButton->getBoundingBox().getMinY();
    t_LangButton = getButtonMade("large_button.png", "large_button.png", CC_CALLBACK_1(SettingPopUp::languageButttonCallBack,this));
       t_LangButton->setPosition(Vec2(pos.x,pos.y-t_NotifyButton->getBoundingBox().size.height*0.7));
    
    Label *LangLabel = createButtonTextLabel("Language",35);
    LangLabel->setPosition(Vec2(width+t_LangButton->getBoundingBox().size.width*0.5,t_LangButton->getBoundingBox().size.height/2+15.0f));
    t_LangButton->addChild(LangLabel,1);

    Sprite *LangIcon = Sprite::create("language.png");
    LangIcon->setPosition(Vec2(LangIcon->getBoundingBox().size.width-5,t_LangButton->getBoundingBox().size.height/2+10.0f));
    LangIcon->setName("Icon");
    t_LangButton->addChild(LangIcon,2);

    
    // how to play button
    t_HtoPlayButton = getButtonMade("large_button.png", "large_button.png", CC_CALLBACK_1(SettingPopUp::OnButtonPressed,this));
       t_HtoPlayButton->setPosition(Vec2(pos.x,t_LangButton->getBoundingBox().getMinY()-t_HtoPlayButton->getBoundingBox().size.height*0.45));
       t_HtoPlayButton->setTag(2);
    
    Sprite *HtoPIcon = Sprite::create("haw-to-play.png");
    HtoPIcon->setPosition(Vec2(HtoPIcon->getBoundingBox().size.width-5,t_HtoPlayButton->getBoundingBox().size.height/2+10.0f));
    HtoPIcon->setName("Icon");
    t_HtoPlayButton->addChild(HtoPIcon,2);
    
    Label *HtoPLabel = createButtonTextLabel("How to play",35);
    HtoPLabel->setPosition(Vec2(width+t_HtoPlayButton->getBoundingBox().size.width*0.5,t_HtoPlayButton->getBoundingBox().size.height/2+15.0f));
    t_HtoPlayButton->addChild(HtoPLabel,1);

    
    // feedback button
    t_FeedbackButton = getButtonMade("large_button.png", "large_button.png", CC_CALLBACK_1(SettingPopUp::OnButtonPressed,this));
       t_FeedbackButton->setPosition(Vec2(pos.x,t_LangButton->getBoundingBox().getMinY()-t_FeedbackButton->getBoundingBox().size.height*0.7));
       t_FeedbackButton->setTag(3);
    
    Label *feedLabel = createButtonTextLabel("Feedback",35);
    feedLabel->setPosition(Vec2(width+t_FeedbackButton->getBoundingBox().size.width*0.5,t_FeedbackButton->getBoundingBox().size.height/2+15.0f));
    t_FeedbackButton->addChild(feedLabel,1);

    Sprite *feedbackIcon = Sprite::create("feedback.png");
    feedbackIcon->setPosition(Vec2(feedbackIcon->getBoundingBox().size.width-5,t_FeedbackButton->getBoundingBox().size.height/2+10.0f));
    feedbackIcon->setName("Icon");
    t_FeedbackButton->addChild(feedbackIcon,2);
    
    // RATE BUTTON
    pos.y = (t_FeedbackButton->getBoundingBox().getMinY());
    t_RateButton = getButtonMade("empty_smallButton.png", "empty_smallButton.png", CC_CALLBACK_1(SettingPopUp::OnButtonPressed,this));
       t_RateButton->setPosition(Vec2(pos.x-10-t_RateButton->getBoundingBox().size.width/2,t_FeedbackButton->getBoundingBox().getMinY()-t_RateButton->getBoundingBox().size.height*0.7));
       t_RateButton->setTag(4);

    txtWidth = 240*t_NotifyButton->getScaleY();
    
    Label *label = createButtonTextLabel("Rate",45);
    label->setPosition(Vec2(t_RateButton->getContentSize().width/2,t_RateButton->getContentSize().height/2+15));
    t_RateButton->addChild(label,1);
    
    // SHARE BUTTON
    t_ShareButton = getButtonMade("empty_smallButton.png", "empty_smallButton.png", CC_CALLBACK_1(SettingPopUp::OnButtonPressed,this));
       t_ShareButton->setPosition(Vec2(pos.x+10+t_ShareButton->getBoundingBox().size.width/2,t_FeedbackButton->getBoundingBox().getMinY()-t_ShareButton->getBoundingBox().size.height*0.7));
       t_ShareButton->setTag(5);
    
    txtWidth = 200*t_ShareButton->getScaleY();
    
    Label *shareLabel = createButtonTextLabel("Share",45);
    shareLabel->setPosition(Vec2(t_ShareButton->getContentSize().width/2,t_ShareButton->getContentSize().height/2+15));
    t_ShareButton->addChild(shareLabel,1);
    
    // CLOSE BUTTON
    MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(SettingPopUp::OnButtonPressed,this));
    t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
    t_miCloseButton->setTag(0);
    
    m_mSettingMenu = Menu::create(t_miCloseButton,t_SoundButton,t_MusicButton,t_NotifyButton,t_LangButton,t_FeedbackButton, t_RateButton,t_ShareButton,NULL);
    m_mSettingMenu->setPosition(Point::ZERO);
    this->addChild(m_mSettingMenu, 2);

    printf("posx-->%f posy-->%f\n",m_SprPopUpBg->getPositionX(),m_SprPopUpBg->getPositionY());
}
/// CUSTOM BUTTON MADE
MenuItemSprite * SettingPopUp :: getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}
Label *SettingPopUp::createButtonTextLabel(std::string txt,float fontsize)
{
    Label*label;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(txt.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        label = Label::createWithTTF(langstr, FONT_NAME,fontsize, Size(0, 100));
    }
    else
    {
       label = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       label->setWidth(label->getContentSize().width);
       label->setHeight(label->getContentSize().height+20);
        if(label->getContentSize().width>txtWidth)
        {
            float scale = (txtWidth)/(label->getContentSize().width+5);
            
            label->setSystemFontSize(fontsize*scale);
        }
    }

    label->setColor(Color3B::WHITE);
    label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    label->enableOutline(Color4B(53,126,32,255),2);
    label->enableBold();
    
    return label;
}
Label *SettingPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

Sprite *SettingPopUp::createButtonSprite(std::string sprite,Point Pos)
{
    Sprite *spriteIcon;
    
    spriteIcon = Sprite::create(sprite);
    spriteIcon->setPosition(Pos);
    
    return spriteIcon;
}
void SettingPopUp::OnButtonPressed(Ref *p_Sender)
{
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    
    int pButtonTag =  Button->getTag();

    switch (pButtonTag)
    {
        case 0:
        {
            status = (char*)"close";
        }break;
        case 1:
        {
            status = (char*)"Notification";
        }break;
        case 2:
        {
            status = (char*)"HowtoPlay";
        }break;
        case 3:
        {
            status = (char*)"Feedback";
        }break;
        case 4:
        {
            status = (char*)"Rate";
        }break;
        case 5:
        {
            status = (char*)"Share";
        }break;
        default:
            break;
    }
    
    if(m_fnSelector != NULL)
    {
        m_fnSelector();
    }

}
void SettingPopUp::onSoundButttonCallBack(Ref* sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)sender;
    Sprite *ButtonIcon = (Sprite*)Button->getChildByName("Icon");
    Sprite *offIcon = (Sprite*)ButtonIcon->getChildByName("off");
    
    
    if(offIcon!=NULL)
    {
        if(GameController::getInstance()->isSFXEnabled())
        {
            GameController::getInstance()->setSFXEnabled(false);
            offIcon->setVisible(true);
        }
        else
        {
            GameController::getInstance()->setSFXEnabled(true);
            offIcon->setVisible(false);
            GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
        }
    }
}

void SettingPopUp::musicButttonCallBack(Ref* sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)sender;

    Sprite *ButtonIcon = (Sprite*)Button->getChildByName("Icon");
    Sprite *offIcon = (Sprite*)ButtonIcon->getChildByName("off");
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    
    if(offIcon!=NULL)
    {
        if(GameController::getInstance()->isBGMEnabled())
        {
            GameController::getInstance()->setBGMEnabled(false);
            GameController::getInstance()->stopSounds();
            offIcon->setVisible(true);
        }
        else
        {
            GameController::getInstance()->setBGMEnabled(true);
            offIcon->setVisible(false);
            GameController::getInstance()->playBGM(BGM_GAME, true);
        }
    }
}

void SettingPopUp::notificationCallBack(Ref* sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    
    MenuItemSprite *Button  = (MenuItemSprite*)sender;
    Sprite *ButtonIcon = (Sprite*)Button->getChildByName("Icon");
    
    Sprite *offIcon = (Sprite*)ButtonIcon->getChildByName("off");
    
    if(offIcon!=NULL)
    {
        if(me_bNotification)
        {
            me_bNotification = false;
            UserDefault::getInstance()->setBoolForKey("Notification", false);
            UserDefault::getInstance()->flush();
            offIcon->setVisible(true);
        }
        else
        {
            me_bNotification = true;
            UserDefault::getInstance()->setBoolForKey("Notification", true);
            UserDefault::getInstance()->flush();
            offIcon->setVisible(false);
        }
    }
}

void SettingPopUp::languageButttonCallBack(Ref *sender)
{
    MenuItemSprite *Button  = (MenuItemSprite*)sender;
    
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    status = (char*)"Language";
    if(m_fnSelector != NULL)
    {
        m_fnSelector();
    }
    
}
SettingPopUp::~SettingPopUp()
{
    this->removeAllChildrenWithCleanup(true);
}
