//
//  HudLayer.cpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#include "HudLayer.h"
#include "GameLayer.h"
#include "GameScene.h"
#include "GameStateManager.h"
#include "MainMenuScene.h"
#include "OptionGrid.h"
#include "AnswerGrid.h"
#include "GameConstants.h"
#include "LevelScene.h"
#include "PopUpManager.h"
#include "OptionGrid.h"
#include "PopUpManager.h"
#include "GameController.h"
#include "InterfaceManagerInstance.h"
#include "StatsManager.h"
#include "AchievementManager.h"
#include "DailyMissionManager.h"
#include "TutorialManager.h"
HudLayer::HudLayer()
{
    
    GameController::getInstance()->showBanner();
    
    me_strScreenName = "GameScreen";
    
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsScreen(me_strScreenName);

    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    Origin = ResolutionSize.origin;
    
    m_ptrGameLayer = NULL;
    m_iWrongAttempt = 0;
    m_CoinCount   = NULL;
    m_bIsEventCreated = false;
    m_bTutorialAvailble = false;
    
    m_BackButton  = NULL;
    m_HintButton  = NULL;
    m_CoinButton  = NULL;
    m_BonusWordsButton = NULL;
    m_DailyButton   = NULL;
    m_ShuffleButton = NULL;
    m_LettrRevealButton = NULL;
    m_FreeCoinsButton = NULL;
    m_FeedBackButton  = NULL;
    
    m_ptrPopUpManager = NULL;
    m_ptrTutorManager = NULL;
    
    t_TopMenu     = NULL;
    t_BottomMenu  = NULL;
    t_shuffleMenu = NULL;
    m_tutNode     = NULL;
    t_RevealMenu  = NULL;
    
    t_HintMenu = NULL;
    toastBg    = NULL;
    toastLbl   = NULL;
    
    if(SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()&&GameController::getInstance()->isBGMEnabled())
    {
        SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
        GameController::getInstance()->playBGM(BGM_GAME_PLAY, true);
    }

    auto KeyListener = EventListenerKeyboard::create();
    KeyListener->setEnabled(true);
    KeyListener->onKeyPressed = CC_CALLBACK_2(HudLayer::onKeyPressed, this);
    KeyListener->onKeyReleased = CC_CALLBACK_2(HudLayer::onKeyReleased, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(KeyListener, this);

    
    //Back Button
    m_BackButton = getButtonMade("more-games-button.png","more-games-button.png",CC_CALLBACK_1(HudLayer::backBtnCallback,this));
    m_BackButton->setScale(m_fScale); m_BackButton->setPosition(Vec2(5+Origin.x+m_BackButton->getBoundingBox().size.width/2,Origin.y+visibleSize.height-10-m_BackButton->getBoundingBox().size.height/2));

    Sprite *backIcon = Sprite::create("back.png");
    backIcon->setPosition(Vec2(m_BackButton->getContentSize().width/2,m_BackButton->getContentSize().height/2+15));
    m_BackButton->addChild(backIcon,1);
    
    //Add Coins Button
    m_CoinButton = getButtonMade("add-dollar-button.png","add-dollar-button.png",CC_CALLBACK_1(HudLayer::coinBtnCallBack,this));
    m_CoinButton->setScale(m_fScale);
   m_CoinButton->setPosition(Vec2(Origin.x+visibleSize.width-20-m_CoinButton->getBoundingBox().size.width/2,Origin.y+visibleSize.height-10-m_CoinButton->getBoundingBox().size.height/2));
       
       
    std::string coinStr = GameController::getInstance()->getCoinsFormat(me_iCoinCount);
    m_CoinCount = Label::createWithTTF(coinStr,FONT_NAME,20);
    m_CoinCount->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    m_CoinCount->enableOutline(Color4B(30, 30, 11, 255),3);
    m_CoinCount->setColor(Color3B::WHITE);
    if (me_iCoinCount<=50) {
        m_CoinCount->setColor(Color3B::RED);
    }
    m_CoinCount->setPosition(Vec2(m_CoinButton->getContentSize().width/2-5,m_CoinButton->getContentSize().height/2));
    m_CoinButton->addChild(m_CoinCount,2);
    
    
    // Daily Event Button
    m_DailyButton = getButtonMade("daily-event-button.png","daily-event-button.png",CC_CALLBACK_1(HudLayer::dailyEventBtnCallback, this));
    m_DailyButton->setScale(m_fScale); m_DailyButton->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height-5-m_DailyButton->getBoundingBox().size.height/2));
    
    Sprite *SprTaskImage = Sprite::create();
    SprTaskImage->setPosition(Vec2(m_DailyButton->getContentSize().width/2,m_DailyButton->getContentSize().height*0.1));
    SprTaskImage->setName("tag");
    SprTaskImage->setVisible(false);
    m_DailyButton->addChild(SprTaskImage,2);

    if(UserDefault::getInstance()->getBoolForKey(NEW_TASK))
    {
        SprTaskImage->setTexture("New_Task.png");
        SprTaskImage->setVisible(true);
    }

    t_TopMenu = Menu::create(m_BackButton,m_DailyButton,m_CoinButton,NULL);
    t_TopMenu->setPosition(Vec2::ZERO);
    this->addChild(t_TopMenu);

    
    float posY = Origin.y+visibleSize.height/2-50; //m_ptrGameLayer->getOptionGrid()->getOptionsBg()->getBoundingBox().getMaxY();
    
    //HINT BUTTON
    m_HintButton = getButtonMade("hint-button.png", "hint-button.png",CC_CALLBACK_1(HudLayer::hintBtnCallback, this));
    m_HintButton->setScale(m_fScale-0.1f);
    m_HintButton->setName("");
    m_HintButton->setPosition(Vec2(Origin.x+visibleSize.width-5-m_HintButton->getBoundingBox().size.width/2,posY-m_HintButton->getBoundingBox().size.height/2));
    
    Sprite *CoinSpr = Sprite::create("coins.png");
    CoinSpr->setPosition(Vec2(m_HintButton->getContentSize().width/2-5-CoinSpr->getBoundingBox().size.width/2,m_HintButton->getBoundingBox().size.height*0.25));
    CoinSpr->setName("coin");
    m_HintButton->addChild(CoinSpr,1);
    
    Label *coinTxt = Label::createWithTTF("70",FONT_NAME,30);
    coinTxt->setAlignment(TextHAlignment::LEFT,TextVAlignment::CENTER);
    coinTxt->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    coinTxt->enableOutline(Color4B(51, 153, 11, 255),3);
    coinTxt->setColor(Color3B::WHITE);
    coinTxt->setPosition(Vec2(5+CoinSpr->getBoundingBox().getMaxX(),m_HintButton->getBoundingBox().size.height*0.25));
    coinTxt->setName("price");
    m_HintButton->addChild(coinTxt,2);

    t_HintMenu = Menu::create(m_HintButton,NULL);
    t_HintMenu->setPosition(Vec2::ZERO);
    this->addChild(t_HintMenu);
    
    //SHUFFLE BUTTON
    m_ShuffleButton = getButtonMade("refresh-button.png","refresh-button.png", CC_CALLBACK_1(HudLayer::shuffleBtnCallback, this));
    m_ShuffleButton->setScale(m_fScale-0.1f);
    m_ShuffleButton->setName("");
    m_ShuffleButton->setPosition(Vec2(Origin.x+visibleSize.width-5-m_ShuffleButton->getBoundingBox().size.width/2,110+m_ShuffleButton->getBoundingBox().size.height/2));
       
    
    t_shuffleMenu = Menu::create(m_ShuffleButton,NULL);
    t_shuffleMenu->setPosition(Vec2::ZERO);
    this->addChild(t_shuffleMenu);

    
    //BONUS WORDS Button
    m_BonusWordsButton = getButtonMade("bonus-button.png","bonus-button.png", CC_CALLBACK_1(HudLayer::bonusBtnCallback, this));
    m_BonusWordsButton->setScale(m_fScale-0.1f);
    m_BonusWordsButton->setPosition(Vec2(Origin.x+5+m_BonusWordsButton->getBoundingBox().size.width/2,110+m_BonusWordsButton->getBoundingBox().size.height/2));
    
    Sprite *t_RedDotSpr = Sprite::create("Claim.png");
    t_RedDotSpr->setPosition(Vec2(m_BonusWordsButton->getContentSize().width/2+5,m_BonusWordsButton->getContentSize().height));
    t_RedDotSpr->setName("dot");
    t_RedDotSpr->setVisible(false);
    m_BonusWordsButton->addChild(t_RedDotSpr,1);
    
    int count = StatsManager::getInstance()->getLangBonusWordsCount();
    std::string Strcount = StringUtils::format("0%d",count);
    if (count==10) {
        Strcount = StringUtils::format("%d",count);
    }
    Label *b_countTxt = Label::createWithTTF(Strcount,FONT_NAME,35);
    b_countTxt->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    b_countTxt->enableOutline(Color4B(51, 153, 11, 255),3);
    b_countTxt->setColor(Color3B::WHITE);
    b_countTxt->setName("bonus");
    b_countTxt->setPosition(Vec2(5+m_BonusWordsButton->getContentSize().width/2,m_BonusWordsButton->getBoundingBox().size.height*0.2));
    m_BonusWordsButton->addChild(b_countTxt,2);
    
    checkForBonusWordButton();
    
    // Reaveal Letters Button
    m_LettrRevealButton = getButtonMade("a-button.png","a-button.png",CC_CALLBACK_1(HudLayer::revealLettrBtnCallBack, this));
    m_LettrRevealButton->setScale(m_fScale-0.1f);
    m_LettrRevealButton->setName("Lock");
    m_LettrRevealButton->setPosition(Vec2(Origin.x+5+m_LettrRevealButton->getBoundingBox().size.width/2,posY-m_LettrRevealButton->getBoundingBox().size.height/2));

    
    Sprite *t_lockSpr = Sprite::create("lock-image.png");
    t_lockSpr->setName("Lock");
    t_lockSpr->setScale(0.7f);
    t_lockSpr->setPosition(Vec2(m_LettrRevealButton->getContentSize().width/2,m_LettrRevealButton->getContentSize().height/2+10));
    t_lockSpr->setVisible(false);
     m_LettrRevealButton->addChild(t_lockSpr,3);
    
    Sprite *t_CoinSpr = Sprite::create("coins.png");
    t_CoinSpr->setName("coin");
     t_CoinSpr->setPosition(Vec2(m_LettrRevealButton->getBoundingBox().size.width/2-20,m_LettrRevealButton->getBoundingBox().size.height*0.25));
     m_LettrRevealButton->addChild(t_CoinSpr,1);
     
     Label *coinsTxt = Label::createWithTTF("150",FONT_NAME,30);
     coinsTxt->setAlignment(TextHAlignment::LEFT,TextVAlignment::CENTER);
     coinsTxt->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
     coinsTxt->enableOutline(Color4B(51, 153, 11, 255),3);
     coinsTxt->setColor(Color3B::WHITE);
     coinsTxt->setName("price");
     coinsTxt->setPosition(Vec2(5+t_CoinSpr->getBoundingBox().getMaxX(),m_LettrRevealButton->getBoundingBox().size.height*0.25));
     m_LettrRevealButton->addChild(coinsTxt,2);
    
    t_RevealMenu = Menu::create(m_LettrRevealButton,NULL);
    t_RevealMenu->setPosition(Vec2::ZERO);
    this->addChild(t_RevealMenu);

    
//    bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));

    Point posy = Point(m_HintButton->getPosition().getMidpoint(m_ShuffleButton->getPosition()));
    // FREE COINS BUTTON
    m_FreeCoinsButton = getButtonMade("free_coins_Btn.png","free_coins_Btn.png",CC_CALLBACK_1(HudLayer::freeCoinsBtnCallBack,this));
    m_FreeCoinsButton->setScale(0.6); m_FreeCoinsButton->setPosition(Vec2(Origin.x+visibleSize.width-5-m_FreeCoinsButton->getBoundingBox().size.width/2,posy.y-10.0f));

    m_FeedBackButton = getButtonMade("feedback-button.png","feedback-button.png",CC_CALLBACK_1(HudLayer::feedBackBtnCallBack,this));
    m_FeedBackButton->setScale(0.7); m_FeedBackButton->setPosition(Vec2(Origin.x+5+m_FeedBackButton->getBoundingBox().size.width/2,posy.y));
    m_FeedBackButton->setVisible(false);
    
    t_BottomMenu = Menu::create(m_BonusWordsButton,m_FreeCoinsButton,m_FeedBackButton,NULL);
    t_BottomMenu->setPosition(Vec2::ZERO);
    this->addChild(t_BottomMenu);
    
    
    m_ptrPopUpManager = new PopUpManager(this,SCREEN_GAME);
    m_ptrPopUpManager->autorelease();
    this->addChild(m_ptrPopUpManager,3);
    
    m_ptrTutorManager = new TutorialManager();
    m_ptrTutorManager->setPosition(Vec2::ZERO);
    this->addChild(m_ptrTutorManager,4);
    
    checkforRevealLetteroption();

    checkForTutorial();
    
    checkForDailyMissionUpdates();
    
    std::string str = StringUtils::format("%d",StatsManager::getInstance()->getLangPlayedlevelCount());
    
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsEvent("levelPlayed","Language", me_Language,"levelnum",str);
}
//CUSTOM BUTTON MADE
MenuItemSprite * HudLayer :: getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    Sprite *_disabledSpr = Sprite::create(normalSpr);
    _disabledSpr->setOpacity(150);

    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr,_SelectedSpr,_disabledSpr,func);
    return Button;
}
/// Create Toast type text for wrong word match and repeated
/// @param str  string to be displayed
void HudLayer::createToast(std::string str)
{
    if(m_bHintAnim)
    {
      return;
    }
    
    float posy = m_ptrGameLayer->getAnswersBg()->getBoundingBox().getMinY();
    
    if(toastBg)
    {
        this->removeChild(toastBg,true);
        toastBg = NULL;
    }
    if(toastLbl)
    {
        this->removeChild(toastLbl,true);
        toastLbl = NULL;
    }
    float scaleX = (visibleSize.width+10)/70;
    toastBg = Sprite::create("pixel.png");
    toastBg->setScale(0, 1.0);
    toastBg->setPosition(Vec2(Origin.x+visibleSize.width/2,posy-toastBg->getBoundingBox().size.height/2));
    toastBg->setColor(Color3B::BLACK);
    toastBg->setOpacity(150);
    this->addChild(toastBg);
    
    std::string toastStr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(str.c_str());
    toastLbl = Label::createWithTTF(toastStr.c_str(),FONT_NAME,30);
    
    if(me_Language.compare(LANG_ENGLISH)!=0)
        toastLbl = Label::createWithSystemFont(toastStr.c_str(),FONT_NAME, 30);

    toastLbl->setDimensions(toastBg->getBoundingBox().size.width-20,toastLbl->getBoundingBox().size.height+10);
    toastLbl->setPosition(toastBg->getPosition());
    toastLbl->setColor(Color3B::WHITE);
    toastLbl->setScale(0);
    this->addChild(toastLbl,1);
    
    ScaleTo *scale1 = ScaleTo::create(0.2f, scaleX,1.0);
    ScaleTo *scale2 = ScaleTo::create(0.2f, 1.0f,1.0);
    RemoveSelf *remove = RemoveSelf::create();
    toastBg->runAction(Sequence::create(scale1,DelayTime::create(1.0f),remove, NULL));
    toastLbl->runAction(Sequence::create(scale2,DelayTime::create(1.0f),remove, NULL));
    
}
void HudLayer::backBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    m_ptrPopUpManager->LoadPopUp(POPUP_GAME_EXIT);
    setEnableButtons(false);
}
void HudLayer::dailyEventBtnCallback(Ref* sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    m_ptrPopUpManager->LoadPopUp(POPUP_DAILY_MISSION);
    setEnableButtons(false);
}
/// This enable's and disables button's click
/// @param p_bEnable  boolean value to enable or disable
void HudLayer::setEnableButtons(bool p_bEnable)
{
    m_BackButton->setEnabled(p_bEnable);
    m_HintButton->setEnabled(p_bEnable);
    m_CoinButton->setEnabled(p_bEnable);
    m_BonusWordsButton->setEnabled(p_bEnable);
    m_DailyButton->setEnabled(p_bEnable);
    m_ShuffleButton->setEnabled(p_bEnable);
    
    m_FreeCoinsButton->setEnabled(p_bEnable);
    m_FeedBackButton->setEnabled(p_bEnable);
    m_LettrRevealButton->setEnabled(p_bEnable);

}
void HudLayer::bonusBtnCallback(Ref* sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    m_ptrPopUpManager->LoadPopUp(POPUP_BONUS_WORD);
    setEnableButtons(false);
}
/// updates Bonus word count found by user 
void HudLayer::UpdateBonusWord()
{
    AchievementManager::getInstance()->OnAchievementUpdate("bonusword",1);
    AchievementManager::getInstance()->CheckAllAchievementsComplete();

    DailyMissionManager::getInstance()->OnDailyMissionUpdate("bonusword",1);
    DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();
    
    {
        StatsManager::getInstance()->updateLangBonusWordsCount(1);
        checkForBonusWordButton();
    }
}
/// this will update the bonus word count in bonus word button
void HudLayer::checkForBonusWordButton()
{
    int count = StatsManager::getInstance()->getLangBonusWordsCount();
    std::string Strcount = StringUtils::format("0%d",count);
    if (count>=10) {
        Strcount = StringUtils::format("%d",count);
    }
    
    if (m_BonusWordsButton)
    {
        Sprite *dotSpr = (Sprite*)m_BonusWordsButton->getChildByName("dot");
        Label *b_countTxt = (Label*)m_BonusWordsButton->getChildByName("bonus");
        b_countTxt->setString(Strcount);
        
        if(StatsManager::getInstance()->getLangBonusWordsCount()>=10)
        {
            dotSpr->setVisible(true);
        }else{
            dotSpr->setVisible(false);
        }
    }
}
// REVEAL BUTTON CLICK
void HudLayer::revealLettrBtnCallBack(Ref *sender)
{
    
    MenuItemSprite *button = (MenuItemSprite*)sender;
    
    if(button->getName()=="Lock")
    {
        m_ptrPopUpManager->LoadPopUp(POPUP_UNLOCK_POWER);
        GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
        setEnableButtons(false);
        return;
    }
    else if (button->getName()=="Free")
    {
        if (m_ptrGameLayer->getOptionGrid()!=NULL) {
          m_ptrGameLayer->getOptionGrid()->showCountofLetters();
        }
        GameController::getInstance()->playSFX(SFX_HINT_LETTER_BONUS);
        removeRevealTutorial();
        return;
    }
    
    
    if(GameStateManager::getInstance()->getGameState()!= GAME_STATE_PAUSE)
    {
          if(me_iCoinCount>=150)
          {
                m_LettrRevealButton->setEnabled(false);
              
              if (m_ptrGameLayer->getOptionGrid()!=NULL) {
                m_ptrGameLayer->getOptionGrid()->showCountofLetters();
              }
                GameController::getInstance()->playSFX(SFX_HINT_LETTER_BONUS);
                me_iCoinCount -= 150;
                UserDefault::getInstance()->setIntegerForKey(COINS,me_iCoinCount);
                UpdateCoins();
              AchievementManager::getInstance()->OnAchievementUpdate("Letter_A",1);
              AchievementManager::getInstance()->CheckAllAchievementsComplete();
              
              DailyMissionManager::getInstance()->OnDailyMissionUpdate("Letter_A",1);
              DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();

          }
          else
          {
              InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage("Not Enough Coins");
              CoinLabelAnimation();
              m_ptrPopUpManager->LoadPopUp(POPUP_BUY_COINS);
              GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
              setEnableButtons(false);
          }
      }
}
///****************************KEYPAD FUNCTIONS********************/
void HudLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event *event)
{
    
    
}
void HudLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *unused_event)
{
    if(keyCode==EventKeyboard::KeyCode::KEY_BACK)
    {
        if(m_bTutorialAvailble) {
            
        }else{
            m_ptrPopUpManager->OnKeyButtonPressed();
        }
    }
}
void HudLayer::hintBtnCallback(Ref *sender)
{
    MenuItemSprite *button = (MenuItemSprite*)sender;
    if(button->getName()=="Free")
    {
        GameController::getInstance()->playSFX(SFX_HINT_LETTER_BONUS);
        m_ptrGameLayer->getAnswerGrid()->doHint();
        removeHintTutorial();
        return;
    }
    
    if(GameStateManager::getInstance()->getGameState()!= GAME_STATE_PAUSE)
    {
        
        if (me_iCoinCount>=70)
        {
            if(m_ptrGameLayer->getAnswerGrid()->doHint())
            {
                GameController::getInstance()->playSFX(SFX_HINT_LETTER_BONUS);
                me_iCoinCount -= 70;
                UserDefault::getInstance()->setIntegerForKey(COINS,me_iCoinCount);
                UpdateCoins();
                
                UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT,false);
                UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE,true);

                
                AchievementManager::getInstance()->OnAchievementUpdate("hints",1);
                AchievementManager::getInstance()->CheckAllAchievementsComplete();
                
                DailyMissionManager::getInstance()->OnDailyMissionUpdate("hints",1);
                DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();

            }
        }
        else
        {
            GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
            setEnableButtons(false);
            CoinLabelAnimation();
            InterfaceManagerInstance::getInstance()->getInterfaceManager()->ShowToastMessage("Not Enough Coins");
            m_ptrPopUpManager->LoadPopUp(POPUP_BUY_COINS);
        }
    }
}
/// updates current coin count to label
void HudLayer::UpdateCoins()
{
    std::string coinStr = GameController::getInstance()->getCoinsFormat(me_iCoinCount);
    
    if(m_CoinCount)
    {
        m_CoinCount->setString(coinStr);
        m_CoinCount->setColor(Color3B::WHITE);
          if (me_iCoinCount<=50) {
              m_CoinCount->setColor(Color3B::RED);
          }

        CoinLabelAnimation();
    }
}
void HudLayer::coinBtnCallBack(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);

    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    setEnableButtons(false);
    m_ptrPopUpManager->LoadPopUp(POPUP_BUY_COINS);
}
///  This called on every level completed
void HudLayer::onLevelCompleted()
{
    GameController::getInstance()->playSFX(SFX_LEVEL_COMPLETE);
    
    std::string country = StatsManager::getInstance()->getCurrentCountry();
    int curlevel = UserDefault::getInstance()->getIntegerForKey(CURR_LEVEL);

    GameController::getInstance()->setIntegerForKey(LEVEL_RATE,GameController::getInstance()->getIntegerForKey(LEVEL_RATE)+1);
        
    if(curlevel>=StatsManager::getInstance()->getPlayedLevelCountForCountries(country))
    {
        StatsManager::getInstance()->updatePlayedLevelCountForCountries(country,1);
        std::string AchiveStr = country+"level";
        AchievementManager::getInstance()->OnAchievementUpdate(AchiveStr, 1);
        AchievementManager::getInstance()->CheckAllAchievementsComplete();
        
        UserDefault::getInstance()->setBoolForKey("PlayedAgain",false);
        
        UserDefault::getInstance()->setIntegerForKey("level",UserDefault::getInstance()->getIntegerForKey("level")+1);
        
        StatsManager::getInstance()->updateLangPlayedlevelCount(1,false);
        
        if(StatsManager::getInstance()->getLangPlayedlevelCount()<=20)
        {
            std::string lang = me_Language;
            lang = lang.substr(0, 3).c_str();

            std::string Eventstr = StringUtils::format("%s_Level_%d",lang.c_str(),StatsManager::getInstance()->getLangPlayedlevelCount());
            InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsEvent(Eventstr,"","","","");
        }

    }
    else
    {
        UserDefault::getInstance()->setBoolForKey("PlayedAgain",true);
    }
    AchievementManager::getInstance()->SaveAchievements();
        
    DailyMissionManager::getInstance()->OnDailyMissionUpdate("levels", 1);
    DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();

    DailyMissionManager::getInstance()->SaveMissions();
    
    GameStateManager::getInstance()->SetGameState(GAME_STATE_OVER);
    setEnableButtons(false);
    
    CallFunc *callfunc = CallFunc::create(std::bind(&PopUpManager::LoadPopUp,m_ptrPopUpManager,POPUP_LEVEL_COMPLETE));
    this->runAction(Sequence::create(DelayTime::create(0.5f),callfunc, NULL));
}
/// this calls on click of next in level complete popup
void HudLayer::loadNextLevel()
{
    Director::getInstance()->purgeCachedData();

    LevelScene *scene = new LevelScene();
    scene->autorelease();
    Director::getInstance()->replaceScene(TransitionMoveInL::create(0.5f, scene));
    scene = NULL;
    
}
/// THIS UNLOAD POPUP IF EXIST AND GO BACK TO MAIN MENU
void HudLayer::UnloadPopUpAndExitGame(float p_fDelayTime, bool p_bIsPopUpExitAnimation)
{
    if(p_bIsPopUpExitAnimation)
        m_ptrPopUpManager->PopUpTransitionAnimation(POPUP_EXIT);
    
    DelayTime *t_DelayeTime = DelayTime::create(p_fDelayTime);
    CallFunc *t_CallFunc = CallFunc::create(std::bind(&HudLayer::onGameExit,this));
    this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
}
/// this exits from gameplay to levelscreen
void HudLayer::onGameExit()
{
    if(GameController::getInstance()->isBGMEnabled()) {
        SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    }
    LevelScene *scene = new LevelScene();
    scene->autorelease();
    Director::getInstance()->replaceScene(TransitionSlideInL::create(0.5f, scene));
    scene = NULL;
}
/// on No  click from Gameplay exit PopUp
void HudLayer::onNoFromExitPopUp()
{
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
    setEnableButtons(true);
}
void HudLayer::shuffleBtnCallback(Ref *sender)
{
    GameController::getInstance()->playSFX(SFX_REFRESH_BUTTON);

    MenuItemSprite *button = (MenuItemSprite*)sender;

    if (m_ptrGameLayer->getOptionGrid()!=NULL)
    {
        m_ptrGameLayer->getOptionGrid()->shuffleOptionLetters();
        m_ShuffleButton->setEnabled(false);
        m_ShuffleButton->setOpacity(150);
        
        UserDefault::getInstance()->setBoolForKey("shuffleused",true);
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE,false);

        if(button->getName()=="Free")
        {
            removeShuffleTutorial();
        }
    }
    
    
    DelayTime *delay = DelayTime::create(0.5f);
    CallFunc *callfunc = CallFunc::create([&,this]()
    {
        if(m_ShuffleButton!=NULL)
        {
            m_ShuffleButton->setEnabled(true);
            m_ShuffleButton->setOpacity(255);
        }
    });
    this->runAction(Sequence::create(delay,callfunc, NULL));

}
/// Coin label scale  animation after coin update
void HudLayer::CoinLabelAnimation()
{
    if(m_CoinCount)
    {
        ScaleTo *scale1 = ScaleTo::create(0.4f,1.2f,1.2f);
        ScaleTo *scale2 = ScaleTo::create(0.4f,1.0f,1.0f);
        m_CoinCount->runAction(Sequence::create(scale1,scale2, NULL));

    }
}
void HudLayer::checkForAndroidDeviceBack()
{
    Event *unused_event = NULL;
    onKeyReleased(EventKeyboard::KeyCode::KEY_BACK,unused_event);
}
// THIS SET GAME LAYER INTO HUD LAYER
void HudLayer::setGameLayer(GameLayer *p_PtrGameLayer)
{
    m_ptrGameLayer = p_PtrGameLayer;
    
    checkforFirstLevelTutorial();
}
/// THIS WILL REMOVE CURRENT ONE AND CALL ANOTHER POPUP WAIT FOR ANIMATION OF CURRENT POPUP
void HudLayer::LoadOtherPopUpWithDelay(float p_fDelayTime, const std::string &p_PopUpName)
{
    DelayTime *t_DelayeTime = DelayTime::create(p_fDelayTime);
    CallFuncN *t_CallFunc = CallFuncN::create(std::bind(&HudLayer::LoadOtherPopUp, this, this, p_PopUpName));
    this->runAction(Sequence::create(t_DelayeTime,t_CallFunc, NULL));
}

/// THIS LOAD OTHER POP UP BY COMPARING WITH STRING
void HudLayer::LoadOtherPopUp(Ref *p_Sender, const std::string &p_String)
{
    if(p_String.compare("")==0)
    {
//        setEnableButtons(false);
    }
    else if(p_String.compare("Language")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_LANGUAGE);
    }
    else if(p_String.compare("RemoveAds")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_REMOVE_ADS);
    }
    else if(p_String.compare("FreeCoins")==0)
    {
        setEnableButtons(false);
        m_ptrPopUpManager->LoadPopUp(POPUP_FREE_COINS);
    }

}
// THIS RETURN GAME LAYER OBJECT
GameLayer * HudLayer::getGameLayer()
{
    return m_ptrGameLayer;
}
/// THIS CREATE UPDATE COIN EVENT
void HudLayer::CreateUpdateCoinEvent()
{
    if(!m_bIsEventCreated)
    {
        m_bIsEventCreated = true;
        auto t_UpdateCoins = EventListenerCustom::create(EVENT_UPDATE_COINS, CC_CALLBACK_0(HudLayer::UpdateCoins, this));
        Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(t_UpdateCoins, this);
    }
}
void HudLayer::freeCoinsBtnCallBack(Ref *p_sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    m_ptrPopUpManager->LoadPopUp(POPUP_FREE_COINS);
    setEnableButtons(false);
}
void HudLayer::feedBackBtnCallBack(Ref *p_sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
    m_ptrPopUpManager->LoadPopUp(POPUP_REPORT_WORD);
    setEnableButtons(false);
}
///  This is to  enable hint use animation if user enters continues wrong ans
/// @param m_AnsStr answer obtained by user
void HudLayer::onWrongAnswer(std::string m_AnsStr)
{
    if(UserDefault::getInstance()->getBoolForKey(TUTORIAL_START)) {
        return;
    }
    
    t_WrongWordsArr.push_back(m_AnsStr);
    if(t_WrongWordsArr.size()>=2)
    {
        if(me_iCoinCount>=70)
        {
            if(!m_bHintAnimation)
            {
                m_bHintAnim = true;
                DelayTime *delay = DelayTime::create(0.5f);
                FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_0(HudLayer::showHintUseAnimation,this));
                this->runAction(Sequence::create(delay,_call,NULL));
            }
        }
    }
    
    if(m_WrongAns=="")
    {
        m_WrongAns = m_AnsStr;
    }
    
    if(m_WrongAns==m_AnsStr)
    {
        m_iAnsSame++;
        if(!m_FeedBackButton->isVisible()&&m_iAnsSame==3)
        {
            m_FeedBackButton->setVisible(true);
            ScaleTo *scale2 = ScaleTo::create(0.2f,m_FeedBackButton->getScale()+0.2f);
            ScaleTo *scale3 = ScaleTo::create(0.2f,m_FeedBackButton->getScale());
            Sequence *seq = Sequence::create(scale2,scale3, NULL);
            Repeat *repe = Repeat::create(seq,4);
            m_FeedBackButton->runAction(repe);
        }
        else if(!m_bfeedPopupShow&&m_iAnsSame>3)
        {
            feedBackBtnCallBack(NULL);
            m_bfeedPopupShow = true;
        }
    }
    else
    {
        m_iAnsSame = 1;
        m_WrongAns = "";
    }
}
void HudLayer::onLevelComplete()
{
//    GameController::getInstance()->showInterstial();
    
    if(GameController::getInstance()->isBGMEnabled()) {
        SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    }
    
    {
        LevelScene *scene = new LevelScene();
        scene->autorelease();
        Director::getInstance()->replaceScene(TransitionMoveInL::create(0.5f, scene));
        scene = NULL;
    }
//    ));
}
HudLayer::~HudLayer()
{
    
    if(m_bIsEventCreated)
    {
       Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(EVENT_UPDATE_COINS);
    }

    
    if (m_ptrPopUpManager)
    {
        this->removeChild(m_ptrPopUpManager,true);
        m_ptrPopUpManager = NULL;
    }
    
    if(m_ptrTutorManager)
    {
        this->removeChild(m_ptrTutorManager,true);
        m_ptrTutorManager = NULL;

    }
    if (t_WrongWordsArr.size()>0) {
        t_WrongWordsArr.clear();
    }
    
    this->removeAllChildrenWithCleanup(true);
}
/// this is to disable all Hud Buttons on showing first level tutorial
void HudLayer::checkforFirstLevelTutorial()
{
  
    if(UserDefault::getInstance()->getBoolForKey(TUTORIAL_START))
    {
        if(t_TopMenu!=NULL &&t_BottomMenu!=NULL)
        {
            t_TopMenu->retain();
            t_BottomMenu->retain();
            t_HintMenu->retain();
            t_shuffleMenu->retain();
            t_RevealMenu->retain();
            
            this->removeChild(t_TopMenu,true);
            this->removeChild(t_BottomMenu,true);
            this->removeChild(t_HintMenu,true);

            this->removeChild(t_shuffleMenu,true);
            this->removeChild(t_RevealMenu,true);

            m_ptrGameLayer->addChild(t_TopMenu);
            m_ptrGameLayer->addChild(t_BottomMenu);
            m_ptrGameLayer->addChild(t_HintMenu);
            m_ptrGameLayer->addChild(t_shuffleMenu);
            m_ptrGameLayer->addChild(t_RevealMenu);

            setEnableButtons(false);
            m_bTutorialAvailble = true;
        }
    }
}
/// this is to check Reveal letter button Enabling after tutorail shown
void HudLayer::checkforRevealLetteroption()
{

    if(UserDefault::getInstance()->getBoolForKey(TUTORIAL_REVEAL))
    {
        Sprite *sprLock = (Sprite*)m_LettrRevealButton->getChildByName("Lock");
        sprLock->setVisible(true);
    }
    else
    {
        m_LettrRevealButton->setName("");
        m_LettrRevealButton->setEnabled(true);
    }
    
}
/// THIS UPDATE  BUTTON TAGS AND DATA
void HudLayer::UpdateMenuData()
{
    switch (m_ptrPopUpManager->getPopUpType())
    {
        case POPUP_DAILY_MISSION:
        {
            checkForDailyMissionUpdates();
        }break;
        case POPUP_DAILY_REWARD:
        {
            
        }break;
        case POPUP_ACHIEVEMENT:
        {
            
        }break;
        case POPUP_BONUS_WORD:
        {
            checkForBonusWordButton();
        }break;
        case POPUP_LANGUAGE:
        {
            
        }break;
        default:
            break;
    }
}
/// This will be called Daily Mission's Complete
void HudLayer::checkForDailyMissionUpdates()
{
    if(m_DailyButton)
    {
        Sprite* SprTask = (Sprite*)m_DailyButton->getChildByName("tag");
        if(!UserDefault::getInstance()->getBoolForKey(NEW_TASK))
        {
            SprTask->setVisible(false);
        }
        
        if(DailyMissionManager::getInstance()->checkAllDailyMissionsClaimed())
        {
            SprTask->setTexture("Task_Completed.png");
            SprTask->setVisible(true);
            return;
        }
        
        if(DailyMissionManager::getInstance()->getCountOfDailyMissionsToClaim()>=1)
        {
            SprTask->setTexture("Claim.png");
            SprTask->setVisible(true);
        }
        else if(!UserDefault::getInstance()->getBoolForKey(NEW_TASK))
        {
            SprTask->setVisible(false);
        }

    }
}

#pragma mark TUTORIAL START
/// check for HUD Buttons tutorial
void HudLayer::checkForTutorial()
{

    int levelcount = UserDefault::getInstance()->getIntegerForKey("level");
    
    if(levelcount==2&&UserDefault::getInstance()->getBoolForKey(TUTORIAL_HINT))
    {
        GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
          
        Point pos = Vec2(m_HintButton->getPositionX(),m_HintButton->getBoundingBox().getMinY());
        m_ptrTutorManager->showHintTutorial(pos);
        
        setEnableButtons(false);
        m_HintButton->setName("Free");
        m_HintButton->setEnabled(true);
        
        m_bTutorialAvailble = true;
        Sprite *sprcoin = (Sprite*)m_HintButton->getChildByName("coin");
        sprcoin->setVisible(false);
        
        Label *coinTxt = (Label*)m_HintButton->getChildByName("price");
        coinTxt->setString("Free");
        coinTxt->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        coinTxt->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        coinTxt->setPosition(Vec2(m_HintButton->getContentSize().width/2,m_HintButton->getBoundingBox().size.height*0.05));
        
        t_HintMenu->setLocalZOrder(4);
        
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsEvent("Tutorial_Hint","", "", "","");
    }
    else if(levelcount==3&&UserDefault::getInstance()->getBoolForKey(TUTORIAL_SHUFFLE))
    {
        m_bTutorialAvailble = true;
        GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
          
        Point pos = Vec2(m_ShuffleButton->getPositionX(),m_ShuffleButton->getBoundingBox().getMaxY());
        m_ptrTutorManager->showShuffleTutorial(pos);
        
        setEnableButtons(false);
        m_ShuffleButton->setName("Free");
        m_ShuffleButton->setEnabled(true);
                
        t_shuffleMenu->setLocalZOrder(4);
        
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsEvent("Tutorial_Shuffle","", "", "","");
    }
    else if(levelcount==7&&UserDefault::getInstance()->getBoolForKey(TUTORIAL_REVEAL))
    {
        m_bTutorialAvailble = true;
        GameStateManager::getInstance()->SetGameState(GAME_STATE_PAUSE);
          
        Point pos = Vec2(m_LettrRevealButton->getPositionX(),m_LettrRevealButton->getBoundingBox().getMaxY());

        m_ptrTutorManager->showRevealButtonTutorial(pos);
        
        setEnableButtons(false);
        m_LettrRevealButton->setName("Free");
        m_LettrRevealButton->setEnabled(true);
                
        Sprite *t_lockSpr = (Sprite*)m_LettrRevealButton->getChildByName("Lock");
        t_lockSpr->setVisible(false);
        
        Sprite *sprcoin = (Sprite*)m_LettrRevealButton->getChildByName("coin");
        sprcoin->setVisible(false);
        
        Label *coinTxt = (Label*)m_LettrRevealButton->getChildByName("price");
        coinTxt->setString("Free");
        coinTxt->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
        coinTxt->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        coinTxt->setPosition(Vec2(m_LettrRevealButton->getContentSize().width/2,m_LettrRevealButton->getBoundingBox().size.height*0.05));
        t_RevealMenu->setLocalZOrder(4);
        
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->PostFirebaseAnalyticsEvent("Tutorial_Reveal","", "", "","");
    }
}
/// removes  Shuffle Button Tutorial
void HudLayer::removeShuffleTutorial()
{
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
    setEnableButtons(true);

    m_ShuffleButton->setName("");
    t_shuffleMenu->setLocalZOrder(1);

    UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE,false);
    
    m_ptrTutorManager->removeTuorial();
    m_bTutorialAvailble = false;
}
/// removes reaveal letter button Tutorial
void HudLayer::removeRevealTutorial()
{
    
    GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
    setEnableButtons(true);

    m_LettrRevealButton->setName("");
    t_RevealMenu->setLocalZOrder(1);


    Sprite *sprcoin = (Sprite*)m_LettrRevealButton->getChildByName("coin");
    sprcoin->setVisible(true);
    
    Label *coinsTxt = (Label*)m_LettrRevealButton->getChildByName("price");
    coinsTxt->setString("150");
    coinsTxt->setAlignment(TextHAlignment::LEFT,TextVAlignment::CENTER);
    coinsTxt->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    coinsTxt->setPosition(Vec2(5+sprcoin->getBoundingBox().getMaxX(),m_LettrRevealButton->getBoundingBox().size.height*0.25));
        
    UserDefault::getInstance()->setBoolForKey(TUTORIAL_REVEAL,false);

    m_ptrTutorManager->removeTuorial();
    m_bTutorialAvailble = false;
}
/// removes Hint Tutorial
void HudLayer::removeHintTutorial()
{

    GameStateManager::getInstance()->SetGameState(GAME_STATE_PLAY);
    setEnableButtons(true);

    m_HintButton->setName("");
    t_HintMenu->setLocalZOrder(1);

    
    Sprite *sprcoin = (Sprite*)m_HintButton->getChildByName("coin");
    sprcoin->setVisible(true);
    
    Label *coinTxt = (Label*)m_HintButton->getChildByName("price");
    coinTxt->setString("70");
    coinTxt->setAlignment(TextHAlignment::LEFT,TextVAlignment::CENTER);
    coinTxt->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    coinTxt->setPosition(Vec2(5+sprcoin->getBoundingBox().getMaxX(),m_HintButton->getBoundingBox().size.height*0.25));
    
    UserDefault::getInstance()->setBoolForKey(TUTORIAL_HINT,false);
    
    if(!UserDefault::getInstance()->getBoolForKey("shuffleused"))
        UserDefault::getInstance()->setBoolForKey(TUTORIAL_SHUFFLE,true);
    
    m_ptrTutorManager->removeTuorial();
    m_bTutorialAvailble = false;
}
#pragma mark -------END TUTORIAL---------

///  This will be called on contionues wright word ans to display  Good excelent ,, Tags
/// @param attempt attempt number
void HudLayer::checkForAttemptAnim(int attempt)
{
    Sprite *winSpr = NULL;
    switch (m_iNum)
    {
        case 2:
        {
            // GOOD
            m_iNum = 3;
            AchievementManager::getInstance()->OnAchievementUpdate("good",1);
            DailyMissionManager::getInstance()->OnDailyMissionUpdate("good",1);
            
            winSpr = Sprite::create("word_win_0.png");
            winSpr->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-20));
            winSpr->setOpacity(0);
            this->addChild(winSpr,2);
        }break;
        case 3:
        {
            // GREAT
            m_iNum = 4;
            AchievementManager::getInstance()->OnAchievementUpdate("great",1);
            DailyMissionManager::getInstance()->OnDailyMissionUpdate("great",1);
            
            winSpr = Sprite::create("word_win_1.png");
            winSpr->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-20));
            winSpr->setOpacity(0);
            this->addChild(winSpr,2);
        }break;
        case 4:
        {
            // AMAZING
            m_iNum = 5;
            AchievementManager::getInstance()->OnAchievementUpdate("amazing",1);
            DailyMissionManager::getInstance()->OnDailyMissionUpdate("amazing",1);
            
            winSpr = Sprite::create("word_win_2.png");
            winSpr->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-20));
            winSpr->setOpacity(0);
            this->addChild(winSpr,2);
        }break;
        case 5:
        {
            // AWESOME
            m_iNum = 6;
            AchievementManager::getInstance()->OnAchievementUpdate("awesome",1);
            DailyMissionManager::getInstance()->OnDailyMissionUpdate("awesome",1);
            
            winSpr = Sprite::create("word_win_3.png");
            winSpr->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-20));
            winSpr->setOpacity(0);
            this->addChild(winSpr,2);
        }break;
        case 6:
        {
            // EXCELLENT
            winSpr = Sprite::create("word_win_4.png");
            winSpr->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2-20));
            winSpr->setOpacity(0);
            this->addChild(winSpr,2);
        }break;
        default:
            break;
    }
    AchievementManager::getInstance()->CheckAllAchievementsComplete();
    DailyMissionManager::getInstance()->CheckAllDailyMissionsComplete();

    if (winSpr!=NULL)
    {
        DelayTime *delay = DelayTime::create(0.45f);
        FadeIn *fade = FadeIn::create(0.3f);
        ScaleTo *scale = ScaleTo::create(0.4f,winSpr->getScale()+0.3f);
//        ScaleTo *scale2 = ScaleTo::create(0.4f,winSpr->getScale());
        Spawn *spawn = Spawn::create(fade,scale,NULL);
        EaseElasticIn *ease = EaseElasticIn::create(spawn);
        DelayTime *delay1 = DelayTime::create(0.8f);
        FadeOut *fadeout = FadeOut::create(0.5f);
        Sequence *seq = Sequence::create(delay,ease,delay1,fadeout,RemoveSelf::create(),NULL);
        winSpr->runAction(seq);
    }
}
/// coin Add Animation
/// @param endPos position wher the coin anim ends
void HudLayer::coinAddAnimation(Point startPos)
{
    Point  _endPos = Vec2(m_CoinButton->getPosition().x,m_CoinButton->getPosition().y);
    Point _startPosOppo = startPos;
    for (int i=0; i<10; i++)
    {
        Sprite *_spr = Sprite::create("coins.png");
        _spr->setScale(_spr->getScale());
        _spr->setPosition(_startPosOppo);
        _spr->setOpacity(0);
        ccBezierConfig bezier;
        bezier.controlPoint_1 = _startPosOppo;
        bezier.controlPoint_2 = Point(_startPosOppo.getMidpoint(_endPos)-Vec2(200,0));
        bezier.endPosition =_endPos;
        BezierTo *_bez = BezierTo::create(.5f, bezier);
        this->addChild(_spr,5);
        FadeTo *_fade = FadeTo::create(.04*i,255);
        FiniteTimeAction *_call = CallFuncN::create(CC_CALLBACK_1(HudLayer::removeSprite,this));
        FiniteTimeAction*_seq = Sequence::create(_fade,_bez,_call,NULL);
        _spr->runAction(_seq);
    }
    
    me_iCoinCount += 1;
    UpdateCoins();
}
/// removes coin Sprite after coin Animation
void HudLayer::removeSprite(Ref *_sender)
{
    Sprite *_spr = (Sprite*) _sender;
    _spr ->removeFromParentAndCleanup(true);
    
    GameController::getInstance()->playSFX(SFX_COIN_COLLECT);

    Point  _endPos = Vec2(m_CoinButton->getPosition().x,m_CoinButton->getPosition().y);
    GameController::getInstance()->doParticleEffect(this,_endPos,5);
}
/// display's Hint use animation
void HudLayer::showHintUseAnimation()
{
    float posy = m_ptrGameLayer->getAnswersBg()->getBoundingBox().getMinY();

    if(m_HintButton!=NULL)
    {
        m_bHintAnimation = true;
        Sprite *t_Sprhand = Sprite::create("hand.png");
        t_Sprhand->setScale(0.4f);
        t_Sprhand->setRotation(60.0f);
        t_Sprhand->setPosition(Vec2(m_HintButton->getPositionX()-20,m_HintButton->getPositionY()-t_Sprhand->getBoundingBox().size.height/2));
        this->addChild(t_Sprhand,2);
        
        float scaleX = (visibleSize.width+10)/70;
        Sprite *toastBg = Sprite::create("pixel.png");
        toastBg->setScale(0, 2.5);
        toastBg->setPosition(Vec2(Origin.x+visibleSize.width/2,posy));
        toastBg->setColor(Color3B::BLACK);
        toastBg->setOpacity(150);
        this->addChild(toastBg);
        
        std::string toastStr = LanguageTranslator::getInstance()->getTranslatorStringWithTag("useHint");
        Label *toastLbl = Label::createWithTTF(toastStr.c_str(),FONT_NAME,30);
        
        if(me_Language.compare(LANG_ENGLISH)!=0)
            toastLbl = Label::createWithSystemFont(toastStr.c_str(),FONT_NAME, 30);

        toastLbl->setDimensions(toastBg->getBoundingBox().size.width-20,toastLbl->getBoundingBox().size.height+10);
        toastLbl->setPosition(toastBg->getPosition());
        toastLbl->setColor(Color3B::WHITE);
        toastLbl->setScale(0);
        this->addChild(toastLbl,1);
        
        ScaleTo *scale1 = ScaleTo::create(0.2f, scaleX,1.0);
        ScaleTo *scale2 = ScaleTo::create(0.2f, 1.0f,1.0);
        RemoveSelf *remove = RemoveSelf::create();
        
        Sequence *seq = Sequence::create(ScaleTo::create(0.3f,0.4f),ScaleTo::create(0.3f,0.5f),NULL);
        RepeatForever *rep =RepeatForever::create(seq);
        t_Sprhand->runAction(rep);
        
        toastBg->runAction(Sequence::create(scale1,DelayTime::create(4.0f),remove, NULL));
        toastLbl->runAction(Sequence::create(scale2,DelayTime::create(4.0f),remove, NULL));
                
        CallFuncN *t_CallFunc = CallFuncN::create(std::bind(&HudLayer::removeHand, this, t_Sprhand));
        this->runAction(Sequence::create(DelayTime::create(4.1f),t_CallFunc, NULL));

    }
}
/// removes Hand on tSpecfic tutorial complete
void HudLayer::removeHand(Ref *sender)
{
    m_bHintAnim = false;
    Sprite* sprhand = (Sprite*)sender;
    if(sprhand!=NULL)
    {
        this->removeChild(sprhand,true);
        sprhand = NULL;
    }
    
}
/*{"L":"ಸ,ಮ,ರ","W":["ಸಮರ","ಮರ","ಸರ","ರಸ"],"B":["ಸಮ"]},
{"L":"ಪ್ರ,ಮು,ಖ","W":["ಪ್ರಮುಖ","ಮುಖ"],"B":[]},
{"L":"ಅಂ,ಕ,ಣ","W":["ಅಂಕಣ","ಅಂಕ","ಕಣ"],"B":[]},
{"L":"നം,ച്ച്‍,വാ","W":["വാച്ച്‍","വാനം"],"B":[]},
{"L":"ந,டி,ப்,பு,ப","W":["நடி","படி","படிப்பு","நடிப்பு"],"Extra":[]},
{"L":"ತ,ರಂ,ಗ","W":["ತರಂಗ","ರಂಗ","ಗತ"],"B":[]},
{"L":"ಸಾ,ಧ,ನ","W":["ಧನ","ಸಾಧನ"],"B":[]},
*/
