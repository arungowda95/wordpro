//
//  SplashScene.cpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#include "SplashScene.h"
#include "MainMenuScene.h"
#include "GameConstants.h"
#include "GameController.h"
#include "StatsManager.h"
#include "AchievementManager.h"
#include "DailyMissionManager.h"
#include "LanguageTranslator.h"
#include "LabelAnimation.h"
#include "FacebookManager.h"
#include "InterfaceManagerInstance.h"
SplashScene::SplashScene()
{
    
    me_strScreenName = "SplashScreen";
    
    LayerColor *colorLayer = LayerColor::create(Color4B(0,0,0,255));
    this->addChild(colorLayer);
    
    m_ptrSplashLayer = new SplashLayer();
    m_ptrSplashLayer->autorelease();
    this->addChild(m_ptrSplashLayer);
}
SplashScene::~SplashScene()
{
    this->removeAllChildrenWithCleanup(true);
}


#pragma mark SPLASH LAYER
SplashLayer::SplashLayer()
{
    m_SprSplashBg = NULL;
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    Size visibleSize = ResolutionSize.size;
    Vec2 Origin = ResolutionSize.origin;
    
    
    m_SprSplashBg = Sprite::create("Splash-bg.jpg");
    m_SprSplashBg->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
    m_SprSplashBg->setScale(m_fScale);
    this->addChild(m_SprSplashBg,1);
    
    Sprite *m_SprLogo = Sprite::create("word-pro_logo.png");
    m_SprLogo->setScale(m_fScale);
    m_SprLogo->setPosition(Vec2(Origin.x+visibleSize.width/2-10,Origin.y+visibleSize.height*0.55));
    this->addChild(m_SprLogo,1);
    
    Label *gameTitle = Label::createWithTTF("Loading...","fonts/FUTURAXK_0.TTF",60);
    gameTitle->setColor(Color3B::WHITE);
    gameTitle->setPosition(Vec2(Origin.x+visibleSize.width/2,m_SprLogo->getBoundingBox().getMinY()-150));
    //    this->addChild(gameTitle,1);
    
    
    CCLabelBMFontAnimated *label;
    label = CCLabelBMFontAnimated::createWithTTF("Loading...", "fonts/FUTURAXK_0.TTF", 60.0f,
                                                 Size(visibleSize.width, 40),
                                                 cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::TOP);
    label->setColor(Color3B::WHITE);
    label->setPosition(Vec2(Origin.x+visibleSize.width/2,m_SprLogo->getBoundingBox().getMinY()-150));
    this->addChild(label,1);
    
    label->flyPastAndRemove();
    
    DelayTime *delay1 = DelayTime::create(0.0f);
    CallFunc *callfunc1 = CallFunc::create(std::bind(&SplashLayer::loadGameAssets,this));
    CallFunc *callfunc2 = CallFunc::create(std::bind(&SplashLayer::loadMenuScene,this));
    DelayTime *delay2 = DelayTime::create(3.0f);
    this->runAction(Sequence::create(delay1,callfunc1,delay2,callfunc2, NULL));
    
}
/// THIS LOAD ALL GAME ASSTES
void SplashLayer::loadGameAssets()
{
    
    me_Language   = UserDefault::getInstance()->getStringForKey(LANGUAGE);
    me_iCoinCount = UserDefault::getInstance()->getIntegerForKey(COINS);
    
    me_bNotification = UserDefault::getInstance()->getBoolForKey("Notification");
    
    me_bDailyRewardAvailable = UserDefault::getInstance()->getBoolForKey(DAILY_REWARD_AVAILABLE);
    
    GameController::getInstance()->setBoolForKey("isMoreGameLoaded", false);
    GameController::getInstance()->setBoolForKey("isLevelfinishGamesLoaded", false);
    GameController::getInstance()->setBoolForKey("GAME_ENTER",false);
    GameController::getInstance()->setBoolForKey("ExitPromotionAvailable", false);
    
    GameController::getInstance()->setBoolForKey(NO_ADS_FOR_SESSION, false);
    
    
    UserDefault::getInstance()->setBoolForKey("FROM_LAUNCH",true);
    
    // get/set last played day info
    GameController::getInstance()->initLastDay();
    AchievementManager::getInstance()->LoadAchievementJson();
    DailyMissionManager::getInstance()->LoadDailyMissionsJson();
    
    LanguageTranslator::getInstance()->LoadTranslationFile();
    
    //    StatsManager::getInstance()->LoadLevelMapData();
    
    GameController::getInstance()->checkForRatePopUp();
    
    if(!UserDefault::getInstance()->getBoolForKey("Initialize_levels"))
    {
        UserDefault::getInstance()->setBoolForKey("Initialize_levels",true);
        StatsManager::getInstance()->InitialUnlockedCountryForLanguages();
    }
    
    
    if(!UserDefault::getInstance()->getBoolForKey("update_1_5"))
    {
        UserDefault::getInstance()->setBoolForKey("update_1_5",true);
        UserDefault::getInstance()->setBoolForKey("SALE_POPUP_SHOWN",false);
    }
    
    /// MUSIC FILES PRELOAD ON LAUNCH
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_COIN_COLLECT);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_BUTTON_CLICKED);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_COUNTRY_UNLOCK);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_GAMEPLAY_ENTER);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_HINT_LETTER_BONUS);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_LEVEL_COMPLETE);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_REFRESH_BUTTON);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_SCREEN_MOVE);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_CORRECT_WORD);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_WRONG_WORD);
    SimpleAudioEngine::getInstance()->preloadEffect(SFX_GIFT_UNLOCK);
    
    
    InterfaceManagerInstance::getInstance()->getInterfaceManager()->initialize();
    
    if(!IsUserConnected()&&GameController::getInstance()->getStringForKey("langDataUserIds","").empty())
    {
        InterfaceManagerInstance::getInstance()->getInterfaceManager()->getLanguageDataUserIds();
    }
}
bool SplashLayer::IsUserConnected()
{
    bool connect = true;
    
    if(CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID)
    {
        if(strcmp(GameController::getInstance()->getStringForKey("connectAs").c_str(),"facebook")!=0)
        {
            connect = false;
        }
    }
    else if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
    {
        if(strcmp(GameController::getInstance()->getStringForKey("connectAs").c_str(),"facebook")!=0)
        {
            if(strcmp(GameController::getInstance()->getStringForKey("connectAs").c_str(),"Apple")!=0)
            {
                return false;
            }
            else
            {
                return true;
            }
            return false;
        }
    }
    return connect;
}
/// CONSTRUCTOR AND DESTRUCTOR
void SplashLayer::loadMenuScene()
{
    MainMenuScene *scene = new MainMenuScene();
    scene->autorelease();
    Director::getInstance()->replaceScene(TransitionSlideInL::create(0.2f, scene));
    scene = NULL;
}

SplashLayer::~SplashLayer()
{
    this->removeAllChildrenWithCleanup(true);
}
