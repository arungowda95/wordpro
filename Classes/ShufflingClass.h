
#ifndef _ShufflingClass_
#define _ShufflingClass_

#include <iostream>
#include "cocos2d.h"

using namespace cocos2d;
using namespace std;


// SINGLETON CLASS
class ShufflingClass
{
    static ShufflingClass *m_PtrShufflingClass;
    ShufflingClass();
public:
    // RETURN GAME HANDLER
    static ShufflingClass *getInstance();
    
    
    std::vector<int> & randomShuffle(const std::vector<int> &p_numsArray, std::vector<int> &elems);
    std::vector<int> randomShuffle(const std::vector<int> &p_numsArray);
    
    std::vector<int> shuffleForRange(int minRange,int maxRange,int _sizeVector);
    
    std::vector<std::vector<int>> & randomShuffle(const std::vector<std::vector<int>> &p_numsArray, std::vector<std::vector<int>> &elems);
    std::vector<std::vector<int>> randomShuffle(const std::vector<std::vector<int>> &p_numsArray);
    
    
    std::vector<cocos2d::Point> & randomShuffle(const std::vector<cocos2d::Point> &p_numsArray, std::vector<cocos2d::Point> &elems);
    std::vector<cocos2d::Point> randomShuffle(const std::vector<cocos2d::Point> &p_numsArray);
    
    
    std::vector<std::string> & randomShuffle(const std::vector<std::string> &p_numsArray, std::vector<std::string> &elems);
    std::vector<std::string> randomShuffle(const std::vector<std::string> &p_numsArray);
    
    // TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
    
    // TO REMOVE COMMA FROM STRING AND CONVERT ONE STRING ARRAY
    std::vector<std::string> split(const std::string &s, char delim);

};

#endif /* defined(__HelloCpp__ShufflingClass__) */
