//
//  HudLayer.hpp
//  wordPro-mobile
//
//  Created by id on 19/02/20.
//

#ifndef HudLayer_h
#define HudLayer_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameLayer;
class PopUpManager;
class TutorialManager;
class HudLayer:public Layer
{
    GameLayer       *m_ptrGameLayer;
    MenuItemSprite  *m_BackButton,*m_HintButton,*m_ShuffleButton,*m_CoinButton,*m_LettrRevealButton,*m_DailyButton;
    MenuItemSprite  *m_FreeCoinsButton;
    Size visibleSize ;
    Vec2 Origin ;

    Label *m_CoinCount;
    PopUpManager *m_ptrPopUpManager;
    TutorialManager *m_ptrTutorManager;
    
    bool m_bIsEventCreated;
    
    CC_SYNTHESIZE(MenuItemSprite*,m_BonusWordsButton,bonusButton);
    CC_SYNTHESIZE(MenuItemSprite*,m_FeedBackButton,feedBackButton);
    
    CC_SYNTHESIZE(Sprite*,toastBg,toastBg);
    CC_SYNTHESIZE(Label*,toastLbl,toastLbl);
    
    std::vector<std::string> t_WrongWordsArr;
    bool m_iWrongAttempt;
    std::string m_WrongAns="";
    
    Menu *t_BottomMenu;
    Menu *t_HintMenu;
    
    Menu *t_shuffleMenu;
    Menu *t_RevealMenu;
    
    Menu *t_TopMenu;
    
    Sprite *m_TutorialBox;
    
    Node *m_tutNode;
    
    int m_iNum=2;

    bool m_bHintAnimation = false,m_bHintAnim = false;
    int m_iAnsSame = 0;
    bool m_bfeedPopupShow = false;
public:
    bool m_bTutorialAvailble;
    HudLayer();
    ~HudLayer();

    /// HUD BUTTON's  CLICK
    void backBtnCallback(Ref *sender);
    void hintBtnCallback(Ref *sender);
    void shuffleBtnCallback(Ref *sender);
    void coinBtnCallBack(Ref *sender);
    void dailyEventBtnCallback(Ref* sender);
    void bonusBtnCallback(Ref* sender);
    void revealLettrBtnCallBack(Ref *sender);
    void freeCoinsBtnCallBack(Ref *p_sender);
    void feedBackBtnCallBack(Ref *p_sender);

    
    /// THIS SET GAME LAYER
    void setGameLayer(GameLayer *p_PtrGameLayer);
    
    /// THIS RETURN GAME LAYER OBJECT
    GameLayer* getGameLayer();
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
        
    /// Create Toast type text for wrong word match and repeated
    /// @param str  string to be displayed
    void createToast(std::string str);
    
    ///  This called on every level completed
    void onLevelCompleted();
    
    /// this calls on click of next in level complete popup
    void loadNextLevel();
    
    /// updates current coin count to label
    void UpdateCoins();
    
    /// Coin label scale  animation after coin update
    void CoinLabelAnimation();
    /****************************KEYPAD FUNCTIONS********************/
    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* unused_event);
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* unused_event);

    void checkForAndroidDeviceBack();
        
    /// THIS WILL REMOVE CURRENT ONE AND CALL ANOTHER POPUP WAIT FOR ANIMATION OF CURRENT POPUP
    void LoadOtherPopUpWithDelay(float p_fDelayTime, const std::string &p_PopUpName);
    
    /// THIS LOAD OTHER POP UP BY COMPARING WITH STRING
    void LoadOtherPopUp(Ref *p_Sender, const std::string &p_String);
    
    /// THIS UNLOAD POPUP IF EXIST AND GO BACK TO MAIN MENU
    void UnloadPopUpAndExitGame(float p_fDelayTime, bool p_bIsPopUpExitAnimation);
    
    /// THIS CREATE UPDATE COIN EVENT
    void CreateUpdateCoinEvent();
    
    /// this exits from gameplay to levelscreen
    void onGameExit();
    
    /// on No  click from Gameplay exit PopUp
    void onNoFromExitPopUp();
    
    /// This enable's and disables button's click
    /// @param p_bEnable  boolean value to enable or disable
    void setEnableButtons(bool p_bEnable);
    
    /// updates Bonus word count found by user 
    void UpdateBonusWord();
    
    /// this will update the bonus word count in bonus word button
    void checkForBonusWordButton();
    
    ///  This is to  enable hint use animation if user enters continues wrong ans
    /// @param m_AnsStr answer obtained by user
    void onWrongAnswer(std::string m_AnsStr);
    
    /// display's Hint use animation
    void showHintUseAnimation();

    /// this is to disable all Hud Buttons on showing first level tutorial
    void checkforFirstLevelTutorial();
    
    /// check for HUD Buttons tutorial
    void checkForTutorial();
    
    /// this is to check Reveal letter button Enabling after tutorail shown
    void checkforRevealLetteroption();
    
    /// removes hint  Button Tutorial
    void removeHintTutorial();
    
    /// removes  Shuffle Button Tutorial
    void removeShuffleTutorial();
    
    /// removes reaveal letter button Tutorial
    void removeRevealTutorial();
    
    /// THIS UPDATE  BUTTON TAGS AND DATA
    void UpdateMenuData();
    
    /// This will be called Daily Mission's Complete
    void checkForDailyMissionUpdates();
    
    ///  This called on every level completed
    void onLevelComplete();
    
    ///  This will be called on contionues wright word ans to display  Good excelent ,, Tags
    /// @param attempt attempt number
    void checkForAttemptAnim(int attempt);
    
    /// coin Add Animation
    /// @param endPos position wher the coin anim ends
    void coinAddAnimation(Point endPos);
    
    /// removes coin Sprite after coin Animation
    void removeSprite(Ref *_sender);
    
    /// removes Hand on tSpecfic tutorial complete
    void removeHand(Ref *sender);
};

#endif /* HudLayer_hpp */
