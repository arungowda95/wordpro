//
//  LeaderBoardPopUp.hpp
//  wordPro-mobile
//
//  Created by id on 10/07/20.
//

#ifndef LeaderBoardPopUp_hpp
#define LeaderBoardPopUp_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "PopUp.h"
#include "ui/UIListView.h"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"

using namespace cocos2d::ui;
using namespace cocos2d::network;

class LeaderBoardPopUp:public PopUp
{
    
    Sprite* m_SprPopUpBg,*m_SprPopUpTop;
 
    
    ScrollView  *m_ptrScrolView;
    
    std::vector<std::string> fbidArray;
    std::vector<std::string> nameArray;
    std::vector<std::string> imageUrlArr;
    std::vector<int> levelArr;
    std::vector<int> coincntArr;
    
    Vector<Sprite*> *m_photosArr;
    
    std::vector<HttpRequest*> httpRequest;
    
    std::string userName,imageUrl;
    int userlevel,coinCount;
    
    bool userPresent = false;
    bool jsonError = false;

public:

    LeaderBoardPopUp(std::function<void()> func);
    ~LeaderBoardPopUp();
    
    void OnButtonPressed(Ref *p_Sender);
    
    void creatLeaderboard(int size);
 
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    void downloadUsersPhoto(std::string fbIdindex,std::string url);
    
    void onPhotoDownloadSuccess(HttpClient *sender, HttpResponse *response);
        
    void createUserDetailsNode(int size);
};

#endif /* LeaderBoardPopUp_hpp */
