//
//  CW_LevelScene.h
//  wordPro-mobile
//
//  Created by id on 03/04/20.
//

#ifndef CW_LevelScene_h
#define CW_LevelScene_h

#include <stdio.h>
#include "cocos2d.h"
#include "ui/UIListView.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class CW_LevelLayer;
class CW_LevelScene : public Scene
{
    
    CW_LevelLayer *m_ptrLayer;
    
    
    
public:
    CW_LevelScene();
    ~CW_LevelScene();
    
};

#pragma mark LEVEL LAYER
class CW_LevelLayer : public Layer
{
    ScrollView      *m_ptrScrolView;
    MenuItemSprite  *m_BackButton,*m_miUpButton,*m_miDownButton;
    
    
    Size visibleSize;
    Vec2 origin;
    
    Sprite *CW_Sprite;
    
    int m_iCountriesCount;
    
    std::vector<std::string> m_CountriesArray;
    
    std::map<std::string ,int> m_CountrieslevelCntMap;
    
public:
    CW_LevelLayer();
    ~CW_LevelLayer();
    
    ///  Create Level Map Scroll View
    void createChallengeLevelMap();
    
    /// onclick of play button
    void levelMapBtnCallBack(Ref *sender);
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void backBtnCallback(Ref *sender);
    
    /// on scroll of scrollbar this will trigger
    void onScrolling(Ref* sender, ui::ScrollView::EventType type);
    
    /****************************KEYPAD FUNCTIONS********************/
    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* unused_event);
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* unused_event);
    
    /// arrow buttons callback to move up and down for scroll
    void ArrowBtnsCallback(Ref *p_Sender);
    
    void AddAlphaBg();
    
    /// parse Json level map data
    void ParseLevelMapData();
    
    /// This will return The Starting Country According to language
    /// @return returns  country name string
    std::string getLangBasedStartingCountry();
    
    /// this is to update the scroll to current country unlocked
    void checkForCurrentCountryScroll();
    
    /// Creates Label Based on current selected language
    /// @param labelStr String tag for translation
    /// @param fontsize font size current label string
    /// @return returns label
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
};

#endif /* CW_LevelScene_h */
