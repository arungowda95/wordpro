//
//  TutorialScene.cpp
//  wordPro-mobile
//
//  Created by id on 28/04/20.
//

#include "TutorialManager.h"
#include "GameConstants.h"
#include "LanguageTranslator.h"

TutorialManager::TutorialManager()
{
 
    cocos2d::Rect ResolutionSize =  Director::getInstance()->getSafeAreaRect();
    visibleSize = ResolutionSize.size;
    Origin = ResolutionSize.origin;
    tempNode = NULL;
    _touchLayer = NULL;
    
    createTempTutNode();
    
    listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(TutorialManager::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(TutorialManager::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(TutorialManager::onTouchEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

}

void TutorialManager::showHintTutorial(Point ArrowPos)
{
    float xscale = visibleSize.width/64.0f;
     float yscale = visibleSize.height/64.0f;
     Sprite*m_SprBgAlpha = Sprite::create("pixel.png");
     m_SprBgAlpha->setScale(xscale,yscale);
     m_SprBgAlpha->setColor(Color3B::BLACK); //setColor(Color3B::BLACK);
     m_SprBgAlpha->setOpacity(150);
     m_SprBgAlpha->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
     tempNode->addChild(m_SprBgAlpha,1);
     
     Sprite *m_SprTutorBox = Sprite::create("Tutorials-box.png");
     m_SprTutorBox->setScale(m_fScale);
     m_SprTutorBox->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+90+m_SprTutorBox->getBoundingBox().size.height/2));
     tempNode->addChild(m_SprTutorBox,1);
     //GET STUCK ?\n
     Label *wordslbl = createLabelBasedOnLang("TutorHint",40);
     wordslbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
     wordslbl->setColor(Color3B::WHITE);
     wordslbl->setWidth(m_SprTutorBox->getBoundingBox().size.width-20);
     wordslbl->setHeight(wordslbl->getBoundingBox().size.height+20);
     wordslbl->setPosition(Vec2(m_SprTutorBox->getContentSize().width/2,m_SprTutorBox->getContentSize().height/2+10));
     m_SprTutorBox->addChild(wordslbl,1);
    
    Sprite *m_TutorArrow = Sprite::create("arrow.png");
    m_TutorArrow->setScale(m_fScale);
    m_TutorArrow->setFlippedY(true);
    m_TutorArrow->setPosition(Vec2(ArrowPos.x,m_SprTutorBox->getBoundingBox().getMaxY() +m_TutorArrow->getBoundingBox().size.height-50));
    
    tempNode->addChild(m_TutorArrow,1);
    
    MoveTo *move  = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()+5));
    MoveTo *move1 = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()-5));
    
    m_TutorArrow->runAction(RepeatForever::create(Sequence::create(move,move1,NULL)));
}
void TutorialManager::showShuffleTutorial(Point ArrowPos)
{
    float xscale = visibleSize.width/64.0f;
     float yscale = visibleSize.height/64.0f;
     Sprite*m_SprBgAlpha = Sprite::create("pixel.png");
     m_SprBgAlpha->setScale(xscale,yscale);
     m_SprBgAlpha->setColor(Color3B::BLACK); //setColor(Color3B::BLACK);
     m_SprBgAlpha->setOpacity(150);
     m_SprBgAlpha->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
     tempNode->addChild(m_SprBgAlpha,1);
     
     Sprite *m_SprTutorBox = Sprite::create("Tutorials-box.png");
     m_SprTutorBox->setScale(m_fScale);
     m_SprTutorBox->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
     tempNode->addChild(m_SprTutorBox,1);
     //GET STUCK ?\n
     Label *wordslbl = createLabelBasedOnLang("TutorShuffle",35);
     wordslbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
     wordslbl->setColor(Color3B::WHITE);
     wordslbl->setWidth(m_SprTutorBox->getBoundingBox().size.width-20);
     wordslbl->setHeight(wordslbl->getBoundingBox().size.height+20);
     wordslbl->setPosition(Vec2(m_SprTutorBox->getContentSize().width/2,m_SprTutorBox->getContentSize().height/2+10));
     m_SprTutorBox->addChild(wordslbl,1);
    
    
    Sprite *m_TutorArrow = Sprite::create("arrow.png");
    m_TutorArrow->setScale(m_fScale);
//    m_TutorArrow->setFlippedY(false);
    m_TutorArrow->setPosition(Vec2(ArrowPos.x,ArrowPos.y+m_TutorArrow->getBoundingBox().size.height/2+10));
    
    tempNode->addChild(m_TutorArrow,1);
    
    MoveTo *move  = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()+5));
    MoveTo *move1 = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()-5));
    
    m_TutorArrow->runAction(RepeatForever::create(Sequence::create(move,move1,NULL)));

}

void TutorialManager::showRevealButtonTutorial(Point ArrowPos)
{
    float xscale = visibleSize.width/64.0f;
     float yscale = visibleSize.height/64.0f;
     Sprite*m_SprBgAlpha = Sprite::create("pixel.png");
     m_SprBgAlpha->setScale(xscale,yscale);
     m_SprBgAlpha->setColor(Color3B::BLACK); //setColor(Color3B::BLACK);
     m_SprBgAlpha->setOpacity(120);
     m_SprBgAlpha->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2));
     tempNode->addChild(m_SprBgAlpha,1);
     
     Sprite *m_SprTutorBox = Sprite::create("Tutorials-box.png");
     m_SprTutorBox->setScale(m_fScale);
     m_SprTutorBox->setPosition(Vec2(Origin.x+visibleSize.width/2,Origin.y+visibleSize.height/2+m_SprTutorBox->getBoundingBox().size.height));
     tempNode->addChild(m_SprTutorBox,1);
     //GET STUCK ?\n
     Label *wordslbl = createLabelBasedOnLang("TutorFreq",30);
     wordslbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
     wordslbl->setColor(Color3B::WHITE);
     wordslbl->setWidth(m_SprTutorBox->getBoundingBox().size.width-20);
     wordslbl->setHeight(wordslbl->getBoundingBox().size.height+20);
     wordslbl->setPosition(Vec2(m_SprTutorBox->getContentSize().width/2,m_SprTutorBox->getContentSize().height/2+10));
     m_SprTutorBox->addChild(wordslbl,1);
    
    
    Sprite *m_TutorArrow = Sprite::create("arrow.png");
    m_TutorArrow->setScale(m_fScale);
    m_TutorArrow->setPosition(Vec2(ArrowPos.x,ArrowPos.y+m_TutorArrow->getBoundingBox().size.height/2+10));
    
    tempNode->addChild(m_TutorArrow,1);
    
    MoveTo *move  = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()+5));
    MoveTo *move1 = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()-5));
    
    m_TutorArrow->runAction(RepeatForever::create(Sequence::create(move,move1,NULL)));

}
void TutorialManager::showTutorialForDailyTaskButton(Point ArrowPos)
{
     createTempTutNode();
     createTuorialTouch();
     cocos2d::Size viewSize = Director::getInstance()->getWinSize();
     std::string textStr = "check out daily Task's and complete Task's to earn more coins";

     float xscale = viewSize.width/64.0f;
     float yscale = viewSize.height/64.0f;
     Sprite*m_SprBgAlpha = Sprite::create("pixel.png");
     m_SprBgAlpha->setScale(xscale,yscale);
     m_SprBgAlpha->setColor(Color3B::BLACK); //setColor(Color3B::BLACK);
     m_SprBgAlpha->setOpacity(120);
     m_SprBgAlpha->setPosition(Vec2(viewSize/2));
     tempNode->addChild(m_SprBgAlpha,1);
     
     Sprite *m_SprTutorBox = Sprite::create("Tutorials-box.png");
     m_SprTutorBox->setScale(m_fScale);
    m_SprTutorBox->setPosition(Vec2(Origin.x+visibleSize.width/2,ArrowPos.y-m_SprTutorBox->getBoundingBox().size.height*1.3));
     tempNode->addChild(m_SprTutorBox,1);
    
    _touchLayer->setPosition(Vec2(ArrowPos.x,ArrowPos.y+_touchLayer->getContentSize().height/2));
    
     //GET STUCK ?\n
     Label *wordslbl = createLabelBasedOnLang(textStr,35);
     wordslbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
     wordslbl->setColor(Color3B::WHITE);
     wordslbl->setWidth(m_SprTutorBox->getBoundingBox().size.width-20);
     wordslbl->setHeight(wordslbl->getBoundingBox().size.height+20);
     wordslbl->setPosition(Vec2(m_SprTutorBox->getContentSize().width/2,m_SprTutorBox->getContentSize().height/2+10));
     m_SprTutorBox->addChild(wordslbl,1);
    
    
    Sprite *m_TutorArrow = Sprite::create("arrow.png");
    m_TutorArrow->setScale(m_fScale);
    m_TutorArrow->setFlippedY(true);
    m_TutorArrow->setPosition(Vec2(ArrowPos.x,ArrowPos.y-m_TutorArrow->getBoundingBox().size.height/2));
    
    tempNode->addChild(m_TutorArrow,1);
    
    MoveTo *move  = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()+5));
    MoveTo *move1 = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()-5));
    
    m_TutorArrow->runAction(RepeatForever::create(Sequence::create(move,move1,NULL)));

}
void TutorialManager::showTutorialForLeaderboard(Point ArrowPos)
{
     createTempTutNode();
     createTuorialTouch();
     cocos2d::Size viewSize = Director::getInstance()->getWinSize();
     std::string textStr = "login with Facebook to join the leaderboard";

     float xscale = viewSize.width/64.0f;
     float yscale = viewSize.height/64.0f;
     Sprite*m_SprBgAlpha = Sprite::create("pixel.png");
     m_SprBgAlpha->setScale(xscale,yscale);
     m_SprBgAlpha->setColor(Color3B::BLACK); //setColor(Color3B::BLACK);
     m_SprBgAlpha->setOpacity(120);
     m_SprBgAlpha->setPosition(Vec2(viewSize/2));
     tempNode->addChild(m_SprBgAlpha,1);
     
     Sprite *m_SprTutorBox = Sprite::create("Tutorials-box.png");
     m_SprTutorBox->setScale(m_fScale);
    m_SprTutorBox->setPosition(Vec2(Origin.x+visibleSize.width/2,ArrowPos.y+m_SprTutorBox->getBoundingBox().size.height*1.3));
     tempNode->addChild(m_SprTutorBox,1);
    
    _touchLayer->setPosition(Vec2(ArrowPos.x,ArrowPos.y-_touchLayer->getContentSize().height/2));
    
     //GET STUCK ?\n
     Label *wordslbl = createLabelBasedOnLang(textStr,35);
     wordslbl->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
     wordslbl->setColor(Color3B::WHITE);
     wordslbl->setWidth(m_SprTutorBox->getBoundingBox().size.width-20);
     wordslbl->setHeight(wordslbl->getBoundingBox().size.height+20);
     wordslbl->setPosition(Vec2(m_SprTutorBox->getContentSize().width/2,m_SprTutorBox->getContentSize().height/2+10));
     m_SprTutorBox->addChild(wordslbl,1);
    
    
    Sprite *m_TutorArrow = Sprite::create("arrow.png");
    m_TutorArrow->setScale(m_fScale);
    m_TutorArrow->setPosition(Vec2(ArrowPos.x,ArrowPos.y+m_TutorArrow->getBoundingBox().size.height/2));
    
    tempNode->addChild(m_TutorArrow,1);
    
    MoveTo *move  = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()+5));
    MoveTo *move1 = MoveTo::create(0.4f, Vec2(m_TutorArrow->getPositionX(),m_TutorArrow->getPositionY()-5));
    
    m_TutorArrow->runAction(RepeatForever::create(Sequence::create(move,move1,NULL)));

}
Label *TutorialManager::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
}

void TutorialManager::removeTuorial()
{
    if(tempNode)
    {
        tempNode->removeAllChildren();
        this->removeChild(tempNode,true);
        tempNode = NULL;
        
        _touchLayer = NULL;
    }
    this->removeAllChildrenWithCleanup(true);
}
void TutorialManager::createTempTutNode()
{
    if (tempNode==NULL) {
        tempNode = Node::create();
        tempNode->setPosition(Vec2::ZERO);
        this->addChild(tempNode);
    }
}
// TOUCH BEGAN
bool TutorialManager::onTouchBegan(Touch *p_Touch, Event *p_Event)
{
    bool b_enable = true;
    Vec2 t_Location = p_Touch->getLocationInView();
    t_Location = Director::getInstance()->convertToGL(t_Location);
    t_Location = this->convertToNodeSpace(t_Location);
    
    if(_touchLayer!=NULL)
    {
        if(_touchLayer->getBoundingBox().containsPoint(t_Location))
        {
            b_enable = false;
        }
    }
    else{
        b_enable = false;
    }
    return b_enable;
}
// TOUCH MOVED
void TutorialManager::onTouchMoved(cocos2d::Touch *p_Touch, cocos2d::Event *p_Event)
{
    Vec2 t_Location = p_Touch->getLocationInView();
    t_Location = Director::getInstance()->convertToGL(t_Location);
    t_Location = this->convertToNodeSpace(t_Location);
}
// TOUCH ENDED
void TutorialManager::onTouchEnded(cocos2d::Touch *p_Touch, cocos2d::Event *p_Event)
{
    Vec2 t_Location = p_Touch->getLocationInView();
    t_Location = Director::getInstance()->convertToGL(t_Location);
    t_Location = this->convertToNodeSpace(t_Location);
    
    if (_touchLayer!=NULL)
    {
        if(_touchLayer->getBoundingBox().containsPoint(t_Location))
        {
            //            this->onTutorialTouched();
        }
    }
}
void TutorialManager::createTuorialTouch()
{
    _touchLayer = Sprite::create("pixel.png");
    _touchLayer->setOpacity(0);
    _touchLayer->setContentSize(Size(150,150));
    _touchLayer->setPosition(Vec2(1517,209));
    _touchLayer->setName("touchlayer");
    tempNode->addChild(_touchLayer,3);
}

TutorialManager::~TutorialManager()
{
    if(tempNode)
    {
        tempNode->removeAllChildren();
        this->removeChild(tempNode,true);
        tempNode = NULL;
        
        _touchLayer = NULL;
    }

    this->removeAllChildrenWithCleanup(true);
}
