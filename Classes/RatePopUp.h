//
//  RatePopUp.hpp
//  wordPro-mobile
//
//  Created by id on 08/05/20.
//

#ifndef RatePopUp_hpp
#define RatePopUp_hpp

#include <stdio.h>
#include "PopUp.h"

class RatePopUp :public PopUp
{
    Sprite *m_SprPopUpBg,*m_SprPopUpTop;

    MenuItemSprite *t_miRateYesBtn,*t_miRateNoBtn;
    
    Vector<MenuItemSprite*>  *t_starsArr;
    
    MenuItemSprite *t_miRateButton;
    
    Label *t_rateLabel;
    
    Label *t_tSubRateLabel;
public:
    RatePopUp(std::function<void()> func);
    ~RatePopUp();
    
    void OnButtonPressed(Ref *p_Sender) override;
    
    Label *createLabelBasedOnLang(std::string labelStr,float fontsize);
    
    MenuItemSprite *getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func);
    
    void CreatePopupForRateButton();

    void starButtonCallback(Ref *sender);
    
    void setStringForLabelBasedOnStar(int starNum);
    
    void rateButtonCallback(Ref*sender);
};
#endif /* RatePopUp_hpp */
