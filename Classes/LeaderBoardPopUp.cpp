//
//  LeaderBoardPopUp.cpp
//  wordPro-mobile
//
//  Created by id on 10/07/20.
//

#include "LeaderBoardPopUp.h"
#include "FacebookManager.h"

LeaderBoardPopUp::LeaderBoardPopUp(std::function<void()> func)
{
    m_fnSelector = func;
    m_ptrScrolView = NULL;
    m_photosArr = new Vector<Sprite*>();
    
    Rect ResolutionSize = Director::getInstance()->getSafeAreaRect();
    Size visibleSize = Size(ResolutionSize.size.width,ResolutionSize.size.height);
    Point origin = ResolutionSize.origin;

    
    m_ZeroPosition = Point(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2);
    
    m_SprPopUpBg = Sprite::create("PopUp_Bg.png");
    m_SprPopUpBg->setPosition(Vec2(origin.x+visibleSize.width/2,origin.y+visibleSize.height/2)-m_ZeroPosition);
    this->addChild(m_SprPopUpBg,1);
    
    m_SprPopUpTop = Sprite::create("PopUp_Top.png");
    m_SprPopUpTop->setScaleX(m_SprPopUpTop->getScaleX()+0.1f);
    m_SprPopUpTop->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(m_SprPopUpTop,2);
    
    std::string t_Label = "Leaderboard";
    Label *t_tLabel = createLabelBasedOnLang(t_Label,50);
    t_tLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    t_tLabel->setColor(Color3B(255,244,55));
    t_tLabel->enableOutline(Color4B(196,56,63,255),3);
    t_tLabel->setWidth(m_SprPopUpTop->getContentSize().width-70);
    t_tLabel->setHeight(150);
    t_tLabel->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMaxY()));
    this->addChild(t_tLabel, 2);
    
    // CLOSE BUTTON
     MenuItemSprite *t_miCloseButton =getButtonMade("close_button.png","close_button.png",CC_CALLBACK_1(LeaderBoardPopUp::OnButtonPressed,this));
    t_miCloseButton->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMaxX()-10,m_SprPopUpBg->getBoundingBox().getMaxY()-10));
    t_miCloseButton->setTag(0);

    Menu *m_Menu = Menu::create(t_miCloseButton,NULL);
    m_Menu->setPosition(Vec2::ZERO);
    this->addChild(m_Menu,2);

    m_ptrScrolView = ScrollView::create();
    m_ptrScrolView->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_ptrScrolView->setTouchEnabled(true);
    m_ptrScrolView->setBounceEnabled(false);
    Size contentsize = Size(m_SprPopUpBg->getBoundingBox().size.width,m_SprPopUpTop->getBoundingBox().getMinY()-120-m_SprPopUpBg->getBoundingBox().getMinY());
    m_ptrScrolView->setContentSize(Size(contentsize));
    m_ptrScrolView->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpTop->getBoundingBox().getMinY()-30-m_ptrScrolView->getBoundingBox().size.height/2));
    m_ptrScrolView->setScrollBarEnabled(false);
    m_ptrScrolView->setSwallowTouches(false);
    m_ptrScrolView->setAnchorPoint(Vec2(0.5f,0.5f));
    this->addChild(m_ptrScrolView,2);

    rapidjson::Document m_JsonDocument;
    
    std::string  fullPath = FileUtils::getInstance()->fullPathForFilename("sampleboardjson.json");
    printf("\n Full path =%s\n",fullPath.c_str());
        
//    char *p_Buffer = new char[strlen(FileUtils::getInstance()->getStringFromFile(fullPath).c_str()) + sizeof(int) + 1];
//    strcpy(p_Buffer, FileUtils::getInstance()->getStringFromFile(fullPath).c_str());
//    m_JsonDocument.Parse<rapidjson::kParseStopWhenDoneFlag>(p_Buffer);
    
    
//    @"leaderboardJson"
    
    {
        me_strLeaderBoardJson = UserDefault::getInstance()->getStringForKey("leaderboardJson","");
    }
    
//    if(CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID)
    {
        if(me_strLeaderBoardJson.empty())
        {
//            closeButtonCallback(NULL);
            std::string t_Label = "NoPlayers";
            Label *t_tLabelplay = createLabelBasedOnLang(t_Label,45);
            t_tLabelplay->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
            t_tLabelplay->setColor(Color3B(255,244,55));
            t_tLabelplay->enableOutline(Color4B(196,56,63,255),3);
            t_tLabelplay->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
            t_tLabelplay->setHeight(t_tLabelplay->getBoundingBox().size.height+20);
            t_tLabelplay->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()));
            this->addChild(t_tLabelplay, 2);
            return;
        }
        m_JsonDocument.Parse<rapidjson::kParseStopWhenDoneFlag>(me_strLeaderBoardJson.c_str());
    }
    
    if(m_JsonDocument.HasParseError())
    {
        CCLOG("GetParseError %u\n",m_JsonDocument.GetParseError());
        return;
    }
    
    size_t listsize=0;
    if(m_JsonDocument.HasMember(me_Language.c_str()))
    {
        const  rapidjson::Value& b = m_JsonDocument[me_Language.c_str()];
        
        size_t totSize = b.Size();
        
        listsize = b.Size();
        
        if (listsize>=100) {
            listsize = 100;
        }
        
        if(!b.IsNull())
        {
            for (int j = 0; j < listsize; j++)
            {
                const  rapidjson::Value& c = b[j];
                if(c.HasMember("fbId"))
                    fbidArray.push_back(c["fbId"].GetString());

                if(c.HasMember("name"))
                    nameArray.push_back(c["name"].GetString());
                
                if(c.HasMember("profileUrl"))
                    imageUrlArr.push_back(c["profileUrl"].GetString());

                if(c.HasMember("currentLevel"))
                    levelArr.push_back(c["currentLevel"].GetInt());

                if(c.HasMember("coinCount"))
                    coincntArr.push_back(c["coinCount"].GetInt());
            }
        }
        std::string strfbId = GameController::getInstance()->getStringForKey("facebookID");
        userPresent =  std::find(fbidArray.begin(), fbidArray.end(), strfbId.c_str()) != fbidArray.end();


        // in top list no user
        if(!userPresent&&FacebookManager::getInstance()->isLoggedIn())
        {
            int index = UserDefault::getInstance()->getIntegerForKey("UserIndex",0);
            if(index<totSize&&index!=-1)
            {
                const  rapidjson::Value& c = b[index];
                
                if(!c.IsNull())
                {
                    if(c.HasMember("name"))
                        userName = c["name"].GetString();
                    
                    if(c.HasMember("profileUrl"))
                        imageUrl = c["profileUrl"].GetString();
                    
                    if(c.HasMember("currentLevel"))
                        userlevel = c["currentLevel"].GetInt();
                    
                    if(c.HasMember("coinCount"))
                        coinCount = c["coinCount"].GetInt();
                }
                else{
                    jsonError = true;
                }
            }
        }

    }
    
    printf("fbidArray-->%lu\n",fbidArray.size());
    printf("nameArray-->%lu\n",nameArray.size());
    printf("imageUrlArr-->%lu\n",imageUrlArr.size());
    
    if(listsize>0)
    {
        creatLeaderboard((int)listsize);
    }
    else
    {
        std::string t_Label = "NoPlayers";
        Label *t_tLabelplay = createLabelBasedOnLang(t_Label,45);
        t_tLabelplay->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
        t_tLabelplay->setColor(Color3B(255,244,55));
        t_tLabelplay->enableOutline(Color4B(196,56,63,255),3);
        t_tLabelplay->setWidth(m_SprPopUpBg->getBoundingBox().size.width-64);
        t_tLabelplay->setHeight(t_tLabelplay->getBoundingBox().size.height+20);
        t_tLabelplay->setPosition(Vec2(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMidY()));
        this->addChild(t_tLabelplay, 2);
    }
    

}
void LeaderBoardPopUp::creatLeaderboard(int size)
{
    
    Sprite *path = Sprite::create("coins_popup_Bg.png");
    path->setScaleY(path->getScaleY()+0.1);

    float pathHeight = path->getBoundingBox().size.height+5;
     
    int tempSize = size;
    
    if(size<5)
    {
        tempSize = 10;
        m_ptrScrolView->setTouchEnabled(false);
    }
        
    Size Containersize = Size(m_SprPopUpBg->getBoundingBox().size.width,pathHeight*(tempSize));
    m_ptrScrolView->setInnerContainerSize(Size(Containersize.width,Containersize.height));

    Point t_position = Vec2(m_ptrScrolView->getContentSize().width/2,Containersize.height-5);
  
    for(int index = 1; index<=size; index++)
    {
         Layout *layout = Layout::create();
         layout->setColor(Color3B::WHITE);
         layout->setContentSize(Size(path->getBoundingBox().size.width, path->getBoundingBox().size.height));
         layout->setAnchorPoint(Vec2(0.5f,0.5f));
         layout->setPosition(Vec2(t_position.x,t_position.y-layout->getBoundingBox().size.height/2));
         m_ptrScrolView->addChild(layout,1);
         
         Sprite *t_ImageView = Sprite::create("coins_popup_Bg.png");

        if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
        {
            if(fbidArray.at(index-1).compare(GameController::getInstance()->getStringForKey("facebookID"))==0)
            {
                t_ImageView = Sprite::create("popup-box4.png");
            }
            else if (fbidArray.at(index-1).compare(GameController::getInstance()->getStringForKey("AppleId"))==0)
            {
                t_ImageView = Sprite::create("popup-box4.png");
            }
        }
        else
        {
          if(fbidArray.at(index-1).compare(GameController::getInstance()->getStringForKey("facebookID"))==0)
          {
              t_ImageView = Sprite::create("popup-box4.png");
          }
        }
         t_ImageView->setScaleY(t_ImageView->getScaleY()+0.1);
         t_ImageView->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
         layout->addChild(t_ImageView);
          
          Label *m_rankNum = Label::createWithTTF(to_string(index),FONT_NAME,50);
          m_rankNum->setColor(Color3B::WHITE);
          m_rankNum->enableOutline(Color4B(15,80,114,255),3);
          m_rankNum->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
          layout->addChild(m_rankNum,1);

          std::string rankIc = "rankNumBg.png";
          if (index<=3) {
               rankIc = StringUtils::format("%dst.png",index);
              m_rankNum->setVisible(false);
          }
          Sprite *t_rankNumBg = Sprite::create(rankIc);
              t_rankNumBg->setPosition(Vec2(10+t_rankNumBg->getContentSize().width/2,layout->getContentSize().height/2+10));
              layout->addChild(t_rankNumBg,1);
          m_rankNum->setPosition(Vec2(t_rankNumBg->getPositionX(),t_rankNumBg->getPositionY()));

          Sprite *t_ProfileIcon = Sprite::create("photo1.png");
          t_ProfileIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        t_ProfileIcon->setPosition(Vec2(t_rankNumBg->getBoundingBox().getMaxX()+10,layout->getContentSize().height/2+5));
          layout->addChild(t_ProfileIcon,1);

          // profile pic
          std::string str = FileUtils::getInstance()->getWritablePath().append(fbidArray.at(index-1))+".png";
          float offx = 15.0f;
          if(FileUtils::getInstance()->getStringFromFile(str).empty())
          {
              str = "photo1.png";
              offx = 10.0f;
              downloadUsersPhoto(fbidArray.at(index-1), imageUrlArr.at(index-1));
          }

          Sprite *t_ProfilePic = Sprite::create(str);
        if(t_ProfilePic == NULL){
            t_ProfilePic = Sprite::create("photo1.png");
            offx = 10.0f;
        }
          t_ProfilePic->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
            t_ProfilePic->setPosition(Vec2(t_rankNumBg->getBoundingBox().getMaxX()+offx,layout->getContentSize().height/2+5));
          t_ProfilePic->setName(fbidArray.at(index-1));
          layout->addChild(t_ProfilePic,2);
          m_photosArr->pushBack(t_ProfilePic);
          
          Label *m_playerName = Label::createWithSystemFont(nameArray.at(index-1),FONT_NAME,30);
          m_playerName->setColor(Color3B::WHITE);
          m_playerName->enableOutline(Color4B(15,80,114,255),3);
          m_playerName->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
          m_playerName->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
          m_playerName->setPosition(Vec2(t_ProfileIcon->getBoundingBox().getMaxX()+20,layout->getContentSize().height*0.85));
          layout->addChild(m_playerName,1);
          
          //coins image
          Sprite *CoinAd = Sprite::create("shop-coins.png");
          CoinAd->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
          CoinAd->setScale(0.5f);
          CoinAd->setPosition(Vec2(t_ProfileIcon->getBoundingBox().getMaxX(),layout->getContentSize().height*0.4));
          layout->addChild(CoinAd,1);

          //coin count
          Label *m_coincount = Label::createWithTTF(to_string(coincntArr.at(index-1)),FONT_NAME,27);
          m_coincount->setColor(Color3B(252,243,70));
          m_coincount->enableOutline(Color4B(0,0,0,255),3);
          m_coincount->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
          m_coincount->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
          m_coincount->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX(),layout->getContentSize().height*0.5));
          layout->addChild(m_coincount,1);

          //level num
          Label *m_levelnum = Label::createWithTTF(StringUtils::format("LEVEL %d",levelArr.at(index-1)),FONT_NAME,22);
          m_levelnum->setColor(Color3B(105,205,255));
          m_levelnum->enableOutline(Color4B(0,0,0,255),3);
          m_levelnum->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
          m_levelnum->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
          m_levelnum->setPosition(Vec2(m_coincount->getBoundingBox().getMinX()+25,layout->getContentSize().height*0.23));
          layout->addChild(m_levelnum,1);

        if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
        {
            if(fbidArray.at(index-1).compare(GameController::getInstance()->getStringForKey("facebookID"))==0)
            {
                t_ProfileIcon->setTexture("photo2.png");
                m_levelnum->setColor(Color3B(252,243,70));
            }
            else if (fbidArray.at(index-1).compare(GameController::getInstance()->getStringForKey("AppleId"))==0)
            {
                t_ProfileIcon->setTexture("photo2.png");
                t_ProfilePic->setTexture("photo2.png");
                m_levelnum->setColor(Color3B(252,243,70));
            }
        }
        else
        {
          if(fbidArray.at(index-1).compare(GameController::getInstance()->getStringForKey("facebookID"))==0)
          {
              t_ProfileIcon->setTexture("photo2.png");
              m_levelnum->setColor(Color3B(252,243,70));
          }
        }
          t_position.y -= layout->getContentSize().height+5;
    }
    
    if(!userPresent&&FacebookManager::getInstance()->isLoggedIn())
    {
        createUserDetailsNode(0);
    }
}
void LeaderBoardPopUp::createUserDetailsNode(int size)
{
    
    if (jsonError) {
        return;
    }
    std::string strfbId = GameController::getInstance()->getStringForKey("facebookID");

    int index = UserDefault::getInstance()->getIntegerForKey("UserIndex",0);

    if(UserDefault::getInstance()->getIntegerForKey("UserIndex",0)>size)
    {
        
    }
        
    Sprite *path = Sprite::create("coins_popup_Bg.png");
    path->setScale(path->getScaleX()+0.05,path->getScaleY()+0.1);

    Point pos = Point(m_SprPopUpBg->getBoundingBox().getMidX(),m_SprPopUpBg->getBoundingBox().getMinY()-40);
    
    Layout *layout = Layout::create();
    layout->setColor(Color3B::WHITE);
    layout->setContentSize(Size(path->getBoundingBox().size.width, path->getBoundingBox().size.height));
    layout->setAnchorPoint(Vec2(0.5f,0.5f));
    layout->setPosition(Vec2(pos.x,pos.y+layout->getContentSize().height/2));
    this->addChild(layout,2);

    Sprite *t_ImageView = Sprite::create("popup-box4.png");
    t_ImageView->setScale(t_ImageView->getScaleX()+0.05,t_ImageView->getScaleY()+0.1);
    t_ImageView->setPosition(Vec2(layout->getContentSize().width/2,layout->getContentSize().height/2));
    layout->addChild(t_ImageView);
    
    Label *m_rankNum = Label::createWithTTF(to_string(index),FONT_NAME,50);
    m_rankNum->setColor(Color3B::WHITE);
    m_rankNum->enableOutline(Color4B(15,80,114,255),3);
    m_rankNum->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    layout->addChild(m_rankNum,1);
    
    std::string rankIc = "rankNumBg.png";
    Sprite *t_rankNumBg = Sprite::create(rankIc);
    t_rankNumBg->setPosition(Vec2(10+t_rankNumBg->getContentSize().width/2,layout->getContentSize().height/2+10));
    m_rankNum->setPosition(Vec2(t_rankNumBg->getPositionX(),t_rankNumBg->getPositionY()));
    layout->addChild(t_rankNumBg,1);

    Sprite *t_ProfileIcon = Sprite::create("photo1.png");
    t_ProfileIcon->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    t_ProfileIcon->setPosition(Vec2(t_rankNumBg->getBoundingBox().getMaxX()+10,layout->getContentSize().height/2+5));
    layout->addChild(t_ProfileIcon,1);
    
    // profile pic
    std::string str = FileUtils::getInstance()->getWritablePath().append(GameController::getInstance()->getStringForKey("facebookID"))+".png";
    float offx = 15.0f;
    if(FileUtils::getInstance()->getStringFromFile(str).empty())
    {
        str = "photo1.png";
        offx = 10.0f;
        downloadUsersPhoto(strfbId,imageUrl);
    }
    
    Sprite *t_ProfilePic = Sprite::create(str);
    if(t_ProfilePic == NULL){
        t_ProfilePic = Sprite::create("photo1.png");
        offx = 10.0f;
    }
    t_ProfilePic->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    t_ProfilePic->setPosition(Vec2(t_rankNumBg->getBoundingBox().getMaxX()+offx,layout->getContentSize().height/2+5));
    layout->addChild(t_ProfilePic,2);
    m_photosArr->pushBack(t_ProfilePic);
    
    Label *m_playerName = Label::createWithSystemFont(userName,FONT_NAME,30);
    m_playerName->setColor(Color3B::WHITE);
    m_playerName->enableOutline(Color4B(15,80,114,255),3);
    m_playerName->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
    m_playerName->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    m_playerName->setPosition(Vec2(t_ProfileIcon->getBoundingBox().getMaxX()+20,layout->getContentSize().height*0.85));
    layout->addChild(m_playerName,1);
    
    //coins image
    Sprite *CoinAd = Sprite::create("shop-coins.png");
    CoinAd->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    CoinAd->setScale(0.5f);
    CoinAd->setPosition(Vec2(t_ProfileIcon->getBoundingBox().getMaxX(),layout->getContentSize().height*0.4));
    layout->addChild(CoinAd,1);
    
    //coin count
    Label *m_coincount = Label::createWithTTF(to_string(coinCount),FONT_NAME,27);
    m_coincount->setColor(Color3B(252,243,70));
    m_coincount->enableOutline(Color4B(0,0,0,255),3);
    m_coincount->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
    m_coincount->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    m_coincount->setPosition(Vec2(CoinAd->getBoundingBox().getMaxX(),layout->getContentSize().height*0.5));
    layout->addChild(m_coincount,1);
    
    //level num
    Label *m_levelnum = Label::createWithTTF(StringUtils::format("LEVEL %d",userlevel),FONT_NAME,22);
    m_levelnum->setColor(Color3B(105,205,255));
    m_levelnum->enableOutline(Color4B(0,0,0,255),3);
    m_levelnum->setAlignment(TextHAlignment::LEFT,TextVAlignment::TOP);
    m_levelnum->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    m_levelnum->setPosition(Vec2(m_coincount->getBoundingBox().getMinX()+25,layout->getContentSize().height*0.23));
    layout->addChild(m_levelnum,1);
    
}
void LeaderBoardPopUp::OnButtonPressed(Ref *p_Sender)
{
    GameController::getInstance()->playSFX(SFX_BUTTON_CLICKED);
    
    MenuItemSprite *Button  = (MenuItemSprite*)p_Sender;
    
    int pButtonTag =  Button->getTag();

    switch (pButtonTag)
    {
        case 0:
        {
            status = (char*)"close";
        }break;
        default:
            break;
    }
    
    if(m_fnSelector != NULL)
    {
        m_fnSelector();
    }
    
}
MenuItemSprite *LeaderBoardPopUp::getButtonMade(const std::string normalSpr,const std::string selSpr,const ccMenuCallback& func)
{
    Sprite *_normalSpr = Sprite::create(normalSpr);
    
    Sprite *_SelectedSpr = Sprite::create(selSpr);
    _SelectedSpr->setOpacity(150);
    
    MenuItemSprite *Button = MenuItemSprite::create(_normalSpr, _SelectedSpr ,func);
    return Button;
}

Label *LeaderBoardPopUp::createLabelBasedOnLang(std::string labelStr,float fontsize)
{
    Label *t_tLabel;
    std::string langstr = LanguageTranslator::getInstance()->getTranslatorStringWithTag(labelStr.c_str());
    if(me_Language.compare(LANG_ENGLISH)==0)
    {
        t_tLabel = Label::createWithTTF(langstr, FONT_NAME,fontsize);
    }
    else
    {
       t_tLabel = Label::createWithSystemFont(langstr, SYS_FONT_NAME,fontsize);
       t_tLabel->enableBold();
    }
    
    return t_tLabel;
    
}
/// download  Profile icon with url
void LeaderBoardPopUp::downloadUsersPhoto(std::string fbIdindex,std::string url)
{
    if(url.empty())
    {
        log("empty url");
    }
        
    HttpRequest* request = new (std::nothrow) HttpRequest();
    request->setUrl(url.c_str());
    request->setRequestType(HttpRequest::Type::GET);
    request->setResponseCallback(CC_CALLBACK_2(LeaderBoardPopUp::onPhotoDownloadSuccess, this));
    request->setTag(fbIdindex);
    HttpClient::getInstance()->send(request);
    httpRequest.push_back(request);
}
/// success callback for icon download
void LeaderBoardPopUp::onPhotoDownloadSuccess(HttpClient *sender, HttpResponse *response)
{
    if (!response || !response->isSucceed())
    {
        CCLOG("response failed! error buffer: %s", response->getErrorBuffer());
//        downloadUsersPhoto(0,);
        return;
    }
     std::vector<char> *buffer = response->getResponseData();
    Image *img = new Image();
    
    try
    {
        if(img-> initWithImageData(reinterpret_cast<const unsigned char*>(&(buffer->front())), buffer->size()))
        {

        }
        else
            img = nullptr;
    }
    catch(const std::exception& e){
        CCLOG(" ERROR%s",e.what());
    }
    
    if (img != nullptr)
    {
        Texture2D *adTexture = new Texture2D();
        adTexture->initWithImage(img);
        FILE *m_File = NULL;

        std::string iconname = response->getHttpRequest()->getTag();
        std::string str = FileUtils::getInstance()->getWritablePath().append(iconname.c_str())+".png";

           //CCLOG("%s",path.c_str());
           m_File = fopen(str.c_str(), "w+");
           if (!m_File)
           {
               CCLOG("File Not Found");
               return;
           }
        
           
        fwrite(str.c_str(), 1,strlen(str.c_str()), m_File);
        fclose(m_File);
        
        
        for (int index =0;index<m_photosArr->size();index++)
        {
            std::string sprTagname = m_photosArr->at(index)->getName().c_str();
            if(strcmp(sprTagname.c_str(),iconname.c_str())==0)
            {
                m_photosArr->at(index)->setTexture(adTexture);
                m_photosArr->at(index)->setScale(m_fScale);
                m_photosArr->at(index)->setPositionX(m_photosArr->at(index)->getPositionX()+6);
            }
        }
        if (img->saveToFile(str.c_str(), true))
        {
            img->release();
        }
    }
}

LeaderBoardPopUp::~LeaderBoardPopUp()
{
    for (int i = 0;i < (int)httpRequest.size();i++)
    {
        httpRequest.at(i)->setResponseCallback(nullptr);
        httpRequest.at(i)->release();
    }

    if (httpRequest.size()>0) {
        httpRequest.clear();
    }
    
    if(m_ptrScrolView)
    {
        m_ptrScrolView->removeAllChildren();
        m_ptrScrolView = NULL;
    }

    this->removeAllChildrenWithCleanup(true);
}
