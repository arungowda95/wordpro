//
//  GameConstants.h
//  wordPro
//
//  Created by id on 19/02/20.
//

#ifndef GameConstants_h
#define GameConstants_h

#include "SimpleAudioEngine.h"

using namespace CocosDenshion;

extern float m_fScale;
extern std::string me_Language,me_strScreenName,me_strLeaderBoardJson,me_strUserIdsJson,me_strUserDataJson;
extern int me_iCoinCount;
extern bool me_bSound,me_bMusic,me_bNotification,me_bDailyRewardAvailable,me_bShowAds;

// LANGUAGES
#define LANG_ENGLISH    "ENGLISH"
#define LANG_KANNADA    "KANNADA"
#define LANG_HINDI      "HINDI"
#define LANG_TELUGU     "TELUGU"
#define LANG_MALAYALAM  "MALAYALAM"
#define LANG_MARATHI    "MARATHI"
#define LANG_ODIA       "ODIA"
#define LANG_TAMIL      "TAMIL"
#define LANG_BANGLA     "BANGLA"
#define LANG_GUJARATI   "GUJARATI"

#define LANGUAGE        "Language"

#define CURR_LEVEL      "CurrLevel"
#define LEVEL_NUM       "levelNum%s"
#define COINS           "Coins"


#define FONT_NAME           "fonts/FUTURAXK_0.TTF"
#define SYS_FONT_NAME       "fonts/arial.ttf"

// To compare given strings
#define STR_COMPARE(str1, str2) ((std::string(str1).compare(str2) == 0) ? true : false)


#define BonusWords        "BonusWordsCount_%s"

#define CountriesCount    "countriesCount"

#define CountriesUnlockedCount "countriesUnlockedCount_%s"

#define CountriesUnlocked "countriesUnlocked_%s"
#define COUNTRY           "Country_%s"
#define CURRENT_STATE     "CurrentState_%s"


#define NEXT_COUNTRY           "NextCountry_%s"

                                        // _countryName_Language
#define CountriesTotLevelCount          "countriesTotLevelCount_%s_%s"
#define StatesTotLevelCount             "statesTotLevelCount_%s_%s"

                                        // _countryName_Language
#define CountriesPlayedLevelCount       "countriesPlayedLevelCount_%s_%s"
#define StatesPlayedLevelCount          "statesPlayedLevelCount_%s_%s"

#define IsCountryPlayed                 "country_%s_%s"//Countryname_Language

#define CountriesTotGiftsCount          "countriesTotGiftsCount_%s_%s"
#define CountriesGiftsUnlockCount       "countriesGiftsUnlockCount_%s_%s"
#define CountriesGiftsClaimed           "CountriesGiftsClaimed_%s_%s_%d"


#define LEVEL_RATE      "levelRate"

// Daily mission
#define NEW_TASK                "NewTask"

#define MISSION_START_TIME       "MISSION_START_TIME"
#define DAY_1                    "Day1"
#define DAY_2                    "Day2"
#define DAY_3                    "Day3"
#define DAY_4                    "Day4"
#define DAY_5                    "Day5"
#define MISSION_DAY              "MISSION_DAY"


// Daily reward
#define REWARD_START_TIME        "REWARD_START_TIME"
#define CLAIM_START_TIME         "CLAIM_START_TIME"
#define REWARD_DAY_1             "REWARD_DAY_1"
#define REWARD_DAY_2             "REWARD_DAY_2"
#define REWARD_DAY_3             "REWARD_DAY_3"
#define REWARD_DAY_4             "REWARD_DAY_4"
#define REWARD_DAY_5             "REWARD_DAY_5"
#define REWARD_DAY_6             "REWARD_DAY_6"
#define REWARD_DAY_7             "REWARD_DAY_7"

#define DAILY_REWARD_AVAILABLE   "DailyRewardAvailable"

//
#define REMOVE_ADS "remove_ads"
#define MEGA_DEAL  "coins_deal"

#define NO_ADS_FOR_SESSION         "noAdsForSession"

//
#define EVENT_VIDEO_ADS_SUCCEED    "videoAdSuccess"
#define EVENT_VIDEO_ADS_FAILED     "videoAdFailure"

#define EVENT_VIDEO_ADS_AVAILABLE  "videoAdAvailable"

#define EVENT_PURCHASE_SUCCESS     "purchaseSuccess"
#define EVENT_PURCHASE_FAILURE     "purchaseFailure"

#define EVENT_REMOVE_ADS_SUCCESS   "RemoveAdsSuccess"

#define EVENT_RESTORE_SUCCESS   "RestoreSuccess"

#define EVENT_UPDATE_COINS         "UpdateCoins"


#define SALE_START_TIME            "SaleStartTime"
#define LIMITED_SALE_AVAILABLE     "LimitedSaleAvailable"


#define INTERNET_ERROR "Check your Internet"

//ACHIEVEMENTS
#define ACHIEVED_30_GOOD_COMBOS                 "Get 30 GOOD combos"
#define ACHIEVED_30_GREAT_COMBOS                "Get 30 GREAT combos"
#define ACHIEVED_30_AMAZING_COMBOS              "Get 30 Amazing combos"
#define ACHIEVED_30_AWESOME_COMBOS              "Get 30 Awesome combos"

#define ACHIEVED_100_WORDS_SPELL                "Spell 100 words"
#define ACHIEVED_25_TIMES_HINT                  "Use Hint for 25 times"
#define ACHIEVED_50_TIMES_HINT                  "Use Hint option for 50 times"

#define ACHIEVED_25_TIMES_LETTER_A              "Use letter(A) count option for 25 times"
#define ACHIEVED_50_TIMES_LETTER_A              "Use letter(A) count option for 50 times"

#define ACHIEVED_10_GIFT_COLLECT                "Collect the Gift box items of level map for 10 times"
#define ACHIEVED_25_GIFT_COLLECT                "Collect the Gift box items of level map for 25 times"

#define ACHIEVED_15_DAYS_BONUS_COLLECT          "Collect daily Bonus for continuous 15 days"
#define ACHIEVED_50_DAYS_BONUS_COLLECT          "Collect daily Bonus for 50 times"

#define ACHIEVED_50_LEVELS_WITHOUT_MISSPELL     "Clear any 50 levels without misspelling"

#define ACHIEVED_25_BONUS_WORDS_COLLECT          "Collect 25 Bonus word"
#define ACHIEVED_50_BONUS_WORDS_COLLECT          "Collect 50 Bonus word"

#define ACHIEVED_ALL_LEVELS_OF_INDIA      "Complete all levels of India"
#define ACHIEVED_ALL_LEVELS_OF_AMERICA    "Complete all levels of America"
#define ACHIEVED_ALL_LEVELS_OF_AUSTRALIA  "Complete all levels of Australia"
#define ACHIEVED_ALL_LEVELS_OF_ENGLAND    "Complete all levels of England"
#define ACHIEVED_ALL_LEVELS_OF_RUSSIA     "Complete all levels of Russia"
#define ACHIEVED_ALL_LEVELS_OF_INDONESIA  "Complete all levels of Indonesia"
#define ACHIEVED_ALL_LEVELS_OF_JAPAN      "Complete all levels of Japan"
#define ACHIEVED_ALL_LEVELS_OF_BRAZIL     "Complete all levels of Brazil"
#define ACHIEVED_ALL_LEVELS_OF_FRANCE     "Complete all levels of France"
#define ACHIEVED_ALL_LEVELS_OF_CANADA     "Complete all levels of Canada"
#define ACHIEVED_ALL_LEVELS_OF_GERMANY    "Complete all levels of Germany"



#define TUTORIAL_START   "TutorialStart"
#define TUTORIAL_HINT   "TutorialHint"
#define TUTORIAL_SHUFFLE   "Tutorialshuffle"
#define TUTORIAL_REVEAL   "TutorialREVEAL"

// MUSIC FILES
#define SFX_BUTTON_CLICKED         "Sound/sfx_button_click.mp3"
#define SFX_COIN_COLLECT           "Sound/sfx_coin_collect.mp3"
#define SFX_COUNTRY_UNLOCK         "Sound/sfx_country_unlock.mp3"
#define SFX_GAMEPLAY_ENTER         "Sound/sfx_gameplay_enter.mp3"
#define SFX_HINT_LETTER_BONUS      "Sound/sfx_hint_letter_bonus.mp3"
#define SFX_LEVEL_COMPLETE         "Sound/sfx_level_complete.mp3"
#define SFX_REFRESH_BUTTON         "Sound/sfx_refresh_button.mp3"
#define SFX_SCREEN_MOVE            "Sound/sfx_screen_move.mp3"
#define SFX_CORRECT_WORD           "Sound/sfx_correct_word.mp3"
#define SFX_WRONG_WORD             "Sound/sfx_wrong_word.mp3"
#define SFX_GIFT_UNLOCK            "Sound/sfx_gift_unlock.mp3"

#define BGM_GAME                   "Sound/bgm_gameBg.mp3"
#define BGM_GAME_PLAY              "Sound/bgm_gameplay.mp3"

#define FB_LOGIN_SUCCESS           "FbLoginSuccess"
#define FB_LOGIN_FAILURE           "FbLoginFailure"

#define APPLE_LOGIN_SUCCESS        "AppleLoginSuccess"
#define APPLE_LOGIN_FAILURE        "AppleLoginFailure"

#if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
#define MORE_GAMES_URL          "https://exaweb.in/games/word_pro/iOS/more_games.json"
#define LEVEL_FINISH_GAMES_URL  "https://exaweb.in/games/word_pro/iOS/level_finish_more_games.json"
#else
#define MORE_GAMES_URL          "https://exaweb.in/games/word_pro/more_games.json"
#define LEVEL_FINISH_GAMES_URL  "https://exaweb.in/games/word_pro/level_finish_more_games.json"
#endif

#if(CC_TARGET_PLATFORM==CC_PLATFORM_IOS)
#define EXIT_PROMOTION_URL  "https://exaweb.in/games/word_pro/iOS/exit_promotion.json"
#define POPUP_PROMOTION_URL "https://exaweb.in/games/word_pro/iOS/popup_promotion.json"
#else
#define EXIT_PROMOTION_URL  "https://exaweb.in/games/word_pro/exit_promotion.json"
#define POPUP_PROMOTION_URL "https://exaweb.in/games/word_pro/popup_promotion.json"
#endif

#define APP_UPDATE_TIME   "appUpdateTime"

#define POPUP_TEXT "popupText"

#define USER_PREDICT_TYPE "predictType"
#define PREDICTION_COINS  "predict_Coins"
#endif /* GameConstants_h */
