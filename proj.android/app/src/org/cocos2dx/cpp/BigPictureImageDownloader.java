package org.cocos2dx.cpp;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;


import org.cocos2dx.cpp.Firebase.PushMessagingService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class BigPictureImageDownloader extends AsyncTask<String, Void, Bitmap> {
    private Context mContext;
    private String title, message, imageUrl;
    Map<String, String> payloadData;

    private static final String TAG = "BigPictureImageDownload";
    public BigPictureImageDownloader(Context context, String title, String message, String imageUrl,Map<String, String> data)
    {
        super();
        this.mContext = context;
        this.title = title;
        this.message = message;
        this.imageUrl = imageUrl;
        this.payloadData = data;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        InputStream in;
        String stringUrl = imageUrl;
        Bitmap  bitmap = null;
        InputStream inputStream;
        try {
            inputStream = new java.net.URL(stringUrl).openStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
        } catch (IOException e) {
            Log.e(TAG, "doInBackground: "+e.getMessage() );
            e.printStackTrace();
        }
        return bitmap;
//        try {
//            URL url = new URL(this.imageUrl);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            in = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(in);
//
//            return myBitmap;
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//            Log.e(TAG, "doInBackground: " );
//        } catch (IOException e) {
//            Log.e(TAG, "doInBackground: " );
//            e.printStackTrace();
//        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        Log.e(TAG, "onPostExecute: " );
        PushMessagingService.sendNotification(mContext,title,message,imageUrl,result,payloadData);
//        Intent intent = new Intent(mContext, AppActivity.class);
//        intent.putExtra("key", "value");
//        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, intent, PendingIntent.FLAG_ONE_SHOT);
//
//        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//        Notification notif = new Notification.Builder(mContext)
//                .setContentIntent(pendingIntent)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setLargeIcon(result)
//                .setStyle(new Notification.BigPictureStyle().bigPicture(result))
//                .build();
//        notif.flags |= Notification.FLAG_AUTO_CANCEL;
//        notificationManager.notify(1, notif);
    }



}
