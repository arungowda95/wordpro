package org.cocos2dx.cpp;

import androidx.multidex.MultiDexApplication;
public class WordProApplication extends MultiDexApplication {
    private static WordProApplication mInstance;

    public static synchronized WordProApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}

