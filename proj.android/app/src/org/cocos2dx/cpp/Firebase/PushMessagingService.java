package org.cocos2dx.cpp.Firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.word.pro.R;

import org.cocos2dx.cpp.AppActivity;
import org.cocos2dx.cpp.BigPictureImageDownloader;
import org.cocos2dx.cpp.Firebase.AppConstants;
import org.cocos2dx.cpp.Firebase.AppPreferences;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class PushMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    private String title, message, imageUrl;
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "MessageReceivedFrom: " + remoteMessage.getFrom());

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();
        Log.e(TAG, "Message data payload: " + data);

        if(notification.getImageUrl()!=null)
        {
            imageUrl = notification.getImageUrl().toString();
        }

        title = getString(R.string.app_name);
        if(notification.getTitle()!=null) {
            title = notification.getTitle();
        }

        if(notification.getBody()!=null) {
            message = notification.getBody();
        }

        // Check if message contains a data payload.
        if (imageUrl != null && !"".equals(imageUrl)) {
                new BigPictureImageDownloader(this,title, message, imageUrl,data).execute();
            }else{
                sendNotification(this,title, message, null,null,data);
            }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }
    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob()
    {

        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    public static void handleDataMessage(Map<String, String> data) {
        Log.e(TAG, "handleDataMessage() called with: data = [" + data.toString() + "]");
        AppPreferences.getInstance().setNotificationData("");
        if(data.size()>0){
            AppPreferences.getInstance().setNotificationData(data.toString());
        }
    }
    /**
     * Create and show a simple notification containing the received FCM message.
     *
     */
    public static void sendNotification(Context context, String label, String message, String imageUrl, Bitmap image, Map<String, String> data)
    {
        Log.e(TAG, "sendNotification() called with: context = [" + context + "], label = [" + label + "], message = [" + message + "], imageUrl = [" + imageUrl + "], image = [" + image + "], data = [" + data + "]");
        Intent intent = new Intent(context, AppActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.notify_sound);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, context.getString(R.string.default_notification_channel_id));

        if(data.get(AppConstants.KEY_TYPE)!=null)
        {
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.KEY_TYPE, data.get(AppConstants.KEY_TYPE));

            if (data.get(AppConstants.KEY_TYPE).equals(AppConstants.KEY_URL))
            {
                if(data.get(AppConstants.KEY_URL)!=null)
                    bundle.putString(AppConstants.KEY_URL, data.get(AppConstants.KEY_URL));
            }
            else if (data.get(AppConstants.KEY_TYPE).equals(AppConstants.KEY_GIFT))
            {
                if(data.get(AppConstants.KEY_GIFT_COIN)!=null)
                    bundle.putString(AppConstants.KEY_GIFT_COIN, data.get(AppConstants.KEY_GIFT_COIN));
            }
            intent.putExtras(bundle);
            AppPreferences.getInstance().setGiftCoins(0);
        }

        handleDataMessage(data);
        notificationBuilder.setSmallIcon(R.drawable.ic_stat_notifications);
        notificationBuilder.setColor(context.getResources().getColor(R.color.colorAccent));
        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));

        if (!TextUtils.isEmpty(imageUrl)) {
            Log.e(TAG, "sendNotification: " );
            RemoteViews notificationLayout = new RemoteViews(context.getPackageName(), R.layout.notification_view);
            notificationLayout.setImageViewBitmap(R.id.image,image);
//            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(image));
            notificationBuilder.setContent(notificationLayout);
        }else {
            if (TextUtils.isEmpty(label)) {
                notificationBuilder.setContentTitle(context.getString(R.string.app_name));
            } else {
                notificationBuilder.setContentTitle(label);
            }

            if (!TextUtils.isEmpty(message)) {
                notificationBuilder.setContentText(message);
            }
        }
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSound(sound);
            notificationBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager)context. getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();

            NotificationChannel channel = new NotificationChannel(context.getString(R.string.default_notification_channel_id), "pushnotification", NotificationManager.IMPORTANCE_DEFAULT);
                   channel.setSound(sound,audioAttributes);

            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    // [END on_new_token]
}
