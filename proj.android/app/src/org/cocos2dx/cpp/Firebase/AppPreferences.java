package org.cocos2dx.cpp.Firebase;

import android.content.Context;
import android.content.SharedPreferences;
import org.cocos2dx.cpp.WordProApplication;

public class AppPreferences {

    private static SharedPreferences mSharedpreferences;
    private static AppPreferences mInstance;
    private static final String PREFERENCE_FILE = "WordPro";

    private AppPreferences() {

    }

    synchronized public static AppPreferences getInstance()
    {
        if (mInstance == null) {
            mSharedpreferences = WordProApplication.getInstance()
                    .getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
            mInstance = new AppPreferences();
        }
        return mInstance;
    }

    public void setVideoRewardStatus(boolean value)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putBoolean("videoReward", value);
        editor.apply();
    }

    public boolean isVideoRewardAvailable() {
        return mSharedpreferences.getBoolean("videoReward", false);
    }

    public void setRewardPopUpVisible(boolean value)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putBoolean("rewardvideo", value);
        editor.apply();
    }

    public boolean isRewardPopUpVisible() {
        return mSharedpreferences.getBoolean("rewardvideo", false);
    }

    public void setNotifyUrl(String urlStr)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putString("notifyUrl", urlStr);
        editor.apply();
    }

    public String getNotificationUrl() {
        return mSharedpreferences.getString("notifyUrl", "");
    }

    public void setGiftCoins(int coins)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putInt("giftCoins", coins);
        editor.apply();
    }

    public int getGiftCoins() {
        return mSharedpreferences.getInt("giftCoins", 0);
    }

    public void setRewardType(String rewardType)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putString("RewardType", rewardType);
        editor.apply();
    }

    public String getRewardType() {
        return mSharedpreferences.getString("RewardType", "");
    }

    public void setNotificationData(String data)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putString("notificationData",data);
        editor.apply();
    }

    public String getNotificationData()
    {
        return mSharedpreferences.getString("notificationData", "");
    }

    public boolean ischurnUserRewarded() {
        return mSharedpreferences.getBoolean("churn_user_reward", false);
    }

    public void setchurnUserRewarded(boolean value)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putBoolean("churn_user_reward", value);
        editor.apply();
    }
    public boolean isPurchaseUserPopUpShown() {
        return mSharedpreferences.getBoolean("purchase_sale", false);
    }

    public void setPurchaseUserPopUpShown(boolean value)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putBoolean("purchase_sale", value);
        editor.apply();
    }

    public void saveFbIdInPref(String fbId)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putString("UserFbId", fbId);
        editor.apply();
    }
    public String getUserFbId()
    {
        return mSharedpreferences.getString("UserFbId", "");
    }

    public void setNotificationSeqImageId(int Id)
    {
        SharedPreferences.Editor editor = mSharedpreferences.edit();
        editor.putInt("localNotify", Id);
        editor.apply();
    }

    public int getNotifyImageId() {
        return mSharedpreferences.getInt("localNotify", 0);
    }
}
