package org.cocos2dx.cpp.Firebase;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.cocos2dx.cpp.AppActivity;
import org.cocos2dx.cpp.Firebase.Model.LevelScore;
import org.cocos2dx.cpp.Firebase.Model.UserData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FirebaseDatabaseService {


    public native void onleaderBoardJsonSuccess(String jsonString,int userIndex);

    public native void onlanguageDataUserIdsSuccess(String jsonString);

    public native void onUserLanguageDataSuccess(String jsonString);

    DatabaseReference mDatabase=null;
    private final boolean SYNC_ENABLE = false;
    private static FirebaseDatabaseService instance;
    private static final String TAG = "FirebaseDatabase";
    private static String fbUserId = "";
    private DatabaseReference mLeaderBoardInfoTable,mLangLeaderboardTable,m_UserLangTable,m_UsersTable,m_UserLangData;


    public static FirebaseDatabaseService getInstance()
    {
        if(instance == null)
        {
            instance = new FirebaseDatabaseService();
            fbUserId = AppPreferences.getInstance().getUserFbId();
            instance.mDatabase = FirebaseDatabase.getInstance().getReference();
        }
        return instance;
    }

    public static void saveUserFbId(String fbid)
    {
        AppPreferences.getInstance().saveFbIdInPref(fbid);
    }

    public static void pushFirebaseUserLanguageData(String fbId,String language,int currentLevel,int coins)
    {
        if (!fbId.isEmpty())
        {
            Log.d(TAG, "callInsertFirebaseUserData");
            FirebaseDatabaseService
                    .getInstance()
                    .getUserLanguageDataTable("languageData")
                    .updateUserLanguageData(fbId, language, currentLevel, coins);
        }
    }

    private FirebaseDatabaseService getUserLanguageDataTable(String userdata)
    {
        m_UserLangTable = mDatabase.child(userdata);
        m_UserLangTable.keepSynced(SYNC_ENABLE);
        return instance;
    }

    private void updateUserLanguageData(String fbId,String language,int currentLevel,int coins)
    {
        Log.e(TAG, "updateUserLanguageData() called with: fbId = [" + fbId + "], language = [" + language + "], currentLevel = [" + currentLevel + "], coins = [" + coins + "]");
        if(fbId != null)
        {
            UserData userData = new UserData(currentLevel,coins);
            Map<String, Object> userValues = userData.toMap();

            m_UserLangTable.child(""+fbId).child(language).updateChildren(userValues);
        }
        m_UserLangTable = null;

    }

    private FirebaseDatabaseService getDataTable(String userdata)
    {
        m_UsersTable = mDatabase.child(userdata);
        m_UsersTable.keepSynced(SYNC_ENABLE);
        return instance;
    }

    // users in languagedata
    public void getLanguageDataUserIds()
    {
        Log.e("*******","getLanguageDataUserIds");
        FirebaseDatabaseService
                .getInstance()
                .getDataTable("languageData");
        // Most viewed posts

        m_UsersTable.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 if(dataSnapshot.getValue()==null)
                {
                    Log.d("getLanguageDataUserIds","getLanguageDataUserIds NUll");
                    return;
                }
                Log.d("*****", dataSnapshot.getValue().toString());
                if(m_UsersTable != null)
                    m_UsersTable.removeEventListener(this);
                m_UsersTable = null;
                Log.e("getLanguageDataUserIds",dataSnapshot.getKey());

                String mainkey = dataSnapshot.getKey();
                final JSONArray contests = new JSONArray();
                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren())
                {
                    contests.put(childDataSnapshot.getKey());
                }

                final JSONObject obj = new JSONObject();
                try {
                    obj.put(mainkey, contests);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                ((AppActivity)AppActivity.getContext()).runOnGLThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        onlanguageDataUserIdsSuccess(obj.toString());
                        Log.e("run()",obj.toString());
                    }
                });
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(m_UsersTable != null)
                    m_UsersTable.removeEventListener(this);
                m_UsersTable = null;
            }
        });
    }

    private FirebaseDatabaseService getUserLangDataTable(String userid)
    {
        m_UserLangData = mDatabase.child(userid);
        m_UserLangData.keepSynced(SYNC_ENABLE);
        return instance;
    }
    //language data for fbid
    public void getUserLanguageData(final String fbid)
    {
        Log.e("*******","getUserLanguageData");
        FirebaseDatabaseService
                .getInstance()
                .getUserLangDataTable("languageData"+ "/" +fbid);
        // Most viewed posts

        m_UserLangData.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()==null)
                {
                    Log.d("getUserLanguageData","getUserLanguageData NUll");
                    return;
                }
                Log.d("*****", dataSnapshot.getValue().toString());
                if(m_UserLangData != null)
                    m_UserLangData.removeEventListener(this);
                m_UserLangData = null;

                Log.e("getUserLanguageData",dataSnapshot.getKey());

                String mainkey = dataSnapshot.getKey();
                final JSONObject contests = new JSONObject();
                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren())
                {

                    UserData contest = childDataSnapshot.getValue(UserData.class);
                    try {
                        contests.put(childDataSnapshot.getKey(),contest.getJson());
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                final JSONObject obj = new JSONObject();
                try {
                    obj.put(mainkey, contests);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                ((AppActivity)AppActivity.getContext()).runOnGLThread(new Runnable() {

                    @Override
                    public void run() {
                        Log.d("getUserLanguageData",obj.toString());
                        onUserLanguageDataSuccess(obj.toString());
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(m_UsersTable != null)
                    m_UsersTable.removeEventListener(this);
                m_UsersTable = null;
            }
        });
    }

    // leaderboard
    public static void pushFirebaseLeaderBoardData(String fbId,String username,String profileUrl,String language,int currentLevel,int coins)
    {
        if (!fbId.isEmpty())
        {
            Log.d(TAG, "callInsertFirebaseUserData");
            FirebaseDatabaseService
                    .getInstance()
                    .getLeaderBoardTable("leaderboard")
                    .updateuserLevelScore(fbId, username, profileUrl, language, currentLevel, coins);
        }

    }

    private FirebaseDatabaseService getLeaderBoardTable(String userdata)
    {
        mLeaderBoardInfoTable = mDatabase.child(userdata);
        mLeaderBoardInfoTable.keepSynced(SYNC_ENABLE);
        return instance;
    }

    void updateuserLevelScore(String fbId,String username,String profileUrl,String language,int currentLevel,int coins)
    {
        Log.d(TAG, "updateuserLevelScore() called with: fbId = [" + fbId + "], username = [" + username + "], profileUrl = [" + profileUrl + "], language = [" + language + "], currentLevel = [" + currentLevel + "]");
        if(fbId != null)
        {
            LevelScore scoreData = new LevelScore(fbId,username,profileUrl,currentLevel,coins);
            Map<String, Object> scoreValues = scoreData.toMap();

            mLeaderBoardInfoTable.child(""+language).child(fbId).updateChildren(scoreValues);
        }
        mLeaderBoardInfoTable = null;
    }

    private FirebaseDatabaseService getLanguageleaderboard(String language)
    {
        Log.d(TAG, "getlanguageleaderbord() called with: language = [" + language + "]");

        mLangLeaderboardTable = mDatabase.child(language);
        mLangLeaderboardTable.keepSynced(SYNC_ENABLE);
        return instance;
    }

    public void getLevelScoreLeaderboard(final String language)
    {
        fbUserId = AppPreferences.getInstance().getUserFbId();

        final ArrayList<String> fbIdList = new ArrayList<String>();
        final ArrayList<LevelScore> userlist = new ArrayList<LevelScore>();

        Log.e("*******","topScoresFromLevel");
        FirebaseDatabaseService
                .getInstance()
                .getLanguageleaderboard("leaderboard"+ "/" +language);
        // Most viewed posts

        mLangLeaderboardTable.orderByChild("currentLevel").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG,"getTopScores");
                if(dataSnapshot.getValue()==null)
                {
                    Log.d("NUll","NUll");
                    return;
                }
                Log.e("*****", dataSnapshot.getValue().toString());
                if(mLangLeaderboardTable != null)
                    mLangLeaderboardTable.removeEventListener(this);
                    mLangLeaderboardTable = null;

                JSONArray jsArray = new JSONArray();
                for (DataSnapshot contestSnapshot: dataSnapshot.getChildren())
                {
                    LevelScore contest = contestSnapshot.getValue(LevelScore.class);
                    userlist.add(contest);

                    fbIdList.add(contestSnapshot.child("fbId").getValue().toString());
                }

                Collections.reverse(userlist);

                Collections.reverse(fbIdList);// 50

                Log.e(TAG, "index of user"+fbIdList);
                Log.e(TAG, "index of user-->"+fbIdList.indexOf(fbUserId));
                final int index = fbIdList.indexOf(fbUserId);

                for (int i=0; i < userlist.size(); i++) {
                    jsArray.put(userlist.get(i).getJson());
                }

                final JSONObject obj = new JSONObject();
                try {
                    obj.put(language, jsArray);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                ((AppActivity)AppActivity.getContext()).runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        onleaderBoardJsonSuccess(obj.toString(),index);
                        Log.e(TAG, "run() called"+obj.toString());
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(mLangLeaderboardTable != null)
                    mLangLeaderboardTable.removeEventListener(this);
                mLangLeaderboardTable = null;
            }
        });
    }

    public void getCurrentUserRank(final String language)
    {


    }

    public void cleanUp() {
        mDatabase = null;
    }

}
