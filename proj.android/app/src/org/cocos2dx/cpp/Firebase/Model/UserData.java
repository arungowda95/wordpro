package org.cocos2dx.cpp.Firebase.Model;

import com.google.firebase.database.Exclude;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UserData {


     public int currentLevel;
     public int coinCount;

    public UserData() {
        // Default constructor required for calls to DataSnapshot.getValue(Contest.class)
    }

    public UserData(int playedLevel,int coins)
    {
        this.currentLevel = playedLevel;
        this.coinCount = coins;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("currentLevel", currentLevel);
        result.put("coinCount",coinCount);
        return result;
    }

    @Exclude
    public JSONObject getJson(){
        JSONObject scoreJson = new JSONObject();
        try {
            scoreJson.put("currentLevel",currentLevel);
            scoreJson.put("coinCount",coinCount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return scoreJson;
    }
}
