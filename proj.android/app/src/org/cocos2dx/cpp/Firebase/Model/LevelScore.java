package org.cocos2dx.cpp.Firebase.Model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class LevelScore {

    public String fbId;
    public String name;
    public String profileUrl;
    public int currentLevel;
    public int coinCount;

    public LevelScore() {
        // Default constructor required for calls to DataSnapshot.getValue(Contest.class)
    }

    public LevelScore(String fbId,String username,String profileUrl,int playedLevel,int coins) {
        this.fbId = fbId;
        this.name = username;
        this.profileUrl = profileUrl;
        this.currentLevel = playedLevel;
        this.coinCount = coins;

    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("fbId", fbId);
        result.put("name", name);
        result.put("profileUrl", profileUrl);
        result.put("currentLevel", currentLevel);
        result.put("coinCount",coinCount);
        return result;
    }

    @Exclude
    public JSONObject getJson(){
        JSONObject scoreJson = new JSONObject();
        try {
            scoreJson.put("fbId", fbId);
            scoreJson.put("name", name);
            scoreJson.put("profileUrl", profileUrl);
            scoreJson.put("currentLevel",currentLevel);
            scoreJson.put("coinCount",coinCount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return scoreJson;
    }
}
