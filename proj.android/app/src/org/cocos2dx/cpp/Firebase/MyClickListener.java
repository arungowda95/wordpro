package org.cocos2dx.cpp.Firebase;

import android.util.Log;

import com.google.firebase.inappmessaging.FirebaseInAppMessagingClickListener;
import com.google.firebase.inappmessaging.model.Action;
import com.google.firebase.inappmessaging.model.InAppMessage;

import java.util.Map;

public class MyClickListener implements FirebaseInAppMessagingClickListener
{
    @Override
    public void messageClicked(InAppMessage inAppMessage, Action action) {

        // Determine which URL the user clicked
        String url = action.getActionUrl();

        Log.e("inappMsg", "messageClicked() called with: inAppMessage = [" + inAppMessage + "], action = [" + action + "]");
        Map dataBundle = inAppMessage.getData();
        // ...
    }
}
