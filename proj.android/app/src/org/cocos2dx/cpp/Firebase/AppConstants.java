package org.cocos2dx.cpp.Firebase;

public class AppConstants {

    //Notification
    public static final String KEY_TYPE = "type";
    public static final String KEY_URL  = "url";
    public static final String KEY_GIFT = "gift";

    public static final String KEY_GIFT_COIN    = "coin";


    //Remote config constants
    public static final String KEY_PURCHASE_PREDICT = "purchase_prediction";
    public static final String KEY_CHURN_PREDICT    = "churn_prediction";
    public static final String KEY_FORCE_UPDATE     = "shouldForceUpdate";
    public static final String KEY_CHURN_COINS     = "churn_coins";


}
