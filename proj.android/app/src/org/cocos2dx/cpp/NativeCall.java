package org.cocos2dx.cpp;

public class NativeCall {

    public static native void callUserAction(int actionType);

    public static native void setNotificationGiftType(String type,int rewardCount);

    public static native void setUserPredictionType(int type,int coins);


}
