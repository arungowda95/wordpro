/****************************************************************************
Copyright (c) 2015-2016 Chukong Technologies Inc.
Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;

import org.cocos2dx.cpp.Firebase.AppConstants;
import org.cocos2dx.cpp.Firebase.AppPreferences;
import org.cocos2dx.cpp.Firebase.FirebaseDatabaseService;
import org.cocos2dx.cpp.Firebase.MyClickListener;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Build;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.GameRequestContent;
import com.facebook.share.widget.GameRequestDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.word.pro.BuildConfig;
import com.word.pro.R;

import java.text.BreakIterator;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Currency;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AppActivity extends com.sdkbox.plugin.SDKBoxActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    boolean m_bUpdateShown = false;
    private String TAG = "Activity";
    public static native void onFaceBookLoginSuccess(int bool);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setEnableVirtualButton(false);
        super.onCreate(savedInstanceState);
        // Workaround in https://stackoverflow.com/questions/16283079/re-launch-of-activity-on-home-button-but-only-the-first-time/16447508
        if (!isTaskRoot()) {
            // Android launched another instance of the root activity into an existing task
            //  so just quietly finish and go away, dropping the user back into the activity
            //  at the top of the stack (ie: the last state of this task)
            // Don't need to finish it again since it's finished in super.onCreate .
            return;
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        new BigPictureImageDownloader(this, "hi", "hello",
//                "https://i.imgur.com/MKqtacu.jpg").execute();
//        PushMessagingService.sendNotification(this,"hello","hi;",null,null);
        // Make sure we're running on Pie or higher to change cutout mode
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // Enable rendering into the cutout area
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
            getWindow().setAttributes(lp);
        }
        // DO OTHER INITIALIZATION BELOW

        //Firebase Analytics
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Firebase IAM
        MyClickListener listener = new MyClickListener();
        FirebaseInAppMessaging.getInstance().addClickListener(listener);

        //Firebase Config
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(30)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);

        //local Notification schedule
        scheduleNotification();

//        DayNotification dayNotification = new DayNotification();
//        dayNotification.showNotification(this,"test");
        // Remote config
        fetchRemoteConfigValues();
    }

    // on game start game side calls
    private void initialize()
    {
        Log.e(TAG, "initialize() called");
        // at start empty
        AppPreferences.getInstance().setNotificationData("");

        if(getFirebaseBoolValue(AppConstants.KEY_FORCE_UPDATE))
        {
            checkforForceUpdate();
        }
        notificationAction(getIntent());
    }
    private void fetchRemoteConfigValues()
    {
        if(mFirebaseRemoteConfig==null)
            return;

        mFirebaseRemoteConfig.fetchAndActivate()
        .addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull com.google.android.gms.tasks.Task<Boolean> task) {
                if (task.isSuccessful())
                {
                    boolean updated = task.getResult();
                    Log.d(TAG, "Config params updated: " +updated);
                } else {

                }

                if(getFirebaseBoolValue(AppConstants.KEY_FORCE_UPDATE)) {
                    checkforForceUpdate();
                }

                if(getFirebaseBoolValue(AppConstants.KEY_CHURN_PREDICT)&&!AppPreferences.getInstance().ischurnUserRewarded())
                {
                    AppPreferences.getInstance().setchurnUserRewarded(true);
                    Log.d(TAG, "Config params update task = [" + mFirebaseRemoteConfig.getLong(AppConstants.KEY_CHURN_COINS) + "]");
                    NativeCall.setUserPredictionType(1, (int) mFirebaseRemoteConfig.getLong(AppConstants.KEY_CHURN_COINS));
                }

                if(getFirebaseBoolValue(AppConstants.KEY_PURCHASE_PREDICT)&&!AppPreferences.getInstance().isPurchaseUserPopUpShown()){

                    AppPreferences.getInstance().setPurchaseUserPopUpShown(true);
                    NativeCall.setUserPredictionType(2,0);
                }
            }
        });

    }
    private boolean getFirebaseBoolValue(String keyName)
    {
        return mFirebaseRemoteConfig.getBoolean(keyName);
    }
    @Override
    protected void onNewIntent(Intent intent) {
         super.onNewIntent(intent);
        notificationAction(intent);
    }



    private void notificationAction(Intent i)
    {
        Bundle bundle = i.getExtras();
        Log.e(TAG, "notificationAction() called with: i = [" + bundle + "]");
        String notifYdata = AppPreferences.getInstance().getNotificationData();

        if (bundle != null||!notifYdata.isEmpty())
        {
            Log.e(TAG, "notificationAction()  [" + notifYdata + "]");
            if(!notifYdata.isEmpty())
            {
                Bundle notifYBundle = new Bundle();
                HashMap<String, Object> bundleData = new HashMap<String, Object>();

                try {
                    bundleData = jsonToMap(notifYdata);
                    for (Map.Entry<String, Object> entry : bundleData.entrySet()) {
                        notifYBundle.putString(entry.getKey(), entry.getValue().toString());
                    }
                } catch (JSONException e) {
                    //some exception handler code.
                }
                handleNotification(notifYBundle);
            }else {
                handleNotification(bundle);
            }
        }
        AppPreferences.getInstance().setNotificationData("");
    }

    private void handleNotification(Bundle bundle)
    {
        if(bundle==null)
            return;
        String notifyType = bundle.getString(AppConstants.KEY_TYPE);
        Log.d(TAG, "handleNotification() called with: bundle = [" + notifyType + "]");

        if(notifyType!=null)
        {
            if(notifyType.isEmpty())
                return;

            String url = bundle.getString(AppConstants.KEY_URL);
            if(url != null&&!url.isEmpty())
            {
                AppPreferences.getInstance().setNotifyUrl(url);
            }
            Log.d(TAG, "handleNotification() =  [" + notifyType + "]");

            switch (notifyType)
            {
                case "coin":{
                    NativeCall.callUserAction(1);
                }
                    break;
                case "url":{
                    openUrl(AppPreferences.getInstance().getNotificationUrl());
                    NativeCall.callUserAction(2);
                }
                    break;
                default:
                    break;
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }

    public void openUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }



    public void gameFeedBack()
    {
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=Word Pro : Feedback"+ "&body=" + "Write Feedback here....." + "&to=" + "gameshubav2@gmail.com");
        testIntent.setData(data);
        startActivity(testIntent);
    }

    public void OpenInStore()
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + getPackageName()));
        startActivity(intent);
    }
    public void shareGame()
    {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Word Pro");
            String shareMessage= "\nBest fun game! An Awesome word puzzle game. Clear the  level and travel across the world !\n" +
                    "Try this game for free,\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "Share using."));
        } catch(Exception e) {
            //e.toString();
        }
    }

    public void showToastMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AppActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }



    public  boolean appInstalledOrNot(String uri)
    {
        System.out.println("appid :: "+uri);
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    public void scheduleNotification()
    {
        Calendar firingCal= Calendar.getInstance();
        Calendar currentCal = Calendar.getInstance();

        firingCal.set(Calendar.HOUR_OF_DAY,7); // At the hour you wanna fire
        firingCal.set(Calendar.MINUTE, 0); // Particular minute
        firingCal.set(Calendar.SECOND, 0); // particular second


        long intendedTime = firingCal.getTimeInMillis();
        long currentTime = currentCal.getTimeInMillis();

        {
            Intent intent = new Intent(AppActivity.this, DayNotification.class);
//            intent.putExtra("text", text[i]);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(AppActivity.this, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            if(intendedTime >= currentTime){
                // you can add buffer time too here to ignore some small differences in milliseconds
                // set from today
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, intendedTime, AlarmManager.INTERVAL_DAY, pendingIntent);
            } else{
                // set from next day
                // you might consider using calendar.add() for adding one day to the current day
                firingCal.add(Calendar.DAY_OF_MONTH, 1);
                intendedTime = firingCal.getTimeInMillis();

                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, intendedTime, AlarmManager.INTERVAL_DAY, pendingIntent);
            }
        }
    }

    public void postFirebaseAnalyticsEvent(final String eventName, final String param1, final String paramVal_1, final String param2,
                                           final String paramVal_2)
    {
        Log.e("Activity", "postFirebaseAnalyticsEvent: " + eventName);
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                Bundle bundle = new Bundle();
                if (param1 != null&&!param1.isEmpty())
                    bundle.putString(param1, paramVal_1);
                if (param2 != null&&!param2.isEmpty())
                    bundle.putString(param2, paramVal_2);

                mFirebaseAnalytics.logEvent(eventName, bundle);
            }
        });
    }

    public void postFirebaseAnalyticsScreen(final String screenName)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bundle bundle = new Bundle();

                mFirebaseAnalytics.setCurrentScreen(AppActivity.this, screenName, "WordPro");
            }
        });


    }
    public String getCurrencySymbol(String code)
    {
        Currency currency = Currency.getInstance(code);
        String symbol = currency.getSymbol();

        return symbol;
    }

    public void getLeaderBoardjson(String fbId)
    {
        FirebaseDatabaseService.getInstance().getLevelScoreLeaderboard(fbId);
    }

    public void getLanguageDataUserIds()
    {
        FirebaseDatabaseService.getInstance().getLanguageDataUserIds();
    }

    public void getUserLanguageDataJson(String fbId)
    {
        FirebaseDatabaseService.getInstance().getUserLanguageData(fbId);
    }

    public void getCurrentUserRank(String language)
    {
        FirebaseDatabaseService.getInstance().getCurrentUserRank(language);
    }

    public void openFBInviteDialog()
    {

    }

    public HashMap<String, Object> jsonToMap(String t) throws JSONException {

        HashMap<String, Object> map = new HashMap<String, Object>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            Object value = jObject.get(key);
            map.put(key, value);

        }

        return map;
    }
    public boolean isFacebookLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    private void loginWithFaceBook()
    {

    }


    AppUpdateManager appUpdateManager;

    public void checkforAppUpdate()
    {
        Log.e("activity", "checkforAppUpdate: " );
        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
        appUpdateManager.registerListener(installStateUpdatedListener);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo result) {
                if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && result.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                    showUpdate(result);
                    Log.e("appupdate", "onSuccess: " );
                }
                else{
                    Log.e("appupdate", "onfailure: " );
                }
            }
        });
    }

    //for force update
    public void checkforForceUpdate()
    {
        if(!m_bUpdateShown)
        {
            m_bUpdateShown = true;
            Log.e(TAG, "checkforForceUpdate() called");
            if(getFirebaseBoolValue(AppConstants.KEY_FORCE_UPDATE)) {

            appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
            appUpdateManager.registerListener(installStateUpdatedListener);
            com.google.android.play.core.tasks.Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
            appUpdateInfoTask.addOnSuccessListener(new com.google.android.play.core.tasks.OnSuccessListener<AppUpdateInfo>() {
                @Override
                public void onSuccess(AppUpdateInfo result) {
                    if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                            && result.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                        showForceUpdate(result);
                        Log.e("appupdate", "onSuccess: ");
                    } else {
                        Log.e("appupdate", "onfailure: ");
                    }
                }
            });
            }
        }
    }

    private void showForceUpdate(AppUpdateInfo result)
    {
        Log.e(TAG, "showForceUpdate() called with: result = [" + result + "]");
        try {
            appUpdateManager.startUpdateFlowForResult(result,
                    AppUpdateType.IMMEDIATE,
                    this,
                    1);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "showForceUpdate() called with: result = [" + result + "]");
        }
    }

    //flexible update
    private void showUpdate(AppUpdateInfo result)
    {
        Log.e(TAG, "showUpdate() called with: result = [" + result + "]");
        try {

            appUpdateManager.startUpdateFlowForResult(result,
                    AppUpdateType.FLEXIBLE,
                    this,
                    1);
        }
        catch (IntentSender.SendIntentException e)
        {
            Log.e(TAG,"show update "+ e.getLocalizedMessage());
        }
    }


    InstallStateUpdatedListener installStateUpdatedListener = new
            InstallStateUpdatedListener() {
                @Override
                public void onStateUpdate(InstallState state) {
                    if (state.installStatus() == InstallStatus.DOWNLOADED){

                        Toast.makeText(getApplicationContext(),"success",Toast.LENGTH_SHORT).show();

                    } else if (state.installStatus() == InstallStatus.INSTALLED){
                        if (appUpdateManager != null){
                            appUpdateManager.unregisterListener(installStateUpdatedListener);
                        }

                    } else {
                        Log.i("inn app update", "InstallStateUpdatedListener: state: " + state.installStatus());
                    }
                }
            };

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int request, int response, final Intent data)
    {
        super.onActivityResult(request, response, data);
    }
}
