package org.cocos2dx.cpp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.word.pro.R;

import org.cocos2dx.cpp.Firebase.AppPreferences;

import java.util.ArrayList;
import java.util.List;

public class DayNotification extends BroadcastReceiver {
    String mText;
    ArrayList<String> notificationText = new ArrayList<String>();
    private Uri notifsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    RemoteViews remoteView;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

         Log.e("receiver","in");
        prepareNotificationText();
        showNotification(context, "");
    }
    public void showNotification(Context context, String notifyText)
    {
        Intent intent = new Intent(context, AppActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context, 1019, intent, 0);
        Bitmap large_icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        SharedPreferences prefs = context.getSharedPreferences("notification_count", context.MODE_PRIVATE);
        int count = prefs.getInt("notification_count", 0);

        remoteView = new RemoteViews(context.getPackageName(),R.layout.custom_notification_layout);
        setnotificationImage();

        if (notificationText.size()>0)
            notifyText = notificationText.get(count).toString();

        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.notify_sound);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, context.getString(R.string.local_notification_channel_id))
                .setSmallIcon(R.drawable.ic_stat_notifications)
                .setLargeIcon(large_icon)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(notifyText)
                .setContent(remoteView);
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(notifyText));
        mBuilder.setContentIntent(pi);
//        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setSound(sound);
        mBuilder.setAutoCancel(true);



        // Issue the notification.
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {

            // Creating an Audio Attribute
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();


            NotificationChannel channel = new NotificationChannel(context.getString(R.string.local_notification_channel_id),
                    "localnotification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(sound,audioAttributes);

            mNotificationManager.createNotificationChannel(channel);
        }
        mNotificationManager.notify(1019, mBuilder.build());


        if (count == notificationText.size() - 1)
        {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("notification_count", 0);
            editor.apply();
        }
        else {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("notification_count", count + 1);
            editor.apply();
        }
    }

    void setnotificationImage()
    {
        int Id = AppPreferences.getInstance().getNotifyImageId();

        int imageid = R.drawable.notification1;
        if (Id>5){
            Id = generate(1,5);
        }
        switch (Id)
        {
            case 1:
                imageid = R.drawable.notification1;
                break;
            case 2:
                imageid = R.drawable.notification2;
                break;
            case 3:
                imageid = R.drawable.notification3;
                break;
            case 4:
                imageid = R.drawable.notification4;
                break;
            case 5:
                imageid = R.drawable.notification5;
                break;
            default:
                break;
        }
        remoteView.setImageViewResource(R.id.image,imageid);
        Id++;
        AppPreferences.getInstance().setNotificationSeqImageId(Id);

    }
    public void prepareNotificationText()
    {
        notificationText.add("Your Daily Bonus is Awaits. Visit Word Pro");
        notificationText.add("Your Free Coins are ready to be collected. Log in to Word Pro!");
        notificationText.add("Clear word levels and travel across the world");
        notificationText.add("Word Pro - Challenge Your knowledge");
        notificationText.add("Play Word Pro and Win exciting Rewards");
    }

    public static int generate(int min,int max) {
        return min + (int)(Math.random() * ((max - min) + 1));
    }
}
