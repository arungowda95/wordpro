//
//  AuthController.h
//  wordPro-mobile
//
//  Created by Arun on 09/10/20.
//


#import <AuthenticationServices/AuthenticationServices.h>


@interface AuthController:UIViewController<ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding>
{
    
}
typedef void(^PickerCallback)(NSString *Id, NSString *name);

@property (nonatomic, copy) PickerCallback callback;

+(AuthController*)sharedInstance;
- (void)initialization;
- (void)handleAuthrization ;
- (void)removeSignInObserver;

@end
