/****************************************************************************
 Copyright (c) 2010-2013 cocos2d-x.org
 Copyright (c) 2013-2016 Chukong Technologies Inc.
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "Firebase.h"
#import "FirebaseViewController.h"
#import "AuthController.h"
#include "GameController.h"
@implementation AppController

@synthesize window;

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;
NSString *const kGCMMessageIDKey = @"firebaseMsg";


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    cocos2d::Application *app = cocos2d::Application::getInstance();
    
    // Initialize the GLView attributes
    app->initGLContextAttrs();
    cocos2d::GLViewImpl::convertAttrs();
    
    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];

    // Use RootViewController to manage CCEAGLView
    _viewController = [[RootViewController alloc]init];
    _viewController.wantsFullScreenLayout = YES;
    

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: _viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:_viewController];
    }

    [window makeKeyAndVisible];

    [[UIApplication sharedApplication] setStatusBarHidden:true];
    
    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView((__bridge void *)_viewController.view);
    cocos2d::Director::getInstance()->setOpenGLView(glview);
    
    
    [FIRApp configure];

    // [START set_messaging_delegate]
    [FIRMessaging messaging].delegate = self;
    // [END set_messaging_delegate]

    
    // [START register_for_notifications]
     if ([UNUserNotificationCenter class] != nil) {
       // iOS 10 or later
       // For iOS 10 display notification (sent via APNS)
       [UNUserNotificationCenter currentNotificationCenter].delegate = self;
       UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
           UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
       [[UNUserNotificationCenter currentNotificationCenter]
           requestAuthorizationWithOptions:authOptions
           completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
           }];
     } else {
       // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
       UIUserNotificationType allNotificationTypes =
       (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
       UIUserNotificationSettings *settings =
       [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
       [application registerUserNotificationSettings:settings];
     }

   //  [application registerForRemoteNotifications];

    [self scheduleNotification];
    //run the cocos2d-x game scene
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
      didFinishLaunchingWithOptions:launchOptions];


    [[FirebaseViewController sharedInstance] initialization];
    
    [[AuthController sharedInstance] initialization];

    app->run();

    return YES;
}
- (void) shareGame {
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    NSString *text = [NSString stringWithFormat:@"Best fun game! An Awesome word puzzle game. Clear the  level and travel across the world !\n Try this game for free,"];
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/app/id1381417804"];
    [sharingItems addObject:text];
    [sharingItems addObject:url];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    AppController* appController = (AppController*) [UIApplication sharedApplication].delegate;
    [appController.viewController presentViewController:activityController animated:YES completion:nil];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        //[appController.viewController presentViewController:activityController animated:YES completion:nil];
    }
    //if iPad
    else  {
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityController];
        activityController.popoverPresentationController.sourceView = _viewController.view;
        [popup presentPopoverFromRect:CGRectMake(_viewController.view.frame.size.width/2, _viewController.view.frame.size.height/4, 0, 0)inView:_viewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}
- (void) displayToastMessage:(NSString*)msgStr
{
    UIAlertView * messageBox = [[UIAlertView alloc] initWithTitle: @"Message"
                                                          message: msgStr
                                                         delegate: nil
                                                cancelButtonTitle: @"Ok"
                                                otherButtonTitles: nil];
    
    [messageBox autorelease];
    [messageBox show];
}
- (void) openInAppStore {
    
    NSString *iTunesLink = @"https://itunes.apple.com/app/id1519063163";
    //itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}
- (bool) appInstalledOrNot:(NSString*)packageId
{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"dice-with-friends-yatzy-king://"]];
}

//+ (NSString *)addFormatPrice:(double)dblPrice removeCurSymbol:(BOOL)booRemoveCurSymbol;

- (void) postFirebaseAnalyticsEvent:(NSString*)eventName param1:(NSString*)param1 paramVal:(NSString*)paramVal_1 param2:(NSString*)param2 paramVal2:(NSString*)paramVal_2
{
    [FIRAnalytics logEventWithName:eventName
    parameters:@{
                 param1:paramVal_1,
                 param2:paramVal_2
                }];
}
- (void) postFirebaseAnalyticsScreen:(NSString*)screenName
{
    [FIRAnalytics setScreenName:screenName screenClass:@"WordPro"];
}

-(NSString*)getCurrencySymbol:(NSString*)currcode
{
    NSString *currencyCode = currcode;
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;

    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currencyCode}];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:localeIde];

    NSString *symbol = formatter.currencySymbol;
    return symbol;    
}

- (void) sendGameFeedBack
{
    NSString *recipients = @"mailto:gameshubav2@gmail.com?subject=Word Pro : Feedback";
    NSString *body = @"&body=Write Feedback here.....";

    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}
-(void) scheduleNotification
{
    UIApplication* app = [UIApplication sharedApplication];
    NSArray *oldNotifications = [app scheduledLocalNotifications];
    for(int i = 0; i < oldNotifications.count; i++)
    {
        UILocalNotification* oneEvent = [oldNotifications objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *NotificationID = [NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"localNotification"]];
        if ([NotificationID isEqualToString:@"1001"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
        }
    }
    
    NSArray *text = @[@"Your Daily Bonus is Awaits. Visit Word Pro",
                     @"Your Free Coins are ready to be collected. Log in to Word Pro!",
                     @"Clear word levels and travel across the world",
                     @"Word Pro - Challenge Your knowledge",
                     @"Play Word Pro and Win exciting Rewards"];

    NSInteger index = [[NSUserDefaults standardUserDefaults] integerForKey:@"NotifyIndex"];

    if(index<=0)
    {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"NotifyIndex"];
        
         index = [[NSUserDefaults standardUserDefaults] integerForKey:@"NotifyIndex"];
    }

    NSString *notifiationstr = @"";
    {
        notifiationstr = [text objectAtIndex:index];

        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24];
        localNotification.repeatInterval = NSCalendarUnitDay;
         
        localNotification.alertBody = notifiationstr;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.userInfo = [NSDictionary dictionaryWithObject:@"1001" forKey:@"localNotification"];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    if(index == text.count-1)
    {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"NotifyIndex"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setInteger:index+1 forKey:@"NotifyIndex"];
    }
}
-(void)onloginSuccess:(NSString*)UserId name:(NSString*)username
{
    NSLog(@"userid: %@",UserId);
    NSLog(@"username：%@", username);
//
    GameController::getInstance()->connectedToAppleId([UserId UTF8String], [username UTF8String]);
    
}
-(void)onloginfailure
{
//
    GameController::getInstance()->FailedToConnectAppleId();
    
}
-(void)setUsrRank:(NSInteger)UserId
{
    GameController::getInstance()->saveUserRank((int)UserId);
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    // We don't need to call this method any more. It will interrupt user defined game pause&resume logic
    /* cocos2d::Director::getInstance()->pause(); */
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    // We don't need to call this method any more. It will interrupt user defined game pause&resume logic
    /* cocos2d::Director::getInstance()->resume(); */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {

  BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
    openURL:url
    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
    annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
  ];
  // Add any custom logic here.
  return handled;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {

  BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
    openURL:url
    sourceApplication:sourceApplication
    annotation:annotation
  ];
  // Add any custom logic here.
  return handled;
}

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


#if __has_feature(objc_arc)
#else
- (void)dealloc {
    [window release];
    [_viewController release];
    [_authController removeSignInObserver];
    [super dealloc];
}
#endif


@end
