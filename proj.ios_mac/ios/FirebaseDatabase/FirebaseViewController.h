//
//  FirebaseViewController.h
//  wordPro-mobile
//
//  Created by Arun on 26/08/20.
//

#import "FirebaseViewController.h"
#import "Firebase.h"

@interface FirebaseViewController:UIViewController
{
    NSString *lang;
    NSString *userfbId;
    NSMutableArray *array;
    NSInteger leaderboardcount;
}

@property(strong, nonatomic) FIRDatabaseReference *ref;
@property(strong, nonatomic) FIRDatabaseReference *refchild;



+ (FirebaseViewController*)sharedInstance;

//Initialization
- (void)initialization;
- (void)getUserLanguageDataTable:(NSString *) userdata;
- (void)getUserLanguageData:(NSString *)fbid completion:(void (^)(NSString*))completionBlock;

-(void) pushFirebaseLeaderBoardData:(NSString *)fbId userName:(NSString *)name profileUrl:(NSString *)url language:(NSString*)lang currLevel:(NSNumber *)level coinCount:(NSNumber *)coins;

- (void)getLanguageDataUserIds:(void (^)(NSString*))completionBlock;

- (void)getLeaderBoardData:(NSString *)language completion:(void (^)(NSString*))completionBlock;

- (void)updateUserLanguageData:(NSString *)fbId language:(NSString *)language currlevel:(NSNumber *)level coinCount:(NSNumber *)coins;

-(void) setUserfbId:(NSString *)fbid;

-(void) onleaderboardsuccess:(NSDictionary *)dict;

@end
