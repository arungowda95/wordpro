//
//  FirebaseViewController.m
//  wordPro-mobile
//
//  Created by Arun on 26/08/20.
//

#import "FirebaseViewController.h"
#import "UserData.h"
#import "LevelScore.h"
#import "AppController.h"
@implementation FirebaseViewController

static FirebaseViewController* pFirebaseiewController = nil;

+ (FirebaseViewController*)sharedInstance {
    if(pFirebaseiewController == nil)
        pFirebaseiewController = [[FirebaseViewController alloc] init];
    

    return pFirebaseiewController;
}

- (id)init {
    self = [super init];
    if(self == nil)   return nil;
    
    return self;
}

- (void)initialization
{
   self.ref = [[FIRDatabase database] reference];
}
-(void)getUserLanguageDataTable:(NSString *) userdata
{

}

//(String fbId,String username,String profileUrl,String language,int currentLevel,int coins)
-(void) pushFirebaseLeaderBoardData:(NSString *)fbId userName:(NSString *)name profileUrl:(NSString *)url language:(NSString*)lang currLevel:(NSNumber *)level coinCount:(NSNumber *)coins
{
        self.refchild = [[[_ref child:@"leaderboard"] child:lang] child:fbId];
    //                m_UserLangTable.child(""+fbId).child(language).updateChildren(userValues);

        if(fbId != nil)
        {
            NSDictionary *post = @{@"fbId": fbId,
                                   @"name": name,
                                   @"profileUrl": url,
                                   @"currentLevel": level,
                                   @"coinCount": coins};
            [self.refchild updateChildValues:post];
            // My top posts by number of stars
        }


    
}

- (void)updateUserLanguageData:(NSString *)fbId language:(NSString *)language currlevel:(NSNumber *)level coinCount:(NSNumber *)coins
{
    
//    NSString *combined = [NSString stringWithFormat:@"%@/%@", fbId, language];

    
    self.refchild = [[[_ref child:@"languageData"] child:fbId]child:language];
//                m_UserLangTable.child(""+fbId).child(language).updateChildren(userValues);


    if(fbId != nil)
    {
        NSDictionary *post = @{@"currentLevel": level,
                               @"coinCount": coins};
        [self.refchild updateChildValues:post];
        // My top posts by number of stars
    }



//  NSString *key = [[_ref child:@"posts"] childByAutoId].key;
//  NSDictionary *childUpdates = @{[@"/posts/" stringByAppendingString:key]: post,
//                                 [NSString stringWithFormat:@"/user-posts/%@/%@/", userID, key]: post};
  // [END write_fan_out]
}
-(void)getLanguageDataUserIds:(void (^)(NSString*))completionBlock
{

    NSLog(@"getLanguageDataUserIds");

    [[_ref child:@"languageData"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
      // Get user value
        NSDictionary *postDict = snapshot.value;
        NSError *error=nil;
        
        NSString *strkey = snapshot.key;
        if (snapshot.childrenCount>0)
        {
            
            NSMutableArray *childarray = [[NSMutableArray alloc]init];

            for(FIRDataSnapshot *object in snapshot.children)
            {
                [childarray addObject:object.key];
            }
            // add fbid key as main
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:postDict.count];
            [dict setObject:childarray forKey:strkey];
            
             // convert dict to json
             NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
             
             NSString* myString;
             myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             
            completionBlock(myString);

         }

      // ...
    } withCancelBlock:^(NSError * _Nonnull error) {
      NSLog(@"error%@", error.localizedDescription);
    }];
}
-(void) getUserLanguageData:(NSString *)fbid completion:(void (^)(NSString*))completionBlock
{
    // My top posts by number of stars
    
    if(fbid==nil) {
      return;
    }
    
     [[[_ref child:@"languageData"] child:fbid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
      // Get user value
         NSDictionary *postDict = snapshot.value;
         NSError *error=nil;

         NSString *strkey = snapshot.key;
                  
         if (postDict!=nil)
         {
             
             // add fbid key as main
             NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:postDict.count];
             [dict setObject:postDict forKey:strkey];

             // convert dict to json
             NSData *jsonData=[NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
             
             NSString* myString;
             myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             
            completionBlock(myString);

         }

      // ...
    } withCancelBlock:^(NSError * _Nonnull error) {
      NSLog(@"%@", error.localizedDescription);
    }];


}

-(void) getLeaderBoardData:(NSString *)language completion:(void (^)(NSString*))completionBlock
{
    array = [[NSMutableArray alloc]init];
    NSMutableArray *myfbIds = [[NSMutableArray alloc] init];
    // My top posts by number of stars
    if(language==nil) {
      return;
    }
    lang = language;
    NSMutableDictionary *Jsondict = [[NSMutableDictionary alloc]init];
    NSError *error=nil;

    FIRDatabaseQuery *myTopScoreQuery = [[[self.ref child:@"leaderboard"]child:language] queryOrderedByChild:@"currentLevel"];
    
    [myTopScoreQuery observeEventType:FIRDataEventTypeValue
    withBlock:^(FIRDataSnapshot *snapshot) {
         NSEnumerator *children = [snapshot children];
        FIRDataSnapshot *child;
        while (child = [children nextObject]) {
          // ...
            NSLog(@"fbid key-->%@",child.key);
            [array addObject:child.value];
            [myfbIds addObject:child.key];
        }
//        [myfbIds ]
        NSArray *reversed = [[array reverseObjectEnumerator] allObjects];
        NSArray *reversed1 = [[myfbIds reverseObjectEnumerator] allObjects];
        
        
        userfbId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userfbId"];

        
        NSInteger Aindex = [reversed1 indexOfObject:userfbId];
        [[NSUserDefaults standardUserDefaults] setInteger:Aindex forKey:@"UserIndex"];

        AppController *appDelegate  = (AppController *)[[UIApplication sharedApplication] delegate];
        [appDelegate setUsrRank:[[NSUserDefaults standardUserDefaults] integerForKey:@"UserIndex"]];

        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"leaderboardJson"];

        [Jsondict setObject:reversed forKey:lang];
        // convert dict to json
        NSData *jsonData=[NSJSONSerialization dataWithJSONObject:Jsondict options:NSJSONWritingPrettyPrinted error:&error];

         NSString* myString;
         myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        [[NSUserDefaults standardUserDefaults] setObject:myString forKey:@"leaderboardJson"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];


}
-(void) setUserfbId:(NSString *)fbid
{
    userfbId = fbid;
    
    [[NSUserDefaults standardUserDefaults] setObject:userfbId forKey:@"userfbId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void) onleaderboardsuccess:(NSDictionary *)dict
{
    [array addObject:dict];
    NSMutableDictionary *Jsondict = [[NSMutableDictionary alloc]init];
    
    // add fbid key as main

    NSError *error=nil;

    if(leaderboardcount>0)
    {
        if(leaderboardcount==array.count)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"leaderboardJson"];

            [Jsondict setObject:array forKey:lang];
            // convert dict to json
            NSData *jsonData=[NSJSONSerialization dataWithJSONObject:Jsondict options:NSJSONWritingPrettyPrinted error:&error];

             NSString* myString;
             myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];


            [[NSUserDefaults standardUserDefaults] setObject:myString forKey:@"leaderboardJson"];
            [[NSUserDefaults standardUserDefaults] synchronize];

//            NSLog(@"\n Path = %@\n",filePath);
            
//            [jsonData writeToFile:filePath atomically : YES];
        }
    }

}
@end

/*
if (postDict.count>0)
      {
          
          NSArray *dictValues = [postDict allValues];

          NSMutableDictionary *Jsondict = [[NSMutableDictionary alloc]init];
          
          // add fbid key as main
          [Jsondict setObject:dictValues forKey:strkey];

          // convert dict to json
          NSData *jsonData=[NSJSONSerialization dataWithJSONObject:Jsondict options:NSJSONWritingPrettyPrinted error:&error];
          
          NSString* myString;
          myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

          NSLog(@"leaderboard JSON Object: %@ Or Error is: %@", myString, error);
          
         completionBlock(myString);

      }
*/
