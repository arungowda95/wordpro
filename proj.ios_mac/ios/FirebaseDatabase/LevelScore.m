//
//  Copyright (c) 2015 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "LevelScore.h"

@implementation LevelScore

//- (instancetype)initWithfbId:(NSString *)uid
//andname:(NSString *)name
// andprofileUrl:(NSString *)url
//  andlevel:(int)level
//  andcoins:(int)coin ;

- (instancetype)init {
    return [self initWithfbId:@"" andname:@"" andprofileUrl:@"" andlevel:0 andcoins:0];
}

- (instancetype)initWithfbId:(NSString *)uid
                 andname:(NSString *)author
                  andprofileUrl:(NSString *)title
                    andlevel:(NSNumber *)level
                    andcoins:(NSNumber *)coin
{
  self = [super init];
  if (self) {
    self.fbId = uid;
    self.name = author;
    self.profileUrl = title;
    self.currentLevel = level;
    self.coinCount = coin;
  }
  return self;
}
@end
